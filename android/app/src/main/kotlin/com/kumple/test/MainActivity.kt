package com.kumple.test

import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import androidx.annotation.NonNull
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine


class MainActivity: FlutterActivity() {

    private val facebookPlugin = FacebookPlugin()
    private val sharePlugin = SharePlugin()
    private val notificationPlugin = NotificationPlugin()

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        flutterEngine.plugins.add(facebookPlugin)
        flutterEngine.plugins.add(notificationPlugin)
        flutterEngine.plugins.add(sharePlugin)
        super.configureFlutterEngine(flutterEngine)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }
}

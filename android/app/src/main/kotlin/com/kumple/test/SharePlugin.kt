package com.kumple.test

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.annotation.NonNull
import androidx.core.content.ContextCompat.startActivity
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.PluginRegistry


class SharePlugin: FlutterPlugin, MethodChannel.MethodCallHandler, ActivityAware {
    private lateinit var channel : MethodChannel
    private lateinit var context: Context

    private companion object Factory {
        fun registerWith(registrar: PluginRegistry.Registrar) {
            val channel = MethodChannel(registrar.messenger(), "flutter_share")
            channel.setMethodCallHandler(SharePlugin())
        }
    }

    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "flutter_share")
        channel.setMethodCallHandler(this)
        context = flutterPluginBinding.applicationContext
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: MethodChannel.Result) {
        var arguments = call.arguments as HashMap<String, String>
        var text = arguments.get("text")

        if (call.method == "instagram") {
            val intent = Intent(Intent.ACTION_SEND)
            intent.setComponent(ComponentName("com.instagram.android","com.instagram.direct.share.handler.DirectShareHandlerActivity"))
            intent.setType("text/plain")
            intent.putExtra(Intent.EXTRA_TEXT, text)
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(context, intent, null);
        } else if (call.method == "sms") {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse("sms:")
            intent.putExtra("sms_body", text);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(context, intent, null);
        }
        result.success("ok");

    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
    }

    override fun onDetachedFromActivityForConfigChanges() {
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
    }

    override fun onDetachedFromActivity() {
    }
}
package com.kumple.test

import android.content.Context
import android.os.Bundle
import io.flutter.plugin.common.MethodChannel.Result
import androidx.annotation.NonNull
import io.flutter.plugin.common.MethodChannel
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.PluginRegistry

import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import java.util.*


class FacebookPlugin: FlutterPlugin, MethodChannel.MethodCallHandler {
    private lateinit var channel : MethodChannel
    private lateinit var context: Context
    private lateinit var appEventsLogger: AppEventsLogger


    private companion object Factory {
        fun registerWith(registrar: PluginRegistry.Registrar) {
            val channel = MethodChannel(registrar.messenger(), "flutter_facebook")
            channel.setMethodCallHandler(FacebookPlugin())
        }
    }

    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "flutter_facebook")
        channel.setMethodCallHandler(this)
        context = flutterPluginBinding.applicationContext
        appEventsLogger = AppEventsLogger.newLogger(flutterPluginBinding.applicationContext)
        FacebookSdk.setAutoLogAppEventsEnabled(true)
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: MethodChannel.Result) {
        when (call.method) {
            "logEvent" -> handleLogEvent(call, result)
            "logPurchase" -> handlePurchased(call, result)
            else -> result.notImplemented()
        }
    }

    private fun handlePurchased(call: MethodCall, result: Result) {
        var amount = (call.argument("amount") as? Double)?.toBigDecimal()
        var currency = Currency.getInstance(call.argument("currency") as? String)
        val parameters = call.argument("parameters") as? Map<String, Object>
        val parameterBundle = createBundleFromMap(parameters) ?: Bundle()

        appEventsLogger.logPurchase(amount, currency, parameterBundle)
        result.success(null)
    }

    private fun handleLogEvent(call: MethodCall, result: Result) {
        val eventName = call.argument("name") as? String
        val parameters = call.argument("parameters") as? Map<String, Object>
        val valueToSum = call.argument("_valueToSum") as? Double

        if (valueToSum != null && parameters != null) {
            val parameterBundle = createBundleFromMap(parameters)
            appEventsLogger.logEvent(eventName, valueToSum, parameterBundle)
        } else if (valueToSum != null) {
            appEventsLogger.logEvent(eventName, valueToSum)
        } else if (parameters != null) {
            val parameterBundle = createBundleFromMap(parameters)
            appEventsLogger.logEvent(eventName, parameterBundle)
        } else {
            appEventsLogger.logEvent(eventName)
        }

        result.success(null)
    }

    private fun createBundleFromMap(parameterMap: Map<String, Any>?): Bundle? {
        if (parameterMap == null) {
            return null
        }

        val bundle = Bundle()
        for (jsonParam in parameterMap.entries) {
            val value = jsonParam.value
            val key = jsonParam.key
            if (value is String) {
                bundle.putString(key, value as String)
            } else if (value is Int) {
                bundle.putInt(key, value as Int)
            } else if (value is Long) {
                bundle.putLong(key, value as Long)
            } else if (value is Double) {
                bundle.putDouble(key, value as Double)
            } else if (value is Boolean) {
                bundle.putBoolean(key, value as Boolean)
            } else if (value is Map<*, *>) {
                val nestedBundle = createBundleFromMap(value as Map<String, Any>)
                bundle.putBundle(key, nestedBundle as Bundle)
            } else {
                throw IllegalArgumentException(
                    "Unsupported value type: " + value.javaClass.kotlin)
            }
        }
        return bundle
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }
}
// Copyright 2013 The Flutter Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// ignore_for_file: avoid_print

import 'dart:async';
import 'dart:io';

import 'package:integration_test/integration_test_driver.dart';

const String _package = 'com.kumple.test';

Future<void> main() async {
  final bool adbExists =
      Process.runSync('which', <String>['adb']).exitCode == 0;
  if (!adbExists) {
    print(r'This test needs ADB to exist on the $PATH. Skipping...');
    exit(0);
  }
  print('Granting camera permissions...');
  Process.runSync('adb',
      <String>['shell', 'pm', 'grant', _package, 'android.permission.CAMERA']);
  Process.runSync('adb', <String>[
    'shell',
    'pm',
    'grant',
    _package,
    'android.permission.POST_NOTIFICATIONS'
  ]);
  print('Starting test.');
  await integrationDriver();
}

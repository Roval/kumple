{
   "save": "Save",
   "next": "Next",
   "maintenance": "Sorry, we're down for scheduled maintenance.\nWe will be back in 2 minutes!",
   "host": "Host",
   "discord": {
      "title": "Discord #Buddies",
      "body": "Click to join our community"
   },
   "welcome": {
      "id": "ID {id}",
      "segments": {
         "games": "Games",
         "store": "Shop",
         "settings": "Settings"
      },
      "myGames": {
         "started": "In progress",
         "notStarted": "Pending to start",
         "waiting": "Awaiting",
         "yourTurn": "Your turn",
         "joinAgain": "New game",
         "noYourGames": "You don't have any active games",
         "noYourGamesHint": "Games are available for 24 hours from the last activity",
         "fetchGamesError": "We couldn't download your games :(",
         "fetchGamesHintError": "Check your internet connection"
      },
      "store": {
         "news": "News",
         "promoTitle": "Free bundle!",
         "promoSubtitle": "Share the game to get a bundle"
      },
      "error": {
         "joinRoom": "Something went wrong :(\nYou cannot join the game.",
         "maxPlayersCount": "Maximum number of players in the room.",
         "noRoom": "The room could not be found.",
         "inOtherRoom": "You can't join another game at the moment!",
         "unknown": "An unknown error occurred: {error}"
      }
   },
   "setupGame": {
      "bundles": "Bundles",
      "hardcoreWarningTitle": "Thank you for purchasing the Hardcore bundle.",
      "hardcoreWarningSubtitle": "Note that it is disabled by default before every game.",
      "title": {
         "newGame": "New game",
         "nextGame": "Next game",
         "editGame": "Edit game"
      },
      "error": {
         "createRoom": "Something went wrong :(\nThe game could not be created."
      },
      "settings": {
         "roundsCount": "Number of rounds",
         "quesitonTypes": {
            "title": "Types of questions",
            "noImage": "No photos",
            "noDrag": "No assignment",
            "moreDisabled": "{count} disabled",
            "all": "All",
            "noQuestionHint1": "Impact on gameplay",
            "noQuestionHint2": "Disabling questions may make them repeat more often."
         },
         "language": {
            "title": "Question language",
            "polish": "Polish",
            "english": "English",
            "ukrainian": "Ukrainian",
            "german": "German"
         }
      }
   },
   "waitingRoom": {
      "id": "ID {id}",
      "noInviteFriends": {
         "title": "We are actively working on the two-player version 🔥",
         "shareMessage": "Join me and let's play Buddies!",
         "infoTitle": "Whom to invite?",
         "infoCaption": "Friends, acquaintances, family - anyone you know even a little!"
      },
      "button": {
         "inviteFriends": "Add players",
         "noInviteFriends": "No one to play with",
         "waitForPlayers": "Awaiting players",
         "startGame": "Start the game"
      },
      "status": {
         "notEnoughPlayers2": "Add 2 friends 👦🏼👩🏽 and start the fun!",
         "playersList": "Player list"
      },
      "list": {
         "requirePlayer3": "Add the third player",
         "addPlayer": "Add player"
      },
      "tags": {
         "roundCount": "{count} rounds",
         "remainingBundlesCount": "+{count} more",
         "showLess": "Collapse"
      }
   },
   "joinGame": {
      "title": "Join game",
      "insertRoomId": "Enter ID",
      "roomId": "Room ID",
      "button": "Join"
   },
   "editProfile": {
      "title": "Your profile",
      "name": "Player's name",
      "avatar": "Avatar",
      "insertName": "Enter your name",
      "takePicture": "Take a profile picture",
      "readyButton": "Ready"
   },
   "settings": {
      "game": "Game",
      "sounds": "Sounds",
      "links": "Links",
      "fillProfile": "Fill out the profile",
      "editProfile": "Edit",
      "privacyPolicy": "Privacy policy",
      "contact": "Help / Contact",
      "license": "Licenses",
      "email": "help@buddies.fun",
      "emailFallbackTitle": "Please contact us: {email}",
      "emailFallbackSubtitle": "We will reply as soon as possible!",
      "help": {
         "title": "I need help with...",
         "question1": {
            "title": "I have a new phone and lost access to questions",
            "content": "Make sure you are logged in to the App Store or Google Play with the account you used to purchase the sets. Then:\n\n • Ensure you have an internet connection\n • Open the main screen of Buddies\n • Go to the <bold>Store tab</bold>\n • Under all sets, at the very bottom, you will find the <bold>“Restore purchases”</bold> button\n • After pressing it, your sets will be restored within a few seconds.\n\nIf the above solution doesn't work, please contact us."
         },
         "question2": {
            "title": "I purchased a set and don't have access to it",
            "content": "If there was an error during the purchase of a new set, and the money was deducted from your account:\n\n • Ensure you have an internet connection\n • Open and close the Buddies app\n • Go to the <bold>Store tab</bold> on the main screen\n • Under all sets, at the very bottom, you will find the <bold>“Restore purchases”</bold> button\n • After pressing it, your purchase should be visible.\n\nIf the above solution doesn't work, please contact us."
         },
         "questionFallback": {
            "title": "Other Issue"
         },
         "contact": "Contact us"
      }
   },
   "gameboards": {
      "send": "Send",
      "roundsCountAll": "Round {count}/{all}",
      "sayTruthHint": "Answer truthfully",
      "predictAnswer": "Choose like the majority of players",
      "text": {
         "yourAnswer": "Enter your answer"
      },
      "awaitingOthers": {
         "readyState": "Ready",
         "readyStateAnswer": "Answered",
         "pendingState": "Awaiting...",
         "waitForNextRound": "Waiting for the next round {count}/{all}",
         "waitForOtherPlayers": "Waiting for answers {count}/{all}",
         "awaitHint": "A little game never killed nobody!",
         "surveyStart": "Help us improve the game",
         "surveyDone": "Thank you for your feedback 👌🏻"
      },
      "bestOf": {
         "answerHint": "Best answer wins"
      },
      "result": {
         "allAnswersDifferent": "Everyone replied differently. No points :(",
         "nextQuestion": "Continue playing",
         "points": {
            "one": "You scored 1 point",
            "other": "You scored {} points"
         },
         "assign": {
            "header": {
               "one": "1 answer was assigned correctly",
               "other": "{} answers was assigned correctly"
            },
            "oneSuccessHeader": "Answer was assigned correctly",
            "oneFailHeader": "Not correctly assigned :("
         }
      },
      "photo": {
         "captureAgain": "Capture again"
      },
      "assign": {
         "onePlayerHint": "Guess the answer of player:",
         "oneAnswerHint": "Guess who the author is: ",
         "answersHint": "Assign the answers to the players!",
         "answersTitle": "Answers"
      },
      "helper": {
         "reportQuestion": {
            "button": "Report a bug in the question",
            "confirm": "Send",
            "title": "Describe the problem",
            "confirmation": "The question has been reported",
            "confirmationSubtitle": "Thank you ❤️"
         },
         "howTo": {
            "text": {
               "title": "Assigning",
               "content": "Answer the question truthfully. In the next step, you will be guessing the answers of other players."
            },
            "assign": {
               "title": "Assigning",
               "content": "Guess the other players' answers. You will get a point for each answer assigned to the correct player."
            },
            "assignOne": {
               "title": "Assigning",
               "content": "If you choose like the mentioned player you will get a point."
            },
            "photo": {
               "title": "Photo",
               "content": "Try to take a photo that most players will like."
            },
            "photoAnswer": {
               "title": "Photo",
               "content": "If you choose a picture that most players like, you will get a point."
            },
            "draw": {
               "title": "Drawing",
               "content": "Try to create a drawing that most players will like."
            },
            "drawAnswer": {
               "title": "Drawing",
               "content": "If you choose a drawing that most players like, you will get a point."
            },
            "oneOf": {
               "title": "Guess the answer",
               "content": "If you choose like the mentioned player you will get a point."
            },
            "bestOf": {
               "title": "Best answer",
               "content": "Try to come up with an answer that most players will like."
            },
            "bestOfAnswer": {
               "title": "Best answer",
               "content": "If you choose the answer that most players like, you will get a point."
            },
            "oneUser": {
               "title": "Pick a player",
               "content": "If you choose the person that most players choose, you will get a point."
            },
            "orOr": {
               "title": "Grouping",
               "content": "Answer the question and guess how others answered. You will get a point for each player assigned to the correct group."
            }
         },
         "title": "How do you play this type of question?",
         "questionOrigin": "The question comes from the {category} category",
         "otherInfo": "Other information"
      }
   },
   "finish": {
      "title": "Results",
      "playAgain": "One more!",
      "winnerOne": "Congratulations to {player}!",
      "winnerMore": "Congratulations to {players}!"
   },
   "store": {
      "unlockAll": "Buy all bundles",
      "unlockAllShort": "Buy all",
      "restore": "Restore purchases",
      "allBoughtHint": "You have all bundles!",
      "boughtHint": {
         "one": "Your account has 1 bundle",
         "other": "Your account has {} bundles"
      },
      "whyHint": "Each paid bundle includes more than 60 new questions.",
      "error": {
         "notVerified": "Purchase verification failed. In the Settings / Help tab, you will find instructions that should resolve the issue.",
         "storeUnavailable": "Could not connect to the store.",
         "notFound": "The product could not be found.",
         "storeError": "There was an error while purchasing the product."
      },
      "offer": {
         "disappearSoon": "Disappears soon!",
         "endsSoon": "Ends soon!",
         "daysToEnd": {
            "one": "1 day left",
            "other": "{} days left"
         }
      }
   },
   "alerts": {
      "camera": {
         "requiredTitle": "Camera is required to play",
         "requiredSubtitle": "It is necessary to change the permissions!",
         "settingsOpen": "Open settings",
         "noDevice": "Camera not detected on this device!",
         "errorTitle": "Failed to take photo!",
         "errorSubtitle": "You can try to add a photo in a different way :)",
         "openLibrary": "Choose from the gallery",
         "openExternal": "Open the camera"
      },
      "error": {
         "ups": "Oops :(",
         "unknown": "An unknown error occurred",
         "noInternet": "Looks like you don't have an internet connection!"
      },
      "updateRequired": {
         "title": "You need to update the game to continue playing.",
         "subtitle": "Please visit the store to download the latest version.",
         "openShop": "Open a store"
      },
      "rate": {
         "prompt1": "👋 Hey, do you like the game BUDDIES?",
         "prompt2": "😻 Great, do you mind to rate us?"
      },
      "leave": {
         "hostTitle": "Are you sure you want to go back to the menu?",
         "participantTitle": "Leaving the room",
         "participantSubtitle": "Decide how you want to leave the room",
         "confirm": "Abandon the game",
         "goBack": "Back to menu"
      },
      "ok": "Ok",
      "yes": "Yes",
      "no": "No"
   },
   "refresh": {
      "hint": "Oops.\nIt looks like you've lost the connection",
      "button": "Refresh"
   },
   "offer": {
      "title": "Try {name} bundle",
      "subtitle": "Tired of the previous questions? {example} <bold>Buy before the next game!</bold>",
      "buttons": {
         "buy": "Buy {name} ({price})",
         "close": "Close"
      }
   },
   "chat": {
      "inputHint": "Enter message",
      "emptyHint": "Start chat 🔥",
      "unknownPlayer": "Unknown player"
   },
   "questionUsage": {
      "hint": {
         "host": {
            "subtitle": {
               "all":  "You have seen all questions at least once.",
               "duplicates": "Purchase new sets to diversify the game."
            },
            "title": {
               "freeCategory": {
                  "duplicates": "You've gone through half of the questions."
               },
               "other": {
                  "duplicates": "Are the questions starting to repeat?"
               }
            }
         }
      },
      "offer": {
         "title": {
            "example1": "The same questions = boring game.",
            "example2": "Additional excitement awaits you.",
            "example3": "Amuse your friends with new questions.",
            "allDone": "Questions will start repeating."
         }
      }
   }
}
//
//  NotificationsPlugin.swift
//  Runner
//
//  Created by Dominik Majda on 13/12/2022.
//

import Flutter
import Foundation
import UserNotifications

class NotificationsPlugin {
    static let shared = NotificationsPlugin()

    private init() {}

    static func register(forController controller: FlutterViewController) {
        let channel = FlutterMethodChannel(
            name: "flutter_notification",
            binaryMessenger: controller.binaryMessenger
        )
        channel.setMethodCallHandler {
            (call: FlutterMethodCall, result: @escaping FlutterResult) in
            if call.method == "remove" {
                let arguments = call.arguments as? [String: String] ?? [:]
                let tag = arguments["tag"]
                var toRemove = [String]()
                if let tag = tag {
                    UNUserNotificationCenter.current().getDeliveredNotifications { notifications in
                        for notification in notifications {
                            let content = notification.request.content
                            let data = content.userInfo
                            let identifier = notification.request.identifier
                            if ((data["room"] as? String) == tag) {
                                toRemove.append(identifier)
                            }
                        }
                        toRemove.append(tag)
                        
                        UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: toRemove)
                    }
                }
                
                result("ok")
            }
        }
    }
}

//
//  FacebookPlugin.swift
//  Runner
//
//  Created by Dominik Majda on 18/07/2022.
//

import AdSupport
import AppTrackingTransparency
import FBSDKCoreKit
import FBSDKShareKit
import Flutter
import Foundation

class FacebookPlugin: NSObject, FlutterPlugin {
    static let shared = FacebookPlugin()
    private var callback: FlutterResult?

    override private init() {}

    static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "flutter_facebook", binaryMessenger: registrar.messenger())

        // Required for FB SDK 9.0, as it does not initialize the SDK automatically any more.
        // See: https://developers.facebook.com/blog/post/2021/01/19/introducing-facebook-platform-sdk-version-9/
        // "Removal of Auto Initialization of SDK" section
        ApplicationDelegate.shared.initializeSDK()

        registrar.addMethodCallDelegate(shared, channel: channel)
        registrar.addApplicationDelegate(shared)
    }

    /// Connect app delegate with SDK
    public func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [AnyHashable: Any] = [:]) -> Bool {
        var options = [UIApplication.LaunchOptionsKey: Any]()
        for (k, value) in launchOptions {
            let key = k as! UIApplication.LaunchOptionsKey
            options[key] = value
        }
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: options)
        return true
    }

    public func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        AEMReporter.configure(networker: nil, appID: "1057543648470787", reporter: nil)
        AEMReporter.setConversionFilteringEnabled(true)
        AEMReporter.enable()
        AEMReporter.handle(url)
        
        let processed = ApplicationDelegate.shared.application(
            app, open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
        return processed
    }

    func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        switch call.method {
        case "logEvent":
            handleLogEvent(call, result: result)
        case "logPurchase":
            handlePurchased(call, result: result)
        case "updateAttStatus":
            let arguments = call.arguments as? [String: Bool] ?? [:]
            let isEnabled = arguments["isEnabled"] ?? false
            enableAtt(isEnabled, result: result)
        default:
            result(FlutterMethodNotImplemented)
        }
    }
    
    private func enableAtt(_ isEnabled: Bool, result: @escaping FlutterResult) {
        print("[ATT]: Updated Facebook collection")
        if (isEnabled) {
            Settings.shared.isAutoLogAppEventsEnabled = true
            Settings.shared.isAdvertiserTrackingEnabled = true
            Settings.shared.isAdvertiserIDCollectionEnabled = true
        } else {
            Settings.shared.isAutoLogAppEventsEnabled = true
            Settings.shared.isAdvertiserTrackingEnabled = false
            Settings.shared.isAdvertiserIDCollectionEnabled = false
        }
        result(nil)
    }

    private func handlePurchased(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        let arguments = call.arguments as? [String: Any] ?? [String: Any]()
        let amount = arguments["amount"] as! Double
        let currency = arguments["currency"] as! String
        let parameters = arguments["parameters"] as? [AppEvents.ParameterName: Any] ?? [AppEvents.ParameterName: Any]()
        AppEvents.shared.logPurchase(amount: amount, currency: currency, parameters: parameters)

        result(nil)
    }

    private func handleLogEvent(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        let arguments = call.arguments as? [String: Any] ?? [String: Any]()
        let eventName = arguments["name"] as! String
        let parameters = arguments["parameters"] as? [AppEvents.ParameterName: Any] ?? [AppEvents.ParameterName: Any]()
        if arguments["_valueToSum"] != nil && !(arguments["_valueToSum"] is NSNull) {
            let valueToDouble = arguments["_valueToSum"] as! Double
            AppEvents.shared.logEvent(AppEvents.Name(eventName), valueToSum: valueToDouble, parameters: parameters)
        } else {
            AppEvents.shared.logEvent(AppEvents.Name(eventName), parameters: parameters)
        }

        result(nil)
    }

}

extension FacebookPlugin: SharingDelegate {
    func sharer(_: Sharing, didCompleteWithResults _: [String: Any]) {
        FacebookPlugin.shared.callback?("ok")
    }

    func sharer(_: Sharing, didFailWithError error: Error) {
        print(error)
        FacebookPlugin.shared.callback?("error")
    }

    func sharerDidCancel(_: Sharing) {
        FacebookPlugin.shared.callback?("cancel")
    }
}

import FBAEMKit
import FBSDKCoreKit
import Flutter
import AppsFlyerLib
import UIKit

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        let controller: FlutterViewController = window?.rootViewController as! FlutterViewController
        NotificationsPlugin.register(forController: controller)
//        AppsFlyerLib.shared().useUninstallSandbox = true
        
        FacebookPlugin.register(with: registrar(forPlugin: "FacebookPlugin")!)
        GeneratedPluginRegistrant.register(with: self)
        
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }

    override func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        ApplicationDelegate.shared.application(application, continue: userActivity)
        
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb,
           let url = userActivity.webpageURL
        {
            AEMReporter.configure(networker: nil, appID: "1057543648470787", reporter: nil)
            AEMReporter.enable()
            AEMReporter.handle(url)
        }
        
        return super.application(application, continue: userActivity, restorationHandler: restorationHandler)
    }
}

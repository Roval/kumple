#!/bin/bash

# Check for the required parameter
if [ "$#" -ne 1 ]; then
    exit 1
fi

# Assign the parameter to a variable
env=$1

# Function to replace text in files for "prod" environment
replace_for_prod() {
    sed -i '' 's/com.kumple.test/com.kumple/g' lib/environment_config.dart
    sed -i '' 's/com.kumple.test/com.kumple/g' ios/Runner.xcodeproj/project.pbxproj
    sed -i '' 's/com.kumple.test/com.kumple/g' ios/Runner/Info.plist
    sed -i '' 's/com.kumple.test/com.kumple/g' android/app/build.gradle
    cp ios/Runner/GoogleService-Info_prod.plist ios/Runner/GoogleService-Info.plist
}

# Function to replace text in files for "test" environment
replace_for_test() {
    sed -i '' 's/com.kumple/com.kumple.test/g' lib/environment_config.dart
    sed -i '' 's/com.kumple/com.kumple.test/g' ios/Runner.xcodeproj/project.pbxproj
    sed -i '' 's/com.kumple/com.kumple.test/g' ios/Runner/Info.plist
    sed -i '' 's/com.kumple/com.kumple.test/g' android/app/build.gradle
    cp ios/Runner/GoogleService-Info_test.plist ios/Runner/GoogleService-Info.plist
}

# Main logic to decide which function to run
if [ "$env" == "prod" ]; then
    replace_for_prod
elif [ "$env" == "test" ]; then
    replace_for_test
else
    exit 1
fi

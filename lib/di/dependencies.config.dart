// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import '../cubits/categories_usage_cubit.dart' as _i11;
import '../cubits/chat_cubit.dart' as _i3;
import '../cubits/game_cubit.dart' as _i12;
import '../cubits/games_list_cubit.dart' as _i13;
import '../cubits/loading_cubit.dart' as _i4;
import '../cubits/maintenence_cubit.dart' as _i5;
import '../cubits/notifications_cubit.dart' as _i6;
import '../cubits/store_buy_cubit.dart' as _i14;
import '../cubits/store_cubit.dart' as _i10;
import '../repositories/server_repository.dart' as _i8;
import '../utils/notifications_service.dart' as _i7;
import '../utils/share_util.dart'
    as _i9; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(
  _i1.GetIt get, {
  String? environment,
  _i2.EnvironmentFilter? environmentFilter,
}) {
  final gh = _i2.GetItHelper(
    get,
    environment,
    environmentFilter,
  );
  gh.singleton<_i3.ChatCubit>(_i3.ChatCubit());
  gh.singleton<_i4.LoadingCubit>(_i4.LoadingCubit());
  gh.singleton<_i5.MaintenenceCubit>(_i5.MaintenenceCubit());
  gh.singleton<_i6.NotificationsCubit>(_i6.NotificationsCubit());
  gh.singleton<_i7.NotificationsService>(_i7.NotificationsService());
  gh.singleton<_i8.ServerRepository>(_i8.ServerRepository());
  gh.singleton<_i9.ShareUtil>(_i9.ShareUtil());
  gh.singleton<_i10.StoreCubit>(_i10.StoreCubit());
  gh.singleton<_i11.CategoriesUsageCubit>(
      _i11.CategoriesUsageCubit(repository: get<_i8.ServerRepository>()));
  gh.singleton<_i12.GameCubit>(
      _i12.GameCubit(repository: get<_i8.ServerRepository>()));
  gh.singleton<_i13.GamesListCubit>(
      _i13.GamesListCubit(repository: get<_i8.ServerRepository>()));
  gh.singleton<_i14.StoreBuyCubit>(_i14.StoreBuyCubit(get<_i10.StoreCubit>()));
  return get;
}

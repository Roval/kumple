// ignore: avoid_classes_with_only_static_members
class EnvironmentConfig {
  static const String serverUrl = String.fromEnvironment(
    'SERVER_URL',
    defaultValue: 'ws://192.168.0.122:8765/', // local
    // defaultValue: 'wss://ws.kumple.fun:9876', // test
    // defaultValue: 'wss://ws.kumple.fun:8765', // prod
  );

  static const String package = String.fromEnvironment(
    'PACKAGE',
    defaultValue: 'com.kumple.test',
  );
  static bool get isTestPackage => package.contains('test');

  static const String dynamicLinkUrl = String.fromEnvironment(
    'DYNAMIC_LINK',
    defaultValue: 'dev.kumple.fun',
  );
}

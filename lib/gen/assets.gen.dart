/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/widgets.dart';

class $AssetsAnimationsGen {
  const $AssetsAnimationsGen();

  /// File path: assets/animations/alert.json
  String get alert => 'assets/animations/alert.json';

  /// File path: assets/animations/crying.json
  String get crying => 'assets/animations/crying.json';

  /// File path: assets/animations/glasses.json
  String get glasses => 'assets/animations/glasses.json';

  /// File path: assets/animations/loader.json
  String get loader => 'assets/animations/loader.json';

  /// File path: assets/animations/sadface.json
  String get sadface => 'assets/animations/sadface.json';

  /// File path: assets/animations/spinner.json
  String get spinner => 'assets/animations/spinner.json';

  /// File path: assets/animations/stars.json
  String get stars => 'assets/animations/stars.json';

  /// File path: assets/animations/waiting.json
  String get waiting => 'assets/animations/waiting.json';

  /// File path: assets/animations/wifi.json
  String get wifi => 'assets/animations/wifi.json';

  /// File path: assets/animations/yawn.json
  String get yawn => 'assets/animations/yawn.json';

  /// List of all assets
  List<String> get values => [
        alert,
        crying,
        glasses,
        loader,
        sadface,
        spinner,
        stars,
        waiting,
        wifi,
        yawn
      ];
}

class $AssetsAudioGen {
  const $AssetsAudioGen();

  /// File path: assets/audio/pop.wav
  String get pop => 'assets/audio/pop.wav';

  /// List of all assets
  List<String> get values => [pop];
}

class $AssetsImagesGen {
  const $AssetsImagesGen();

  /// File path: assets/images/back_small_icon.png
  AssetGenImage get backSmallIcon =>
      const AssetGenImage('assets/images/back_small_icon.png');

  /// File path: assets/images/camera_icon.png
  AssetGenImage get cameraIcon =>
      const AssetGenImage('assets/images/camera_icon.png');

  /// File path: assets/images/cart_icon.png
  AssetGenImage get cartIcon =>
      const AssetGenImage('assets/images/cart_icon.png');

  /// File path: assets/images/cart_list_icon.png
  AssetGenImage get cartListIcon =>
      const AssetGenImage('assets/images/cart_list_icon.png');

  /// File path: assets/images/chat_icon.png
  AssetGenImage get chatIcon =>
      const AssetGenImage('assets/images/chat_icon.png');

  /// File path: assets/images/clear_icon.png
  AssetGenImage get clearIcon =>
      const AssetGenImage('assets/images/clear_icon.png');

  /// File path: assets/images/close_icon.png
  AssetGenImage get closeIcon =>
      const AssetGenImage('assets/images/close_icon.png');

  /// File path: assets/images/close_icon_black.png
  AssetGenImage get closeIconBlack =>
      const AssetGenImage('assets/images/close_icon_black.png');

  /// File path: assets/images/ctrlz_icon.png
  AssetGenImage get ctrlzIcon =>
      const AssetGenImage('assets/images/ctrlz_icon.png');

  /// File path: assets/images/discord_icon.png
  AssetGenImage get discordIcon =>
      const AssetGenImage('assets/images/discord_icon.png');

  /// File path: assets/images/edit_avatar_icon.png
  AssetGenImage get editAvatarIcon =>
      const AssetGenImage('assets/images/edit_avatar_icon.png');

  /// File path: assets/images/english_icon.png
  AssetGenImage get englishIcon =>
      const AssetGenImage('assets/images/english_icon.png');

  /// File path: assets/images/error_icon.png
  AssetGenImage get errorIcon =>
      const AssetGenImage('assets/images/error_icon.png');

  /// File path: assets/images/eye_icon.png
  AssetGenImage get eyeIcon =>
      const AssetGenImage('assets/images/eye_icon.png');

  /// File path: assets/images/fbmessanger_icon.png
  AssetGenImage get fbmessangerIcon =>
      const AssetGenImage('assets/images/fbmessanger_icon.png');

  /// File path: assets/images/flash_icon.png
  AssetGenImage get flashIcon =>
      const AssetGenImage('assets/images/flash_icon.png');

  /// File path: assets/images/flash_icon_inactive.png
  AssetGenImage get flashIconInactive =>
      const AssetGenImage('assets/images/flash_icon_inactive.png');

  /// File path: assets/images/free_icon.png
  AssetGenImage get freeIcon =>
      const AssetGenImage('assets/images/free_icon.png');

  /// File path: assets/images/free_icon_big.png
  AssetGenImage get freeIconBig =>
      const AssetGenImage('assets/images/free_icon_big.png');

  /// File path: assets/images/game_arrow_icon.png
  AssetGenImage get gameArrowIcon =>
      const AssetGenImage('assets/images/game_arrow_icon.png');

  /// File path: assets/images/german_icon.png
  AssetGenImage get germanIcon =>
      const AssetGenImage('assets/images/german_icon.png');

  /// File path: assets/images/grip_icon.png
  AssetGenImage get gripIcon =>
      const AssetGenImage('assets/images/grip_icon.png');

  /// File path: assets/images/info_icon.png
  AssetGenImage get infoIcon =>
      const AssetGenImage('assets/images/info_icon.png');

  /// File path: assets/images/info_icon_small.png
  AssetGenImage get infoIconSmall =>
      const AssetGenImage('assets/images/info_icon_small.png');

  /// File path: assets/images/instagram_icon.png
  AssetGenImage get instagramIcon =>
      const AssetGenImage('assets/images/instagram_icon.png');

  /// File path: assets/images/logo_big_en.png
  AssetGenImage get logoBigEn =>
      const AssetGenImage('assets/images/logo_big_en.png');

  /// File path: assets/images/logo_big_pl.png
  AssetGenImage get logoBigPl =>
      const AssetGenImage('assets/images/logo_big_pl.png');

  /// File path: assets/images/logo_small.png
  AssetGenImage get logoSmall =>
      const AssetGenImage('assets/images/logo_small.png');

  /// File path: assets/images/pen_icon.png
  AssetGenImage get penIcon =>
      const AssetGenImage('assets/images/pen_icon.png');

  /// File path: assets/images/platform.png
  AssetGenImage get platform =>
      const AssetGenImage('assets/images/platform.png');

  /// File path: assets/images/plus_icon.png
  AssetGenImage get plusIcon =>
      const AssetGenImage('assets/images/plus_icon.png');

  /// File path: assets/images/poland_icon.png
  AssetGenImage get polandIcon =>
      const AssetGenImage('assets/images/poland_icon.png');

  /// File path: assets/images/question_icon.png
  AssetGenImage get questionIcon =>
      const AssetGenImage('assets/images/question_icon.png');

  /// File path: assets/images/reverse_icon.png
  AssetGenImage get reverseIcon =>
      const AssetGenImage('assets/images/reverse_icon.png');

  /// File path: assets/images/selected_icon.png
  AssetGenImage get selectedIcon =>
      const AssetGenImage('assets/images/selected_icon.png');

  /// File path: assets/images/selected_white_icon.png
  AssetGenImage get selectedWhiteIcon =>
      const AssetGenImage('assets/images/selected_white_icon.png');

  /// File path: assets/images/send_icon.png
  AssetGenImage get sendIcon =>
      const AssetGenImage('assets/images/send_icon.png');

  /// File path: assets/images/settings_small_icon.png
  AssetGenImage get settingsSmallIcon =>
      const AssetGenImage('assets/images/settings_small_icon.png');

  /// File path: assets/images/shutter_icon.png
  AssetGenImage get shutterIcon =>
      const AssetGenImage('assets/images/shutter_icon.png');

  /// File path: assets/images/sms_icon.png
  AssetGenImage get smsIcon =>
      const AssetGenImage('assets/images/sms_icon.png');

  /// File path: assets/images/star_icon.png
  AssetGenImage get starIcon =>
      const AssetGenImage('assets/images/star_icon.png');

  /// File path: assets/images/telegram_icon.png
  AssetGenImage get telegramIcon =>
      const AssetGenImage('assets/images/telegram_icon.png');

  /// File path: assets/images/timer_icon.png
  AssetGenImage get timerIcon =>
      const AssetGenImage('assets/images/timer_icon.png');

  /// File path: assets/images/ukraine_icon.png
  AssetGenImage get ukraineIcon =>
      const AssetGenImage('assets/images/ukraine_icon.png');

  /// File path: assets/images/unlock_all_bg.png
  AssetGenImage get unlockAllBg =>
      const AssetGenImage('assets/images/unlock_all_bg.png');

  /// File path: assets/images/unlock_all_bg_small.png
  AssetGenImage get unlockAllBgSmall =>
      const AssetGenImage('assets/images/unlock_all_bg_small.png');

  /// File path: assets/images/unlock_all_icon.png
  AssetGenImage get unlockAllIcon =>
      const AssetGenImage('assets/images/unlock_all_icon.png');

  /// File path: assets/images/waiting_icon.png
  AssetGenImage get waitingIcon =>
      const AssetGenImage('assets/images/waiting_icon.png');

  /// File path: assets/images/whatsapp_icon.png
  AssetGenImage get whatsappIcon =>
      const AssetGenImage('assets/images/whatsapp_icon.png');

  /// List of all assets
  List<AssetGenImage> get values => [
        backSmallIcon,
        cameraIcon,
        cartIcon,
        cartListIcon,
        chatIcon,
        clearIcon,
        closeIcon,
        closeIconBlack,
        ctrlzIcon,
        discordIcon,
        editAvatarIcon,
        englishIcon,
        errorIcon,
        eyeIcon,
        fbmessangerIcon,
        flashIcon,
        flashIconInactive,
        freeIcon,
        freeIconBig,
        gameArrowIcon,
        germanIcon,
        gripIcon,
        infoIcon,
        infoIconSmall,
        instagramIcon,
        logoBigEn,
        logoBigPl,
        logoSmall,
        penIcon,
        platform,
        plusIcon,
        polandIcon,
        questionIcon,
        reverseIcon,
        selectedIcon,
        selectedWhiteIcon,
        sendIcon,
        settingsSmallIcon,
        shutterIcon,
        smsIcon,
        starIcon,
        telegramIcon,
        timerIcon,
        ukraineIcon,
        unlockAllBg,
        unlockAllBgSmall,
        unlockAllIcon,
        waitingIcon,
        whatsappIcon
      ];
}

class $AssetsSvgGen {
  const $AssetsSvgGen();

  /// File path: assets/svg/emoji1.svg.vec
  String get emoji1Svg => 'assets/svg/emoji1.svg.vec';

  /// File path: assets/svg/emoji10.svg.vec
  String get emoji10Svg => 'assets/svg/emoji10.svg.vec';

  /// File path: assets/svg/emoji11.svg.vec
  String get emoji11Svg => 'assets/svg/emoji11.svg.vec';

  /// File path: assets/svg/emoji12.svg.vec
  String get emoji12Svg => 'assets/svg/emoji12.svg.vec';

  /// File path: assets/svg/emoji13.svg.vec
  String get emoji13Svg => 'assets/svg/emoji13.svg.vec';

  /// File path: assets/svg/emoji14.svg.vec
  String get emoji14Svg => 'assets/svg/emoji14.svg.vec';

  /// File path: assets/svg/emoji15.svg.vec
  String get emoji15Svg => 'assets/svg/emoji15.svg.vec';

  /// File path: assets/svg/emoji16.svg.vec
  String get emoji16Svg => 'assets/svg/emoji16.svg.vec';

  /// File path: assets/svg/emoji17.svg.vec
  String get emoji17Svg => 'assets/svg/emoji17.svg.vec';

  /// File path: assets/svg/emoji18.svg.vec
  String get emoji18Svg => 'assets/svg/emoji18.svg.vec';

  /// File path: assets/svg/emoji19.svg.vec
  String get emoji19Svg => 'assets/svg/emoji19.svg.vec';

  /// File path: assets/svg/emoji2.svg.vec
  String get emoji2Svg => 'assets/svg/emoji2.svg.vec';

  /// File path: assets/svg/emoji20.svg.vec
  String get emoji20Svg => 'assets/svg/emoji20.svg.vec';

  /// File path: assets/svg/emoji21.svg.vec
  String get emoji21Svg => 'assets/svg/emoji21.svg.vec';

  /// File path: assets/svg/emoji22.svg.vec
  String get emoji22Svg => 'assets/svg/emoji22.svg.vec';

  /// File path: assets/svg/emoji23.svg.vec
  String get emoji23Svg => 'assets/svg/emoji23.svg.vec';

  /// File path: assets/svg/emoji24.svg.vec
  String get emoji24Svg => 'assets/svg/emoji24.svg.vec';

  /// File path: assets/svg/emoji25.svg.vec
  String get emoji25Svg => 'assets/svg/emoji25.svg.vec';

  /// File path: assets/svg/emoji26.svg.vec
  String get emoji26Svg => 'assets/svg/emoji26.svg.vec';

  /// File path: assets/svg/emoji27.svg.vec
  String get emoji27Svg => 'assets/svg/emoji27.svg.vec';

  /// File path: assets/svg/emoji28.svg.vec
  String get emoji28Svg => 'assets/svg/emoji28.svg.vec';

  /// File path: assets/svg/emoji29.svg.vec
  String get emoji29Svg => 'assets/svg/emoji29.svg.vec';

  /// File path: assets/svg/emoji3.svg.vec
  String get emoji3Svg => 'assets/svg/emoji3.svg.vec';

  /// File path: assets/svg/emoji30.svg.vec
  String get emoji30Svg => 'assets/svg/emoji30.svg.vec';

  /// File path: assets/svg/emoji31.svg.vec
  String get emoji31Svg => 'assets/svg/emoji31.svg.vec';

  /// File path: assets/svg/emoji32.svg.vec
  String get emoji32Svg => 'assets/svg/emoji32.svg.vec';

  /// File path: assets/svg/emoji33.svg.vec
  String get emoji33Svg => 'assets/svg/emoji33.svg.vec';

  /// File path: assets/svg/emoji34.svg.vec
  String get emoji34Svg => 'assets/svg/emoji34.svg.vec';

  /// File path: assets/svg/emoji35.svg.vec
  String get emoji35Svg => 'assets/svg/emoji35.svg.vec';

  /// File path: assets/svg/emoji36.svg.vec
  String get emoji36Svg => 'assets/svg/emoji36.svg.vec';

  /// File path: assets/svg/emoji37.svg.vec
  String get emoji37Svg => 'assets/svg/emoji37.svg.vec';

  /// File path: assets/svg/emoji38.svg.vec
  String get emoji38Svg => 'assets/svg/emoji38.svg.vec';

  /// File path: assets/svg/emoji39.svg.vec
  String get emoji39Svg => 'assets/svg/emoji39.svg.vec';

  /// File path: assets/svg/emoji4.svg.vec
  String get emoji4Svg => 'assets/svg/emoji4.svg.vec';

  /// File path: assets/svg/emoji40.svg.vec
  String get emoji40Svg => 'assets/svg/emoji40.svg.vec';

  /// File path: assets/svg/emoji41.svg.vec
  String get emoji41Svg => 'assets/svg/emoji41.svg.vec';

  /// File path: assets/svg/emoji42.svg.vec
  String get emoji42Svg => 'assets/svg/emoji42.svg.vec';

  /// File path: assets/svg/emoji43.svg.vec
  String get emoji43Svg => 'assets/svg/emoji43.svg.vec';

  /// File path: assets/svg/emoji44.svg.vec
  String get emoji44Svg => 'assets/svg/emoji44.svg.vec';

  /// File path: assets/svg/emoji45.svg.vec
  String get emoji45Svg => 'assets/svg/emoji45.svg.vec';

  /// File path: assets/svg/emoji46.svg.vec
  String get emoji46Svg => 'assets/svg/emoji46.svg.vec';

  /// File path: assets/svg/emoji47.svg.vec
  String get emoji47Svg => 'assets/svg/emoji47.svg.vec';

  /// File path: assets/svg/emoji48.svg.vec
  String get emoji48Svg => 'assets/svg/emoji48.svg.vec';

  /// File path: assets/svg/emoji49.svg.vec
  String get emoji49Svg => 'assets/svg/emoji49.svg.vec';

  /// File path: assets/svg/emoji5.svg.vec
  String get emoji5Svg => 'assets/svg/emoji5.svg.vec';

  /// File path: assets/svg/emoji50.svg.vec
  String get emoji50Svg => 'assets/svg/emoji50.svg.vec';

  /// File path: assets/svg/emoji51.svg.vec
  String get emoji51Svg => 'assets/svg/emoji51.svg.vec';

  /// File path: assets/svg/emoji52.svg.vec
  String get emoji52Svg => 'assets/svg/emoji52.svg.vec';

  /// File path: assets/svg/emoji53.svg.vec
  String get emoji53Svg => 'assets/svg/emoji53.svg.vec';

  /// File path: assets/svg/emoji54.svg.vec
  String get emoji54Svg => 'assets/svg/emoji54.svg.vec';

  /// File path: assets/svg/emoji55.svg.vec
  String get emoji55Svg => 'assets/svg/emoji55.svg.vec';

  /// File path: assets/svg/emoji56.svg.vec
  String get emoji56Svg => 'assets/svg/emoji56.svg.vec';

  /// File path: assets/svg/emoji57.svg.vec
  String get emoji57Svg => 'assets/svg/emoji57.svg.vec';

  /// File path: assets/svg/emoji58.svg.vec
  String get emoji58Svg => 'assets/svg/emoji58.svg.vec';

  /// File path: assets/svg/emoji59.svg.vec
  String get emoji59Svg => 'assets/svg/emoji59.svg.vec';

  /// File path: assets/svg/emoji6.svg.vec
  String get emoji6Svg => 'assets/svg/emoji6.svg.vec';

  /// File path: assets/svg/emoji60.svg.vec
  String get emoji60Svg => 'assets/svg/emoji60.svg.vec';

  /// File path: assets/svg/emoji61.svg.vec
  String get emoji61Svg => 'assets/svg/emoji61.svg.vec';

  /// File path: assets/svg/emoji62.svg.vec
  String get emoji62Svg => 'assets/svg/emoji62.svg.vec';

  /// File path: assets/svg/emoji63.svg.vec
  String get emoji63Svg => 'assets/svg/emoji63.svg.vec';

  /// File path: assets/svg/emoji64.svg.vec
  String get emoji64Svg => 'assets/svg/emoji64.svg.vec';

  /// File path: assets/svg/emoji65.svg.vec
  String get emoji65Svg => 'assets/svg/emoji65.svg.vec';

  /// File path: assets/svg/emoji66.svg.vec
  String get emoji66Svg => 'assets/svg/emoji66.svg.vec';

  /// File path: assets/svg/emoji67.svg.vec
  String get emoji67Svg => 'assets/svg/emoji67.svg.vec';

  /// File path: assets/svg/emoji68.svg.vec
  String get emoji68Svg => 'assets/svg/emoji68.svg.vec';

  /// File path: assets/svg/emoji69.svg.vec
  String get emoji69Svg => 'assets/svg/emoji69.svg.vec';

  /// File path: assets/svg/emoji7.svg.vec
  String get emoji7Svg => 'assets/svg/emoji7.svg.vec';

  /// File path: assets/svg/emoji70.svg.vec
  String get emoji70Svg => 'assets/svg/emoji70.svg.vec';

  /// File path: assets/svg/emoji71.svg.vec
  String get emoji71Svg => 'assets/svg/emoji71.svg.vec';

  /// File path: assets/svg/emoji72.svg.vec
  String get emoji72Svg => 'assets/svg/emoji72.svg.vec';

  /// File path: assets/svg/emoji8.svg.vec
  String get emoji8Svg => 'assets/svg/emoji8.svg.vec';

  /// File path: assets/svg/emoji9.svg.vec
  String get emoji9Svg => 'assets/svg/emoji9.svg.vec';

  /// File path: assets/svg/timer.svg.vec
  String get timerSvg => 'assets/svg/timer.svg.vec';

  /// List of all assets
  List<String> get values => [
        emoji1Svg,
        emoji10Svg,
        emoji11Svg,
        emoji12Svg,
        emoji13Svg,
        emoji14Svg,
        emoji15Svg,
        emoji16Svg,
        emoji17Svg,
        emoji18Svg,
        emoji19Svg,
        emoji2Svg,
        emoji20Svg,
        emoji21Svg,
        emoji22Svg,
        emoji23Svg,
        emoji24Svg,
        emoji25Svg,
        emoji26Svg,
        emoji27Svg,
        emoji28Svg,
        emoji29Svg,
        emoji3Svg,
        emoji30Svg,
        emoji31Svg,
        emoji32Svg,
        emoji33Svg,
        emoji34Svg,
        emoji35Svg,
        emoji36Svg,
        emoji37Svg,
        emoji38Svg,
        emoji39Svg,
        emoji4Svg,
        emoji40Svg,
        emoji41Svg,
        emoji42Svg,
        emoji43Svg,
        emoji44Svg,
        emoji45Svg,
        emoji46Svg,
        emoji47Svg,
        emoji48Svg,
        emoji49Svg,
        emoji5Svg,
        emoji50Svg,
        emoji51Svg,
        emoji52Svg,
        emoji53Svg,
        emoji54Svg,
        emoji55Svg,
        emoji56Svg,
        emoji57Svg,
        emoji58Svg,
        emoji59Svg,
        emoji6Svg,
        emoji60Svg,
        emoji61Svg,
        emoji62Svg,
        emoji63Svg,
        emoji64Svg,
        emoji65Svg,
        emoji66Svg,
        emoji67Svg,
        emoji68Svg,
        emoji69Svg,
        emoji7Svg,
        emoji70Svg,
        emoji71Svg,
        emoji72Svg,
        emoji8Svg,
        emoji9Svg,
        timerSvg
      ];
}

class $AssetsTranslationsGen {
  const $AssetsTranslationsGen();

  /// File path: assets/translations/de.json
  String get de => 'assets/translations/de.json';

  /// File path: assets/translations/en.json
  String get en => 'assets/translations/en.json';

  /// File path: assets/translations/pl.json
  String get pl => 'assets/translations/pl.json';

  /// File path: assets/translations/uk.json
  String get uk => 'assets/translations/uk.json';

  /// List of all assets
  List<String> get values => [de, en, pl, uk];
}

class Assets {
  Assets._();

  static const $AssetsAnimationsGen animations = $AssetsAnimationsGen();
  static const $AssetsAudioGen audio = $AssetsAudioGen();
  static const $AssetsImagesGen images = $AssetsImagesGen();
  static const $AssetsSvgGen svg = $AssetsSvgGen();
  static const $AssetsTranslationsGen translations = $AssetsTranslationsGen();
}

class AssetGenImage {
  const AssetGenImage(this._assetName);

  final String _assetName;

  Image image({
    Key? key,
    AssetBundle? bundle,
    ImageFrameBuilder? frameBuilder,
    ImageErrorWidgetBuilder? errorBuilder,
    String? semanticLabel,
    bool excludeFromSemantics = false,
    double? scale,
    double? width,
    double? height,
    Color? color,
    Animation<double>? opacity,
    BlendMode? colorBlendMode,
    BoxFit? fit,
    AlignmentGeometry alignment = Alignment.center,
    ImageRepeat repeat = ImageRepeat.noRepeat,
    Rect? centerSlice,
    bool matchTextDirection = false,
    bool gaplessPlayback = false,
    bool isAntiAlias = false,
    String? package,
    FilterQuality filterQuality = FilterQuality.low,
    int? cacheWidth,
    int? cacheHeight,
  }) {
    return Image.asset(
      _assetName,
      key: key,
      bundle: bundle,
      frameBuilder: frameBuilder,
      errorBuilder: errorBuilder,
      semanticLabel: semanticLabel,
      excludeFromSemantics: excludeFromSemantics,
      scale: scale,
      width: width,
      height: height,
      color: color,
      opacity: opacity,
      colorBlendMode: colorBlendMode,
      fit: fit,
      alignment: alignment,
      repeat: repeat,
      centerSlice: centerSlice,
      matchTextDirection: matchTextDirection,
      gaplessPlayback: gaplessPlayback,
      isAntiAlias: isAntiAlias,
      package: package,
      filterQuality: filterQuality,
      cacheWidth: cacheWidth,
      cacheHeight: cacheHeight,
    );
  }

  ImageProvider provider({
    AssetBundle? bundle,
    String? package,
  }) {
    return AssetImage(
      _assetName,
      bundle: bundle,
      package: package,
    );
  }

  String get path => _assetName;

  String get keyName => _assetName;
}

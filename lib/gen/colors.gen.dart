/// GENERATED CODE - DO NOT MODIFY BY HAND
/// *****************************************************
///  FlutterGen
/// *****************************************************

// coverage:ignore-file
// ignore_for_file: type=lint
// ignore_for_file: directives_ordering,unnecessary_import,implicit_dynamic_list_literal,deprecated_member_use

import 'package:flutter/painting.dart';
import 'package:flutter/material.dart';

class ColorName {
  ColorName._();

  /// Color: #937A70
  static const Color brown = Color(0xFF937A70);

  /// Color: #2A6032
  static const Color darkGreen = Color(0xFF2A6032);

  /// Color: #C22B2B
  static const Color darkRed = Color(0xFFC22B2B);

  /// Color: #519B34
  static const Color green = Color(0xFF519B34);

  /// Color: #757575
  static const Color grey = Color(0xFF757575);

  /// Color: #D68C62
  static const Color lightBrown = Color(0xFFD68C62);

  /// Color: #E4DFE1
  static const Color lightGrey = Color(0xFFE4DFE1);

  /// Color: #FFFBE9
  static const Color lightYellow = Color(0xFFFFFBE9);

  /// Color: #E49700
  static const Color orange = Color(0xFFE49700);

  /// Color: #ED5452
  static const Color paleRed = Color(0xFFED5452);

  /// Color: #FADADA
  static const Color pink = Color(0xFFFADADA);

  /// Color: #FFE6D6
  static const Color primaryBackground = Color(0xFFFFE6D6);

  /// Color: #E06788
  static const Color questionColor1 = Color(0xFFE06788);

  /// Color: #DC5353
  static const Color questionColor2 = Color(0xFFDC5353);

  /// Color: #CB7154
  static const Color questionColor3 = Color(0xFFCB7154);

  /// Color: #9645CE
  static const Color questionColor4 = Color(0xFF9645CE);

  /// Color: #26A469
  static const Color questionColor5 = Color(0xFF26A469);

  /// Color: #5528C1
  static const Color questionColor6 = Color(0xFF5528C1);

  /// Color: #2B7AF0
  static const Color questionColor7 = Color(0xFF2B7AF0);

  /// Color: #FF1B1B
  static const Color red = Color(0xFFFF1B1B);

  /// Color: #F5EFE9
  static const Color secondaryBackground = Color(0xFFF5EFE9);

  /// Color: #000000
  static const Color text = Color(0xFF000000);

  /// Color: #80000000
  static const Color text50 = Color(0x80000000);

  /// Color: #B2000000
  static const Color text70 = Color(0xB2000000);
}

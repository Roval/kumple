import 'package:easy_localization/easy_localization.dart';

import '../gen/translations.gen.dart';
import '../models/event.dart';
import 'camera_util.dart';

String answerBasedOnResult(final QuestionResultData result,
    {required final Player forPlayer, required final Players allPlayers}) {
  final String answer = result.answers![forPlayer.id]! as String;

  switch (result.type) {
    case QuestionType.bestOfOneUser:
    case QuestionType.assign:
    case QuestionType.assignOne:
    case QuestionType.dragOrOr:
    case QuestionType.oneOf:
      return answer;
    case QuestionType.bestOfImageOneUser:
    case QuestionType.drawOneUser:
    case QuestionType.oneUser:
      return allPlayers
          .firstWhere((final Player element) => element.id == answer)
          .name;
    case QuestionType.text:
    case QuestionType.orOr:
    case QuestionType.bestOf:
    case QuestionType.bestOfImage:
    case QuestionType.draw:
      return '';
  }
}

List<String> headerBasedOnResult(final QuestionResultData result,
    {required final Players players, required final int myPoints}) {
  switch (result.type) {
    case QuestionType.bestOfOneUser:
      final List<String> answers =
          (result.correctAnswer! as List<dynamic>).cast();
      return _bulletAnswers(answers, players);
    case QuestionType.bestOfImageOneUser:
    case QuestionType.drawOneUser:
    case QuestionType.oneUser:
      final List<String> playerUuids =
          (result.correctAnswer! as List<dynamic>).cast();
      final List<String> playerNames = players
          .where((final Player element) => playerUuids.contains(element.id))
          .map((final Player e) => e.name)
          .toList();

      return _bulletAnswers(playerNames, players);
    case QuestionType.oneOf:
      final String correct = result.correctAnswer! as String;
      return <String>[correct];
    case QuestionType.assignOne:
      return <String>[
        if (myPoints > 0)
          LocaleKeys.gameboards_result_assign_oneSuccessHeader.tr()
        else
          LocaleKeys.gameboards_result_assign_oneFailHeader.tr()
      ];
    case QuestionType.assign:
    case QuestionType.dragOrOr:
      final String correct =
          LocaleKeys.gameboards_result_assign_header.plural(myPoints);
      return <String>[correct];
    case QuestionType.text:
    case QuestionType.orOr:
    case QuestionType.bestOf:
    case QuestionType.bestOfImage:
    case QuestionType.draw:
      return <String>[''];
  }
}

List<String> _bulletAnswers(
  final List<String> answers,
  final List<Player> players,
) {
  if (answers.length == players.length) {
    return <String>[LocaleKeys.gameboards_result_allAnswersDifferent.tr()];
  }
  if (answers.length == 1) {
    return <String>[answers[0]];
  }
  return answers;
}

List<String> imagesBasedOnResult(
  final QuestionResultData result, {
  required final Players players,
  required final String roomKey,
}) {
  switch (result.type) {
    case QuestionType.bestOfImageOneUser:
    case QuestionType.drawOneUser:
      final List<String> playerUuids =
          (result.correctAnswer! as List<dynamic>).cast();

      return playerUuids
          .map(
            (final String e) => photoAnswerUrl(
              player: e,
              roomKey: roomKey,
              questionKey: result.id,
            ),
          )
          .toList();

    // ignore: no_default_cases
    default:
      return <String>[];
  }
}

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../cubits/categories_usage_state.dart';
import '../../cubits/store_cubit.dart';
import '../../cubits/store_state.dart';
import '../../di/dependencies.dart';
import '../../gen/assets.gen.dart';
import '../../gen/colors.gen.dart';
import '../../gen/translations.gen.dart';
import '../../models/event.dart';
import '../../repositories/remote_config.dart';
import '../../routing/router.gr.dart';
import '../../scenes/store_offer_screen.dart';
import '../logger.dart';

class HintUsageModel {
  HintUsageModel({
    required this.title,
    required this.description,
    required this.animation,
    required this.backgroundColor,
  });

  final String title;
  final String description;
  final String animation;
  final Color backgroundColor;
}

class _HintUsageSummary {
  _HintUsageSummary(
      {required this.used, required this.total, required this.onlyFriends});

  final int used;
  final int total;
  final bool onlyFriends;
  bool get allUsed => total - used <= 0;
  bool get lowOnQuestions =>
      total - used < RemoteConfigHelper.usageWarningMinQuestions;
}

HintUsageModel? hostCreateUsageHintModel(final FetchedUsageState state) {
  return _usageHintModel(state.usage);
}

HintUsageModel? _usageHintModel(final Map<String, dynamic> usage) {
  String title;
  String description;
  String animation;
  Color backgroundColor;

  final _HintUsageSummary summary = _calculateUsage(usage);
  if (!summary.lowOnQuestions) {
    return null;
  }

  if (_allBundlesBought) {
    logger.i('all bundles bought, do not show prompt');
    return null;
  }

  if (summary.onlyFriends) {
    title = summary.allUsed
        ? LocaleKeys.questionUsage_offer_title_allDone.tr()
        : LocaleKeys.questionUsage_hint_host_title_freeCategory_duplicates.tr();
    description = LocaleKeys.questionUsage_hint_host_subtitle_duplicates.tr();
  } else {
    title = summary.allUsed
        ? LocaleKeys.questionUsage_offer_title_allDone.tr()
        : LocaleKeys.questionUsage_hint_host_title_other_duplicates.tr();
    description = summary.allUsed
        ? LocaleKeys.questionUsage_hint_host_subtitle_all.tr()
        : LocaleKeys.questionUsage_hint_host_subtitle_duplicates.tr();
  }
  animation =
      summary.allUsed ? Assets.animations.alert : Assets.animations.sadface;
  backgroundColor =
      summary.allUsed ? ColorName.pink : ColorName.primaryBackground;

  return HintUsageModel(
      title: title,
      description: description,
      animation: animation,
      backgroundColor: backgroundColor);
}

Future<bool> showQuestionUsageAlertIfNeeded(
    final FetchedUsageState state) async {
  final UsedQuestions usage = state.usage;
  final _HintUsageSummary summary = _calculateUsage(usage);
  if (!summary.lowOnQuestions) {
    return false;
  }

  if (_allBundlesBought) {
    logger.i('all bundles bought, do not show dialog');
    return false;
  }

  StoreOfferMode mode;
  if (summary.onlyFriends) {
    mode = summary.allUsed ? StoreOfferMode.freeDone : StoreOfferMode.freeHalf;
  } else {
    mode = summary.allUsed ? StoreOfferMode.allDone : StoreOfferMode.random;
  }

  final AppRouter router = getIt<AppRouter>();
  if (router.currentPath == '/new_game') {
    router.push(StoreOffer(mode: mode));
    return true;
  }
  return false;
}

_HintUsageSummary _calculateUsage(final Map<String, dynamic> usage) {
  bool onlyFriends = true;
  int used = 0;
  int total = 0;
  for (final MapEntry<String, dynamic> entry in usage.entries) {
    if (entry.key !=
        getIt<StoreCubit>().lastFetchedStoreResponse?.response.free.bundle) {
      onlyFriends = false;
    }
    final Map<String, dynamic> categoryData =
        (entry.value as Map<String, dynamic>).cast();
    used += (categoryData['used'] as int?) ?? 0;
    total += (categoryData['total'] as int?) ?? 0;
  }
  return _HintUsageSummary(used: used, total: total, onlyFriends: onlyFriends);
}

bool get _allBundlesBought {
  final FetchStoreState? storeResponse =
      getIt<StoreCubit>().lastFetchedStoreResponse;
  if (storeResponse != null && storeResponse.notBoughtInStoreIaps.isEmpty) {
    return true;
  }
  return false;
}

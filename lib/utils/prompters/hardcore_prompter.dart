import 'package:easy_localization/easy_localization.dart';

import '../../cubits/store_state.dart';
import '../../di/dependencies.dart';
import '../../gen/translations.gen.dart';
import '../../repositories/local_storage.dart';
import '../../routing/router.gr.dart';

Future<void> showHardcoreDialogIfNeeded(final FetchStoreState state) async {
  final List<String> unlockedBundles = state.unlockedProductsBundles;
  if (unlockedBundles.contains('HARDCORE')) {
    // Warn user that hardcore is disabled by default
    if (!(await LocalStorage.isUserWarnedAboutHardcore())) {
      await LocalStorage.markAsWarnedAboutHardcore();
      Future<dynamic>.delayed(const Duration(seconds: 1)).then((final _) {
        // This has to be done with delay because
        // when we want to show alert and welcome screen is still
        // routing to "new game" screen this
        // causes a problem of multiple states to be created
        getIt<AppRouter>().push(
          InfoAlert(
            message: LocaleKeys.setupGame_hardcoreWarningTitle.tr(),
            subtitle: LocaleKeys.setupGame_hardcoreWarningSubtitle.tr(),
          ),
        );
      });
    }
  }
}

import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:in_app_review/in_app_review.dart';

import '../../constants.dart';
import '../../di/dependencies.dart';
import '../../gen/translations.gen.dart';
import '../../repositories/local_storage.dart';
import '../../repositories/remote_config.dart';
import '../../routing/router.gr.dart';
import '../../scenes/alert_screen.dart';
import '../../widget/game_button.dart';
import '../logger.dart';

Future<void> showRateDialogIfNeeded() async {
  if (!RemoteConfigHelper.isRatingEnabled) {
    logger.i('rating is disabled');
    return;
  }

  final bool isRated = await LocalStorage.isAppRated();
  if (isRated) {
    logger.i('app already rated');
    return;
  }

  final int requiredGamesCount = RemoteConfigHelper.rateMinGamesCount;
  final int requiredDaysBetween = RemoteConfigHelper.rateMinDaysBetween;

  final int howManyFinished = await LocalStorage.getHowManyGamesFinished();
  final int? lastPromptedTimestamp =
      await LocalStorage.getLastPromptTimestamp();

  if (howManyFinished < requiredGamesCount) {
    logger.i("user didn't finish at least $requiredGamesCount games");
    return;
  }

  if (lastPromptedTimestamp == null) {
    logger.i('user not prompted before, do it now!');
    await _showPrompt();
    return;
  }

  final DateTime lastPromptedDate =
      DateTime.fromMillisecondsSinceEpoch(lastPromptedTimestamp);
  final DateTime now = DateTime.now();
  final int daysDiffs = now.difference(lastPromptedDate).inDays;
  logger.i('$daysDiffs days between last prompt');

  if (daysDiffs > requiredDaysBetween) {
    logger.i(
        'at least $requiredDaysBetween days passed ($daysDiffs), show rate dialog');
    await _showPrompt();
  }
}

Future<void> _showPrompt() async {
  final AppRouter router = getIt<AppRouter>();
  router.push(
    InfoAlert(
      message: LocaleKeys.alerts_rate_prompt1.tr(),
      actions: <InfoAlertAction>[
        InfoAlertAction(
          onTap: (final VoidCallback close) {
            FirebaseAnalytics.instance.logEvent(name: 'rate_1_accepted');
            close();
            Future<void>.delayed(const Duration(milliseconds: 500))
                .then((final _) => _showFollowUpPrompt());
          },
          title: LocaleKeys.alerts_yes.tr(),
        ),
        InfoAlertAction(
          onTap: (final VoidCallback close) {
            FirebaseAnalytics.instance.logEvent(name: 'rate_1_declined');
            close();
          },
          title: LocaleKeys.alerts_no.tr(),
          smaller: true,
          style: const GameButtonStyle.none(),
        ),
      ],
    ),
  );
  await LocalStorage.markPrompted();
}

Future<void> _showFollowUpPrompt() async {
  final AppRouter router = getIt<AppRouter>();
  router.pushAndPopUntil(
    InfoAlert(
      message: LocaleKeys.alerts_rate_prompt2.tr(),
      actions: <InfoAlertAction>[
        InfoAlertAction(
          onTap: (final VoidCallback close) async {
            FirebaseAnalytics.instance.logEvent(name: 'rate_2_accepted');

            final InAppReview inAppReview = InAppReview.instance;
            await LocalStorage.markAsRated();

            if (await inAppReview.isAvailable()) {
              inAppReview.requestReview();
            } else {
              inAppReview.openStoreListing(appStoreId: kAppStoreId);
            }

            close();
          },
          title: LocaleKeys.alerts_yes.tr(),
        ),
        InfoAlertAction(
          onTap: (final VoidCallback close) {
            FirebaseAnalytics.instance.logEvent(name: 'rate_2_declined');
            close();
          },
          title: LocaleKeys.alerts_no.tr(),
          smaller: true,
          style: const GameButtonStyle.none(),
        ),
      ],
    ),
    predicate: (final Route<dynamic> route) =>
        route.settings.name != InfoAlert.name,
  );
}

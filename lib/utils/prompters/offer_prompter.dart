import '../../cubits/store_state.dart';
import '../../di/dependencies.dart';
import '../../extensions/list_extensions.dart';
import '../../models/store.dart';
import '../../repositories/local_storage.dart';
import '../../repositories/remote_config.dart';
import '../../routing/router.gr.dart';
import '../logger.dart';

StoreItem? randomNotBoughtBundle(final FetchStoreState state) {
  final List<String> unlockedBundles = state.unlockedProductsBundles;

  // Get bundles which are not unlocked and pick one randomly
  final List<StoreItem> notBought = state.response.store
      .where((final StoreItem element) =>
          !unlockedBundles.contains(element.bundle) &&
          element.bundle != 'HARDCORE')
      .toList();

  if (notBought.isEmpty) {
    logger.i('no products to offer, everything bought');
    return null;
  }
  return notBought.randomItem();
}

/// Checks if offer should be shown - and if so displays new screen with the offer.
/// Returns true if prompt was displayed
Future<bool> showBundleOfferIfNeeded(final FetchStoreState state) async {
  final StoreItem? toPromote = randomNotBoughtBundle(state);

  if (toPromote == null) {
    return false;
  }

  final int howManyResumed = await LocalStorage.getHowManyGamesPlayedAgain();
  final int requiredGamesBetween = RemoteConfigHelper.offersGamesBetween;
  final int gamesPassed = howManyResumed % requiredGamesBetween;
  if (gamesPassed != 0) {
    logger.i(
        'offer should not be shown now (current: $howManyResumed), perhaps next time');
    return false;
  }

  logger.i('will show offer for ${toPromote.bundle}');

  await Future<dynamic>.delayed(const Duration(seconds: 1));
  final AppRouter router = getIt<AppRouter>();
  router.push(
    Offer(item: toPromote, price: state.prices[toPromote.iap] ?? '...'),
  );
  return true;
}

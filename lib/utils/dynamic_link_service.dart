import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';

import '../cubits/maintenence_cubit.dart';
import '../cubits/store_buy_cubit.dart';
import '../di/dependencies.dart';
import '../mixins/join_action_mixin.dart';
import '../repositories/local_storage.dart';
import '../repositories/remote_config.dart';
import 'logger.dart';

class DynamicLinkService with JoinAction {
  factory DynamicLinkService() {
    return _instance;
  }

  DynamicLinkService._privateConstructor();

  bool _isStarted = false;

  static final DynamicLinkService _instance =
      DynamicLinkService._privateConstructor();

  void startListeniningForDynamicLinks() {
    if (_isStarted) {
      // Do not allow to call it multiple times
      return;
    }
    _isStarted = true;

    _checkInitialLink();

    logger.i('start listening for dynamic links');
    FirebaseDynamicLinks.instance.onLink
        .listen((final PendingDynamicLinkData dynamicLinkData) {
      _handleLink(dynamicLinkData);
      // ignore: always_specify_types
    }).onError((final error) {
      logger.e('problem receiving dynamic link: $error');
    });
  }

  Future<void> _checkInitialLink() async {
    final PendingDynamicLinkData? initialLink;
    try {
      initialLink = await FirebaseDynamicLinks.instance.getInitialLink();
    } on FirebaseException {
      logger.e('unable to fetch init link, firebase exception!');
      LocalStorage.markInitialLinkCheckedDuringFirstLaunch();
      return;
    }
    logger.i('initial link $initialLink');
    if (initialLink != null) {
      // If this is first time app is checking the initial link and it exists - mark user source as link
      if (await LocalStorage.isUserCheckingInitialLinkDuringFirstLaunch()) {
        FirebaseAnalytics.instance
            .setUserProperty(name: 'source', value: 'dynamic_link');
      }
      _handleLink(initialLink);
    }
    LocalStorage.markInitialLinkCheckedDuringFirstLaunch();
  }

  Future<void> _handleLink(
    final PendingDynamicLinkData dynamicLinkData,
  ) async {
    if (getIt<MaintenenceCubit>().isEnabled) {
      logger.i('maintenance enabled, do nothing');
      return;
    }

    final Uri deeplink = dynamicLinkData.link;

    if (deeplink.path == '/join' &&
        deeplink.queryParameters.containsKey('room')) {
      final String roomKey = deeplink.queryParameters['room']!;
      logger.i('open room $roomKey from deeplink');
      joinRoom(roomKey,
          isJoiningForTheFirstTime:
              true); // Here we are not sure (that might be second click on the link) if they join for the first time but we can assume it
    } else if (deeplink.path == '/start_promo') {
      logger.i('promo link clicked, add promo bundle');
      await getIt<StoreBuyCubit>()
          .buyLocally(productId: RemoteConfigHelper.installPromoIap);
    } else {
      logger.w('unrecognized deeplink');
    }
  }
}

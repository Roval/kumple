import 'logger.dart';

typedef VoidCallback = dynamic Function();

class Throttler {
  Throttler({this.throttleGapInMillis = 1000});

  final int throttleGapInMillis;

  int? lastActionTime;

  void run(final VoidCallback action) {
    if (lastActionTime == null) {
      action();
      lastActionTime = DateTime.now().millisecondsSinceEpoch;
    } else {
      if (DateTime.now().millisecondsSinceEpoch - lastActionTime! >
          throttleGapInMillis) {
        action();
        lastActionTime = DateTime.now().millisecondsSinceEpoch;
      } else {
        logger.d('action throttled');
      }
    }
  }
}

import 'dart:async';
import 'dart:io';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:injectable/injectable.dart';

import '../cubits/notifications_cubit.dart';
import '../di/dependencies.dart';
import '../mixins/join_action_mixin.dart';
import 'appsflyer_helper.dart';
import 'logger.dart';

@singleton
class NotificationsService with JoinAction {
  NotificationsService();

  late StreamController<String> _notificationStreamController;
  late Stream<String> notificationClickedStream;

  /// Game will revert this value null if user should open the chat
  String? _lastNotificationRoomClicked;

  bool _isStarted = false;
  bool _isInitialChecked = false;

  /// Returns channel key if chat awaits open
  String? consumeLastNotificationRoom() {
    final String? backup = _lastNotificationRoomClicked;
    _lastNotificationRoomClicked = null;
    return backup;
  }

  Future<void> checkInitialPush() async {
    if (_isInitialChecked) {
      // Do not allow to call it multiple times
      return;
    }
    _isInitialChecked = true;
    _notificationStreamController = StreamController<String>();
    notificationClickedStream =
        _notificationStreamController.stream.asBroadcastStream();
    // Fake subscription. Stream controller buffers all data before
    // it is subscribed for the first time. So we want to start it and reject
    final StreamSubscription<String> subsription =
        notificationClickedStream.listen((final String event) {
      // Do nothing
    });
    subsription.cancel();

    final FirebaseMessaging messaging = FirebaseMessaging.instance;
    final RemoteMessage? message = await messaging.getInitialMessage();
    if (message != null) {
      _resolveNotification(message);
    }
  }

  Future<void> startListeniningNotifications() async {
    if (_isStarted) {
      // Do not allow to call it multiple times
      return;
    }
    _isStarted = true;

    String? token;
    try {
      // Getting token can fail
      token = await FirebaseMessaging.instance.getToken();
      logger.i('firebase token $token');
    } catch (e) {
      logger.e('failed getting token for user: (e)');
    }

    if (Platform.isIOS) {
      final String? apsToken = await FirebaseMessaging.instance.getAPNSToken();
      logger.i('aps token $apsToken');
      if (apsToken != null) {
        enableUnistallCount(apsToken);
      }
    } else if (Platform.isAndroid) {
      if (token != null) {
        enableUnistallCount(token);
      }
    }

    FirebaseMessaging.onMessageOpenedApp.listen((final RemoteMessage message) {
      _resolveNotification(message);
    });

    FirebaseMessaging.onMessage.listen((final RemoteMessage event) {
      logger.d(
          '${event.data} ${event.notification?.title ?? ''} ${event.notification?.body ?? ''}');
      getIt<NotificationsCubit>().receivedNotification(event);
    });
  }

  Future<bool> wasPermissionAsked() async {
    final FirebaseMessaging messaging = FirebaseMessaging.instance;
    final NotificationSettings settings =
        await messaging.getNotificationSettings();
    return settings.authorizationStatus != AuthorizationStatus.notDetermined;
  }

  Future<void> requestPermission() async {
    final FirebaseMessaging messaging = FirebaseMessaging.instance;
    final NotificationSettings settings = await messaging.requestPermission();
    logger
        .i('background notifications status: ${settings.authorizationStatus}');
  }

  void _resolveNotification(final RemoteMessage message) {
    logger.i('Opened app with data: ${message.data}');

    if (message.data.containsKey('room')) {
      final String roomKey = message.data['room'] as String;
      logger.i('open room $roomKey from notification');
      publishNotificationClicked(
          roomKey, message.data['action'] as String? ?? '');
    }
  }

  void publishNotificationClicked(final String roomKey, final String action) {
    logger.i('notification clicked for room: $roomKey');
    if (action == 'chat') {
      // Only allow to open chat if this is user's message
      _lastNotificationRoomClicked = roomKey;
      _notificationStreamController.add(roomKey);
    }

    joinRoom(roomKey, isJoiningForTheFirstTime: false);
  }
}

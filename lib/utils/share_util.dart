import 'dart:io';
import 'dart:math';

import 'package:device_apps/device_apps.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:injectable/injectable.dart';
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';

import '../environment_config.dart';
import '../gen/assets.gen.dart';
import '../gen/translations.gen.dart';
import '../plugins/share_plugin.dart';
import 'logger.dart';

enum ShareApp {
  whatsapp,
  fbMessanger,
  telegram,
  instagram,
  sms;

  AssetGenImage get image {
    switch (this) {
      case ShareApp.whatsapp:
        return Assets.images.whatsappIcon;
      case ShareApp.fbMessanger:
        return Assets.images.fbmessangerIcon;
      case ShareApp.telegram:
        return Assets.images.telegramIcon;
      case ShareApp.instagram:
        return Assets.images.instagramIcon;
      case ShareApp.sms:
        return Assets.images.smsIcon;
    }
  }

  String get value {
    switch (this) {
      case ShareApp.whatsapp:
        return 'whatsapp://';
      case ShareApp.fbMessanger:
        return 'fb-messenger://';
      case ShareApp.instagram:
        return 'instagram://';
      case ShareApp.telegram:
        return 'tg://';
      case ShareApp.sms:
        return 'sms://';
    }
  }

  String? get package {
    switch (this) {
      case ShareApp.whatsapp:
        return 'com.whatsapp';
      case ShareApp.instagram:
        return 'com.instagram.android';
      // ignore: no_default_cases
      default:
        return null;
    }
  }
}

@singleton
class ShareUtil {
  final List<ShareApp> _possibleApps = <ShareApp>[];
  final Map<String, Uri> _cache = <String, Uri>{};

  List<ShareApp> get possibleApps => _possibleApps.length < 3
      ? <ShareApp>[]
      : _possibleApps.sublist(0, min(4, _possibleApps.length));

  Future<void> checkPossibleApps() async {
    for (final ShareApp item in ShareApp.values) {
      if (await canLaunchUrl(Uri.parse(item.value))) {
        _possibleApps.add(item);
      } else if (Platform.isAndroid) {
        if (item.package != null &&
            await DeviceApps.isAppInstalled(item.package!)) {
          _possibleApps.add(item);
        }
      }
    }
    logger.i('possible share apps: $_possibleApps');
  }

  Future<String> linkForRoom(final String roomKey) async {
    if (_cache[roomKey] == null) {
      final DynamicLinkParameters parameters = DynamicLinkParameters(
        uriPrefix: 'https://${EnvironmentConfig.dynamicLinkUrl}',
        link: Uri.parse(
            'https://${EnvironmentConfig.dynamicLinkUrl}/join?room=$roomKey'),
        androidParameters: const AndroidParameters(
          packageName: EnvironmentConfig.package,
        ),
        iosParameters: const IOSParameters(
          bundleId: EnvironmentConfig.package,
        ),
      );

      final ShortDynamicLink shortDynamicLink =
          await FirebaseDynamicLinks.instance.buildShortLink(parameters);
      final Uri uri = shortDynamicLink.shortUrl;
      _cache[roomKey] = uri;
    }
    return _cache[roomKey].toString();
  }

  Future<void> shareToExternalApp(final ShareApp app,
      {required final String roomKey}) async {
    final String link = await linkForRoom(roomKey);
    final String text = _msgWithLink(link);
    switch (app) {
      case ShareApp.whatsapp:
        FirebaseAnalytics.instance.logEvent(name: 'share_link_whatsapp');
        launchUrl(Uri.parse('${app.value}send?text=$text'));
        break;
      case ShareApp.fbMessanger:
        FirebaseAnalytics.instance.logEvent(name: 'share_link_fbmessanger');
        launchUrl(Uri.parse('${app.value}share?text=asdasd&link=$link'));
        break;
      case ShareApp.sms:
        FirebaseAnalytics.instance.logEvent(name: 'share_link_sms');
        if (Platform.isIOS) {
          launchUrl(Uri.parse('${app.value}?&body=$text'));
        } else {
          SharePlugin.sms(text: text);
        }
        break;
      case ShareApp.telegram:
        FirebaseAnalytics.instance.logEvent(name: 'share_link_telegram');
        launchUrl(Uri.parse('${app.value}msg?text=$text'));
        break;
      case ShareApp.instagram:
        FirebaseAnalytics.instance.logEvent(name: 'share_link_instagram');
        if (Platform.isIOS) {
          launchUrl(Uri.parse('${app.value}sharesheet?text=$text'));
        } else {
          SharePlugin.instagram(text: text);
        }
        break;
    }
  }

  void shareContactOnFb() {
    FirebaseAnalytics.instance.logEvent(name: 'contact_fallback_facebook');
    launchUrl(
        Uri.parse('${ShareApp.fbMessanger.value}user-thread/100083354106601'));
  }

  Future<void> shareByExtension(final String roomKey) async {
    final String link = await linkForRoom(roomKey);
    await Share.share(_msgWithLink(link));
  }

  String _msgWithLink(final String url) {
    return '${LocaleKeys.waitingRoom_noInviteFriends_shareMessage.tr()}\n$url';
  }
}

// ignore: avoid_classes_with_only_static_members
import 'dart:async';

import 'package:firebase_database/firebase_database.dart';

import 'logger.dart';

typedef DatabaseSubscription = StreamSubscription<DatabaseEvent>;

// ignore: avoid_classes_with_only_static_members
class DatabaseHelper {
  DatabaseHelper._();

  static Future<DatabaseSubscription?> startObservingChildAdded<T>({
    required final String path,
    required final Function(String, T?) onValue,
    required final Function(dynamic) onError,
  }) async {
    final DatabaseReference ref = FirebaseDatabase.instance.ref(path);
    return ref.limitToLast(1).onChildAdded.listen((final DatabaseEvent event) {
      if (event.snapshot.key != null) {
        onValue(event.snapshot.key!, event.snapshot.value as T?);
      }
    }, onError: (final dynamic message, final _) {
      onError(message);
    });
  }

  static Future<T?> getValue<T>(
      {required final String path,
      final String? before,
      final String? after,
      final int count = 25}) async {
    Query ref = FirebaseDatabase.instance.ref(path).orderByKey();
    if (before != null) {
      ref = ref.endBefore(before);
    }
    if (after != null) {
      ref = ref.startAfter(after);
    }
    ref = ref.limitToLast(count);
    final DataSnapshot snapshot = await ref.get();
    return snapshot.value as T?;
  }

  static Future<DatabaseSubscription?> startObserving<T>(
      {required final String path, required final Function(T?) onValue}) async {
    final DatabaseReference ref = FirebaseDatabase.instance.ref(path);
    T? cast(final DataSnapshot snapshot) {
      return snapshot.value as T?;
    }

    final DataSnapshot snapshot = await ref.get();
    onValue(cast(snapshot));

    return ref.onValue.listen((final DatabaseEvent event) {
      onValue(cast(event.snapshot));
    });
  }

  static Future<void> addNew(
      {required final String path,
      required final Map<String, Object> data}) async {
    final String? newKey =
        FirebaseDatabase.instance.ref().child(path).push().key;
    if (newKey == null) {
      logger.e('unable to generate new key for FirebaseDatabase');
      return;
    }
    final Map<String, Map<String, Object>> updates =
        <String, Map<String, Object>>{
      '$path/$newKey': data,
    };
    return FirebaseDatabase.instance.ref().update(updates);
  }
}

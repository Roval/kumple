import 'package:flutter/material.dart';

import 'app_theme.dart';

bool isSmallDevice = false;
bool _alreadyCalculated = false;

void calculateDeviceMetrics(final BuildContext context) {
  if (_alreadyCalculated) {
    return;
  }
  _alreadyCalculated = true;

  final Size size = MediaQuery.of(context).size;
  if (size.width < 350) {
    isSmallDevice = true;
  }
  Defaults.updateBottomSpacing(context);
}

double smallIfNeeded(final double value) {
  if (!isSmallDevice) {
    return value;
  }
  return 0.4 * value;
}

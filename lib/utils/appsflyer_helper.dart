import 'dart:io';

import 'package:appsflyer_sdk/appsflyer_sdk.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/foundation.dart';

import '../environment_config.dart';
import 'logger.dart';

late AppsflyerSdk _appsflyerSdk;

void configureAppsFlyer() {
  const String appsFlyerKey = 'NzA2cnR5yuUyoqABDM6Lue';

  if (appsFlyerKey.isNotEmpty) {
    final AppsFlyerOptions appsFlyerOptions = AppsFlyerOptions(
      afDevKey: appsFlyerKey,
      appId: EnvironmentConfig.isTestPackage || kDebugMode
          ? '674532459' // test
          : '1615045598', // prod
      // showDebug: kDebugMode,
      timeToWaitForATTUserAuthorization: 50, // for iOS 14.5
      disableAdvertisingIdentifier: false,
      disableCollectASA: false,
    );

    _appsflyerSdk = AppsflyerSdk(appsFlyerOptions);
    _appsflyerSdk.onInstallConversionData((final dynamic res) {
      logger.i('[af] install conversion data res: $res');
      // Get user source and mark it in subsequent events
      final Map<dynamic, dynamic> payload =
          // ignore: always_specify_types
          (res as Map)['payload'] as Map<dynamic, dynamic>;
      final bool isNonOrganic =
          (payload['af_status'] as String?) == 'Non-organic';
      if (isNonOrganic) {
        final String? source = payload['media_source'] as String?;
        FirebaseAnalytics.instance
            .setUserProperty(name: 'source', value: source);
      }
    });

    _appsflyerSdk.onDeepLinking((final DeepLinkResult dp) {
      switch (dp.status) {
        case Status.FOUND:
          logger.i('[af] ${dp.deepLink}');
          logger.i('[af] deep link value: ${dp.deepLink?.deepLinkValue}');
          break;
        case Status.NOT_FOUND:
          logger.i('[af] deep link not found');
          break;
        case Status.ERROR:
          logger.i('[af] deep link error: ${dp.error}');
          break;
        case Status.PARSE_ERROR:
          logger.i('[af] deep link status parsing error');
          break;
      }
    });

    if (kDebugMode && Platform.isAndroid) {
      _appsflyerSdk.setCollectAndroidId(true);
    }

    _appsflyerSdk.initSdk(
      registerConversionDataCallback: true,
      registerOnDeepLinkingCallback: true,
    );
  }
}

Future<bool?> logAppsFlyerEvent(final String eventName,
    {final Map<String, dynamic> params = const <String, dynamic>{}}) {
  return _appsflyerSdk.logEvent(eventName, params);
}

void enableUnistallCount(final String apnsToken) {
  return _appsflyerSdk.updateServerUninstallToken(apnsToken);
}

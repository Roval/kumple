import 'package:flutter/material.dart';

import '../gen/assets.gen.dart';
import '../models/store.dart';
import '../models/survey.dart';
import '../repositories/remote_config.dart';
import '../widget/new_game/change_language_setting.dart';
import 'logger.dart';

Locale _appLocale = const Locale('en');
bool _alreadySet = false;

void setupLocale(final Locale locale) {
  if (!_alreadySet) {
    logger.i('app locale set to $locale');
    _appLocale = locale;
    _alreadySet = true;
  }
}

AssetGenImage get localisedLogo {
  switch (_appLocale.languageCode) {
    case 'pl':
      return Assets.images.logoBigPl;
    default:
      return Assets.images.logoBigEn;
  }
}

Language get defaultQuestionsLanguage {
  switch (_appLocale.languageCode) {
    case 'pl':
      return Language.polish;
    case 'uk':
      return Language.ukrainian;
    case 'de':
      return Language.german;
    default:
      return Language.english;
  }
}

String localisedStoreItemTitle(final StoreItem item) {
  return item.title[_appLocale.languageCode] ?? item.title['en']!;
}

String localisedStoreItemHint(final StoreItem item) {
  final Map<String, Map<String, String>> allHints =
      RemoteConfigHelper.offersHints;
  final Map<String, String>? hint = allHints[item.bundle];
  if (hint == null) {
    return '';
  }
  return hint[_appLocale.languageCode] ?? hint['en']!;
}

String localisedHint(final Map<String, String> hint) {
  return hint[_appLocale.languageCode] ?? hint['en']!;
}

String? localisedSurveyHeader(final Survey survey) {
  return survey.header?[_appLocale.languageCode];
}

String localisedSurveyQuestion(final Survey survey) {
  return survey.question[_appLocale.languageCode] ?? survey.question['en']!;
}

String? localisedSurveyData(final Survey survey) {
  return survey.data[_appLocale.languageCode] ?? survey.data['en']!;
}

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:uuid/uuid.dart';

import '../repositories/local_storage.dart';
import 'logger.dart';

const List<String> kTestAccounts = <String>[
  'BOT_AND_TEST',
  'BOT_DOMINIK',
  '0E8XMGPXHOXM3OOIQ1C1'
];

String? _userUuid;

/// Temporary, static uuid used before Firebase Auth is created
/// It is very unlikely now that it will be used
/// As before the user is created in edit profile
/// we check if the user is logged in
/// but just in case lets keep it static for the time being
String _temporaryUuid = const Uuid().v4();

Future<void> generateUniqueUserUuid() async {
  final String? name = await LocalStorage.getUsername();
  if (kTestAccounts.contains(name)) {
    setAsTestAccount(name: name!);
    return;
  }
  if (_userUuid != null) {
    return;
  }

  final UserCredential userCredential =
      await FirebaseAuth.instance.signInAnonymously();
  logger.i('user credentials created in Firebase');
  _userUuid = userCredential.user?.uid;
  _setFirebaseIdInServices();
  logger.i('set user id in memory cache');
}

bool get isUserUuidGenerated {
  return FirebaseAuth.instance.currentUser?.uid != null;
}

String getUserUuid() {
  if (_userUuid != null) {
    return _userUuid!;
  }
  if (FirebaseAuth.instance.currentUser?.uid != null) {
    return FirebaseAuth.instance.currentUser!.uid;
  }
  logger.e('no firebase user, unexpected!');
  return _temporaryUuid;
}

void setAsTestAccount({required final String name}) {
  if (name == '0E8XMGPXHOXM3OOIQ1C1') {
    _userUuid = '0E8XmGpxhoXM3ooIq1C1qD7eEaa2';
  } else {
    _userUuid = 'hLantJIcVbaJbDePOyqSisutThW2';
  }
  _setFirebaseIdInServices();
}

void _setFirebaseIdInServices() {
  if (_userUuid != null) {
    FirebaseCrashlytics.instance.setUserIdentifier(_userUuid!);
    FirebaseAnalytics.instance.setUserId(id: _userUuid);
  }
}

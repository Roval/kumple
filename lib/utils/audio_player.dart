import 'dart:io';

import 'package:audioplayers/audioplayers.dart';

Future<void> playIncomingMessageSound() async {
  final AudioPlayer incomingMessagePlayer = AudioPlayer();
  if (Platform.isIOS) {
    await incomingMessagePlayer.setPlayerMode(PlayerMode.lowLatency);
    await incomingMessagePlayer.setAudioContext(
      const AudioContext(
        android: AudioContextAndroid(
          audioFocus: AndroidAudioFocus.gainTransient,
          usageType: AndroidUsageType.notification,
        ),
        iOS: AudioContextIOS(
          category: AVAudioSessionCategory.ambient,
          options: <AVAudioSessionOptions>[
            AVAudioSessionOptions.mixWithOthers,
          ],
        ),
      ),
    );
  }
  await incomingMessagePlayer.play(AssetSource('audio/pop.wav'));
}

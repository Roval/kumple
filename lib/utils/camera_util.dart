import 'dart:typed_data';

import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:palette_generator/palette_generator.dart';

import '../environment_config.dart';
import '../repositories/local_storage.dart';
import 'session_utils.dart';

class AvatarException implements Exception {
  AvatarException(this.cause);

  AvatarException.noInternet() : cause = noInternetCause;
  AvatarException.otherErorr() : cause = otherErrorCause;

  static const String noInternetCause = 'noInternet';
  static const String otherErrorCause = 'otherError';

  String cause;
}

String avatarUrl({required final String player}) {
  return 'https://firebasestorage.googleapis.com/v0/b/kumple-e666c.appspot.com/o/avatars%2F$player.jpg?alt=media';
}

/// Cached values of current user's avatar
FullMetadata? _currentAvatarMetadata;

/// Returns current avatar metadata.
/// If exists - it means user has the avatar
Future<FullMetadata?> myAvatarMetadata() async {
  if (_currentAvatarMetadata != null) {
    return _currentAvatarMetadata;
  }

  final bool hasInternet = await InternetConnectionChecker().hasConnection;
  if (!hasInternet) {
    throw AvatarException.noInternet();
  }

  final FirebaseStorage storage = FirebaseStorage.instance;
  final Reference ref = storage.ref('avatars/${getUserUuid()}.jpg');
  try {
    final FullMetadata result = await ref.getMetadata();
    _currentAvatarMetadata = result;
    return result;
  } on FirebaseException catch (e) {
    if (e.code == 'object-not-found') {
      return null;
    } else {
      throw AvatarException.otherErorr();
    }
  }
}

Future<bool> saveAvatar(final Uint8List file,
    {final Map<String, String> additionalMetadata =
        const <String, String>{}}) async {
  final Map<String, String> customMetadata = <String, String>{
    'env': EnvironmentConfig.isTestPackage ? 'test' : 'prod'
  };
  customMetadata.addAll(additionalMetadata);

  final FirebaseStorage storage = FirebaseStorage.instance;
  final Reference ref = storage.ref('avatars/${getUserUuid()}.jpg');
  final SettableMetadata metadata = SettableMetadata(
      contentType: 'image/jpeg', customMetadata: customMetadata);
  final TaskSnapshot result = await ref.putData(file, metadata);
  _currentAvatarMetadata = result.metadata;
  return true;
}

String photoAnswerUrl({
  required final String player,
  required final String roomKey,
  required final String questionKey,
}) {
  return 'https://firebasestorage.googleapis.com/v0/b/kumple-e666c.appspot.com/o/answers${EnvironmentConfig.isTestPackage ? '_test' : ''}%2F$roomKey%2F$questionKey%2F$player.jpg?alt=media';
}

Future<bool> savePhotoAnswer({
  required final Uint8List file,
  required final String roomKey,
  required final String questionKey,
}) async {
  final FirebaseStorage storage = FirebaseStorage.instance;
  final Reference ref = storage.ref(
      'answers${EnvironmentConfig.isTestPackage ? '_test' : ''}/$roomKey/$questionKey/${getUserUuid()}.jpg');
  final SettableMetadata metadata = SettableMetadata(
      contentType: 'image/jpeg',
      customMetadata: <String, String>{
        'env': EnvironmentConfig.isTestPackage ? 'test' : 'prod'
      });
  await ref.putData(file, metadata);
  return true;
}

/// Used when emoji introduced as avatar - it checks if avatar
/// is black if so replaces it with sometgin good
/// It only runs once, on first app startup
Future<bool> migrateAvatarIfNeeded() async {
  final bool isAvatarMigrated = await LocalStorage.getAvatarMigrated();
  if (isAvatarMigrated) {
    return false;
  }
  // await LocalStorage.markAvatarMigrated();
  if (!isUserUuidGenerated) {
    return false;
  }
  final NetworkImage provider = NetworkImage(avatarUrl(player: getUserUuid()));
  return _isImageBlack(provider);
}

Future<bool> _isImageBlack(final ImageProvider image) async {
  const double threshold = 0.06;
  final double imageLuminance = await _getAvgLuminance(image);
  return imageLuminance < threshold;
}

Future<double> _getAvgLuminance(final ImageProvider image) async {
  final List<Color> colors = await _getImagePalette(image);
  double totalLuminance = 0;
  for (final Color color in colors) {
    totalLuminance += color.computeLuminance();
  }
  return totalLuminance / colors.length;
}

// Calculate dominant color from ImageProvider
Future<List<Color>> _getImagePalette(final ImageProvider imageProvider) async {
  final PaletteGenerator paletteGenerator =
      await PaletteGenerator.fromImageProvider(imageProvider);
  return paletteGenerator.colors.toList();
}

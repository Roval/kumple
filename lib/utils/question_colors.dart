import 'package:flutter/material.dart';

import '../gen/colors.gen.dart';

const List<Color> questionColors = <Color>[
  ColorName.questionColor1,
  ColorName.questionColor2,
  ColorName.questionColor3,
  ColorName.questionColor4,
  ColorName.questionColor5,
  ColorName.questionColor6,
  ColorName.questionColor7,
];

import 'package:flutter/material.dart';

import '../gen/colors.gen.dart';
import '../gen/fonts.gen.dart';
import '../widget/game_button.dart';

// ignore: avoid_classes_with_only_static_members
class Defaults {
  // Send button
  static double buttomSpacingScroll =
      bottomSpacing + GameButtonViewConfig.height + 24;
  static const double _initBottomSpacing = 24;
  static double bottomSpacing = _initBottomSpacing;

  static void updateBottomSpacing(final BuildContext context) {
    bottomSpacing = _initBottomSpacing + MediaQuery.of(context).padding.bottom;
  }

  // Answer buttons
  static const double buttonsSpacing = 16;

  // Corner radius
  static const double cornerRadiusValue = 10;
  static BorderRadius cornerRadius = BorderRadius.circular(cornerRadiusValue);
  static const Radius bottomSheetCornerRadius = Radius.circular(20);

  // Drag indicator
  static const double dockDragIndicatorTopMargin = 8;
  static const double dockDragIndicatorBottomMargin = 24;
}

final ThemeData appTheme = ThemeData(
  fontFamily: FontFamily.nunito,
  textTheme: const TextTheme(
    displayLarge: TextStyle(
      fontSize: 32,
      fontWeight: FontWeight.w700,
      color: ColorName.text,
    ),
    displayMedium: TextStyle(
      fontSize: 24,
      fontWeight: FontWeight.w700,
      color: ColorName.text,
    ),
    displaySmall: TextStyle(
      fontSize: 24,
      fontWeight: FontWeight.w600,
      color: ColorName.text,
    ),
    headlineLarge: TextStyle(
      fontSize: 20,
      fontWeight: FontWeight.w600,
      color: ColorName.text,
    ),
    headlineMedium: TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.w700,
      color: ColorName.text,
    ),
    headlineSmall: TextStyle(
      fontSize: 18,
      fontWeight: FontWeight.w600,
      color: ColorName.text,
    ),
    titleLarge: TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w700,
      color: ColorName.text,
    ),
    titleMedium: TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w600,
      color: ColorName.text,
    ),
    titleSmall: TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w500,
      color: ColorName.text,
    ),
    bodyLarge: TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w700,
      color: ColorName.text,
    ),
    bodyMedium: TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w600,
      color: ColorName.text,
    ),
    bodySmall: TextStyle(
      fontSize: 14,
      fontWeight: FontWeight.w500,
      color: ColorName.text,
    ),
    labelLarge: TextStyle(
      fontSize: 12,
      fontWeight: FontWeight.w700,
      color: ColorName.text,
    ),
    labelMedium: TextStyle(
      fontSize: 12,
      fontWeight: FontWeight.w600,
      color: ColorName.text,
    ),
  ),
);

import 'package:flutter/material.dart';

import 'app_theme.dart';

enum ElementOrder {
  first,
  middle,
  last,
  onlyOne,
  onlyOneWithBorder;
}

ElementOrder order(final int index, final int all) {
  if (all == 1) {
    return ElementOrder.onlyOne;
  }
  return index == 0
      ? ElementOrder.first
      : index == all - 1
          ? ElementOrder.last
          : ElementOrder.middle;
}

BorderRadius radius(final ElementOrder order) {
  if (order == ElementOrder.onlyOne ||
      order == ElementOrder.onlyOneWithBorder) {
    return Defaults.cornerRadius;
  }
  if (order == ElementOrder.first) {
    return const BorderRadius.only(
        topLeft: Radius.circular(Defaults.cornerRadiusValue),
        topRight: Radius.circular(Defaults.cornerRadiusValue));
  }
  if (order == ElementOrder.last) {
    return const BorderRadius.only(
        bottomLeft: Radius.circular(Defaults.cornerRadiusValue),
        bottomRight: Radius.circular(Defaults.cornerRadiusValue));
  }
  return BorderRadius.zero;
}

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/widgets.dart';

import 'logger.dart';

// https://stackoverflow.com/a/55831114/2734478
final RouteObserver<ModalRoute<void>> routeObserver =
    RouteObserver<ModalRoute<void>>();

mixin RouteAwareAnalytics<T extends StatefulWidget> on State<T>
    implements RouteAware {
  String get screenName;
  String get screenClass;

  @override
  void didChangeDependencies() {
    routeObserver.subscribe(this, ModalRoute.of(context)!);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPop() {}

  @override
  void didPopNext() {
    // Called when the top route has been popped off,
    // and the current route shows up.
    logCurrentScreen();
  }

  @override
  void didPush() {
    // Called when the current route has been pushed.
    logCurrentScreen();
  }

  @override
  void didPushNext() {}

  void logCurrentScreen() {
    logger.d('screen: $screenName');

    FirebaseAnalytics.instance.setCurrentScreen(
      screenName: screenName,
      screenClassOverride: screenClass,
    );
  }
}

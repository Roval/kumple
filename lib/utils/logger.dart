import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:logger/logger.dart';

final Logger logger = Logger(
  printer: PrettyPrinter(methodCount: 0, noBoxingByDefault: true),
  filter: _PassAllLogs(),
  output: kReleaseMode ? _ExternalOutput() : ConsoleOutput(),
);

class _ExternalOutput extends LogOutput {
  @override
  void output(final OutputEvent event) {
    final String text = event.lines.join('\n');
    FirebaseCrashlytics.instance.log(text);
  }
}

class _PassAllLogs extends LogFilter {
  @override
  bool shouldLog(final LogEvent event) {
    return true;
  }
}

class Session {
  Session(this.roomKey, this.myPlayerId);

  final String roomKey;
  final String myPlayerId;
}

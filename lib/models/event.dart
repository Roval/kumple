// ignore_for_file: overridden_fields

import 'package:equatable/equatable.dart';

import '../extensions/string_extensions.dart';
import '../utils/camera_util.dart';
import '../utils/logger.dart';

typedef UsedQuestions = Map<String, dynamic>;
typedef Answers = Map<String, Object>;
typedef Players = List<Player>;

abstract class Event {
  late String type;
}

class Rounds extends Equatable {
  const Rounds(this.current, this.maxRounds);

  final int current;
  final int maxRounds;

  @override
  List<Object?> get props => <Object>[current, maxRounds];
}

class DisconnectEvent extends Event {
  DisconnectEvent(this.reason) : type = 'disconnect';

  @override
  final String type;
  final String? reason;
}

class ErrorEvent extends Event {
  ErrorEvent.fromJson(final Map<String, dynamic> json)
      : type = json['type'] as String,
        error = json['error'] as String;

  @override
  final String type;
  final String error;
}

class KickedEvent extends Event {
  KickedEvent.fromJson(final Map<String, dynamic> json)
      : type = json['type'] as String,
        room = json['room'] as String;

  @override
  final String type;
  final String room;
}

class FetchGamesEvent extends Event {
  FetchGamesEvent.fromJson(final Map<String, dynamic> json)
      : type = json['type'] as String,
        games = (json['games'] as Map<String, dynamic>)
            .cast<String, dynamic>()
            .entries
            .map((final MapEntry<String, dynamic> entry) {
          return PendingGame.fromJson(
              entry.key, entry.value as Map<String, dynamic>);
        }).toList();

  @override
  final String type;
  final List<PendingGame> games;
}

class FetchUsageEvent extends Event {
  FetchUsageEvent.fromJson(final Map<String, dynamic> json)
      : type = json['type'] as String,
        usage = (json['usage'] as Map<dynamic, dynamic>).cast();

  @override
  final String type;
  final UsedQuestions usage;
}

class GameEvent extends Event {
  GameEvent.fromJson(final Map<String, dynamic> json)
      : id = json['id'] as int,
        game = Game.fromJson((json['game'] as Map<dynamic, dynamic>).cast()),
        type = json['type'] as String;

  final int id;
  final Game game;
  @override
  final String type;
}

class PendingGame {
  PendingGame.fromJson(this.roomKey, final Map<String, dynamic> json)
      : state = json['state'] as String,
        expiresAt = json['expires_at'] as int,
        rounds = Rounds(json['rounds'] as int, json['max_rounds'] as int),
        owner = json['created_by'] as String,
        isMyPlayerReady = json['is_ready'] as bool,
        players = (json['players'] as List<dynamic>).cast<String>();

  final String roomKey;
  final Rounds rounds;
  final String owner;
  final String state;
  final int expiresAt;
  final bool isMyPlayerReady;
  final List<String> players;
}

class Game {
  Game.fromJson(final Map<String, dynamic> json)
      : state = json['state'] as String,
        room = json['room'] as String,
        rounds = Rounds(json['round'] as int, json['max_rounds'] as int),
        owner = json['owner'] as String,
        data = _mapBasedOnState(
          json['state'] as String,
          (json['data'] as Map<dynamic, dynamic>).cast(),
        ),
        players = (json['players'] as List<dynamic>)
            .cast<Map<dynamic, dynamic>>()
            .map((final Map<dynamic, dynamic> v) => Player.fromJson(v.cast()))
            .toList();

  final Rounds rounds;
  final String owner;
  final String state;
  final String room;
  final GameData data;
  final Players players;
}

abstract class GameData {}

class UnknownData extends GameData {}

class FinishData extends GameData {
  FinishData.fromJson(final Map<String, dynamic> json)
      : nextRoomKey = json['next_room_key'] as String;

  final String nextRoomKey;
}

class PendingData extends GameData {
  PendingData.fromJson(final Map<String, dynamic> json)
      : readyPlayers = (json['ready_players'] as List<dynamic>).cast(),
        categories = (json['categories'] as List<dynamic>).cast(),
        excludedQuestions =
            (json['excluded_questions'] as List<dynamic>).cast(),
        language = json['language'] as String;

  final List<String> readyPlayers;
  final List<String> categories;
  final List<String> excludedQuestions;
  final String language;
}

enum QuestionType {
  oneUser,
  text,
  assign,
  assignOne,
  oneOf,
  orOr,
  dragOrOr,
  bestOf,
  bestOfOneUser,
  bestOfImage,
  bestOfImageOneUser,
  draw,
  drawOneUser
}

class QuestionData extends GameData {
  QuestionData.fromJson(final Map<String, dynamic> json)
      : id = json['id'] as String,
        type = _mapQuestionType(json['type'] as String),
        answers = (json['answers'] as Map<dynamic, dynamic>).cast(),
        addons = json['addons'],
        data = json['data'] != null ? json['data'] as String : null,
        rawQuestion = json['question'] as String;

  final String rawQuestion;
  final String id;
  final QuestionType type;
  final Answers answers;
  final Object? addons;
  final String? data;

  String get question => rawQuestion.sanitizeTags;
}

class QuestionResultData extends GameData {
  QuestionResultData.fromJson(final Map<String, dynamic> json)
      : id = json['id'] as String,
        readyPlayers = (json['ready_players'] as List<dynamic>).cast(),
        winners = (json['winners'] as Map<dynamic, dynamic>).cast(),
        type = _mapQuestionType(json['type'] as String),
        correctAnswer = json['correct_answer'],
        answers = json['answers'] is Map
            ? (json['answers'] as Map<dynamic, dynamic>).cast()
            : null,
        rawQuestion = json['question'] as String;

  final String id;
  final String rawQuestion;
  final List<String> readyPlayers;
  final Map<String, int> winners;
  final QuestionType type;
  final Object? correctAnswer;
  final Answers? answers;

  String get question => rawQuestion.replaceAll('<', '').replaceAll('>', '');
}

GameData _mapBasedOnState(final String state, final Map<String, dynamic> data) {
  switch (state) {
    case 'pending':
      return PendingData.fromJson(data);
    case 'question':
      return QuestionData.fromJson(data);
    case 'question_result':
      return QuestionResultData.fromJson(data);
    case 'finish':
      return FinishData.fromJson(data);
    default:
      logger.e('detected unknown data $data');
      return UnknownData();
  }
}

QuestionType _mapQuestionType(final String type) {
  switch (type.toLowerCase()) {
    case 'draw':
      return QuestionType.draw;
    case 'draw_one_user':
      return QuestionType.drawOneUser;
    case 'one_user':
      return QuestionType.oneUser;
    case 'text':
      return QuestionType.text;
    case 'assign_one':
      return QuestionType.assignOne;
    case 'assign':
      return QuestionType.assign;
    case 'one_of':
      return QuestionType.oneOf;
    case 'or_or':
      return QuestionType.orOr;
    case 'drag_or_or':
      return QuestionType.dragOrOr;
    case 'best_of_image':
      return QuestionType.bestOfImage;
    case 'best_of':
      return QuestionType.bestOf;
    case 'best_of_one_user':
      return QuestionType.bestOfOneUser;
    case 'best_of_image_one_user':
      return QuestionType.bestOfImageOneUser;
    default:
      throw Exception('unregognized type of question $type!');
  }
}

class Player {
  Player.fromJson(final Map<String, dynamic> json)
      : id = json['id'] as String,
        _name = json['name'] as String,
        connected = json['connected'] as bool,
        points = json['points'] as int;

  final String id;
  final String _name;
  final bool connected;
  final int points;

  String get avatar => avatarUrl(player: id);
  String get name => _name.nonBreaking.replaceAll('BOT_', '');
}

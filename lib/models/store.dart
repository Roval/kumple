import 'package:collection/collection.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../extensions/color_extensions.dart';

class StoreDefinition extends Equatable {
  StoreDefinition.fromJson(final Map<String, dynamic> json)
      : free =
            StoreItem.fromJson((json['free'] as Map<dynamic, dynamic>).cast()),
        unlockAll = IapItem.fromJson(
            (json['unlock_all'] as Map<dynamic, dynamic>).cast()),
        seasonal = ((json['seasonal'] ?? <dynamic>[]) as List<dynamic>)
            .cast<Map<String, dynamic>>()
            .map((final Map<String, dynamic> e) {
              try {
                return StoreItem.fromJson(e.cast());
              } catch (e) {
                return null;
              }
            })
            .whereNotNull()
            .toList(),
        store = (json['store'] as List<dynamic>)
            .cast<Map<String, dynamic>>()
            .map((final Map<String, dynamic> e) {
              try {
                return StoreItem.fromJson(e.cast());
              } catch (e) {
                return null;
              }
            })
            .whereNotNull()
            .toList();

  final StoreItem free;
  final List<StoreItem> seasonal;
  final List<StoreItem> store;
  final IapItem unlockAll;

  List<StoreItem> all({final bool vertical = false}) {
    final List<StoreItem> all = <StoreItem>[free] + seasonal + store;
    if (vertical) {
      final List<StoreItem> even = <StoreItem>[];
      final List<StoreItem> odd = <StoreItem>[];
      all.asMap().forEach((final int index, final StoreItem element) {
        if (index.isOdd) {
          odd.add(element);
        } else {
          even.add(element);
        }
      });
      return even + odd;
    }
    return all;
  }

  @override
  List<Object?> get props => <Object>[free, seasonal, store, unlockAll];
}

class IapItem extends Equatable {
  IapItem.fromJson(final Map<String, dynamic> json)
      : _iapiOS = json['iap_ios'] as String,
        _iapAndroid = json['iap_android'] as String;
  final String _iapiOS;
  final String _iapAndroid;

  String get iap =>
      defaultTargetPlatform == TargetPlatform.android ? _iapAndroid : _iapiOS;

  @override
  List<Object?> get props => <Object?>[iap];
}

class StoreItem extends IapItem {
  StoreItem.fromJson(final Map<String, dynamic> json)
      : bundle = json['bundle'] as String,
        imageUrl = json['logo_url'] as String,
        backgroundUrl = json['bg_url'] as String?,
        color = HexColor.fromHex(json['color'] as String),
        promoSince = json['since'] as String?,
        promoUntil = json['until'] as String?,
        title = <String, String?>{
          'pl': json['title_pl'] as String?,
          'en': json['title_en'] as String?,
          'de': json['title_de'] as String?,
          'uk': json['title_uk'] as String?,
        },
        super.fromJson(json);

  final String? promoSince;
  final String? promoUntil;
  final String bundle;
  final Color color;
  final String imageUrl;
  final String? backgroundUrl;
  final Map<String, String?> title;

  double get daysRemainingForPromo {
    if (promoUntil != null) {
      final DateTime date = DateTime.parse(promoUntil!);
      final double diffInDays = date.difference(DateTime.now()).inHours / 24;
      return diffInDays;
    }
    return 0;
  }

  double get daysToPromo {
    if (promoSince != null) {
      final DateTime date = DateTime.parse(promoSince!);
      final double diffInDays = date.difference(DateTime.now()).inHours / 24;
      return diffInDays;
    }
    return 0;
  }

  @override
  List<Object?> get props =>
      super.props +
      <Object?>[title, imageUrl, backgroundUrl, promoSince, promoUntil];
}

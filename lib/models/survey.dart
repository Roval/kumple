class Survey {
  Survey.fromJson(final Map<String, dynamic> json)
      : id = json['id'] as String,
        minGames = json['minGames'] as int,
        type = _mapSurveyType(json['type'] as String),
        question =
            (json['question'] as Map<dynamic, dynamic>).cast<String, String>(),
        data = (json['data'] as Map<dynamic, dynamic>).cast<String, String?>(),
        header =
            (json['header'] as Map<dynamic, dynamic>?)?.cast<String, String>();

  final String id;
  final SurveyType type;
  final Map<String, String> question;
  final Map<String, String?> data;
  final Map<String, String>? header;
  final int minGames;
}

enum SurveyType { orOr }

SurveyType _mapSurveyType(final String type) {
  switch (type.toLowerCase()) {
    case 'or_or':
      return SurveyType.orOr;
    default:
      throw Exception('unrecognized type of survey $type!');
  }
}

import 'package:flutter/material.dart';

extension ScrollEdgeDetectorExtension on ScrollController {
  bool get isAtTop => offset == 0;
  bool get isAtBottom => position.atEdge && offset != 0;
}

import 'package:auto_route/auto_route.dart';

import '../di/dependencies.dart';
import '../routing/router.gr.dart';

extension AutoRouterLastMainPage on AppRouter {
  RouteData get lastMainRoute => getIt<AppRouter>().stackData.lastWhere(
        (final RouteData path) {
          return path.name != InfoAlert.name;
        },
      );
}

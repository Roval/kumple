const int $nbsp = 0x00A0;

extension StringNonBrakingExtension on String {
  String get breaking => replaceAll(String.fromCharCode($nbsp), ' ');
  String get nonBreaking => replaceAll(' ', String.fromCharCode($nbsp));
}

extension StringTags on String {
  String get sanitizeTags =>
      replaceAll('<', '').replaceAll('>', '').replaceAll(r'\n', '\n');

  List<String> get tags {
    final Iterable<Match> matches =
        RegExp(r"<(\p{L}|\s|\d|\.|-|'|\+|!|,|\?)+>", unicode: true)
            .allMatches(this);
    return matches
        .map((final Match m) => m[0]!)
        .map((final String e) => e.sanitizeTags.toUpperCase())
        .toList();
  }
}

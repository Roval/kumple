import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:image/image.dart' as img;

extension FrameSizeExtension on GlobalKey {
  Size get size => (currentContext!.findRenderObject()! as RenderBox).size;
}

extension CapturePng on GlobalKey {
  Future<Uint8List?> capturePng() async {
    try {
      final RenderRepaintBoundary boundary =
          // ignore: cast_nullable_to_non_nullable
          currentContext!.findRenderObject() as RenderRepaintBoundary;
      final ui.Image image = await boundary.toImage(pixelRatio: 3.0);
      final ByteData? byteData =
          await image.toByteData(format: ui.ImageByteFormat.png);
      final Uint8List? pngBytes = byteData?.buffer.asUint8List();
      if (pngBytes == null) {
        return null;
      }
      final img.Image? imageToConvert = img.decodeImage(pngBytes);
      if (imageToConvert == null) {
        return null;
      }
      final img.Image resized = img.copyResize(imageToConvert, width: 500);

      return Uint8List.fromList(img.encodeJpg(resized, quality: 90));
    } catch (_) {
      return null;
    }
  }
}

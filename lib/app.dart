import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:wakelock/wakelock.dart';

import 'cubits/maintenence_cubit.dart';
import 'cubits/maintenence_state.dart';
import 'di/dependencies.dart';
import 'routing/router.gr.dart';
import 'utils/app_theme.dart';
import 'utils/logger.dart';
import 'utils/route_aware_observer.dart';
import 'widget/overlays/loading_overlay.dart';
import 'widget/overlays/notifications_overlay.dart';
import 'widget/overlays/store_actions_overlay.dart';

// TODO: Use simple animation lib to support all animations across the app

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(final BuildContext context) {
    SystemChrome.setPreferredOrientations(<DeviceOrientation>[
      DeviceOrientation.portraitUp,
    ]);
    Wakelock.enable();
    final AppRouter router = getIt<AppRouter>();

    return BlocListener<MaintenenceCubit, MaintenenceState>(
      listener: (final BuildContext context, final MaintenenceState state) {
        if (state is NoMaintenenceState) {
          logger.i('maintenance disabled');
          if (router.currentPath == '/maintenance') {
            // Restore the app
            router.replace(const Splash());
          }
        } else {
          logger.i('maintenance enabled!');
          getIt<AppRouter>().replaceAll(<PageRouteInfo>[const Maintenance()]);
        }
      },
      child: GestureDetector(
        onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
        child: MaterialApp.router(
          localizationsDelegates: context.localizationDelegates,
          supportedLocales: context.supportedLocales,
          locale: context.locale,
          debugShowCheckedModeBanner: false,
          title: 'Buddies',
          scrollBehavior: ScrollWithoutBounce(),
          theme: appTheme,
          builder: (final BuildContext context, final Widget? child) =>
              StoreActionsOverlay(
            child: NotificationsOverlay(
              child: LoadingOverlay(
                child: child ?? Container(),
              ),
            ),
          ),
          routerDelegate: AutoRouterDelegate(
            router,
            navigatorObservers: () => <NavigatorObserver>[routeObserver],
          ),
          routeInformationParser: router.defaultRouteParser(),
        ),
      ),
    );
  }
}

class ScrollWithoutBounce extends ScrollBehavior {
  @override
  Widget buildOverscrollIndicator(final BuildContext context,
      final Widget child, final ScrollableDetails details) {
    return child;
  }

  @override
  ScrollPhysics getScrollPhysics(final BuildContext context) =>
      const ClampingScrollPhysics();
}

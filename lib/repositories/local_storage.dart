import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';

import '../utils/logger.dart';

// ignore: avoid_classes_with_only_static_members
class LocalStorage {
  static const String _kUsernameKey = 'username';
  static const String _kHardcoreWarning = 'hardcoreWarning';
  static const String _kBoughtProducts = 'purchases';
  static const String _kRatePropmpted = 'ratePrompted';
  static const String _kHowManyGamesFinished = 'howManyFinished';
  static const String _kHowManyGamesResumed = 'howManyResumed';
  static const String _kHowManyCreated = 'howManyCreated';
  static const String _kHowManyJoined = 'howManyJoined';
  static const String _kSurveysDone = 'surveysDone';
  static const String _kLastRatePromptDate = 'rateLastDate';
  static const String _kLastSeenChannel = 'channelLastSeen';
  static const String _kSoundsOn = 'soundsOn';
  static const String _kAvatarMigration = 'avatarMigration';

  /// Used for Analytics - if the app is checking launch link and this is first time
  /// then set user source as dynamic link
  static const String _kFirstTimeCheckingInitialLink = 'initialLinkFirstTime';

  static final StreamController<bool> _isSoundOnController =
      StreamController<bool>();
  static final Stream<bool> isSoundOnStream =
      _isSoundOnController.stream.asBroadcastStream();

  static Future<void> clear() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
  }

  static Future<void> saveUsername(final String name) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(LocalStorage._kUsernameKey, name);
  }

  static Future<String?> getUsername() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(LocalStorage._kUsernameKey);
  }

  static Future<bool> isAppRated() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(LocalStorage._kRatePropmpted) ?? false;
  }

  static Future<void> markAsRated() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(LocalStorage._kRatePropmpted, true);
  }

  static Future<bool> isSoundOn() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(LocalStorage._kSoundsOn) ?? true;
  }

  static Future<void> setSoundOn(final bool isOn) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(LocalStorage._kSoundsOn, isOn);
    _isSoundOnController.add(isOn);
  }

  static Future<void> markPrompted() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final int timestamp = DateTime.now().millisecondsSinceEpoch;
    await prefs.setInt(LocalStorage._kLastRatePromptDate, timestamp);
  }

  static Future<int?> getChannelLastSeen(final String channel) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt('${LocalStorage._kLastSeenChannel}$channel');
  }

  static Future<void> markChannelAsSeen(
      final String channel, final int time) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt('${LocalStorage._kLastSeenChannel}$channel', time);
    logger.i('marked channel $channel as seen');
  }

  static Future<int?> getLastPromptTimestamp() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(LocalStorage._kLastRatePromptDate);
  }

  static Future<void> markPlayAgainSelected() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final int currentCount = await getHowManyGamesPlayedAgain();
    await prefs.setInt(LocalStorage._kHowManyGamesResumed, currentCount + 1);
    logger.i('marked game as played again');
  }

  static Future<int> getHowManyGamesPlayedAgain() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(LocalStorage._kHowManyGamesResumed) ?? 0;
  }

  static Future<void> markGameCreated() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final int currentCount = await getHowManyGamesCreated();
    await prefs.setInt(LocalStorage._kHowManyCreated, currentCount + 1);
    logger.i('marked game as created');
  }

  static Future<int> getHowManyGamesCreated() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(LocalStorage._kHowManyCreated) ?? 0;
  }

  static Future<void> markGameJoined() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final int currentCount = await getHowManyGamesJoined();
    await prefs.setInt(LocalStorage._kHowManyJoined, currentCount + 1);
    logger.i('marked game as joined');
  }

  static Future<int> getHowManyGamesJoined() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(LocalStorage._kHowManyJoined) ?? 0;
  }

  static Future<void> markGameFinished() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final int currentCount = await getHowManyGamesFinished();
    await prefs.setInt(LocalStorage._kHowManyGamesFinished, currentCount + 1);
    logger.i('marked game as finished');
  }

  static Future<int> getHowManyGamesFinished() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(LocalStorage._kHowManyGamesFinished) ?? 0;
  }

  static Future<bool> isUserWarnedAboutHardcore() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(LocalStorage._kHardcoreWarning) ?? false;
  }

  static Future<void> markAsWarnedAboutHardcore() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(LocalStorage._kHardcoreWarning, true);
  }

  static Future<void> saveBoughtProduct({required final String id}) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final List<String> ids =
        prefs.getStringList(LocalStorage._kBoughtProducts) ?? <String>[];
    ids.add(id);
    await prefs.setStringList(
        LocalStorage._kBoughtProducts, ids.toSet().toList());
  }

  static Future<List<String>> getBoughtProducts() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList(LocalStorage._kBoughtProducts) ?? <String>[];
  }

  static Future<void> saveSurveyDone({required final String id}) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final List<String> ids =
        prefs.getStringList(LocalStorage._kSurveysDone) ?? <String>[];
    ids.add(id);
    await prefs.setStringList(LocalStorage._kSurveysDone, ids.toSet().toList());
  }

  static Future<List<String>> getSurveysDone() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getStringList(LocalStorage._kSurveysDone) ?? <String>[];
  }

  static Future<void> markAvatarMigrated() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(LocalStorage._kAvatarMigration, true);
  }

  static Future<bool> getAvatarMigrated() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(LocalStorage._kAvatarMigration) ?? false;
  }

  static Future<void> markInitialLinkCheckedDuringFirstLaunch() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setBool(LocalStorage._kFirstTimeCheckingInitialLink, false);
  }

  static Future<bool> isUserCheckingInitialLinkDuringFirstLaunch() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(LocalStorage._kFirstTimeCheckingInitialLink) ?? true;
  }
}

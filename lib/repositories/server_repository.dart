import 'dart:async';
import 'dart:convert';

import 'package:injectable/injectable.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/status.dart' as status;

import '../contractors/impl_server_repository.dart';
import '../entities/answer_request.dart';
import '../entities/base_request.dart';
import '../entities/create_game_request.dart';
import '../entities/fetch_games_request.dart';
import '../entities/player_continue_request.dart';
import '../entities/player_ready_request.dart';
import '../entities/reconfigure_chat_request.dart';
import '../environment_config.dart';
import '../models/event.dart';
import '../utils/logger.dart';

@singleton
class ServerRepository implements IServerRepository {
  ServerRepository() {
    _eventsStream = _eventsStreamController.stream.asBroadcastStream();
    _connect();
  }

  final String _logTag = '[socket]';

  final StreamController<Event> _eventsStreamController =
      StreamController<Event>();

  /// All cubits will listen to this event stream. It will give them the events
  /// and will hide the connection state.
  /// They will not know if reporistory is connected or not directly.
  /// Of course we will still send some events about it but this stream will
  /// never have to be handled by cubits now, they just connect and listen to it
  late Stream<Event> _eventsStream;

  /// Currently connected socket.
  late IOWebSocketChannel _channel;

  /// Current subscription which is listening for socket messages.
  /// If we receive onDone in stream we reset it.
  StreamSubscription<Event>? _connectionSubscription;

  void _connect() {
    logger.i('$_logTag socket connect');
    _connectionSubscription?.cancel();
    _channel = IOWebSocketChannel.connect(
      Uri.parse(EnvironmentConfig.serverUrl),
      pingInterval: const Duration(seconds: 5),
      connectTimeout: const Duration(seconds: 10),
    );
    _connectionSubscription = _channel.stream
        .cast<String>()
        .map((final String msg) {
          logger.i(msg);
          return (json.decode(msg) as Map<dynamic, dynamic>)
              .cast<String, dynamic>();
        })
        .where((final Map<String, dynamic> dict) => dict['type'] != 'hello')
        .map((final Map<String, dynamic> dict) {
          if (dict['type'] == 'games') {
            return FetchGamesEvent.fromJson(dict);
          } else if (dict['type'] == 'usage') {
            return FetchUsageEvent.fromJson(dict);
          } else if (dict['type'] == 'error') {
            return ErrorEvent.fromJson(dict);
          } else if (dict['type'] == 'kicked') {
            return KickedEvent.fromJson(dict);
          }
          return GameEvent.fromJson(dict);
        })
        .listen(
          (final Event event) {
            _eventsStreamController.add(event);
          },
          onError: (final Object error) {
            logger.e('$_logTag error with the socket $error');
          },
          onDone: () {
            logger.i('$_logTag socket disconnected');
            _eventsStreamController.add(DisconnectEvent(_channel.closeReason));
            _connectionSubscription = null;
          },
        );
  }

  Future<void> _reconnect() async {
    logger.i('$_logTag socket reconnected');
    _connect();
  }

  @override
  Future<void> closeWithCode(final String closeReason) async {
    logger.i('$_logTag socket close with code $closeReason');
    await _channel.sink.close(status.normalClosure, closeReason);
    logger.i('$_logTag close message sent');
  }

  @override
  Stream<Event> observeState() {
    logger.i('$_logTag start observing server events');
    return _eventsStream;
  }

  @override
  void fetchGames(final FetchGamesRequest request) {
    logger.i('$_logTag fatch games ${request.toJson()}');
    _send(request);
  }

  @override
  void fetchUsage(final FetchUsageRequest request) {
    logger.i('$_logTag fatch usage ${request.toJson()}');
    _send(request);
  }

  @override
  void createGame(final CreateGameRequest request) {
    logger.i('$_logTag create room ${request.toJson()}');
    _send(request);
  }

  @override
  void updateGame(final UpdateGameRequest request) {
    logger.i('$_logTag update room ${request.toJson()}');
    _send(request);
  }

  @override
  void leaveGame(final LeaveGameRequest request) {
    logger.i('$_logTag leave room ${request.toJson()}');
    _send(request);
  }

  @override
  void exitGame(final ExitGameRequest request) {
    logger.i('$_logTag exit room ${request.toJson()}');
    _send(request);
  }

  @override
  void removePlayer(final RemovePlayerRequest request) {
    logger.i('$_logTag remove player ${request.toJson()}');
    _send(request);
  }

  @override
  void joinGame(final JoinGameRequest request) {
    logger.i('$_logTag join room ${request.toJson()}');
    _send(request);
  }

  @override
  void reportReady() {
    logger.i('$_logTag report ready');
    _send(PlayerReadyRequest());
  }

  @override
  void reportContinue() {
    logger.i('$_logTag report ready');
    _send(PlayerContinueRequest());
  }

  @override
  void reconfigureChat() {
    logger.i('$_logTag reconfigure chat');
    _send(ReconfigureChatRequest());
  }

  @override
  void sendAnswer(final AnswerQuestionRequest request) {
    logger.i('$_logTag sending answer ${request.toJson()}');
    _send(request);
  }

  Future<void> _send(final BaseRequest request) async {
    if (_connectionSubscription == null) {
      await _reconnect();
    }
    _channel.sink.add(request.toJson());
  }
}

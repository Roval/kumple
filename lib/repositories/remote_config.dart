// ignore: avoid_classes_with_only_static_members
import 'dart:convert';

import 'package:collection/collection.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_remote_config/firebase_remote_config.dart';

import '../extensions/list_extensions.dart';
import '../gen/translations.gen.dart';
import '../models/store.dart';
import '../models/survey.dart';
import '../utils/locale_utils.dart';
import '../utils/logger.dart';

// ignore: avoid_classes_with_only_static_members
class RemoteConfigHelper {
  static StoreDefinition? lastFetchedStoreResponse;
  static List<Map<String, String>> friendlyHints = <Map<String, String>>[];
  static FirebaseRemoteConfig get remoteConfig => FirebaseRemoteConfig.instance;

  static Future<void> configureRemoteConfigForDevelopment() async {
    await remoteConfig.setConfigSettings(RemoteConfigSettings(
      fetchTimeout: const Duration(minutes: 1),
      minimumFetchInterval: const Duration(minutes: 5),
    ));
  }

  static Future<StoreDefinition> storeContent() async {
    await remoteConfig.fetchAndActivate();
    final Map<String, dynamic> json =
        (jsonDecode(remoteConfig.getValue('store').asString())
                as Map<dynamic, dynamic>)
            .cast();
    final StoreDefinition response = StoreDefinition.fromJson(json);
    lastFetchedStoreResponse = response;
    return response;
  }

  static Future<void> loadFriendlyHints() async {
    await remoteConfig.fetchAndActivate();
    friendlyHints =
        (jsonDecode(remoteConfig.getValue('friendlyHints').asString())
                as List<dynamic>)
            .cast<Map<String, dynamic>>()
            .toList()
            .map((final Map<String, dynamic> e) => e.cast<String, String>())
            .toList();
  }

  static Map<String, Map<String, String>> get offersHints {
    try {
      final Map<String, Map<String, String>> hints =
          (jsonDecode(remoteConfig.getValue('offerHints').asString())
                  as Map<dynamic, dynamic>)
              .cast<String, Map<dynamic, dynamic>>()
              .map((final String key, final Map<dynamic, dynamic> value) =>
                  MapEntry<String, Map<String, String>>(
                      key, value.cast<String, String>()));
      return hints;
    } on FormatException {
      return <String, Map<String, String>>{};
    }
  }

  static String get randomHint {
    // Default hints has to be consructed here in order to have translations,
    // cannot be inserted into default remote config (Paweł Sałata error)
    final List<Map<String, String>> defaultHints = <Map<String, String>>[
      <String, String>{
        'pl': LocaleKeys.gameboards_awaitingOthers_awaitHint.tr(),
        'en': LocaleKeys.gameboards_awaitingOthers_awaitHint.tr(),
        'uk': LocaleKeys.gameboards_awaitingOthers_awaitHint.tr(),
        'de': LocaleKeys.gameboards_awaitingOthers_awaitHint.tr()
      }
    ];
    final Map<String, String> hint =
        (friendlyHints.isEmpty ? defaultHints : friendlyHints).randomItem();
    return localisedHint(hint);
  }

  static bool get isRatingEnabled {
    return remoteConfig.getValue('rateEnabled').asBool();
  }

  static bool get areAdsEnabled {
    return remoteConfig.getValue('adsEnabled').asBool();
  }

  static bool get isChatEnabled {
    return remoteConfig.getValue('chatEnabled').asBool();
  }

  static bool get isUsageAlertsEnabled {
    return remoteConfig.getValue('usageAlertsEnabled').asBool();
  }

  static bool get isUsageInfoBoxEnabled {
    return remoteConfig.getValue('usageInfoBoxEnabled').asBool();
  }

  static String? get discordInviteUrl {
    final String value = remoteConfig.getValue('discordInviteUrl').asString();
    return value.isEmpty ? null : value;
  }

  static bool get isFullUnlockVisible {
    return remoteConfig.getValue('fullUnlockVisible').asBool();
  }

  static int get maxPlayers {
    return remoteConfig.getValue('maxPlayers').asInt();
  }

  static int get usageWarningMinQuestions {
    return remoteConfig.getValue('usageWarningMin').asInt();
  }

  static List<Survey> get surveys {
    try {
      final List<Survey> surveys =
          (jsonDecode(remoteConfig.getValue('surveys').asString())
                  as List<dynamic>)
              .cast<Map<String, dynamic>>()
              .map((final Map<String, dynamic> e) {
                try {
                  return Survey.fromJson(e.cast());
                } catch (e) {
                  logger.i('unable to decode $e');
                  return null;
                }
              })
              .whereNotNull()
              .sorted((final Survey a, final Survey b) => a.id.compareTo(b.id))
              .toList();
      return surveys;
    } on FormatException {
      return <Survey>[];
    }
  }

  static int get offersGamesBetween {
    return remoteConfig.getValue('offerGamesBetween').asInt();
  }

  static int get rateMinGamesCount {
    return remoteConfig.getValue('rateMinGames').asInt();
  }

  static int get rateMinDaysBetween {
    return remoteConfig.getValue('rateDaysBetween').asInt();
  }

  static String get installPromoIap {
    return remoteConfig.getValue('startPromoIap').asString();
  }

  static String get sharePromoIap {
    return remoteConfig.getValue('sharePromoIap').asString();
  }

  static void setDefaults() {
    remoteConfig.setDefaults(<String, dynamic>{
      'maxPlayers': 12,
      'startPromoIap': 'com.kumple.iap.muzyka',
      'sharePromoIap': 'com.kumple.iap.sport',
      'friendlyHints': jsonEncode(<dynamic>[]),
      'rateDaysBetween': 7,
      'offerGamesBetween': 1,
      'offerHints': jsonEncode(<String, Map<String, String>>{}),
      'rateMinGames': 3,
      'fullUnlockVisible': false,
      'rateEnabled': true,
      'chatEnabled': true,
      'adsEnabled': false,
      'discordInviteUrl': '',
      'usageWarningMin': 20,
      'usageInfoBoxEnabled': true,
      'usageAlertsEnabled': true
    });
  }
}

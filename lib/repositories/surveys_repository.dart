import '../models/survey.dart';
import '../utils/logger.dart';
import 'local_storage.dart';
import 'remote_config.dart';

Future<Survey?> remainingSurvey() async {
  final int count = await LocalStorage.getHowManyGamesFinished();
  logger.i('user finished $count games');

  final List<String> doneSurveys = await LocalStorage.getSurveysDone();
  final List<Survey> availableSurveys = RemoteConfigHelper.surveys
    ..removeWhere((final Survey element) =>
        // Remove if seen or user should not see it yet
        doneSurveys.contains(element.id) || count < element.minGames);

  if (availableSurveys.isEmpty) {
    return null;
  }
  logger.i('user can see surveys: $availableSurveys');

  return availableSurveys.first;
}

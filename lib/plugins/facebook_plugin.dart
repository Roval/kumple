// ignore_for_file: avoid_classes_with_only_static_members
import 'dart:io';

import 'package:flutter/services.dart';

import '../utils/logger.dart';

enum FacebookShareResponse { error, ok, cancel }

class FacebookPlugin {
  static const MethodChannel _channel = MethodChannel('flutter_facebook');

  static Future<void> logPurchase({
    required final double amount,
    required final String currency,
    final Map<String, dynamic>? parameters,
  }) {
    final Map<String, dynamic> args = <String, dynamic>{
      'amount': amount,
      'currency': currency,
      'parameters': parameters,
    };
    return _channel.invokeMethod<void>('logPurchase', _filterOutNulls(args));
  }

  static Future<void> updateAttStatus({required final bool isEnabled}) async {
    if (Platform.isIOS) {
      logger.i('update Facebook ATT status: $isEnabled');
      await _channel.invokeMethod('updateAttStatus', <String, dynamic>{
        'isEnabled': isEnabled,
      });
    } else {
      return;
    }
  }

  /// Log an app event with the specified [name] and the supplied [parameters] value.
  static Future<void> logEvent({required final String name}) {
    final Map<String, dynamic> args = <String, dynamic>{
      'name': name,
    };

    return _channel.invokeMethod<void>('logEvent', args);
  }

  /// Creates a new map containing all of the key/value pairs from [parameters]
  /// except those whose value is `null`.
  static Map<String, dynamic> _filterOutNulls(
      final Map<String, dynamic> parameters) {
    final Map<String, dynamic> filtered = <String, dynamic>{};
    parameters.forEach((final String key, final dynamic value) {
      if (value != null) {
        filtered[key] = value;
      }
    });
    return filtered;
  }
}

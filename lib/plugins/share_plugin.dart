import 'package:flutter/services.dart';

// ignore: avoid_classes_with_only_static_members
class SharePlugin {
  static const MethodChannel _channel = MethodChannel('flutter_share');

  static Future<void> instagram({required final String text}) {
    final Map<String, dynamic> args = <String, dynamic>{
      'text': text,
    };

    return _channel.invokeMethod<void>('instagram', args);
  }

  static Future<void> sms({required final String text}) {
    final Map<String, dynamic> args = <String, dynamic>{
      'text': text,
    };

    return _channel.invokeMethod<void>('sms', args);
  }
}

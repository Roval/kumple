// ignore_for_file: avoid_classes_with_only_static_members
import 'package:flutter/services.dart';

import '../utils/logger.dart';

class NotificationsPlugin {
  static const MethodChannel _channel = MethodChannel('flutter_notification');

  static Future<bool> removeNotificationsWithTag(final String tag) async {
    logger.i('removing notifications with tag $tag');
    try {
      final String? message =
          await _channel.invokeMethod('remove', <String, dynamic>{'tag': tag});
      logger.i('removed notifications for tag $tag');
      return message == 'ok';
    } on PlatformException catch (e) {
      logger.e('unable to remove notifications with tag $tag, error: $e');
      return false;
    }
  }
}

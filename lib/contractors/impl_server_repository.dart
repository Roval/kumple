import '../entities/answer_request.dart';
import '../entities/create_game_request.dart';
import '../entities/fetch_games_request.dart';
import '../models/event.dart';

abstract class IServerRepository {
  Future<void> closeWithCode(final String closeReason);

  Stream<Event> observeState();
  void fetchGames(final FetchGamesRequest request);
  void fetchUsage(final FetchUsageRequest request);
  void createGame(final CreateGameRequest request);
  void joinGame(final JoinGameRequest request);
  void updateGame(final UpdateGameRequest request);
  void leaveGame(final LeaveGameRequest request);
  void exitGame(final ExitGameRequest request);
  void removePlayer(final RemovePlayerRequest request);
  void reportReady();
  void reportContinue();
  void reconfigureChat();
  void sendAnswer(final AnswerQuestionRequest request);
}

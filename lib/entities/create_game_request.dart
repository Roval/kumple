import 'base_request.dart';

class CreateGameRequest extends BaseRequest {
  CreateGameRequest(
    this.user,
    this.iaps,
    this.rounds,
    this.language,
    this.excludedQuestions,
  );

  final UserJoinRequest user;
  final List<String> iaps;
  final int rounds;
  final String language;
  final List<String> excludedQuestions;

  @override
  Map<String, dynamic> toDict() {
    return <String, dynamic>{
      ...super.toDict(),
      'type': 'create',
      'categories': iaps,
      'rounds': rounds,
      'language': language,
      'exclude': excludedQuestions,
      'user': user.toDict(),
    };
  }
}

class RemovePlayerRequest extends BaseRequest {
  RemovePlayerRequest(this.room, this.userId);

  final String room;
  final String userId;

  @override
  Map<String, dynamic> toDict() {
    return <String, dynamic>{
      ...super.toDict(),
      'type': 'remove',
      'room': room,
      'user': userId,
    };
  }
}

class ExitGameRequest extends BaseRequest {
  ExitGameRequest(this.room, this.userId);

  final String room;
  final String userId;

  @override
  Map<String, dynamic> toDict() {
    return <String, dynamic>{
      ...super.toDict(),
      'type': 'exit',
      'room': room,
      'user': userId,
    };
  }
}

class LeaveGameRequest extends BaseRequest {
  LeaveGameRequest(this.room, this.userId);

  final String room;
  final String userId;

  @override
  Map<String, dynamic> toDict() {
    return <String, dynamic>{
      ...super.toDict(),
      'type': 'leave',
      'room': room,
      'user': userId,
    };
  }
}

class UpdateGameRequest extends BaseRequest {
  UpdateGameRequest(
    this.rounds,
    this.categories,
    this.language,
    this.excludedQuestions,
  );

  final int rounds;
  final List<String> categories;
  final String language;
  final List<String> excludedQuestions;

  @override
  Map<String, dynamic> toDict() {
    return <String, dynamic>{
      ...super.toDict(),
      'type': 'config',
      'categories': categories,
      'rounds': rounds,
      'language': language,
      'exclude': excludedQuestions,
    };
  }
}

class JoinGameRequest extends BaseRequest {
  JoinGameRequest(this.room, this.user);

  final String room;
  final UserJoinRequest user;

  @override
  Map<String, dynamic> toDict() {
    return <String, dynamic>{
      ...super.toDict(),
      'type': 'join',
      'room': room,
      'user': user.toDict(),
    };
  }
}

class UserJoinRequest extends BaseRequest {
  UserJoinRequest(this.id, this.name, this.token);

  final String id;
  final String name;
  final String? token;

  @override
  Map<String, dynamic> toDict() {
    return <String, dynamic>{'id': id, 'name': name, 'token': token};
  }
}

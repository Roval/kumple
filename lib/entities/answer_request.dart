import 'base_request.dart';

class AnswerQuestionRequest extends BaseRequest {
  AnswerQuestionRequest(this.questionId, this.answer);

  final String questionId;
  final Object answer;

  @override
  Map<String, dynamic> toDict() {
    return <String, dynamic>{
      ...super.toDict(),
      'type': 'answer',
      'question': questionId,
      'answer': answer,
    };
  }
}

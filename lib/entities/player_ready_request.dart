import 'base_request.dart';

class PlayerReadyRequest extends BaseRequest {
  PlayerReadyRequest();

  @override
  Map<String, dynamic> toDict() {
    return <String, dynamic>{
      ...super.toDict(),
      'type': 'ready',
    };
  }
}

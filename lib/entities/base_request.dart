import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:package_info_plus/package_info_plus.dart';

abstract class BaseRequest {
  static String version = '2.10';

  Map<String, dynamic> toDict() {
    return <String, dynamic>{'version': version};
  }

  String? toJson() {
    return json.encode(toDict());
  }

  static Future<void> loadAppVersion() async {
    if (kDebugMode) {
      return;
    }
    final PackageInfo info = await PackageInfo.fromPlatform();
    version = info.version;
  }
}

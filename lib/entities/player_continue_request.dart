import 'base_request.dart';

class PlayerContinueRequest extends BaseRequest {
  PlayerContinueRequest();

  @override
  Map<String, dynamic> toDict() {
    return <String, dynamic>{
      ...super.toDict(),
      'type': 'continue',
    };
  }
}

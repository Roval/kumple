import 'base_request.dart';

class FetchGamesRequest extends BaseRequest {
  FetchGamesRequest(this.user);

  final String user;

  @override
  Map<String, dynamic> toDict() {
    return <String, dynamic>{
      ...super.toDict(),
      'type': 'games',
      'user': user,
    };
  }
}

class FetchUsageRequest extends BaseRequest {
  FetchUsageRequest(this.user, this.categories);

  final String user;
  final List<String> categories;

  @override
  Map<String, dynamic> toDict() {
    return <String, dynamic>{
      ...super.toDict(),
      'type': 'usage',
      'user': user,
      'categories': categories,
    };
  }
}

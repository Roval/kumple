import 'base_request.dart';

class ReconfigureChatRequest extends BaseRequest {
  ReconfigureChatRequest();

  @override
  Map<String, dynamic> toDict() {
    return <String, dynamic>{
      ...super.toDict(),
      'type': 'fix_chat',
    };
  }
}

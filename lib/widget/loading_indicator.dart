import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../gen/assets.gen.dart';

class LoadingIndicator extends StatelessWidget {
  const LoadingIndicator({
    super.key,
    this.dimension = 50,
  });

  final double dimension;

  @override
  Widget build(final BuildContext context) {
    return Center(
        child: Lottie.asset(
      Assets.animations.loader,
      height: dimension,
      width: dimension,
    ));
  }
}

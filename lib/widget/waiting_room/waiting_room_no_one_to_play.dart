import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vector_graphics/vector_graphics.dart';

import '../../gen/assets.gen.dart';
import '../../gen/colors.gen.dart';
import '../../gen/translations.gen.dart';
import '../animated_draggable.dart';
import '../default_container.dart';
import '../game_button.dart';
import '../modal_container.dart';

class WaitingRoomNoOneToPlayWith extends StatefulWidget {
  const WaitingRoomNoOneToPlayWith({
    super.key,
    required this.onShare,
    required this.onDragDown,
  });

  final Function() onDragDown;
  final Function() onShare;

  @override
  State<WaitingRoomNoOneToPlayWith> createState() =>
      _WaitingRoomNoOneToPlayWithState();
}

class _WaitingRoomNoOneToPlayWithState
    extends State<WaitingRoomNoOneToPlayWith> {
  final AnimatedDraggableController _draggableController =
      AnimatedDraggableController();

  @override
  void dispose() {
    _draggableController.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return AnimatedDraggable(
      allowCloseOnBackground: true,
      onDraggedDown: widget.onDragDown,
      controller: _draggableController,
      child: ModalContainer(
        padding: EdgeInsets.zero,
        backgroundColor: ColorName.lightYellow,
        child: Column(
          children: <Widget>[
            SizedBox(
              height: 144,
              child: Center(
                child: Padding(
                  padding: const EdgeInsets.only(bottom: 23),
                  child: SvgPicture(
                    AssetBytesLoader(Assets.svg.emoji55Svg),
                    height: 59,
                  ),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
              color: ColorName.secondaryBackground,
              child: Column(
                children: <Widget>[
                  Text(
                    LocaleKeys.waitingRoom_noInviteFriends_title.tr(),
                    textAlign: TextAlign.left,
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                  const SizedBox(height: 16),
                  DefaultContainer(
                    margin: EdgeInsets.zero,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Assets.images.questionIcon.image(color: Colors.black),
                        const SizedBox(width: 16),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                LocaleKeys.waitingRoom_noInviteFriends_infoTitle
                                    .tr(),
                                textAlign: TextAlign.left,
                                style: Theme.of(context).textTheme.titleLarge,
                              ),
                              const SizedBox(height: 4),
                              Text(
                                LocaleKeys
                                    .waitingRoom_noInviteFriends_infoCaption
                                    .tr(),
                                textAlign: TextAlign.left,
                                style: Theme.of(context).textTheme.titleSmall,
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 16),
                  GameButton(
                    title: LocaleKeys.waitingRoom_button_inviteFriends.tr(),
                    onPressed: widget.onShare,
                    margin: EdgeInsets.zero,
                  ),
                ],
              ),
            ),
            Container(
              height: MediaQuery.of(context).viewPadding.bottom,
              color: ColorName.secondaryBackground,
            ),
          ],
        ),
      ),
    );
  }
}

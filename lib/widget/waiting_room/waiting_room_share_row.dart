import 'package:flutter/material.dart';

import '../../di/dependencies.dart';
import '../../gen/assets.gen.dart';
import '../../utils/share_util.dart';
import '../game_button.dart';

class WaitingRoomShareRow extends StatelessWidget {
  const WaitingRoomShareRow({super.key, required this.roomKey});

  final String roomKey;

  @override
  Widget build(final BuildContext context) {
    final List<Widget> elements = getIt<ShareUtil>()
        .possibleApps
        .map((final ShareApp app) => _buttonShareWidget(app, app.image))
        .toList();
    final int count = elements.length - 1;
    for (int i = count; i > 0; i--) {
      elements.insert(i, const Spacer());
    }

    return Column(
      children: <Widget>[
        if (elements.isNotEmpty) ...<Widget>[
          Row(children: <Widget>[...elements]),
          const SizedBox(height: 16),
        ],
      ],
    );
  }

  Widget _buttonShareWidget(final ShareApp app, final AssetGenImage image) {
    return SizedBox(
      width: 50,
      child: GameButton(
        onPressed: () {
          getIt<ShareUtil>().shareToExternalApp(app, roomKey: roomKey);
        },
        style: const GameButtonStyle.whiteWithBorder(),
        image: image,
        margin: EdgeInsets.zero,
      ),
    );
  }
}

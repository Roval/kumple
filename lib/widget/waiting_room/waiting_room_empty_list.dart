import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../gen/assets.gen.dart';
import '../../gen/colors.gen.dart';
import '../../gen/translations.gen.dart';
import '../default_container.dart';
import '../game_button.dart';
import 'waiting_room_share_row.dart';

enum WaitingRoomEmptyListMode { animation, carousel, image }

class WaitingRoomEmptyList extends StatelessWidget {
  const WaitingRoomEmptyList({
    super.key,
    required this.roomKey,
    required this.onNoOneToPlay,
    required this.onScroll,
    required this.onShare,
  });

  final String roomKey;
  final Function() onScroll;
  final Function() onShare;
  final Function() onNoOneToPlay;

  @override
  Widget build(final BuildContext context) {
    return NotificationListener<ScrollUpdateNotification>(
      onNotification: (final ScrollUpdateNotification notification) {
        onScroll();
        return true;
      },
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          DefaultContainer(
            margin: const EdgeInsets.symmetric(vertical: 16, horizontal: 24),
            padding: EdgeInsets.zero,
            child: Column(
              children: <Widget>[
                const _AnimationView(),
                Container(
                  alignment: Alignment.bottomCenter,
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Text(
                        LocaleKeys.waitingRoom_status_notEnoughPlayers2.tr(),
                        textAlign: TextAlign.left,
                        style: Theme.of(context).textTheme.titleLarge,
                      ),
                      const SizedBox(height: 16),
                      WaitingRoomShareRow(roomKey: roomKey),
                      GameButton(
                        margin: EdgeInsets.zero,
                        title: LocaleKeys.waitingRoom_button_inviteFriends.tr(),
                        onPressed: onShare,
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
          GameButton(
            title: LocaleKeys.waitingRoom_button_noInviteFriends.tr(),
            style: const GameButtonStyle.none(),
            onPressed: onNoOneToPlay,
          ),
        ],
      ),
    );
  }
}

class _AnimationView extends StatelessWidget {
  const _AnimationView();

  @override
  Widget build(final BuildContext context) {
    return Container(
      height: 144,
      width: double.infinity,
      color: ColorName.pink,
      child: Lottie.asset(Assets.animations.waiting),
    );
  }
}

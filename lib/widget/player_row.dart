import 'package:flutter/material.dart';

import '../models/event.dart';
import '../utils/camera_util.dart';
import '../utils/element_order.dart';
import 'avatars.dart';
import 'default_container.dart';

class PlayerRowSubtitle {
  PlayerRowSubtitle({required this.text, this.style});

  final String text;
  final TextStyle? style;
}

class PlayerRow extends StatelessWidget {
  const PlayerRow({
    super.key,
    this.backgroundColor = Colors.white,
    required this.player,
    this.order = ElementOrder.onlyOne,
    this.rightChild,
    this.subtitle,
  });

  final Color backgroundColor;
  final Player player;
  final Widget? rightChild;
  final ElementOrder order;
  final PlayerRowSubtitle? subtitle;

  @override
  Widget build(final BuildContext context) {
    return DefaultContainer(
      color: backgroundColor,
      order: order,
      child: Row(
        children: <Widget>[
          Avatar(
            imageUrl: avatarUrl(player: player.id),
          ),
          const SizedBox(width: 16),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  player.name,
                  textAlign: TextAlign.center,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                if (subtitle != null) ...<Widget>[
                  const SizedBox(height: 1),
                  Text(
                    subtitle!.text,
                    textAlign: TextAlign.left,
                    style: subtitle!.style,
                  ),
                ]
              ],
            ),
          ),
          if (rightChild != null) ...<Widget>[
            const SizedBox(width: 16),
            rightChild!
          ],
        ],
      ),
    );
  }
}

//  TODO: Start using it in other places
class PlayerRowV2 extends StatelessWidget {
  const PlayerRowV2({
    super.key,
    this.backgroundColor = Colors.white,
    required this.player,
    this.rightChild,
    this.subtitle,
    this.expandName = true,
  });

  final Color backgroundColor;
  final Player player;
  final Widget? rightChild;
  final PlayerRowSubtitle? subtitle;
  final bool expandName;

  @override
  Widget build(final BuildContext context) {
    return DefaultContainer(
      color: backgroundColor,
      margin: const EdgeInsets.symmetric(vertical: 6, horizontal: 24),
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: Row(
        children: <Widget>[
          const SizedBox(width: 16),
          Avatar(imageUrl: avatarUrl(player: player.id)),
          const SizedBox(width: 16),
          _nameWidget(context),
          const SizedBox(width: 16),
          if (rightChild != null) ...<Widget>[rightChild!],
        ],
      ),
    );
  }

  Widget _nameWidget(final BuildContext context) {
    final Column child = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          player.name,
          textAlign: TextAlign.center,
          maxLines: 1,
          overflow: TextOverflow.ellipsis,
          style: Theme.of(context).textTheme.titleLarge,
        ),
        if (subtitle != null) ...<Widget>[
          const SizedBox(height: 2),
          Text(
            subtitle!.text,
            textAlign: TextAlign.left,
            style: subtitle!.style,
          ),
        ]
      ],
    );
    return expandName ? Expanded(child: child) : child;
  }
}

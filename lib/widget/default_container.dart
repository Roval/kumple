import 'package:flutter/material.dart';

import '../utils/element_order.dart';

// ignore: avoid_classes_with_only_static_members
class DefaultContainerConfig {
  static const double horizontalMargin = 24;
  static const double horizontalPadding = 16;
  static const double verticalPadding = 16;
}

class DefaultContainer extends StatelessWidget {
  const DefaultContainer({
    super.key,
    required this.child,
    this.order = ElementOrder.onlyOne,
    this.margin,
    this.padding,
    this.color = Colors.white,
  });

  final Widget child;
  final ElementOrder order;
  final EdgeInsets? margin;
  final Color color;
  final EdgeInsets? padding;

  double get _offsetBasedOnOrder {
    return order == ElementOrder.onlyOneWithBorder ? 2 : 0;
  }

  @override
  Widget build(final BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: <Widget>[
        if (order == ElementOrder.middle || order == ElementOrder.first)
          Positioned(
            // Fix for 1px gap issue
            left: 0,
            right: 0,
            bottom: -1,
            child: Container(
              margin: margin ??
                  const EdgeInsets.symmetric(
                    horizontal: DefaultContainerConfig.horizontalMargin,
                  ),
              color: Colors.white,
              height: 2,
            ),
          ),
        Container(
          clipBehavior: Clip.antiAlias,
          width: double.infinity,
          margin: margin ??
              const EdgeInsets.symmetric(
                horizontal: DefaultContainerConfig.horizontalMargin,
              ),
          decoration: BoxDecoration(
            color: color,
            borderRadius: radius(order),
            border: order == ElementOrder.onlyOneWithBorder
                ? Border.all(width: 2)
                : null,
          ),
          padding: padding ??
              EdgeInsets.symmetric(
                horizontal: DefaultContainerConfig.horizontalPadding -
                    _offsetBasedOnOrder,
                vertical: DefaultContainerConfig.verticalPadding -
                    _offsetBasedOnOrder,
              ),
          child: child,
        ),
      ],
    );
  }
}

import 'package:flutter/material.dart';

import '../gen/colors.gen.dart';
import 'single_line.dart';

class ScrollWithLine extends StatefulWidget {
  const ScrollWithLine({super.key, required this.child, this.onScroll});

  final Widget child;
  final Function()? onScroll;

  @override
  State<ScrollWithLine> createState() => _ScrollWithLineState();
}

class _ScrollWithLineState extends State<ScrollWithLine> {
  bool _scrollOnTop = true;

  @override
  Widget build(final BuildContext context) {
    return Stack(
      children: <Widget>[
        NotificationListener<ScrollUpdateNotification>(
          onNotification: (final ScrollUpdateNotification notification) {
            setState(() {
              _scrollOnTop = notification.metrics.pixels <= 0;
            });
            widget.onScroll?.call();
            return true;
          },
          child: widget.child,
        ),
        SingleLine(
          color: _scrollOnTop ? Colors.transparent : ColorName.lightGrey,
        ),
      ],
    );
  }
}

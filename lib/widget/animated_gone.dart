import 'package:flutter/material.dart';

class AnimatedVisibility extends StatelessWidget {
  const AnimatedVisibility({
    super.key,
    required this.child,
    required this.visible,
  });

  final Widget child;
  final bool visible;

  @override
  Widget build(final BuildContext context) {
    return AnimatedOpacity(
      opacity: visible ? 1 : 0,
      duration: const Duration(milliseconds: 400),
      child: AnimatedSize(
        curve: Curves.easeOut,
        duration: const Duration(milliseconds: 400),
        child: SizedBox(
          height: visible ? null : 0,
          child: child,
        ),
      ),
    );
  }
}

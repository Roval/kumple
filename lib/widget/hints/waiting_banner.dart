import 'dart:math';

import 'package:flutter/material.dart';

import '../../cubits/store_cubit.dart';
import '../../cubits/store_state.dart';
import '../../di/dependencies.dart';
import '../../models/store.dart';
import '../../models/survey.dart';
import '../../repositories/remote_config.dart';
import '../../repositories/surveys_repository.dart';
import '../../utils/prompters/offer_prompter.dart';
import 'bundle_hint.dart';
import 'discord_banner.dart';
import 'friendly_hint.dart';
import 'survey_hint.dart';

enum _WaitingBannerMode {
  survey,
  friendly,
  discord,
  bundle;
}

class WaitingBanner extends StatefulWidget {
  const WaitingBanner({super.key});

  @override
  State<WaitingBanner> createState() => _WaitingBannerState();
}

class _WaitingBannerState extends State<WaitingBanner> {
  /// Survey which will displayed in this screen if possible
  Survey? _surveyToDisplay;

  // If we should show bundle that field will be set
  StoreItem? _storeItemToDisplay;

  @override
  void initState() {
    super.initState();

    remainingSurvey().then((final Survey? value) {
      if (value == null) {
        return;
      }
      setState(() {
        _surveyToDisplay = value;
      });
    });

    final FetchStoreState? storeResponse =
        getIt<StoreCubit>().lastFetchedStoreResponse;

    if (storeResponse != null) {
      _storeItemToDisplay = randomNotBoughtBundle(storeResponse);
    }
  }

  @override
  Widget build(final BuildContext context) {
    final _WaitingBannerMode mode;
    if (_surveyToDisplay != null) {
      mode = _WaitingBannerMode.survey;
    } else if (_storeItemToDisplay != null && Random().nextInt(2) == 0) {
      // 50% change
      mode = _WaitingBannerMode.bundle;
    } else {
      mode = RemoteConfigHelper.discordInviteUrl != null &&
              Random().nextInt(2) == 0
          ? _WaitingBannerMode.discord
          : _WaitingBannerMode.friendly;
    }

    switch (mode) {
      case _WaitingBannerMode.survey:
        return SurveyHint(survey: _surveyToDisplay!);
      case _WaitingBannerMode.bundle:
        return BundleHint(item: _storeItemToDisplay!);
      case _WaitingBannerMode.discord:
        return const DiscordBanner();
      case _WaitingBannerMode.friendly:
        return const FriendlyHint();
    }
  }
}

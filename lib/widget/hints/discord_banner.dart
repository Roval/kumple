import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../extensions/color_extensions.dart';
import '../../gen/assets.gen.dart';
import '../../gen/translations.gen.dart';
import '../../repositories/remote_config.dart';
import '../default_container.dart';

class DiscordBanner extends StatelessWidget {
  const DiscordBanner({super.key});

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
      onTap: () {
        FirebaseAnalytics.instance.logEvent(name: 'discord_clicked');
        final String? url = RemoteConfigHelper.discordInviteUrl;
        if (url != null) {
          launchUrl(Uri.parse(url));
        }
      },
      child: DefaultContainer(
        padding: const EdgeInsets.symmetric(horizontal: 22, vertical: 16),
        color: HexColor.fromHex('#5865F2'),
        child: Row(
          children: <Widget>[
            Assets.images.discordIcon.image(),
            const SizedBox(width: 19),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Text(
                    LocaleKeys.discord_title.tr(),
                    textAlign: TextAlign.left,
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge
                        ?.copyWith(color: Colors.white),
                  ),
                  Text(
                    LocaleKeys.discord_body.tr(),
                    textAlign: TextAlign.left,
                    style: Theme.of(context)
                        .textTheme
                        .bodyMedium
                        ?.copyWith(color: Colors.white),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

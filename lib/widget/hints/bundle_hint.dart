import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:styled_text/styled_text.dart';

import '../../gen/translations.gen.dart';
import '../../models/store.dart';
import '../../utils/app_theme.dart';
import '../../utils/locale_utils.dart';
import '../default_container.dart';

class BundleHint extends StatelessWidget {
  const BundleHint({
    super.key,
    required this.item,
  });

  final StoreItem item;

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
      onTap: () =>
          FirebaseAnalytics.instance.logEvent(name: 'bundle_hint_clicked'),
      child: DefaultContainer(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 16),
        color: item.color,
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 6),
              child: CachedNetworkImage(
                height: 38,
                imageUrl: item.imageUrl,
              ),
            ),
            const SizedBox(width: 16),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Text(
                    localisedStoreItemTitle(item),
                    textAlign: TextAlign.left,
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge
                        ?.copyWith(color: Colors.white),
                  ),
                  StyledText(
                    text: LocaleKeys.offer_subtitle.tr(
                      namedArgs: <String, String>{
                        'example': localisedStoreItemHint(item)
                      },
                    ),
                    textAlign: TextAlign.left,
                    style: Theme.of(context)
                        .textTheme
                        .bodyMedium
                        ?.copyWith(color: Colors.white),
                    tags: <String, StyledTextTagBase>{
                      'bold': StyledTextTag(
                          style: appTheme.textTheme.bodyLarge
                              ?.copyWith(color: Colors.white))
                    },
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

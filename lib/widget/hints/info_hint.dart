import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../gen/assets.gen.dart';
import '../default_container.dart';

class InfoHint extends StatelessWidget {
  const InfoHint({
    final Key? key,
    this.image,
    this.animationAsset,
    required this.title,
    required this.subtitle,
    required this.backgroundColor,
  }) : super(key: key);

  final AssetGenImage? image;
  final String? animationAsset;
  final String title;
  final String subtitle;
  final Color backgroundColor;

  @override
  Widget build(final BuildContext context) {
    return DefaultContainer(
      color: backgroundColor,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          if (image != null) image!.image(),
          if (animationAsset != null)
            SizedBox(
              height: 18,
              width: 18,
              child: Lottie.asset(animationAsset!),
            ),
          const SizedBox(width: 12),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text(
                  title,
                  textAlign: TextAlign.left,
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                Text(
                  subtitle,
                  textAlign: TextAlign.left,
                  style: Theme.of(context).textTheme.bodySmall,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

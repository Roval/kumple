import 'dart:io';

import 'package:applovin_max/applovin_max.dart';
import 'package:flutter/material.dart';

import '../../utils/logger.dart';

class AdBanner extends StatefulWidget {
  const AdBanner({
    final Key? key,
    required this.onError,
  }) : super(key: key);

  final Function() onError;

  @override
  State<AdBanner> createState() => _AdBannerState();
}

class _AdBannerState extends State<AdBanner> {
  @override
  Widget build(final BuildContext context) {
    final String adUnitId =
        Platform.isAndroid ? 'c8d6e259282b462f' : '74a10bb12dec4c80';
    return MaxAdView(
        adUnitId: adUnitId,
        adFormat: AdFormat.banner,
        listener: AdViewAdListener(
            onAdLoadedCallback: (final MaxAd ad) {
              logger.i('[ad] ad loaded');
            },
            onAdLoadFailedCallback:
                (final String adUnitId, final MaxError error) {
              logger.e('[ad] unable to load ad with error: $error');
              widget.onError();
            },
            onAdClickedCallback: (final MaxAd ad) {
              logger.i('[ad] ad clicked');
            },
            onAdExpandedCallback: (final MaxAd ad) {},
            onAdCollapsedCallback: (final MaxAd ad) {}));
  }
}

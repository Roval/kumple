import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../gen/colors.gen.dart';
import '../../gen/translations.gen.dart';
import '../../models/survey.dart';
import '../../repositories/local_storage.dart';
import '../../utils/app_theme.dart';
import '../../utils/database_helper.dart';
import '../../utils/locale_utils.dart';
import '../../utils/session_utils.dart';
import '../checkbox.dart';
import '../default_container.dart';
import '../hint_header.dart';

class SurveyHint extends StatefulWidget {
  const SurveyHint({
    super.key,
    required this.survey,
  });

  final Survey survey;

  @override
  State<SurveyHint> createState() => _SurveyHintState();
}

class _SurveyHintState extends State<SurveyHint> {
  bool _isFinished = false;

  @override
  Widget build(final BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        HintHeader(
          _isFinished
              ? LocaleKeys.gameboards_awaitingOthers_surveyDone.tr()
              : localisedSurveyHeader(widget.survey) ??
                  LocaleKeys.gameboards_awaitingOthers_surveyStart.tr(),
          topPadding: 4,
        ),
        const SizedBox(height: 16),
        if (!_isFinished)
          Container(
            margin: const EdgeInsets.symmetric(
              horizontal: DefaultContainerConfig.horizontalMargin,
            ),
            width: double.infinity,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: Defaults.cornerRadius,
            ),
            padding: const EdgeInsets.fromLTRB(16, 12, 16, 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                if (!_isFinished) ...<Widget>[
                  const SizedBox(height: 4),
                  Text(
                    localisedSurveyQuestion(widget.survey),
                    textAlign: TextAlign.left,
                    style: Theme.of(context)
                        .textTheme
                        .titleLarge
                        ?.copyWith(color: ColorName.darkRed),
                  ),
                  _surveyControls(context),
                ] else
                  const SizedBox(height: 12),
              ],
            ),
          ),
      ],
    );
  }

  Widget _surveyControls(final BuildContext context) {
    switch (widget.survey.type) {
      case SurveyType.orOr:
        final List<String> options =
            (localisedSurveyData(widget.survey)!).split('|');
        return Column(
          children: <Widget>[
            ...options.map(
              (final String answer) => GestureDetector(
                onTap: () => _sendAnswer(answer),
                child: Padding(
                  padding: const EdgeInsets.symmetric(vertical: 14),
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          answer,
                          textAlign: TextAlign.left,
                          maxLines: 1,
                          style: Theme.of(context).textTheme.titleLarge,
                        ),
                      ),
                      const GameCheckbox(isSelected: false),
                    ],
                  ),
                ),
              ),
            ),
          ],
        );
    }
  }

  Future<void> _sendAnswer(final String answer) async {
    // Add to database
    DatabaseHelper.addNew(
        path: '/surveys/${widget.survey.id}',
        data: <String, Object>{
          'player': getUserUuid(),
          'gamesPlayed': await LocalStorage.getHowManyGamesFinished(),
          'answer': answer,
          'date': DateTime.now().toUtc().toIso8601String(),
        });

    // Save info that survey was done and hide the content
    LocalStorage.saveSurveyDone(id: widget.survey.id);
    setState(() {
      _isFinished = true;
    });
  }
}

import 'package:flutter/material.dart';

import '../../gen/assets.gen.dart';
import '../../repositories/remote_config.dart';
import '../default_container.dart';

class FriendlyHint extends StatelessWidget {
  const FriendlyHint({
    final Key? key,
  }) : super(key: key);

  @override
  Widget build(final BuildContext context) {
    return DefaultContainer(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Assets.images.infoIcon.image(),
          const SizedBox(width: 12),
          Expanded(
            child: Text(
              RemoteConfigHelper.randomHint,
              textAlign: TextAlign.left,
              style: Theme.of(context).textTheme.titleMedium,
            ),
          ),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';

import '../gen/assets.gen.dart';
import '../gen/colors.gen.dart';
import '../utils/app_theme.dart';
import '../utils/throttler.dart';
import 'highlight_button.dart';

// ignore: avoid_classes_with_only_static_members
class GameButtonViewConfig {
  static const double height = 50;
}

class GameButtonStyle {
  GameButtonStyle({
    required this.backgroundColor,
    required this.pressedBackgroundColor,
    required this.textColor,
    required this.borderColor,
    required this.pressedBorderColor,
  });

  const GameButtonStyle.black()
      : backgroundColor = Colors.black,
        pressedBackgroundColor = const Color.fromRGBO(100, 100, 100, 1),
        textColor = Colors.white,
        pressedBorderColor = null,
        borderColor = null;

  const GameButtonStyle.white()
      : backgroundColor = Colors.white,
        pressedBackgroundColor = const Color.fromRGBO(250, 250, 250, 1),
        textColor = Colors.black,
        pressedBorderColor = null,
        borderColor = null;

  const GameButtonStyle.whiteWithBorder()
      : backgroundColor = Colors.white,
        pressedBackgroundColor = const Color.fromRGBO(250, 250, 250, 1),
        textColor = Colors.black,
        pressedBorderColor = const Color.fromRGBO(100, 100, 100, 1),
        borderColor = Colors.black;

  GameButtonStyle.green()
      : backgroundColor = ColorName.darkGreen,
        pressedBackgroundColor = ColorName.darkGreen.withOpacity(0.7),
        textColor = Colors.white,
        pressedBorderColor = null,
        borderColor = null;

  GameButtonStyle.grey()
      : backgroundColor = ColorName.grey,
        pressedBackgroundColor = ColorName.grey.withOpacity(0.7),
        textColor = Colors.white,
        pressedBorderColor = null,
        borderColor = null;

  GameButtonStyle.outline()
      : backgroundColor = Colors.transparent,
        pressedBackgroundColor = Colors.transparent,
        textColor = Colors.black,
        pressedBorderColor = Colors.black.withOpacity(0.4),
        borderColor = Colors.black;

  const GameButtonStyle.none()
      : backgroundColor = Colors.transparent,
        pressedBackgroundColor = Colors.transparent,
        textColor = Colors.black,
        pressedBorderColor = null,
        borderColor = null;

  const GameButtonStyle.red()
      : backgroundColor = Colors.transparent,
        pressedBackgroundColor = Colors.transparent,
        textColor = ColorName.darkRed,
        pressedBorderColor = null,
        borderColor = null;

  final Color backgroundColor;
  final Color pressedBackgroundColor;
  final Color textColor;
  final Color? borderColor;
  final Color? pressedBorderColor;
}

class GameButton extends StatefulWidget {
  const GameButton({
    final Key? key,
    this.title,
    this.image,
    this.textStyle,
    this.onPressed,
    this.isUserInteractionEnabled = true,
    this.dropShadow = false,
    this.margin = const EdgeInsets.fromLTRB(24, 0, 24, 0),
    this.height = GameButtonViewConfig.height,
    this.style = const GameButtonStyle.black(),
    this.throttleInterval = 1000,
  }) : super(key: key);

  final TextStyle? textStyle;
  final void Function()? onPressed;
  final bool isUserInteractionEnabled;
  final String? title;
  final AssetGenImage? image;
  final EdgeInsets? margin;
  final double? height;
  final bool dropShadow;
  final GameButtonStyle style;
  final int throttleInterval;

  @override
  State<GameButton> createState() => _GameButtonState();
}

class _GameButtonState extends State<GameButton> {
  bool get _isEnabled => widget.onPressed != null;
  late final Throttler _throttler =
      Throttler(throttleGapInMillis: widget.throttleInterval);
  bool _pressed = false;

  Color get _pressedColor {
    return _pressed
        ? widget.style.pressedBackgroundColor
        : widget.style.backgroundColor;
  }

  Color? get _pressedBorderColor {
    return _pressed
        ? widget.style.pressedBorderColor
        : widget.style.borderColor;
  }

  Color? get _pressedTextColor {
    return _pressed
        ? widget.style.textColor.withAlpha(130)
        : widget.style.textColor;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return HighlightButton(
      onTap: _onPressed,
      builder: (final BuildContext context, final bool highlight) {
        _pressed = widget.isUserInteractionEnabled && _isEnabled && highlight;
        return Opacity(
          opacity: _isEnabled ? 1 : 0.1,
          child: Container(
            height: widget.height,
            padding: widget.margin == null
                ? const EdgeInsets.symmetric(horizontal: 12)
                : widget.height == null
                    ? const EdgeInsets.fromLTRB(8, 10, 8, 10)
                    : null,
            decoration: BoxDecoration(
              color: _pressedColor,
              borderRadius: Defaults.cornerRadius,
              border: _pressedBorderColor != null
                  ? Border.all(width: 2, color: _pressedBorderColor!)
                  : Border.all(width: 2, color: _pressedColor),
              boxShadow: <BoxShadow>[
                if (widget.dropShadow)
                  BoxShadow(
                    color: Colors.black.withOpacity(0.15),
                    spreadRadius: 8,
                    blurRadius: 25,
                    offset: const Offset(0, 4), // changes position of shadow
                  ),
              ],
            ),
            margin: widget.margin,
            child: _childWidget(context),
          ),
        );
      },
    );
  }

  void _onPressed() {
    _throttler.run(() => widget.onPressed?.call());
  }

  Widget _childWidget(final BuildContext context) {
    assert(widget.title != null || widget.image != null);
    final String? title = widget.title;
    final AssetGenImage? image = widget.image;
    return Row(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        if (image != null) image.image(),
        if (image != null && title != null) const SizedBox(width: 8),
        if (title != null)
          (widget.margin == null)
              ? _titleWidget(title)
              : Expanded(
                  child: _titleWidget(title),
                ),
      ],
    );
  }

  Widget _titleWidget(final String title) {
    return Text(
      title.toUpperCase(),
      textAlign: TextAlign.center,
      style: (widget.textStyle ?? Theme.of(context).textTheme.headlineMedium)
          ?.copyWith(
        color: _pressedTextColor,
      ),
    );
  }
}

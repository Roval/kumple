import 'dart:math';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../gen/translations.gen.dart';
import '../../models/event.dart';
import '../../utils/app_theme.dart';
import '../../utils/results_utils.dart';
import '../../utils/session_utils.dart';
import '../calculating_size.dart';
import '../game_button.dart';
import '../game_header.dart';
import '../player_round_result.dart';
import '../question_header.dart';
import 'board.dart';

// ignore: avoid_classes_with_only_static_members
class QuestionResultViewConfig {
  static const int animationSlideDownDuration = 600;
  static const int animationResultsDuration = 200;
  static const int animationSecondStageDelay = animationSlideDownDuration - 300;
  static int animationSecondStageFinish = max(animationSlideDownDuration,
      animationSecondStageDelay + animationResultsDuration);
  static int animationPlayerBoxDelay = animationSecondStageFinish + 400;
  static int animationPlayerBoxDuration = 500;
  static int animationSingleStarDuration = 300;
  static double bottomSheetHeight(
      final BuildContext context, final double maxHeight) {
    final double height = MediaQuery.of(context).size.height;
    if (maxHeight == 0) {
      return 0;
    }
    return (maxHeight - 16) /
        (height -
            MediaQuery.of(context).padding.top -
            GameHeaderViewConfig.minHeight);
  }
}

class QuestionResult extends StatefulWidget implements Board {
  const QuestionResult({
    final Key? key,
    required this.color,
    required this.result,
    required this.players,
    required this.onContinue,
    required this.question,
    required this.roomKey,
    required this.isFirstRound,
  }) : super(key: key);

  final QuestionResultData result;
  final Players players;
  final Function() onContinue;
  final String question;
  final bool isFirstRound;
  final String roomKey;
  @override
  final Color color;

  @override
  State<QuestionResult> createState() => _QuestionResultState();
}

class _QuestionResultState extends State<QuestionResult>
    with SingleTickerProviderStateMixin {
  // Player list fields
  late Players _sortedPlayers;
  late List<int> _points;

  // My player fields
  Player get _myPlayer => _sortedPlayers
      .firstWhere((final Player element) => element.id == getUserUuid());
  int get _myPoints => widget.result.winners[getUserUuid()] ?? 0;
  late int? _myPlayerPreviousPosition;
  late int? _myPlayerPreviousPoints;

  // Animation fields
  late Animation<double> _resultsAnimation;
  late AnimationController _resultsAnimationController;
  final GlobalKey _resultsFrameKey = GlobalKey();
  double _resultsViewHeight = 0;

  @override
  void initState() {
    super.initState();

    // Create results fields
    _points = widget.players.map((final Player e) => e.points).toSet().toList()
      ..sort((final int a, final int b) => b.compareTo(a));
    _sortedPlayers = List<Player>.from(widget.players)
      ..sort((final Player a, final Player b) => b.points.compareTo(a.points));

    // Calculate my previous position and points (for animation)
    if (widget.isFirstRound) {
      _myPlayerPreviousPoints = null;
      _myPlayerPreviousPosition = null;
    } else {
      final Map<String, int> previousPoints = <String, int>{
        for (final Player player in widget.players)
          player.id:
              max(0, player.points - (widget.result.winners[player.id] ?? 0))
      };
      final List<int> positions = previousPoints.values.toSet().toList()
        ..sort((final int a, final int b) => b.compareTo(a));
      _myPlayerPreviousPoints = previousPoints[getUserUuid()] ?? 0;
      _myPlayerPreviousPosition =
          positions.indexOf(_myPlayerPreviousPoints!) + 1;
    }

    // Start animations if needed
    _startAnimations();
  }

  @override
  void dispose() {
    _resultsAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    final List<String> winningAnswers = headerBasedOnResult(widget.result,
        players: _sortedPlayers, myPoints: _myPoints);
    final String myAnswer = answerBasedOnResult(widget.result,
        forPlayer: _myPlayer, allPlayers: _sortedPlayers);
    final List<String> answerImages = imagesBasedOnResult(widget.result,
        players: _sortedPlayers, roomKey: widget.roomKey);

    return CalculatingSize(
      keyToCalculate: _resultsFrameKey,
      onSizeCalculated: (final Size size) {
        setState(() {
          _resultsViewHeight = size.height;
        });
      },
      child: Stack(
        children: <Widget>[
          _ColorFloodAnimatedContainer(color: widget.color),
          Column(
            children: <Widget>[
              QuestionHeader(
                color: widget.color,
                question: widget.question,
              ),
              Expanded(
                child: _ZoomFadeContainer(
                  value: _resultsAnimation.value,
                  child: Column(
                    children: <Widget>[
                      PlayerResultBox(
                        winningAnswers: winningAnswers,
                        pointsReceived: _myPoints,
                        player: _myPlayer,
                        myAnswer: myAnswer,
                        positionTo: _points.indexOf(_myPlayer.points) + 1,
                        positionFrom: _myPlayerPreviousPosition,
                        previousPoints: _myPlayerPreviousPoints ?? 0,
                        imageUrls: answerImages,
                      ),
                      Expanded(
                        key: _resultsFrameKey,
                        child: Container(),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          DraggableScrollableSheet(
            initialChildSize: _resultsAnimation.value *
                QuestionResultViewConfig.bottomSheetHeight(
                    context, _resultsViewHeight),
            minChildSize: _resultsAnimation.value *
                QuestionResultViewConfig.bottomSheetHeight(
                    context, _resultsViewHeight),
            maxChildSize: 0.8,
            snap: true,
            builder: (final BuildContext context,
                final ScrollController scrollController) {
              return PlayersResultList(
                  scrollController: scrollController,
                  players: _sortedPlayers,
                  result: widget.result,
                  points: _points);
            },
          ),
          Positioned(
            left: 0,
            right: 0,
            bottom: Defaults.bottomSpacing,
            child: GameButton(
              title: LocaleKeys.gameboards_result_nextQuestion.tr(),
              onPressed: widget.onContinue,
              dropShadow: true,
            ),
          ),
        ],
      ),
    );
  }

  void _startAnimations() {
    _resultsAnimationController = AnimationController(
        duration: const Duration(
          milliseconds: QuestionResultViewConfig.animationResultsDuration,
        ),
        vsync: this);
    _resultsAnimation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(_resultsAnimationController)
      ..addListener(() {
        setState(() {
          // The state that has changed here is the animation object’s value.
        });
      });
    Future<dynamic>.delayed(const Duration(
            milliseconds: QuestionResultViewConfig.animationSecondStageDelay))
        .then((final _) {
      _resultsAnimationController.forward();
    });
  }
}

class _ZoomFadeContainer extends StatelessWidget {
  const _ZoomFadeContainer({
    required this.child,
    required this.value,
  });

  final Widget child;
  final double value;

  @override
  Widget build(final BuildContext context) {
    return Transform.scale(
      scale: 1 + ((1.0 - value) * 0.15),
      child: Opacity(
        opacity: value,
        child: child,
      ),
    );
  }
}

class _ColorFloodAnimatedContainer extends StatefulWidget {
  const _ColorFloodAnimatedContainer({
    final Key? key,
    required this.color,
  }) : super(key: key);

  final Color color;

  @override
  State<_ColorFloodAnimatedContainer> createState() =>
      _ColorFloodAnimatedContainerState();
}

class _ColorFloodAnimatedContainerState
    extends State<_ColorFloodAnimatedContainer> {
  /// Changed to true after a couple of seconds to start animation
  bool _animationStarted = false;

  @override
  void initState() {
    super.initState();

    Future<dynamic>.delayed(const Duration(milliseconds: 100)).then(
      (final _) => setState(() {
        _animationStarted = true;
      }),
    );
  }

  @override
  Widget build(final BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(
          milliseconds: QuestionResultViewConfig.animationSlideDownDuration),
      curve: Curves.fastOutSlowIn,
      width: double.infinity,
      height: _animationStarted ? MediaQuery.of(context).size.height : 0,
      decoration: BoxDecoration(
        color: widget.color,
        borderRadius: const BorderRadius.only(
          bottomLeft: Radius.circular(20),
          bottomRight: Radius.circular(20),
        ),
      ),
    );
  }
}

import 'dart:typed_data';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../cubits/loading_cubit.dart';
import '../../di/dependencies.dart';
import '../../gen/colors.gen.dart';
import '../../gen/translations.gen.dart';
import '../../utils/app_theme.dart';
import '../../utils/camera_util.dart';
import '../../utils/logger.dart';
import '../../utils/session_utils.dart';
import '../draw_container.dart';
import '../game_button.dart';
import '../question_header.dart';
import 'board.dart';

class DrawQuestion extends StatefulWidget implements Board {
  const DrawQuestion({
    final Key? key,
    required this.color,
    required this.roomKey,
    required this.question,
    required this.questionKey,
    required this.onSend,
    required this.backgroundUrl,
  }) : super(key: key);

  final String question;
  final String roomKey;
  final String questionKey;
  final String? backgroundUrl;
  final Function(String) onSend;
  @override
  final Color color;

  @override
  State<DrawQuestion> createState() => _DrawQuestionState();
}

class _DrawQuestionState extends State<DrawQuestion> {
  bool _paintingExists = false;
  final DrawController _drawController = DrawController();

  @override
  void initState() {
    super.initState();
    _drawController.addListener(() {
      setState(() {
        _paintingExists = _drawController.paintingExists;
      });
    });
  }

  @override
  void dispose() {
    _drawController.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        QuestionHeader(
          question: widget.question,
          color: widget.color,
        ),
        const SizedBox(height: 24),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: DrawContainer(
              controller: _drawController,
              backgroundUrl: widget.backgroundUrl,
            ),
          ),
        ),
        Container(
          color: ColorName.secondaryBackground,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              const SizedBox(height: DrawQuestionViewConfig.horizontalPadding),
              GameButton(
                title: LocaleKeys.gameboards_send.tr(),
                onPressed: _paintingExists ? _saveImage : null,
              ),
              SizedBox(height: Defaults.bottomSpacing),
            ],
          ),
        ),
      ],
    );
  }

  void markPaintingAsStarted(final bool started) {
    setState(() {
      _paintingExists = started;
    });
  }

  Future<void> _saveImage() async {
    getIt<LoadingCubit>().showLoading();
    final Uint8List? bytes = await _drawController.takeScreenshot();
    if (bytes == null) {
      logger.e('error when getting screenshot from painting');
      return;
    }

    await savePhotoAnswer(
      file: bytes,
      roomKey: widget.roomKey,
      questionKey: widget.questionKey,
    );
    getIt<LoadingCubit>().hideLoading();
    widget.onSend(photoAnswerUrl(
      player: getUserUuid(),
      roomKey: widget.roomKey,
      questionKey: widget.questionKey,
    ));
  }
}

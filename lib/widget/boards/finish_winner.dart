import 'dart:async';
import 'dart:math';

import 'package:auto_route/auto_route.dart';
import 'package:collection/collection.dart';
import 'package:confetti/confetti.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';

import '../../cubits/game_state.dart';
import '../../di/dependencies.dart';
import '../../gen/assets.gen.dart';
import '../../gen/colors.gen.dart';
import '../../gen/translations.gen.dart';
import '../../mixins/join_action_mixin.dart';
import '../../models/event.dart';
import '../../models/session.dart';
import '../../repositories/local_storage.dart';
import '../../routing/router.gr.dart';
import '../../scenes/new_game_screen.dart';
import '../../utils/app_theme.dart';
import '../../utils/camera_util.dart';
import '../../utils/logger.dart';
import '../../utils/prompters/rate_prompter.dart';
import '../../utils/question_colors.dart';
import '../../utils/session_utils.dart';
import '../avatars.dart';
import '../calculating_size.dart';
import '../game_button.dart';
import '../image_button.dart';
import '../join_listener.dart';
import '../player_round_result.dart';
import '../position_badge.dart';

// ignore: avoid_classes_with_only_static_members
class _FinishWinnerViewConfig {
  static const double margin = 24;
  static const double _platformWidth = 342;
  static const double _platformHeight = 127;
  static const double platformAspectRatio = _platformWidth / _platformHeight;
  static const double bigAvatarSize = 90;
  static const double smallAvatarSize = 60;
  static const double platformTopMargin = 16;
  static const double platformHorizontalMargin = 8;
  static const double platformStepAspectRatio = 80 / _platformHeight;
  static const double platformWinnerApsectRatio = 165 / _platformWidth;
  static const double platformWinnersSpacing = -36;
  static const double bottomSheetTopMargin = 40;

  static double bottomSheetHeight(
      final BuildContext context, final double maxHeight) {
    final double height = MediaQuery.of(context).size.height;
    if (maxHeight == 0) {
      return 0;
    }
    return (maxHeight - bottomSheetTopMargin) / height;
  }
}

class FinishWinner extends StatefulWidget {
  const FinishWinner({
    final Key? key,
    required this.host,
    required this.players,
    required this.nextRoomKey,
  }) : super(key: key);

  final Players players;
  final String host;
  final String nextRoomKey;

  @override
  State<FinishWinner> createState() => _FinishWinnerState();
}

class _FinishWinnerState extends State<FinishWinner> with JoinAction {
  late ConfettiController _confettiController;

  final GlobalKey _resultsFrameKey = GlobalKey();
  double _resultsViewHeight = 0;

  // Player list fields
  late Players _sortedPlayers;
  late List<int> _points;

  // Show rate dialog timer
  Timer? _showRateTimer;

  @override
  void initState() {
    super.initState();
    _confettiController =
        ConfettiController(duration: const Duration(seconds: 10));

    // Create results fields
    _points = widget.players.map((final Player e) => e.points).toSet().toList()
      ..sort((final int a, final int b) => b.compareTo(a));
    _sortedPlayers = List<Player>.from(widget.players)
      ..sort((final Player a, final Player b) => b.points.compareTo(a.points));

    // Mark that user saw this screen, this is needed for rate screen
    LocalStorage.markGameFinished();
    FirebaseAnalytics.instance.logEvent(name: 'game_finished');

    // Show rate after after a couple seconds
    _showRateTimer = Timer(
      const Duration(seconds: 1),
      () {
        if (!mounted) {
          return;
        }
        showRateDialogIfNeeded();
      },
    );
  }

  @override
  void dispose() {
    logger.d('finish game disposed');
    _confettiController.dispose();
    _showRateTimer?.cancel();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    // Segragate users into "chunks" where eg.
    // every user on 1st place is in the same array
    final Players sortedPlayers = widget.players.sorted(
        (final Player a, final Player b) => b.points.compareTo(a.points));
    final List<Players> segragatedPlayers = <Players>[];
    int lastPoints = sortedPlayers[0].points;
    Players playersWithTheSamePoints = <Player>[];
    for (final Player player in sortedPlayers) {
      if (player.points != lastPoints) {
        lastPoints = player.points;
        segragatedPlayers.add(playersWithTheSamePoints);
        playersWithTheSamePoints = <Player>[];
      }
      playersWithTheSamePoints.add(player);
    }
    segragatedPlayers.add(playersWithTheSamePoints);

    // Sort so my player is always on top
    final String myPlayerUuid = getUserUuid();
    for (final Players players in segragatedPlayers) {
      players.sort(
          (final Player a, final Player b) => a.id == myPlayerUuid ? -1 : 1);
    }

    // Create winners string
    final Players bestPlayers = segragatedPlayers[0];
    final String winnersStr =
        bestPlayers.map((final Player e) => e.name).join(', ');

    _confettiController.play();

    return CalculatingSize(
      keyToCalculate: _resultsFrameKey,
      onSizeCalculated: (final Size size) {
        setState(() {
          _resultsViewHeight = size.height;
        });
      },
      child: JoinListener(
        onNewGame: navigateToGame,
        child: Stack(
          clipBehavior: Clip.none,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(
                horizontal: _FinishWinnerViewConfig.margin,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  SizedBox(height: 32 + MediaQuery.of(context).padding.top),
                  Text(
                    LocaleKeys.finish_title.tr(),
                    textAlign: TextAlign.left,
                    maxLines: 1,
                    style: Theme.of(context).textTheme.displayMedium,
                  ),
                  const SizedBox(height: 2),
                  Text(
                    bestPlayers.length > 1
                        ? LocaleKeys.finish_winnerOne.tr(
                            namedArgs: <String, String>{'player': winnersStr})
                        : LocaleKeys.finish_winnerMore.tr(
                            namedArgs: <String, String>{'players': winnersStr}),
                    textAlign: TextAlign.left,
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                  const SizedBox(height: 40),
                  _Platform(
                    segragatedPlayers: segragatedPlayers,
                  ),
                  Expanded(key: _resultsFrameKey, child: Container()),
                ],
              ),
            ),
            DraggableScrollableSheet(
              initialChildSize: _FinishWinnerViewConfig.bottomSheetHeight(
                  context, _resultsViewHeight),
              minChildSize: _FinishWinnerViewConfig.bottomSheetHeight(
                  context, _resultsViewHeight),
              maxChildSize: 0.8,
              snap: true,
              builder: (final BuildContext context,
                  final ScrollController scrollController) {
                return PlayersResultList(
                    scrollController: scrollController,
                    players: _sortedPlayers,
                    points: _points);
              },
            ),
            Positioned(
              left: 0,
              right: 0,
              bottom: Defaults.bottomSpacing,
              child: GameButton(
                title: LocaleKeys.finish_playAgain.tr(),
                onPressed: _playAgain,
                dropShadow: true,
              ),
            ),
            Positioned.fill(
              top: -130,
              child: Align(
                alignment: Alignment.topCenter,
                child: ConfettiWidget(
                  shouldLoop: true,
                  minimumSize: const Size(6, 3),
                  maximumSize: const Size(16, 8),
                  colors: questionColors,
                  confettiController: _confettiController,
                  blastDirection: pi / 2,
                  maxBlastForce: 7, // set a lower max blast force
                  minBlastForce: 1, // set a lower min blast force
                  emissionFrequency: 0.1,
                  gravity: 0.05,
                  numberOfParticles: 7,
                ),
              ),
            ),
            Positioned(
              top: 12 + MediaQuery.of(context).padding.top,
              right: 0,
              child: ImageButton(
                padding: const EdgeInsets.all(24),
                image: Assets.images.closeIconBlack,
                onPressed: () {
                  getIt<AppRouter>()
                      .replaceAll(<PageRouteInfo>[const Welcome()]);
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _playAgain() {
    logger.i('user wants to play again, room ${widget.nextRoomKey}');
    FirebaseAnalytics.instance.logEvent(name: 'play_again');
    LocalStorage.markPlayAgainSelected();
    joinRoom(
      widget.nextRoomKey,
      changeRoomIfNeeded: true,
      isJoiningForTheFirstTime: false,
    );
  }

  void navigateToGame(final JoinState state) {
    // Go to waiting room screen
    FirebaseAnalytics.instance.logEvent(
      name: 'join_group',
      parameters: <String, Object>{'source': 'next_game'},
    );

    final List<PageRouteInfo> stack = <PageRouteInfo>[
      WaitingRoom(
        session: Session(
          state.roomKey,
          getUserUuid(),
        ),
      ),
    ];
    if (widget.host == getUserUuid()) {
      logger.i('user is a host, redirect to config room');
      stack.add(NewGame(config: NewGameConfig.resume(nextGameState: state)));
    }
    getIt<AppRouter>().replaceAll(stack);
  }
}

class _Platform extends StatelessWidget {
  const _Platform({
    final Key? key,
    required this.segragatedPlayers,
  }) : super(key: key);

  final List<Players> segragatedPlayers;

  @override
  Widget build(final BuildContext context) {
    // Gather players on platform
    final Players stPlace = segragatedPlayers[0];
    final Players ndPlace =
        // ignore: always_specify_types
        segragatedPlayers.length > 1 ? segragatedPlayers[1] : [];
    final Players trdPlace =
        // ignore: always_specify_types
        segragatedPlayers.length > 2 ? segragatedPlayers[2] : [];

    // Create all size variables to correctly place users on platform
    final double screenWidth = MediaQuery.of(context).size.width;
    final double platformWidth =
        screenWidth - (_FinishWinnerViewConfig.margin * 2);
    final double platformHeight =
        platformWidth / _FinishWinnerViewConfig.platformAspectRatio;
    final double containerHeight = _FinishWinnerViewConfig.bigAvatarSize +
        _FinishWinnerViewConfig.platformTopMargin +
        platformHeight;
    final double stepSpace =
        platformHeight * _FinishWinnerViewConfig.platformStepAspectRatio +
            _FinishWinnerViewConfig.platformTopMargin;
    final double platformWinnerWidth =
        platformWidth * _FinishWinnerViewConfig.platformWinnerApsectRatio;
    final double lowerPlayersHorizontalSpacing =
        ((platformWidth - platformWinnerWidth) / 2) -
            _FinishWinnerViewConfig.platformHorizontalMargin -
            _FinishWinnerViewConfig.smallAvatarSize;
    final double secondPlayerSpacing =
        ndPlace.length > 1 ? 0 : lowerPlayersHorizontalSpacing;
    final double thirdPlayerSpacing =
        trdPlace.length > 1 ? 0 : lowerPlayersHorizontalSpacing;
    const double platformWinnerTopMargin = 0;

    return SizedBox(
      height: containerHeight,
      child: Stack(
        children: <Widget>[
          Positioned(
            bottom: 0,
            left: 0,
            right: 0,
            child: Assets.images.platform.image(fit: BoxFit.fill),
          ),
          Positioned(
              top: platformWinnerTopMargin,
              left: 0,
              right: 0,
              child: _PlayersWrap(
                players: stPlace,
                position: 1,
              )),
          if (ndPlace.isNotEmpty)
            Positioned(
              left: secondPlayerSpacing,
              bottom: stepSpace,
              child: _PlayersWrap(
                players: ndPlace,
                position: 2,
              ),
            ),
          if (trdPlace.isNotEmpty)
            Positioned(
              right: thirdPlayerSpacing,
              bottom: stepSpace,
              child: _PlayersWrap(
                players: trdPlace,
                position: 3,
              ),
            ),
        ],
      ),
    );
  }
}

class _FinishPlayerResult extends StatelessWidget {
  const _FinishPlayerResult({
    final Key? key,
    required this.player,
    required this.maxWidth,
  }) : super(key: key);

  final Player player;
  final double maxWidth;

  @override
  Widget build(final BuildContext context) {
    return Avatar(
      dimension: maxWidth,
      imageUrl: avatarUrl(
        player: player.id,
      ),
    );
  }
}

class _PlayersWrap extends StatelessWidget {
  const _PlayersWrap({
    final Key? key,
    required this.position,
    required this.players,
  }) : super(key: key);

  final Players players;
  final int position;

  @override
  Widget build(final BuildContext context) {
    final double size = position == 1
        ? _FinishWinnerViewConfig.bigAvatarSize
        : _FinishWinnerViewConfig.smallAvatarSize;
    final TextStyle? font = position == 1
        ? Theme.of(context).textTheme.displayLarge
        : Theme.of(context).textTheme.displayMedium;
    return Wrap(
      alignment: WrapAlignment.center,
      spacing: _FinishWinnerViewConfig.platformWinnersSpacing,
      children: <Widget>[
        ...players.take(2).mapIndexed(
          (final int index, final Player e) {
            final Widget child;
            if (index == 0 || (index == 1 && players.length == 2)) {
              child = _FinishPlayerResult(
                player: e,
                maxWidth: size,
              );
            } else {
              child = Container(
                width: size,
                height: size,
                decoration: BoxDecoration(
                  color: ColorName.brown,
                  borderRadius: BorderRadius.circular(size / 2),
                ),
                child: Center(
                  child: Text(
                    '+${players.length - 1}',
                    textAlign: TextAlign.left,
                    maxLines: 1,
                    style: font?.copyWith(color: Colors.white),
                  ),
                ),
              );
            }
            return Stack(
              clipBehavior: Clip.none,
              children: <Widget>[
                child,
                if ((index == 0 && players.length == 1) || index > 0)
                  Positioned(
                    top: position == 1 ? 0 : -4,
                    right: position == 1 ? 0 : -6,
                    child: PositionBadge(
                      position: position,
                    ),
                  )
              ],
            );
          },
        )
      ],
    );
  }
}

import 'package:flutter/material.dart';

import '../../models/event.dart';
import '../../utils/app_theme.dart';
import '../animated_board.dart';
import '../game_button.dart';
import '../hint_header.dart';
import '../question_header.dart';
import '../scroll_with_line.dart';
import 'board.dart';

class PickAnswer extends StatelessWidget implements Board {
  const PickAnswer({
    final Key? key,
    required this.color,
    required this.hint,
    required this.question,
    required this.answers,
    this.onStringAnswer,
    this.onMapAnswer,
    this.assignedPlayer,
  }) : super(key: key);

  final String hint;
  final String question;
  final Answers answers;
  final Player? assignedPlayer;
  final Function(String)? onStringAnswer;
  final Function(Answers)? onMapAnswer;
  @override
  final Color color;

  @override
  Widget build(final BuildContext context) {
    return Column(
      children: <Widget>[
        QuestionHeader(
          question: question,
          color: color,
        ),
        AnimatedBoard(
          topChild: HintHeader(
            hint,
            player: assignedPlayer,
          ),
          bottomChild: ScrollWithLine(
            child: ListView.separated(
              padding: EdgeInsets.only(bottom: Defaults.bottomSpacing),
              itemCount: answers.length,
              itemBuilder: (final BuildContext context, final int index) {
                final MapEntry<String, Object> answer =
                    answers.entries.elementAt(index);

                return GameButton(
                  title: answer.value as String,
                  height: null,
                  onPressed: () {
                    if (onStringAnswer != null) {
                      onStringAnswer!(answer.key);
                    }
                    if (onMapAnswer != null) {
                      final Answers result = <String, String>{
                        assignedPlayer!.id: answer.key,
                      };
                      onMapAnswer!(result);
                    }
                  },
                  style: const GameButtonStyle.whiteWithBorder(),
                );
              },
              separatorBuilder: (final BuildContext context, final int index) =>
                  const SizedBox(height: Defaults.buttonsSpacing),
            ),
          ),
        ),
      ],
    );
  }
}

import 'package:collection/collection.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';

import '../../gen/colors.gen.dart';
import '../../gen/translations.gen.dart';
import '../../models/event.dart';
import '../../utils/app_theme.dart';
import '../../utils/logger.dart';
import '../animated_board.dart';
import '../animated_gone.dart';
import '../drag_and_drop.dart';
import '../drag_indicator.dart';
import '../game_button.dart';
import '../hint_header.dart';
import '../question_header.dart';
import '../scroll_with_line.dart';
import 'board.dart';

class AssignAnswers extends StatefulWidget implements Board {
  const AssignAnswers({
    final Key? key,
    required this.color,
    required this.correctAnswers,
    required this.players,
    required this.onAllAssigned,
    required this.question,
  }) : super(key: key);

  final Answers correctAnswers;
  final Players players;
  final String question;
  final Function(Answers) onAllAssigned;
  @override
  final Color color;

  @override
  State<AssignAnswers> createState() => _AssignAnswersState();
}

class _AssignAnswersState extends State<AssignAnswers> {
  final GlobalKey _draggableKey = GlobalKey();
  PageController? _pageController;
  BuildContext? _modalContext;

  late List<MapEntry<String, Object>> _shuffeldMixedAnswers;

  /// Answers in format player id: other player id,
  /// eg. [userA]:[userB]
  final Answers _answers = <String, Object>{};

  /// Exists if assigned bottom sheet is opened and secondary assign is visible
  int? _answerIndexToAssign;

  /// Returns true if all of the users were assigned
  bool get _allDone => _answers.keys.length == _shuffeldMixedAnswers.length;

  /// Array of all, not assigned users.
  /// Updated when user is assigned or removed from answer box.
  late Players _notAssigned;

  @override
  void initState() {
    super.initState();

    _shuffeldMixedAnswers = widget.correctAnswers.entries.toList();
    // mix the answers other, otherwise this will be in order of the users in waiting list
    _shuffeldMixedAnswers.shuffle();
    _notAssigned = List<Player>.from(widget.players); // copy list
  }

  @override
  void dispose() {
    _dismissAssignDrawer();
    _pageController?.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return Stack(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            QuestionHeader(
              color: widget.color,
              question: widget.question,
            ),
            AnimatedBoard(
              topChild:
                  HintHeader(LocaleKeys.gameboards_assign_answersHint.tr()),
              bottomChild: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  if (_notAssigned.isNotEmpty)
                    DragAndDropUsers(
                      players: _notAssigned,
                      draggableKey: _draggableKey,
                    ),
                  AnimatedVisibility(
                    visible: !_allDone,
                    child: Column(
                      children: <Widget>[
                        HintHeader(
                            LocaleKeys.gameboards_assign_answersTitle.tr()),
                        const SizedBox(height: 16),
                      ],
                    ),
                  ),
                  Expanded(
                    child: ScrollWithLine(
                      child: SingleChildScrollView(
                        padding: EdgeInsets.only(
                            bottom: _allDone
                                ? Defaults.buttomSpacingScroll
                                : Defaults.bottomSpacing),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            ..._shuffeldMixedAnswers.mapIndexed(
                                (final int index,
                                    final MapEntry<String, Object> element) {
                              final MapEntry<String, Object> answer =
                                  _shuffeldMixedAnswers[index];
                              final Player? selectedPlayer =
                                  _playersIn(tagIndex: answer.key);
                              return DragAndDropAnswer(
                                draggableKey: _draggableKey,
                                answer: answer.value as String,
                                assigned: selectedPlayer,
                                answerIndex: index,
                                onPlayerAssigned: (final Player player,
                                    final int answerIndex) {
                                  FirebaseAnalytics.instance
                                      .logEvent(name: 'assigned_by_drag');
                                  _playerAssigned(player, answerIndex);
                                },
                                onSecondaryActionTap: _openAssignDrawer,
                              );
                            })
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
        if (_allDone)
          Positioned(
            left: 0,
            right: 0,
            bottom: Defaults.bottomSpacing,
            child: GameButton(
              title: LocaleKeys.gameboards_send.tr(),
              onPressed: _sendTapped,
              dropShadow: true,
            ),
          ),
      ],
    );
  }

  Widget _buildDraggableContent(
    final Function? setStateModal,
  ) {
    final int answerIndex = _answerIndexToAssign ?? 0;
    final Answers mappedAnswers = _answers.map(
      (final String key, final Object value) {
        return MapEntry<String, Object>(
            value as String, widget.correctAnswers[key]!);
      },
    );

    return Container(
      decoration: const BoxDecoration(
        color: ColorName.secondaryBackground,
        borderRadius: BorderRadius.only(
          topLeft: Defaults.bottomSheetCornerRadius,
          topRight: Defaults.bottomSheetCornerRadius,
        ),
      ),
      child: Stack(
        children: <Widget>[
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              const SizedBox(height: Defaults.dockDragIndicatorTopMargin),
              const DragIndicator(),
              const SizedBox(height: Defaults.dockDragIndicatorBottomMargin),
              DotsIndicator(
                dotsCount: _shuffeldMixedAnswers.length,
                position: answerIndex.toDouble(),
                decorator: const DotsDecorator(
                  activeColor: Colors.black,
                  color: ColorName.lightGrey,
                ),
              ),
              const SizedBox(height: Defaults.dockDragIndicatorBottomMargin),
              Expanded(
                child: PageView.builder(
                    onPageChanged: (final int index) {
                      if (setStateModal == null) {
                        return;
                      }
                      _answerChanged(index, setStateModal);
                    },
                    controller: _pageController,
                    itemCount: _shuffeldMixedAnswers.length,
                    itemBuilder: (final BuildContext context, final int index) {
                      final MapEntry<String, Object> entry =
                          _shuffeldMixedAnswers[index];
                      final String answer = entry.value as String;
                      final Player? selectedPlayer =
                          _playersIn(tagIndex: entry.key);
                      return Column(
                        children: <Widget>[
                          DragAndDropAnswer(
                            answer: answer,
                            assigned: selectedPlayer,
                            answerIndex: index,
                            onPlayerAssigned: _playerAssigned,
                          ),
                          const SizedBox(height: 16),
                          Expanded(
                            child: ScrollWithLine(
                              child: PlayersDragSelectList(
                                players: widget.players,
                                tag: answer,
                                answers: mappedAnswers,
                                currentlyAssigned: <Player>[
                                  if (selectedPlayer != null) selectedPlayer
                                ],
                                onPlayerToggle: (final Player player) {
                                  final String? currentAnswer =
                                      _answers[entry.key] as String?;
                                  if (currentAnswer == null ||
                                      currentAnswer != player.id) {
                                    _playerAssigned(player, answerIndex,
                                        setStateModal: setStateModal);
                                  } else {
                                    _playerReleased(player,
                                        setStateModal: setStateModal);
                                  }
                                },
                              ),
                            ),
                          ),
                        ],
                      );
                    }),
              ),
            ],
          )
        ],
      ),
    );
  }

  void _playerReleased(
    final Player player, {
    final Function? setStateModal,
  }) {
    if (!mounted) {
      return;
    }

    setState(() {
      // Release user
      _notAssigned.add(player);
      _answers.removeWhere(
          (final String key, final Object value) => value == player.id);
    });
    // ignore: avoid_dynamic_calls
    setStateModal?.call(() {});
  }

  void _playerAssigned(
    final Player player,
    final int answerIndex, {
    final Function? setStateModal,
  }) {
    if (!mounted) {
      return;
    }

    final String correctPlayerId = _shuffeldMixedAnswers[answerIndex].key;
    final int playerIndex = _notAssigned.indexOf(player);
    // Save answer in format USERS A (correct) = USER SELECTED
    setState(() {
      // Remove user
      if (playerIndex >= 0) {
        _notAssigned.removeAt(playerIndex);
      }
      // Restore already assigned if needed
      if (_answers[correctPlayerId] != null) {
        final Player previouslyAssigned = widget.players
            .firstWhere((final Player e) => e.id == _answers[correctPlayerId]);
        if (previouslyAssigned != player) {
          _notAssigned.add(previouslyAssigned);
        }
      }

      _answers.removeWhere(
          (final String key, final Object value) => value == player.id);
      _answers[correctPlayerId] = player.id;
    });
    // ignore: avoid_dynamic_calls
    setStateModal?.call(() {});
    if (setStateModal != null) {
      // Drawer opened, move to next question or close
      _moveToNextAnswerIfNeeded();
    }
  }

  Player? _playersIn({required final String tagIndex}) {
    final Object? selectedPlayerId = _answers[tagIndex];
    return widget.players.firstWhereOrNull(
        (final Player element) => element.id == selectedPlayerId);
  }

  void _answerChanged(final int index, final Function setStateModal) {
    // ignore: avoid_dynamic_calls
    setStateModal(() {
      _answerIndexToAssign = index;
    });
  }

  void _moveToNextAnswerIfNeeded() {
    if (_answerIndexToAssign == null) {
      return;
    }

    final int nextIndex = _answerIndexToAssign! + 1;
    if (nextIndex >= _shuffeldMixedAnswers.length) {
      _dismissAssignDrawer();
    } else {
      _pageController?.animateToPage(nextIndex,
          duration: const Duration(milliseconds: 300), curve: Curves.easeOut);
    }
  }

  void _openAssignDrawer(final int answerIndex) {
    logger.i('open drawer to select multiple users');
    FirebaseAnalytics.instance.logEvent(name: 'open_assign_drawer');

    _answerIndexToAssign = answerIndex;
    _pageController?.dispose();
    _pageController =
        PageController(viewportFraction: 0.9, initialPage: answerIndex);

    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      context: context,
      builder: (final BuildContext modalContext) {
        _modalContext = modalContext;
        return StatefulBuilder(builder:
            (final BuildContext modalContext, final Function setStateModal) {
          return SizedBox(
            height: MediaQuery.of(context).size.height * 0.8,
            child: _buildDraggableContent(setStateModal),
          );
        });
      },
    ).whenComplete(() => _modalContext = null);
  }

  void _dismissAssignDrawer() {
    if (_modalContext != null) {
      Navigator.of(_modalContext!).pop();
    }
  }

  void _sendTapped() {
    widget.onAllAssigned(_answers);
  }
}

import 'dart:io';
import 'dart:math';
import 'dart:typed_data';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import '../../cubits/loading_cubit.dart';
import '../../di/dependencies.dart';
import '../../extensions/key_extensions.dart';
import '../../gen/translations.gen.dart';
import '../../utils/app_theme.dart';
import '../../utils/camera_util.dart';
import '../../utils/logger.dart';
import '../../utils/session_utils.dart';
import '../camera.dart';
import '../game_button.dart';
import '../question_header.dart';
import 'board.dart';

class BestOfCameraQuestion extends StatefulWidget implements Board {
  const BestOfCameraQuestion({
    final Key? key,
    required this.color,
    required this.roomKey,
    required this.question,
    required this.questionKey,
    required this.onSend,
    required this.patternUrl,
    required this.onImageTap,
  }) : super(key: key);

  final String question;
  final String roomKey;
  final String questionKey;
  final String? patternUrl;
  final Function()? onImageTap;
  final Function(String) onSend;
  @override
  final Color color;

  @override
  State<BestOfCameraQuestion> createState() => _BestOfCameraQuestionState();
}

class _BestOfCameraQuestionState extends State<BestOfCameraQuestion> {
  final GlobalKey _questionFrameKey = GlobalKey();
  final GlobalKey _stackFrameKey = GlobalKey();
  final InputCameraController _cameraController = InputCameraController();
  final ScrollController _scrollController = ScrollController();

  Uint8List? _imageToSend;
  bool _firstRun = true;
  double _containerHeight = 0;
  double _questionViewHeight = 0;
  bool get _isPictureTaken => _imageToSend != null;

  @override
  void initState() {
    super.initState();
    _cameraController.addListener(() {
      setState(() {
        if (!mounted) {
          return;
        }
        _imageToSend = _cameraController.imageTaken;
      });
      if (_isPictureTaken) {
        _scrollDown();
      }
    });
  }

  @override
  void dispose() {
    logger.d('camera question disposed');
    _scrollController.dispose();
    _cameraController.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    if (_firstRun) {
      SchedulerBinding.instance.addPostFrameCallback((final _) {
        final Size questionSize = _questionFrameKey.size;
        final Size containerSize = _stackFrameKey.size;

        setState(() {
          _questionViewHeight = questionSize.height;
          _containerHeight = containerSize.height;
        });
      });
      _firstRun = false;
    }

    return _isPictureTaken
        ? SingleChildScrollView(
            controller: _scrollController,
            child: _stackInside(),
          )
        : _stackInside();
  }

  Widget _stackInside() {
    final double bottomOverlayHeight = _containerHeight -
        _questionViewHeight -
        MediaQuery.of(context).size.width;

    // Android (AndroidX) library has ratio 4 / 3
    // iOS has ratio 16 / 9
    final double ratio = Platform.isIOS ? 16 / 9 : 4 / 3;

    return Stack(
      clipBehavior: Clip.none,
      key: _stackFrameKey,
      children: <Widget>[
        if (!_isPictureTaken)
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 400,
              color: Colors.black,
            ),
          )
        else
          SizedBox(
            height: max(
              _questionViewHeight +
                  MediaQuery.of(context).size.width +
                  165 +
                  MediaQuery.of(context).padding.bottom,
              _containerHeight,
            ),
          ),
        Positioned.fill(
          child: Container(
            // Hack for camera to not overflow on navigation bar
            clipBehavior: Clip.antiAlias,
            decoration: const BoxDecoration(
              color: Colors.black,
            ),
            child: Stack(
              children: <Widget>[
                Positioned(
                  top: _questionViewHeight,
                  left: 0,
                  right: 0,
                  child: AspectRatio(
                    aspectRatio: 1,
                    child: Stack(
                      clipBehavior: Clip.none,
                      children: <Widget>[
                        Positioned.fill(
                          child: OverflowBox(
                            child: FittedBox(
                              fit: BoxFit.fitWidth,
                              child: Container(
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(16),
                                ),
                                width: 200,
                                height: 200 * ratio,
                                child: Camera(controller: _cameraController),
                              ),
                            ),
                          ),
                        ),
                        Positioned(
                          top: MediaQuery.of(context).size.width,
                          left: 0,
                          right: 0,
                          child: Container(
                            color: _imageToSend == null
                                ? Colors.black
                                : Colors.white,
                            height: MediaQuery.of(context).size.height -
                                _questionViewHeight,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
        // Copy of question header to be "above" the camera
        SizedBox(
          child: QuestionHeader(
            key: _questionFrameKey,
            question: widget.question,
            color: widget.color,
            imageUrl: widget.patternUrl,
            onImageTap: widget.onImageTap,
          ),
        ),
        Positioned(
          // Overflowed camera shows one line pixel above question header
          top: -2,
          left: 0,
          right: 0,
          child: Container(
            height: 4,
            color: widget.color,
          ),
        ),
        if (_imageToSend == null)
          CameraButtons(cameraController: _cameraController)
        else
          Positioned(
            left: 0,
            right: 0,
            bottom: 0,
            child: Container(
              constraints: BoxConstraints(minHeight: bottomOverlayHeight),
              color: Colors.white,
              padding: EdgeInsets.only(bottom: Defaults.bottomSpacing),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  const SizedBox(height: 16),
                  GameButton(
                    title: LocaleKeys.gameboards_photo_captureAgain.tr(),
                    style: const GameButtonStyle.none(),
                    onPressed: _cameraController.retakePicture,
                  ),
                  const SizedBox(height: 16),
                  GameButton(
                    title: LocaleKeys.gameboards_send.tr(),
                    onPressed: _saveImage,
                  ),
                ],
              ),
            ),
          ),
      ],
    );
  }

  Future<void> _scrollDown() async {
    await Future<void>.delayed(const Duration(milliseconds: 200));

    _scrollController.animateTo(
      _scrollController.position.maxScrollExtent,
      duration: const Duration(milliseconds: 500),
      curve: Curves.fastOutSlowIn,
    );
  }

  Future<void> _saveImage() async {
    getIt<LoadingCubit>().showLoading();
    final Uint8List bytes = _imageToSend!;
    await savePhotoAnswer(
      file: bytes,
      roomKey: widget.roomKey,
      questionKey: widget.questionKey,
    );
    getIt<LoadingCubit>().hideLoading();
    widget.onSend(photoAnswerUrl(
      player: getUserUuid(),
      roomKey: widget.roomKey,
      questionKey: widget.questionKey,
    ));
  }
}

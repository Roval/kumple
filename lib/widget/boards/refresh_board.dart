import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

import '../../gen/assets.gen.dart';
import '../../gen/translations.gen.dart';
import '../game_button.dart';
import '../loading_indicator.dart';

class RefreshBoard extends StatefulWidget {
  const RefreshBoard({
    final Key? key,
    required this.onRefresh,
  }) : super(key: key);

  final Function() onRefresh;

  @override
  State<RefreshBoard> createState() => _RefreshBoardState();
}

class _RefreshBoardState extends State<RefreshBoard> {
  bool _refreshClicked = false;

  Timer? _timer;

  @override
  void dispose() {
    super.dispose();
    _timer?.cancel();
  }

  @override
  Widget build(final BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        if (_refreshClicked)
          const LoadingIndicator()
        else ...<Widget>[
          Lottie.asset(Assets.animations.wifi, height: 70),
          const SizedBox(height: 24),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: 24),
            child: Text(
              LocaleKeys.refresh_hint.tr(),
              style: Theme.of(context).textTheme.displayMedium,
              textAlign: TextAlign.center,
            ),
          ),
          const SizedBox(height: 24),
          GameButton(
            title: LocaleKeys.refresh_button.tr(),
            onPressed: () {
              setState(() {
                _refreshClicked = true;
              });

              _timer = Timer(
                const Duration(seconds: 3),
                () {
                  if (!mounted) {
                    return;
                  }
                  setState(() {
                    _refreshClicked = false;
                  });
                },
              );
              widget.onRefresh();
            },
          ),
        ]
      ],
    );
  }
}

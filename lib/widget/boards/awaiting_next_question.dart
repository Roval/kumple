import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../cubits/store_cubit.dart';
import '../../cubits/store_state.dart';
import '../../di/dependencies.dart';
import '../../environment_config.dart';
import '../../gen/translations.gen.dart';
import '../../models/event.dart';
import '../../repositories/remote_config.dart';
import '../../scenes/game_screen.dart';
import '../../utils/question_colors.dart';
import '../animated_board.dart';
import '../hint_header.dart';
import '../hints/ad_banner.dart';
import '../hints/waiting_banner.dart';
import '../players_waiting_grid.dart';
import '../question_header.dart';
import '../scroll_with_line.dart';

// TODO: Combine with awaiting_others.dart
class AwaitingNextQuestion extends StatefulWidget {
  const AwaitingNextQuestion({
    final Key? key,
    required this.players,
    required this.readyPlayers,
    required this.colorController,
  }) : super(key: key);

  final Players players;
  final List<String> readyPlayers;
  final GameplayHeaderController colorController;

  @override
  State<AwaitingNextQuestion> createState() => _AwaitingNextQuestionState();
}

class _AwaitingNextQuestionState extends State<AwaitingNextQuestion> {
  late Timer _timer;

  int _currentColorIndex = 0;

  late bool _shouldDisplayAd;

  @override
  void initState() {
    super.initState();

    final FetchStoreState? storeResponse =
        getIt<StoreCubit>().lastFetchedStoreResponse;

    final bool boughtAnyProduct =
        storeResponse?.boughtInStoreIaps.isNotEmpty ?? false;
    _shouldDisplayAd = !boughtAnyProduct &&
        RemoteConfigHelper.areAdsEnabled &&
        !EnvironmentConfig.isTestPackage;

    widget.colorController.addListener(controllerCallback);
    _timer = Timer.periodic(const Duration(seconds: 2), (final Timer timer) {
      _generateColor();
    });
  }

  @override
  void dispose() {
    widget.colorController.removeListener(controllerCallback);
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    final double bannerMargin = (MediaQuery.of(context).size.width - 320) / 2;

    return Expanded(
      child: Stack(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              BlankQuestionHeader(color: widget.colorController.color),
              const SizedBox(height: 24),
              AnimatedBoard(
                spaceBetweenComponents: 20,
                topChild: const WaitingBanner(),
                bottomChild: Column(children: <Widget>[
                  HintHeader(
                    LocaleKeys.gameboards_awaitingOthers_waitForNextRound.tr(
                      namedArgs: <String, String>{
                        'count': '${widget.readyPlayers.length}',
                        'all': '${widget.players.length}'
                      },
                    ),
                    topPadding: 4,
                  ),
                  const SizedBox(height: 16),
                  Expanded(
                    child: ScrollWithLine(
                      child: PlayersWaitingAnswerList(
                        players: widget.players,
                        readyPlayers: widget.readyPlayers,
                        readyString: LocaleKeys
                            .gameboards_awaitingOthers_readyState
                            .tr(),
                      ),
                    ),
                  ),
                  if (_shouldDisplayAd)
                    SizedBox(
                        height: 50 + MediaQuery.of(context).viewPadding.bottom),
                ]),
              ),
            ],
          ),
          if (_shouldDisplayAd)
            Positioned(
                right: bannerMargin,
                left: bannerMargin,
                bottom: MediaQuery.of(context).viewPadding.bottom,
                child: AdBanner(onError: _onAdFailure)),
        ],
      ),
    );
  }

  void controllerCallback() {
    setState(() {});
  }

  void _generateColor() {
    _currentColorIndex = (_currentColorIndex + 1) % questionColors.length;
    widget.colorController.publishColor(questionColors[_currentColorIndex]);
  }

  void _onAdFailure() {
    setState(() {
      _shouldDisplayAd = false;
    });
  }
}

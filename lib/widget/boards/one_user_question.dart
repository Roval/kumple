import 'package:flutter/material.dart';

import '../../gen/colors.gen.dart';
import '../../models/event.dart';
import '../../utils/app_theme.dart';
import '../../utils/element_order.dart';
import '../animated_board.dart';
import '../highlight_button.dart';
import '../hint_header.dart';
import '../player_row.dart';
import '../question_header.dart';
import '../scroll_with_line.dart';
import 'board.dart';

class OneUserQuestion extends StatelessWidget implements Board {
  const OneUserQuestion({
    final Key? key,
    required this.color,
    required this.hint,
    required this.question,
    required this.players,
    this.onStringAnswer,
    this.onMapAnswer,
    this.assignedAnswer,
  }) : super(key: key);

  final String hint;
  final String question;
  final Players players;
  final MapEntry<String, Object>? assignedAnswer;
  final Function(String)? onStringAnswer;
  final Function(Answers)? onMapAnswer;
  @override
  final Color color;

  @override
  Widget build(final BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        QuestionHeader(
          question: question,
          color: color,
        ),
        AnimatedBoard(
          topChild: HintHeader(
            hint,
            answer: assignedAnswer?.value as String?,
          ),
          bottomChild: ScrollWithLine(
            child: ListView.separated(
              padding: EdgeInsets.only(bottom: Defaults.bottomSpacing),
              itemCount: players.length,
              itemBuilder: (final BuildContext context, final int index) {
                final Player player = players[index];
                return _PlayerToSelect(
                  player: player,
                  onPlayerSelect: _playerSelected,
                );
              },
              separatorBuilder: (final BuildContext context, final int index) =>
                  const SizedBox(height: Defaults.buttonsSpacing),
            ),
          ),
        ),
      ],
    );
  }

  void _playerSelected(final Player player) {
    if (onStringAnswer != null) {
      onStringAnswer!(player.id);
    }
    if (onMapAnswer != null && assignedAnswer != null) {
      final Answers result = <String, String>{
        assignedAnswer!.key: player.id,
      };
      onMapAnswer!(result);
    }
  }
}

class _PlayerToSelect extends StatelessWidget {
  const _PlayerToSelect({
    final Key? key,
    required this.player,
    required this.onPlayerSelect,
  }) : super(key: key);

  final Player player;
  final Function(Player) onPlayerSelect;

  @override
  Widget build(final BuildContext context) {
    return HighlightButton(
      onTap: () {
        onPlayerSelect(player);
      },
      builder: (final BuildContext context, final bool highlight) {
        return PlayerRow(
          order: ElementOrder.onlyOneWithBorder,
          backgroundColor:
              highlight ? ColorName.lightGrey.withOpacity(0.5) : Colors.white,
          player: player,
        );
      },
    );
  }
}

import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import '../../extensions/key_extensions.dart';
import '../../gen/assets.gen.dart';
import '../../gen/colors.gen.dart';
import '../../gen/translations.gen.dart';
import '../../models/event.dart';
import '../../utils/app_theme.dart';
import '../animated_board.dart';
import '../badge.dart';
import '../game_button.dart';
import '../hint_header.dart';
import '../question_header.dart';
import 'board.dart';

// ignore: avoid_classes_with_only_static_members
class _BestOfImageOneUserViewConfig {
  static const double tileMargin = 8;
  static const double tileRadius = 10;
}

class BestOfImageOneUserQuestion extends StatefulWidget implements Board {
  const BestOfImageOneUserQuestion({
    final Key? key,
    required this.color,
    required this.answers,
    required this.players,
    required this.question,
    required this.patternUrl,
    required this.onSend,
  }) : super(key: key);

  final Answers answers;
  final Players players;
  final String question;
  final String? patternUrl;
  final Function(String) onSend;
  @override
  final Color color;

  @override
  State<BestOfImageOneUserQuestion> createState() =>
      _BestOfImageOneUserQuestionState();
}

class _BestOfImageOneUserQuestionState
    extends State<BestOfImageOneUserQuestion> {
  late PageController _pageController;
  late Players _shuffledPlayers;
  final GlobalKey _carouselKey = GlobalKey();
  double _carouselHeight = 0;
  int? _selectedPlayerIndex;
  int _currentPhotoIndex = 0;
  bool _firstRun = true;

  bool get _isInitialized => _carouselHeight != 0;
  double get _tileDimension {
    return MediaQuery.of(context).size.width *
            _pageController.viewportFraction -
        (2 * _BestOfImageOneUserViewConfig.tileMargin);
  }

  @override
  void initState() {
    super.initState();
    _shuffledPlayers = List<Player>.from(widget.players);
    _shuffledPlayers.shuffle();

    // Clear cache for the images (solves issue with new game with the same room id)
    for (final Player player in widget.players) {
      CachedNetworkImage.evictFromCache(widget.answers[player.id]! as String);
    }
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    _calculateSizes();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        QuestionHeader(
          color: widget.color,
          question: widget.question,
          imageUrl: widget.patternUrl,
        ),
        AnimatedBoard(
          topChild: HintHeader(LocaleKeys.gameboards_predictAnswer.tr()),
          bottomChild: Column(
            children: <Widget>[
              Expanded(
                child: Stack(
                  children: <Widget>[
                    PageView.builder(
                      onPageChanged: (final int index) => _photoChanged(index),
                      key: _carouselKey,
                      controller: _isInitialized ? _pageController : null,
                      itemCount: _isInitialized ? _shuffledPlayers.length : 0,
                      itemBuilder:
                          (final BuildContext context, final int pagePosition) {
                        final Player player = _shuffledPlayers[pagePosition];
                        return Container(
                          margin: const EdgeInsets.all(
                              _BestOfImageOneUserViewConfig.tileMargin),
                          child: Center(
                            child: Stack(
                              clipBehavior: Clip.none,
                              children: <Widget>[
                                AspectRatio(
                                  // This has to be done twice - in carousel and here, otherwise it will display full 16:9 photo
                                  aspectRatio: 1,
                                  child: GestureDetector(
                                    onTap: () => _playerSelected(pagePosition),
                                    child: Container(
                                      clipBehavior: Clip.hardEdge,
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(
                                            _BestOfImageOneUserViewConfig
                                                .tileRadius),
                                      ),
                                      child: CachedNetworkImage(
                                        imageUrl: widget.answers[player.id]!
                                            as String,
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ),
                                if (pagePosition == _selectedPlayerIndex)
                                  Container(
                                    width: _tileDimension,
                                    height: _tileDimension,
                                    decoration: BoxDecoration(
                                        border: Border.all(width: 4),
                                        borderRadius: BorderRadius.circular(
                                            _BestOfImageOneUserViewConfig
                                                .tileRadius)),
                                  ),
                                if (pagePosition == _selectedPlayerIndex)
                                  SelectedBadge(
                                    color: ColorName.text,
                                    image: Assets.images.selectedWhiteIcon
                                        .image(fit: BoxFit.none),
                                  ),
                              ],
                            ),
                          ),
                        );
                      },
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 10),
              DotsIndicator(
                dotsCount: _shuffledPlayers.length,
                position: _currentPhotoIndex.toDouble(),
                decorator: DotsDecorator(
                  activeColor: widget.color,
                  color: ColorName.lightGrey,
                ),
              ),
              const SizedBox(height: 16),
              GameButton(
                title: LocaleKeys.gameboards_send.tr(),
                onPressed: _selectedPlayerIndex == null
                    ? null
                    : _confirmSelectedPlayer,
              ),
              SizedBox(height: Defaults.bottomSpacing),
            ],
          ),
        ),
      ],
    );
  }

  void _calculateSizes() {
    if (_firstRun) {
      SchedulerBinding.instance.addPostFrameCallback((final _) {
        final Size carouselSize = _carouselKey.size;
        final double width = MediaQuery.of(context).size.width;
        final double height = carouselSize.height;

        final double viewportFraction = min(height, width) / width;
        final PageController controller =
            PageController(viewportFraction: min(viewportFraction, 0.9));

        setState(() {
          _carouselHeight = height;
          _pageController = controller;
        });
      });
      _firstRun = false;
    }
  }

  void _playerSelected(final int index) {
    setState(() {
      _selectedPlayerIndex = index;
    });
  }

  void _photoChanged(final int index) {
    setState(() {
      _currentPhotoIndex = index;
    });
  }

  void _confirmSelectedPlayer() {
    final Player selectedPlayer = _shuffledPlayers[_selectedPlayerIndex!];
    widget.onSend(selectedPlayer.id);
  }
}

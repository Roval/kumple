import 'package:collection/collection.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';

import '../../gen/colors.gen.dart';
import '../../gen/translations.gen.dart';
import '../../models/event.dart';
import '../../utils/app_theme.dart';
import '../../utils/logger.dart';
import '../animated_board.dart';
import '../animated_gone.dart';
import '../drag_and_drop.dart';
import '../drag_indicator.dart';
import '../game_button.dart';
import '../hint_header.dart';
import '../question_header.dart';
import '../scroll_with_line.dart';
import 'board.dart';

class DragOrOrQuestion extends StatefulWidget implements Board {
  const DragOrOrQuestion({
    final Key? key,
    required this.question,
    required this.color,
    required this.tags,
    required this.players,
    required this.onAllAssigned,
  }) : super(key: key);

  final List<String> tags;
  final Players players;
  final Function(Answers) onAllAssigned;
  final String question;
  @override
  final Color color;

  @override
  State<DragOrOrQuestion> createState() => _DragOrOrQuestionState();
}

class _DragOrOrQuestionState extends State<DragOrOrQuestion> {
  final GlobalKey _draggableKey = GlobalKey();
  PageController? _pageController;
  BuildContext? _modalContext;

  /// Answers in format player id: "answer string",
  /// eg. [user_id_123]: "Xbox"
  final Answers _answers = <String, Object>{};

  /// Exists if assigned bottom sheet is opened and secondary assign is visible
  int? _answerIndexToAssign;

  /// Returns true if all of the users were assigned
  bool get _allDone => _answers.keys.length == widget.players.length;

  /// Array of all, not assigned users.
  /// Updated when user is assigned or removed from answer box.
  late Players _notAssigned;

  @override
  void initState() {
    super.initState();

    _notAssigned = List<Player>.from(widget.players); // copy list
    _notAssigned.shuffle(); // shuffle players
  }

  @override
  void dispose() {
    _dismissAssignDrawer();
    _pageController?.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return Stack(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            QuestionHeader(
              color: widget.color,
              question: widget.question,
            ),
            AnimatedBoard(
              topChild:
                  HintHeader(LocaleKeys.gameboards_assign_answersHint.tr()),
              bottomChild: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  if (_notAssigned.isNotEmpty)
                    DragAndDropUsers(
                      players: _notAssigned,
                      draggableKey: _draggableKey,
                    ),
                  AnimatedVisibility(
                    visible: !_allDone,
                    child: Column(
                      children: <Widget>[
                        HintHeader(
                            LocaleKeys.gameboards_assign_answersTitle.tr()),
                        const SizedBox(height: 16),
                      ],
                    ),
                  ),
                  Expanded(
                    child: ScrollWithLine(
                      child: SingleChildScrollView(
                        padding: EdgeInsets.only(
                            bottom: _allDone
                                ? Defaults.buttomSpacingScroll
                                : Defaults.bottomSpacing),
                        child: Column(
                          children: <Widget>[
                            ...widget.tags.mapIndexed(
                              (final int index, final String answer) {
                                return DragAndDropBoxAnswer(
                                  draggableKey: _draggableKey,
                                  answer: answer,
                                  areAllAssigned: _allDone,
                                  assigned: _playersIn(answer: answer),
                                  answerIndex: index,
                                  onPlayerAssigned: (final Player player,
                                      final int answerIndex) {
                                    FirebaseAnalytics.instance
                                        .logEvent(name: 'assigned_by_drag');
                                    _playerAssigned(player, answerIndex);
                                  },
                                  onSecondaryActionTap: _openAssignDrawer,
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        if (_allDone)
          Positioned(
            left: 0,
            right: 0,
            bottom: Defaults.bottomSpacing,
            child: GameButton(
              title: LocaleKeys.gameboards_send.tr(),
              onPressed: _sendTapped,
              dropShadow: true,
            ),
          ),
      ],
    );
  }

  Widget _buildDraggableContent(
    final Function? setStateModal,
  ) {
    final int answerIndex = _answerIndexToAssign ?? 0;

    return Container(
      decoration: const BoxDecoration(
        color: ColorName.secondaryBackground,
        borderRadius: BorderRadius.only(
          topLeft: Defaults.bottomSheetCornerRadius,
          topRight: Defaults.bottomSheetCornerRadius,
        ),
      ),
      child: Stack(
        children: <Widget>[
          Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              const SizedBox(height: Defaults.dockDragIndicatorTopMargin),
              const DragIndicator(),
              const SizedBox(height: Defaults.dockDragIndicatorBottomMargin),
              DotsIndicator(
                dotsCount: widget.tags.length,
                position: answerIndex.toDouble(),
                decorator: const DotsDecorator(
                  activeColor: Colors.black,
                  color: ColorName.lightGrey,
                ),
              ),
              const SizedBox(height: Defaults.dockDragIndicatorBottomMargin),
              Expanded(
                child: PageView.builder(
                    onPageChanged: (final int index) {
                      if (setStateModal == null) {
                        return;
                      }
                      _answerChanged(index, setStateModal);
                    },
                    controller: _pageController,
                    itemCount: widget.tags.length,
                    itemBuilder: (final BuildContext context, final int index) {
                      final String answer = widget.tags[index];
                      final List<Player> playersAssigned =
                          _playersIn(answer: answer);
                      return Column(
                        children: <Widget>[
                          DragAndDropBoxAnswer(
                            answer: answer,
                            areAllAssigned:
                                // ignore: avoid_bool_literals_in_conditional_expressions
                                playersAssigned.isEmpty ? false : _allDone,
                            assigned: playersAssigned,
                            answerIndex: answerIndex,
                            onPlayerAssigned: _playerAssigned,
                          ),
                          const SizedBox(height: 16),
                          Expanded(
                            child: ScrollWithLine(
                              child: PlayersDragSelectList(
                                players: widget.players,
                                tag: answer,
                                answers: _answers,
                                currentlyAssigned: playersAssigned,
                                onPlayerToggle: (final Player player) {
                                  final String? currentAnswer =
                                      _answers[player.id] as String?;
                                  if (currentAnswer == null ||
                                      currentAnswer != answer) {
                                    _playerAssigned(player, answerIndex,
                                        setStateModal: setStateModal);
                                  } else {
                                    _playerReleased(player,
                                        setStateModal: setStateModal);
                                  }
                                },
                              ),
                            ),
                          ),
                        ],
                      );
                    }),
              ),
            ],
          )
        ],
      ),
    );
  }

  void _playerReleased(
    final Player player, {
    final Function? setStateModal,
  }) {
    setState(() {
      // Release user
      _notAssigned.add(player);
      _answers.remove(player.id);
    });
    // ignore: avoid_dynamic_calls
    setStateModal?.call(() {});
  }

  void _playerAssigned(
    final Player player,
    final int answerIndex, {
    final Function? setStateModal,
  }) {
    final String answer = widget.tags[answerIndex];
    final int playerIndex = _notAssigned.indexOf(player);
    setState(() {
      // Remove user
      if (playerIndex >= 0) {
        _notAssigned.removeAt(playerIndex);
      }

      // Save anser in format USERS A = <TAG>
      _answers[player.id] = answer;
    });
    // ignore: avoid_dynamic_calls
    setStateModal?.call(() {});
  }

  void _answerChanged(final int index, final Function setStateModal) {
    // ignore: avoid_dynamic_calls
    setStateModal(() {
      _answerIndexToAssign = index;
    });
  }

  List<Player> _playersIn({required final String answer}) {
    return _answers.keys
        .where((final String key) => _answers[key] == answer)
        .map((final String playerUuid) => widget.players
            .firstWhereOrNull((final Player player) => player.id == playerUuid))
        .whereNotNull()
        .toList();
  }

  void _openAssignDrawer(final int answerIndex) {
    logger.i('open drawer to select multiple users');
    FirebaseAnalytics.instance.logEvent(name: 'open_assign_drawer');

    _answerIndexToAssign = answerIndex;
    _pageController?.dispose();
    _pageController =
        PageController(viewportFraction: 0.9, initialPage: answerIndex);

    showModalBottomSheet(
      backgroundColor: Colors.transparent,
      isScrollControlled: true,
      context: context,
      builder: (final BuildContext modalContext) {
        _modalContext = modalContext;
        return StatefulBuilder(builder:
            (final BuildContext modalContext, final Function setStateModal) {
          return SizedBox(
            height: MediaQuery.of(context).size.height * 0.8,
            child: _buildDraggableContent(setStateModal),
          );
        });
      },
    ).whenComplete(() => _modalContext = null);
  }

  void _dismissAssignDrawer() {
    if (_modalContext != null) {
      Navigator.of(_modalContext!).pop();
    }
  }

  void _sendTapped() {
    widget.onAllAssigned(_answers);
  }
}

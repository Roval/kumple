import 'dart:async';
import 'dart:math';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';

import '../../gen/colors.gen.dart';
import '../../gen/translations.gen.dart';
import '../../utils/app_theme.dart';
import '../../utils/device_util.dart';
import '../../utils/upper_case_formatter.dart';
import '../animated_board.dart';
import '../game_button.dart';
import '../hint_header.dart';
import '../question_header.dart';
import '../single_line.dart';
import 'board.dart';

class KeyboardQuestion extends StatefulWidget implements Board {
  const KeyboardQuestion({
    final Key? key,
    required this.color,
    required this.question,
    required this.onSend,
    required this.hint,
  }) : super(key: key);

  final String question;
  final Function(String) onSend;
  final String hint;
  @override
  final Color color;

  @override
  State<KeyboardQuestion> createState() => _KeyboardQuestionState();
}

class _KeyboardQuestionState extends State<KeyboardQuestion> {
  final TextEditingController _textFieldController = TextEditingController();
  final FocusNode _focusNode = FocusNode();
  final KeyboardVisibilityController _keyboardVisibilityController =
      KeyboardVisibilityController();
  late StreamSubscription<bool> _keyboardSubscription;
  final ScrollController _scrollController = ScrollController();

  /// Answer from text field
  String get answer => _textFieldController.text.trim();

  bool get _isTextFieldedFocused => _focusNode.hasFocus;

  bool _viewOffsetedOnce = false;

  @override
  void initState() {
    super.initState();

    _textFieldController.addListener(() {
      setState(() {
        // Refresh
      });
    });

    _focusNode.addListener(() {
      if (!_isTextFieldedFocused) {
        Future<void>.delayed(const Duration(seconds: 2))
            .then((final _) => SystemChrome.restoreSystemUIOverlays());
      }
      setState(() {
        // Refresh
      });
    });

    // We add padding on bottom in order to have
    // scrollable view only when keyboard visible
    _keyboardSubscription =
        _keyboardVisibilityController.onChange.listen((final bool visible) {
      Future<void>.delayed(const Duration(milliseconds: 400), () {
        if (!mounted) {
          return;
        }

        setState(() {
          // Refresh
        });
      });
      // Scroll to bottom when keyboard is showned and padding added
      Future<void>.delayed(const Duration(milliseconds: 500), () {
        if (!mounted) {
          return;
        }
        _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
      });
    });

    // When view scrolled at least once - make the question header smaller
    _scrollController.addListener(() {
      setState(() {
        if (!_viewOffsetedOnce) {
          _viewOffsetedOnce = _scrollController.offset > 0;
        }
      });
    });
  }

  @override
  void dispose() {
    _scrollController.dispose();
    _textFieldController.dispose();
    _focusNode.dispose();
    _keyboardSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return Stack(
      children: <Widget>[
        SingleChildScrollView(
          controller: _scrollController,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              QuestionHeader(
                smallFont: _viewOffsetedOnce,
                question: widget.question,
                color: widget.color,
              ),
              AnimatedBoard(
                shouldExpand: false,
                topChild: HintHeader(widget.hint),
                bottomChild: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: Column(
                    children: <Widget>[
                      TextField(
                        focusNode: _focusNode,
                        inputFormatters: <TextInputFormatter>[
                          UpperCaseTextFormatter(),
                          LengthLimitingTextInputFormatter(200),
                        ],
                        textCapitalization: TextCapitalization.characters,
                        enableSuggestions: false,
                        autocorrect: false,
                        textInputAction: TextInputAction.done,
                        onSubmitted: (final String value) =>
                            FocusManager.instance.primaryFocus?.unfocus(),
                        controller: _textFieldController,
                        keyboardType: TextInputType.multiline,
                        maxLines: isSmallDevice ? 1 : 3,
                        minLines: 1,
                        decoration: InputDecoration(
                          hintStyle: Theme.of(context)
                              .textTheme
                              .displayMedium
                              ?.copyWith(color: ColorName.text50),
                          hintText: LocaleKeys.gameboards_text_yourAnswer.tr(),
                          border: InputBorder.none,
                        ),
                        textAlign: TextAlign.left,
                        style: Theme.of(context).textTheme.displayMedium,
                      ),
                      const SingleLine(height: 2, roundCorners: true),
                    ],
                  ),
                ),
              ),
              SizedBox(
                  height: _keyboardHeight() +
                      24 +
                      GameButtonViewConfig.height +
                      16),
            ],
          ),
        ),
        Positioned(
          left: 0,
          right: 0,
          bottom: _isTextFieldedFocused
              ? max(Defaults.bottomSpacing, _keyboardHeight() + 24)
              : Defaults.bottomSpacing,
          child: GameButton(
            title: LocaleKeys.gameboards_send.tr(),
            dropShadow: true,
            onPressed: answer.isEmpty
                ? null
                : () {
                    widget.onSend(answer);
                  },
          ),
        ),
      ],
    );
  }

  double _keyboardHeight() {
    return max(
        EdgeInsets.fromViewPadding(
                View.of(context).viewInsets, View.of(context).devicePixelRatio)
            .bottom,
        0);
  }
}

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../cubits/store_cubit.dart';
import '../../cubits/store_state.dart';
import '../../di/dependencies.dart';
import '../../environment_config.dart';
import '../../gen/translations.gen.dart';
import '../../models/event.dart';
import '../../repositories/remote_config.dart';
import '../animated_board.dart';
import '../hint_header.dart';
import '../hints/ad_banner.dart';
import '../hints/waiting_banner.dart';
import '../players_waiting_grid.dart';
import '../question_header.dart';
import '../scroll_with_line.dart';

class AwaitingOthers extends StatefulWidget {
  const AwaitingOthers({
    final Key? key,
    required this.players,
    required this.answered,
    required this.question,
    required this.boardColor,
  }) : super(key: key);

  final Players players;
  final String question;
  final List<String> answered;
  final Color boardColor;

  @override
  State<AwaitingOthers> createState() => _AwaitingOthersState();
}

class _AwaitingOthersState extends State<AwaitingOthers> {
  late bool _shouldDisplayAd;

  @override
  void initState() {
    super.initState();
    final FetchStoreState? storeResponse =
        getIt<StoreCubit>().lastFetchedStoreResponse;

    final bool boughtAnyProduct =
        storeResponse?.boughtInStoreIaps.isNotEmpty ?? false;
    _shouldDisplayAd = !boughtAnyProduct &&
        RemoteConfigHelper.areAdsEnabled &&
        !EnvironmentConfig.isTestPackage;
  }

  @override
  Widget build(final BuildContext context) {
    final double bannerMargin = (MediaQuery.of(context).size.width - 320) / 2;
    return Expanded(
      child: Stack(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              QuestionHeader(
                question: widget.question,
                color: widget.boardColor,
              ),
              const SizedBox(height: 24),
              AnimatedBoard(
                spaceBetweenComponents: 20,
                topChild: const WaitingBanner(),
                bottomChild: Column(
                  children: <Widget>[
                    HintHeader(
                      LocaleKeys.gameboards_awaitingOthers_waitForOtherPlayers
                          .tr(
                        namedArgs: <String, String>{
                          'count': '${widget.answered.length}',
                          'all': '${widget.players.length}'
                        },
                      ),
                      topPadding: 4,
                    ),
                    const SizedBox(height: 16),
                    Expanded(
                      child: ScrollWithLine(
                        child: PlayersWaitingAnswerList(
                          players: widget.players,
                          readyPlayers: widget.answered,
                          readyString: LocaleKeys
                              .gameboards_awaitingOthers_readyStateAnswer
                              .tr(),
                        ),
                      ),
                    ),
                    if (_shouldDisplayAd)
                      SizedBox(
                          height:
                              50 + MediaQuery.of(context).viewPadding.bottom),
                  ],
                ),
              ),
            ],
          ),
          if (_shouldDisplayAd)
            Positioned(
                right: bannerMargin,
                left: bannerMargin,
                bottom: MediaQuery.of(context).viewPadding.bottom,
                child: AdBanner(onError: _onAdFailure)),
        ],
      ),
    );
  }

  void _onAdFailure() {
    setState(() {
      _shouldDisplayAd = false;
    });
  }
}

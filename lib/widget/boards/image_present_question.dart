import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../gen/translations.gen.dart';
import '../../utils/app_theme.dart';
import '../game_button.dart';
import '../question_header.dart';
import 'board.dart';

class ImagePresentBoard extends StatelessWidget implements Board {
  const ImagePresentBoard({
    final Key? key,
    required this.color,
    required this.question,
    required this.imageUrl,
    required this.onNext,
  }) : super(key: key);

  final String question;
  final Function() onNext;
  final String imageUrl;
  @override
  final Color color;
  @override
  Widget build(final BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          QuestionHeader(
            question: question,
            color: color,
          ),
          const SizedBox(height: 32),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 24),
            child: Column(
              children: <Widget>[
                AspectRatio(
                  aspectRatio: 1,
                  child: CachedNetworkImage(
                    imageUrl: imageUrl,
                    fit: BoxFit.cover,
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: 32),
          GameButton(title: LocaleKeys.next.tr(), onPressed: onNext),
          SizedBox(height: Defaults.bottomSpacing),
        ],
      ),
    );
  }
}

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../gen/translations.gen.dart';
import '../../utils/app_theme.dart';
import '../animated_board.dart';
import '../game_button.dart';
import '../hint_header.dart';
import '../question_header.dart';
import 'board.dart';

class OrOrQuestion extends StatefulWidget implements Board {
  const OrOrQuestion({
    final Key? key,
    required this.color,
    required this.question,
    required this.tags,
    required this.onAswer,
  }) : super(key: key);

  final String question;
  final List<String> tags;
  final Function(String) onAswer;
  @override
  final Color color;

  @override
  State<OrOrQuestion> createState() => _OrOrQuestionState();
}

class _OrOrQuestionState extends State<OrOrQuestion> {
  @override
  Widget build(final BuildContext context) {
    return Column(
      children: <Widget>[
        QuestionHeader(
          question: widget.question,
          color: widget.color,
        ),
        AnimatedBoard(
          topChild: HintHeader(LocaleKeys.gameboards_sayTruthHint.tr()),
          bottomChild: ListView.separated(
            padding: EdgeInsets.only(bottom: Defaults.bottomSpacing),
            itemCount: widget.tags.length,
            itemBuilder: (final BuildContext context, final int index) {
              final String answer = widget.tags[index];

              return GameButton(
                title: answer,
                onPressed: () => widget.onAswer(answer),
                style: const GameButtonStyle.whiteWithBorder(),
              );
            },
            separatorBuilder: (final BuildContext context, final int index) =>
                const SizedBox(height: Defaults.buttonsSpacing),
          ),
        ),
      ],
    );
  }
}

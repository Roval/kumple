import 'package:flutter/material.dart';

class DragIndicatorViewConfig {
  static const double height = 8;
  static const double width = 48;
}

class DragIndicator extends StatelessWidget {
  const DragIndicator({
    final Key? key,
  }) : super(key: key);

  @override
  Widget build(final BuildContext context) {
    return Container(
      width: DragIndicatorViewConfig.width,
      height: DragIndicatorViewConfig.height,
      decoration: BoxDecoration(
          borderRadius:
              BorderRadius.circular(DragIndicatorViewConfig.height / 2),
          color: Colors.black.withOpacity(0.07)),
    );
  }
}

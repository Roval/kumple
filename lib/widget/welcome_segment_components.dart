import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../gen/assets.gen.dart';
import '../gen/colors.gen.dart';
import '../gen/translations.gen.dart';
import '../models/event.dart';
import '../utils/camera_util.dart';
import '../utils/device_util.dart';
import '../utils/element_order.dart';
import '../utils/session_utils.dart';
import 'avatars.dart';
import 'default_container.dart';
import 'tags.dart';

class WelcomeSectionHeader extends StatelessWidget {
  const WelcomeSectionHeader({
    super.key,
    required this.title,
  });

  final String title;

  @override
  Widget build(final BuildContext context) {
    return Column(
      children: <Widget>[
        const SizedBox(height: 24),
        Container(
          margin: const EdgeInsets.symmetric(horizontal: 24),
          width: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Text(
                title,
                textAlign: TextAlign.left,
                maxLines: 1,
                style: Theme.of(context).textTheme.titleLarge,
              ),
            ],
          ),
        ),
        const SizedBox(height: 16),
      ],
    );
  }
}

// ignore: avoid_classes_with_only_static_members
class _GameRowViewConfig {
  static const double avatarHeight = 24;
  static const double avatarOverlap = -10;
  static const double arrowWidth = 16;
  static const double horizontalSpacing = 16;

  static double? _wrapWidth;
  static double wrapWidth(final BuildContext context) {
    if (_wrapWidth != null) {
      return _wrapWidth!;
    }

    final double screenWidth = MediaQuery.of(context).size.width;
    _wrapWidth = (screenWidth -
            2 *
                (DefaultContainerConfig.horizontalPadding +
                    DefaultContainerConfig.horizontalMargin +
                    horizontalSpacing) -
            arrowWidth) /
        4;
    return _wrapWidth!;
  }
}

class NotificationRow extends StatelessWidget {
  const NotificationRow({
    final Key? key,
    required this.onNotificationTap,
    required this.title,
    required this.subtitle,
    required this.order,
  }) : super(key: key);

  final Function() onNotificationTap;
  final String title;
  final String subtitle;
  final ElementOrder order;

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
      onTap: onNotificationTap,
      child: DefaultContainer(
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Text(
                    title,
                    style: Theme.of(context).textTheme.titleMedium,
                  ),
                  const SizedBox(height: 2),
                  Text(
                    subtitle,
                    style: Theme.of(context)
                        .textTheme
                        .titleMedium
                        ?.copyWith(color: ColorName.grey),
                  ),
                ],
              ),
            ),
            const SizedBox(width: _GameRowViewConfig.horizontalSpacing),
            Assets.images.gameArrowIcon.image(),
          ],
        ),
      ),
    );
  }
}

class GameRow extends StatelessWidget {
  const GameRow({
    final Key? key,
    required this.onGameTap,
    required this.entry,
    required this.order,
  }) : super(key: key);

  final Function(String) onGameTap;
  final PendingGame entry;
  final ElementOrder order;

  @override
  Widget build(final BuildContext context) {
    // Players stack
    final List<String> players = entry.players;
    final double wrapWidth = _GameRowViewConfig.wrapWidth(context);
    double width = _GameRowViewConfig.avatarHeight;
    int howManyFits = 0;
    for (int i = 1; i < players.length + 1; i += 1) {
      if (wrapWidth <= width) {
        break;
      }
      width +=
          _GameRowViewConfig.avatarHeight + _GameRowViewConfig.avatarOverlap;
      howManyFits = i;
    }
    int counterRequired = players.length - howManyFits;
    if (counterRequired > 0) {
      howManyFits -= 1; // This will be circle with number
      counterRequired += 1;
    }

    // State
    final Color stateColor =
        entry.isMyPlayerReady ? ColorName.grey : ColorName.darkRed;
    final String state = entry.isMyPlayerReady
        ? LocaleKeys.welcome_myGames_waiting.tr()
        : entry.players.contains(getUserUuid())
            ? LocaleKeys.welcome_myGames_yourTurn.tr()
            : LocaleKeys.welcome_myGames_joinAgain.tr();

    // Expiry date
    final int expiresAt = entry.expiresAt;
    final int now = DateTime.now().millisecondsSinceEpoch ~/ 1000;
    final int diffInSeconds = expiresAt - now;
    final int diffInMinutes = diffInSeconds ~/ 60;
    final int hours = diffInMinutes ~/ 60;
    final int minutes = diffInMinutes % 60;
    final int seconds = diffInSeconds % 60;
    String remaining = '';
    if (hours > 0) {
      remaining += '${hours.toString().padLeft(2, '0')}:';
    }
    remaining +=
        '${minutes.toString().padLeft(2, '0')}:${seconds.toString().padLeft(2, '0')}';

    final TextStyle? font = isSmallDevice
        ? Theme.of(context).textTheme.bodyMedium
        : Theme.of(context).textTheme.titleMedium;

    return GestureDetector(
      onTap: () => onGameTap(entry.roomKey),
      child: DefaultContainer(
        order: order,
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 3,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Text(
                        LocaleKeys.welcome_id.tr(
                          namedArgs: <String, String>{'id': entry.roomKey},
                        ),
                        style: font,
                      ),
                      if (entry.owner == getUserUuid()) ...<Widget>[
                        const SizedBox(width: 8),
                        const HostTag(),
                      ],
                    ],
                  ),
                  const SizedBox(height: 2),
                  Row(
                    children: <Widget>[
                      Text(
                        state,
                        style: font?.copyWith(color: stateColor),
                      ),
                      const SizedBox(width: 6),
                      Container(
                        width: 4,
                        height: 4,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(2),
                          color: ColorName.grey,
                        ),
                      ),
                      const SizedBox(width: 6),
                      Text(
                        remaining,
                        style: font?.copyWith(color: ColorName.grey),
                      ),
                    ],
                  ),
                ],
              ),
            ),
            const SizedBox(width: _GameRowViewConfig.horizontalSpacing),
            Expanded(
              child: SizedBox(
                height: _GameRowViewConfig.avatarHeight,
                child: Wrap(
                  clipBehavior: Clip.antiAlias,
                  spacing: _GameRowViewConfig.avatarOverlap,
                  alignment: WrapAlignment.end,
                  children: <Widget>[
                    ...players.sublist(0, howManyFits).map(
                          (final String pId) => Avatar(
                            imageUrl: avatarUrl(player: pId),
                            dimension: _GameRowViewConfig.avatarHeight,
                          ),
                        ),
                    if (counterRequired > 0)
                      Container(
                        width: _GameRowViewConfig.avatarHeight,
                        height: _GameRowViewConfig.avatarHeight,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(
                            _GameRowViewConfig.avatarHeight / 2,
                          ),
                          color: ColorName.primaryBackground,
                        ),
                        child: Center(
                          child: Text('+$counterRequired',
                              style: Theme.of(context).textTheme.labelMedium),
                        ),
                      )
                  ],
                ),
              ),
            ),
            ...<Widget>[
              const SizedBox(width: _GameRowViewConfig.horizontalSpacing),
              Assets.images.gameArrowIcon.image(),
            ],
          ],
        ),
      ),
    );
  }
}

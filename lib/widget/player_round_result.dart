import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:transparent_pointer/transparent_pointer.dart';

import '../extensions/string_extensions.dart';
import '../gen/colors.gen.dart';
import '../gen/translations.gen.dart';
import '../models/event.dart';
import '../utils/app_theme.dart';
import '../utils/results_utils.dart';
import 'animations.dart';
import 'avatars.dart';
import 'boards/question_result.dart';
import 'bullet_list.dart';
import 'default_container.dart';
import 'drag_indicator.dart';
import 'modal_container.dart';
import 'position_badge.dart';
import 'stars_with_points.dart';

class PlayerRoundResultViewConfig {
  static const double spaceBetweenPoints = 12;
  static const double pointsTextWidth = 34;
}

class PlayerRoundResult extends StatelessWidget {
  const PlayerRoundResult({
    final Key? key,
    required this.player,
    required this.pointsReceived,
    required this.answer,
    required this.position,
  }) : super(key: key);

  final Player player;
  final int? pointsReceived;
  final String? answer;
  final int position;

  bool get _isCorrect => (pointsReceived ?? 0) > 0;

  @override
  Widget build(final BuildContext context) {
    return Row(
      children: <Widget>[
        PositionBadge(position: position),
        _PlayerResultCommonPart(
          player: player,
          answer: answer,
        ),
        if (pointsReceived != null)
          ConstrainedBox(
            constraints: const BoxConstraints(
              minWidth: PlayerRoundResultViewConfig.pointsTextWidth,
              maxWidth: PlayerRoundResultViewConfig.pointsTextWidth,
            ),
            child: Text(
              _isCorrect ? '+$pointsReceived' : '0',
              textAlign: TextAlign.right,
              maxLines: 1,
              style: Theme.of(context).textTheme.titleLarge?.copyWith(
                    color: _isCorrect ? ColorName.green : ColorName.grey,
                  ),
            ),
          ),
        const SizedBox(width: PlayerRoundResultViewConfig.spaceBetweenPoints),
        StarWithPoints(points: player.points),
      ],
    );
  }
}

class PlayerRoundResultAnimated extends StatefulWidget {
  const PlayerRoundResultAnimated({
    final Key? key,
    required this.player,
    required this.answer,
    required this.positionTo,
    required this.previousPoints,
    this.positionFrom,
  }) : super(key: key);

  final Player player;
  final String answer;
  final int previousPoints;
  final int? positionFrom;
  final int positionTo;

  @override
  State<PlayerRoundResultAnimated> createState() =>
      _PlayerRoundResultAnimatedState();
}

class _PlayerRoundResultAnimatedState extends State<PlayerRoundResultAnimated>
    with TickerProviderStateMixin {
  late Animation<double> _animation;
  late AnimationController _animationController;
  late Animation<double> _pointsAnimation;
  late AnimationController _pointsAnimationController;

  late int _pointsToDisplay;

  @override
  void initState() {
    super.initState();

    _pointsToDisplay = widget.previousPoints;
    _animationController = AnimationController(
      duration: Duration(
        milliseconds: QuestionResultViewConfig.animationPlayerBoxDuration,
      ),
      vsync: this,
    );
    _pointsAnimationController = AnimationController(
      duration: Duration(
          milliseconds: QuestionResultViewConfig.animationSingleStarDuration),
      vsync: this,
    );

    _animation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(_animationController)
      ..addListener(() {
        if (!mounted) {
          return;
        }

        setState(() {
          // The state that has changed here is the animation object’s value.
        });
      });
    _pointsAnimation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(_pointsAnimationController)
      ..addListener(() {
        if (!mounted) {
          return;
        }

        setState(() {
          if (_pointsAnimation.value == 1) {
            // Animate to next points value if needed
            _pointsAnimationController.reset();
            _pointsToDisplay += 1;
            if (_pointsToDisplay < widget.player.points) {
              _pointsAnimationController.forward();
            }
          }
        });
      });

    Future<dynamic>.delayed(Duration(
            milliseconds: QuestionResultViewConfig.animationPlayerBoxDelay))
        .then((final _) {
      if (!mounted) {
        return;
      }
      setState(() {
        _animationController.forward();
        if (widget.previousPoints != widget.player.points) {
          // Don't animate points if they dont change
          _pointsAnimationController.forward();
        }
      });
    });
  }

  @override
  void dispose() {
    _animationController.dispose();
    _pointsAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return Row(
      children: <Widget>[
        _badgeAnimation(PositionBadge(position: widget.positionTo)),
        _PlayerResultCommonPart(
          player: widget.player,
          answer: widget.answer,
        ),
        PopAnimation(
          animation: _pointsAnimation,
          scale: 0.5,
          child: StarWithPoints(points: _pointsToDisplay),
        ),
      ],
    );
  }

  Widget _badgeAnimation(final Widget badge) {
    final bool shouldGoDown =
        widget.positionFrom != null && widget.positionFrom! < widget.positionTo;

    final Widget resultWidget;
    if (widget.positionFrom == widget.positionTo) {
      resultWidget = PopAnimation(animation: _animation, child: badge);
    } else {
      resultWidget = Transform.translate(
        offset:
            Offset(0, (shouldGoDown ? 1 : -1) * 25 * (1 - _animation.value)),
        child: Opacity(
          opacity: _animation.value,
          child: badge,
        ),
      );
    }

    return Stack(
      children: <Widget>[
        if (widget.positionFrom != null &&
            widget.positionFrom != widget.positionTo)
          Transform.translate(
            offset: Offset(0, (shouldGoDown ? -1 : 1) * 25 * _animation.value),
            child: Opacity(
              opacity: 1 - _animation.value,
              child: PositionBadge(position: widget.positionFrom!),
            ),
          ),
        resultWidget,
      ],
    );
  }
}

class _PlayerResultCommonPart extends StatelessWidget {
  const _PlayerResultCommonPart({
    final Key? key,
    required this.player,
    required this.answer,
  }) : super(key: key);

  final Player player;
  final String? answer;

  @override
  Widget build(final BuildContext context) {
    return Expanded(
      child: Row(
        children: <Widget>[
          const SizedBox(width: 16),
          Avatar(imageUrl: player.avatar, dimension: 32),
          const SizedBox(width: 16),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  player.name,
                  textAlign: TextAlign.left,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.titleLarge,
                ),
                if (answer != null)
                  Text(
                    answer!.nonBreaking,
                    textAlign: TextAlign.left,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: Theme.of(context).textTheme.bodySmall?.copyWith(
                          color: ColorName.grey,
                        ),
                  ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class PlayersResultList extends StatelessWidget {
  const PlayersResultList({
    final Key? key,
    required this.scrollController,
    required this.players,
    required this.points,
    this.result,
  }) : super(key: key);

  final ScrollController scrollController;
  final Players players;
  final QuestionResultData? result;
  final List<int> points;

  @override
  Widget build(final BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      child: Stack(
        children: <Widget>[
          Positioned.fill(
            child: ListView.separated(
              padding: EdgeInsets.fromLTRB(
                  24,
                  ModalContainerViewConfig.dragHeight,
                  24,
                  Defaults.buttomSpacingScroll),
              controller: scrollController,
              itemCount: players.length,
              itemBuilder: (final BuildContext context, final int index) {
                final Player player = players[index];
                final int? pointsReceived = result?.winners[player.id];
                return PlayerRoundResult(
                  player: player,
                  pointsReceived: pointsReceived,
                  answer: result != null
                      ? answerBasedOnResult(result!,
                          forPlayer: player, allPlayers: players)
                      : null,
                  position: points.indexOf(player.points) + 1,
                );
              },
              separatorBuilder: (final BuildContext context, final int index) {
                return const SizedBox(height: 16);
              },
            ),
          ),
          const Align(
            alignment: Alignment.topCenter,
            child: TransparentPointer(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(height: Defaults.dockDragIndicatorTopMargin),
                  DragIndicator(),
                  SizedBox(height: Defaults.dockDragIndicatorBottomMargin),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class PlayerResultBox extends StatelessWidget {
  const PlayerResultBox({
    final Key? key,
    required this.winningAnswers,
    required this.pointsReceived,
    required this.player,
    required this.myAnswer,
    required this.positionTo,
    required this.previousPoints,
    required this.imageUrls,
    this.positionFrom,
  }) : super(key: key);

  final List<String> winningAnswers;
  final Player player;
  final String myAnswer;
  final int pointsReceived;
  final int previousPoints;
  final int? positionFrom;
  final int positionTo;
  final List<String> imageUrls;

  @override
  Widget build(final BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        DefaultContainer(
          child: Row(
            children: <Widget>[
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    BulletList(strings: winningAnswers),
                    const SizedBox(height: 2),
                    Text(
                      LocaleKeys.gameboards_result_points
                          .plural(pointsReceived),
                      maxLines: 4,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context).textTheme.titleSmall?.copyWith(
                            color: Colors.grey,
                          ),
                    ),
                  ],
                ),
              ),
              if (imageUrls.isNotEmpty)
                Padding(
                  padding: const EdgeInsets.only(left: 8),
                  child: SizedBox(
                    height: 60,
                    width: 60,
                    child: Wrap(
                      children: <Widget>[
                        ...imageUrls.map(
                          (final String e) => Container(
                            width: imageUrls.length == 1 ? 60 : 30,
                            height: imageUrls.length == 1 ? 60 : 30,
                            clipBehavior: Clip.hardEdge,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5)),
                            child: CachedNetworkImage(
                              imageUrl: e,
                              fit: BoxFit.cover,
                              errorWidget: (final BuildContext context,
                                      final String url, final _) =>
                                  Container(),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
            ],
          ),
        ),
        const SizedBox(height: 16),
        DefaultContainer(
          child: PlayerRoundResultAnimated(
              player: player,
              answer: myAnswer,
              positionTo: positionTo,
              positionFrom: positionFrom,
              previousPoints: previousPoints),
        ),
      ],
    );
  }
}

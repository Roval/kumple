// ignore_for_file: depend_on_referenced_packages, implementation_imports

import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_storage_platform_interface/src/full_metadata.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../di/dependencies.dart';
import '../../gen/assets.gen.dart';
import '../../gen/colors.gen.dart';
import '../../gen/translations.gen.dart';
import '../../repositories/local_storage.dart';
import '../../routing/router.gr.dart';
import '../../utils/camera_util.dart';
import '../../utils/element_order.dart';
import '../../utils/session_utils.dart';
import '../avatars.dart';
import '../default_container.dart';
import '../welcome_segment_components.dart';

class SettingsSegment extends StatefulWidget {
  const SettingsSegment({
    final Key? key,
  }) : super(key: key);

  @override
  State<SettingsSegment> createState() => _SettingsSegmentState();
}

class _SettingsSegmentState extends State<SettingsSegment> {
  String? _name;
  bool _avatarExists = false;
  bool _areSoundsEnabled = true;

  @override
  void initState() {
    super.initState();
    reloadData();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const SizedBox(height: 10),
        WelcomeSectionHeader(title: LocaleKeys.editProfile_title.tr()),
        _Profile(_name, _avatarExists, _openEditProfile),
        WelcomeSectionHeader(title: LocaleKeys.settings_game.tr()),
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _SettignsCheckbox(
                currentValue: _areSoundsEnabled,
                action: _changeSoundSetting,
                title: LocaleKeys.settings_sounds.tr(),
                order: ElementOrder.onlyOne),
          ],
        ),
        WelcomeSectionHeader(title: LocaleKeys.settings_links.tr()),
        Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            _SettignsButton(
                action: _openPrivacyPolicy,
                title: LocaleKeys.settings_privacyPolicy.tr(),
                order: ElementOrder.first),
            _SettignsButton(
                action: _openLicenses,
                title: LocaleKeys.settings_license.tr(),
                order: ElementOrder.middle),
            _SettignsButton(
                action: _openHelp,
                title: LocaleKeys.settings_contact.tr(),
                order: ElementOrder.last),
            const SizedBox(height: 16),
          ],
        ),
      ],
    );
  }

  void reloadData() {
    myAvatarMetadata().then((final FullMetadata? value) {
      if (mounted) {
        setState(() {
          _avatarExists = value != null;
        });
      }
    });
    LocalStorage.getUsername().then((final String? value) {
      if (value != null) {
        if (mounted) {
          setState(() {
            _name = value;
          });
        }
      }
    });
    LocalStorage.isSoundOn().then((final bool value) {
      setState(() {
        _areSoundsEnabled = value;
      });
    });
  }

  Future<void> _changeSoundSetting(final bool isEnabled) async {
    FirebaseAnalytics.instance.logEvent(name: 'change_sound_setting');
    LocalStorage.setSoundOn(isEnabled);
    setState(() {
      _areSoundsEnabled = isEnabled;
    });
  }

  void _openEditProfile() {
    FirebaseAnalytics.instance.logEvent(name: 'open_edit_profile');
    getIt<AppRouter>().push(
      EditProfile(
        editMode: true,
        onSave: (final String _) {
          getIt<AppRouter>().popUntilRouteWithName(Welcome.name);
          reloadData();
        },
      ),
    );
  }

  Future<void> _openLicenses() async {
    FirebaseAnalytics.instance.logEvent(name: 'open_licenses');
    getIt<AppRouter>().push(
      const Licenses(),
    );
  }

  Future<void> _openHelp() async {
    FirebaseAnalytics.instance.logEvent(name: 'open_help');
    getIt<AppRouter>().push(const HelpList());
  }

  Future<void> _openPrivacyPolicy() async {
    FirebaseAnalytics.instance.logEvent(name: 'open_privacy_policy');
    await launchUrl(Uri.parse('https://buddies.fun/privacy-policy'));
  }
}

// ignore: avoid_classes_with_only_static_members
class _ProfileConfig {
  static const double horizontalSpacing = 16;
}

class _Profile extends StatelessWidget {
  const _Profile(this.name, this.avatarExists, this.onEditTap);

  final String? name;
  final bool avatarExists;
  final Function() onEditTap;

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
      onTap: onEditTap,
      child: DefaultContainer(
        child: Row(children: <Widget>[
          if (avatarExists)
            Avatar(imageUrl: avatarUrl(player: getUserUuid()))
          else
            const EmptyAvatar(),
          const SizedBox(width: 16),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                if (name != null && name!.isNotEmpty)
                  Text(
                    name!,
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                Text(
                  (name == null)
                      ? LocaleKeys.settings_fillProfile.tr()
                      : LocaleKeys.settings_editProfile.tr(),
                  textAlign: TextAlign.center,
                  style: (name == null
                          ? Theme.of(context).textTheme.bodyLarge
                          : Theme.of(context).textTheme.bodySmall)
                      ?.copyWith(color: ColorName.grey),
                ),
              ],
            ),
          ),
          ...<Widget>[
            const SizedBox(width: _ProfileConfig.horizontalSpacing),
            Assets.images.gameArrowIcon.image(),
          ],
        ]),
      ),
    );
  }
}

class _SettignsButton extends StatelessWidget {
  const _SettignsButton(
      {required this.title, required this.action, required this.order});

  final Function() action;
  final String title;
  final ElementOrder order;

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
      onTap: () => action(),
      child: DefaultContainer(
        order: order,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                title,
                style: Theme.of(context).textTheme.titleLarge,
                textAlign: TextAlign.left,
              ),
            ),
            ...<Widget>[
              const SizedBox(width: _ProfileConfig.horizontalSpacing),
              Assets.images.gameArrowIcon.image(),
            ],
          ],
        ),
      ),
    );
  }
}

class _SettignsCheckbox extends StatelessWidget {
  const _SettignsCheckbox({
    required this.title,
    required this.action,
    required this.order,
    required this.currentValue,
  });

  final bool currentValue;
  final Function(bool) action;
  final String title;
  final ElementOrder order;

  @override
  Widget build(final BuildContext context) {
    return DefaultContainer(
      order: order,
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              title,
              style: Theme.of(context).textTheme.titleLarge,
              textAlign: TextAlign.left,
            ),
          ),
          ...<Widget>[
            const SizedBox(width: _ProfileConfig.horizontalSpacing),
            Switch(
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
              value: currentValue,
              onChanged: action,
              activeColor: ColorName.darkGreen,
              inactiveTrackColor: ColorName.grey,
            )
          ],
        ],
      ),
    );
  }
}

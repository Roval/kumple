import 'dart:async';

import 'package:collection/collection.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../cubits/games_list_cubit.dart';
import '../../cubits/games_list_state.dart';
import '../../cubits/notifications_cubit.dart';
import '../../cubits/notifications_state.dart';
import '../../di/dependencies.dart';
import '../../gen/assets.gen.dart';
import '../../gen/translations.gen.dart';
import '../../models/event.dart';
import '../../scenes/welcome_screen.dart';
import '../../utils/app_theme.dart';
import '../../utils/device_util.dart';
import '../../utils/element_order.dart';
import '../../utils/logger.dart';
import '../../utils/session_utils.dart';
import '../game_button.dart';
import '../loading_indicator.dart';
import '../welcome_segment_components.dart';

// ignore: avoid_classes_with_only_static_members
class MyGamesSegmentViewConfig {
  static Color errorTextColor = Colors.black.withAlpha(204);
}

class MyGamesSegment extends StatefulWidget {
  const MyGamesSegment({
    final Key? key,
    required this.onGameSelected,
  }) : super(key: key);

  final Function(String) onGameSelected;

  @override
  State<MyGamesSegment> createState() => _MyGamesSegmentState();
}

class _MyGamesSegmentState extends State<MyGamesSegment>
    with WidgetsBindingObserver {
  List<PendingGame>? _myGames;
  bool _didTimeout = false;

  /// Sets to true when user enters background.
  /// Then, when returned it checks if true and refetches games.
  /// Used bacause app `return` state is called even when you
  /// open notifcation center.
  bool _refetchOnResume = false;

  @override
  void initState() {
    super.initState();
    _fetchGames();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return BlocListener<NotificationsCubit, NotificationsState>(
      listener: (final BuildContext context, final NotificationsState state) {
        if (state is SilentNotificaitonState) {
          logger.i('received silent notification $state, refresh');
          _fetchGames();
        }
      },
      child: BlocBuilder<GamesListCubit, GameListState>(
        builder: (final BuildContext context, final GameListState state) {
          if (state is FetchedState) {
            final int? currentGamesCount = _myGames?.length;
            _myGames = state.games;

            if (currentGamesCount == null ||
                currentGamesCount != state.games.length) {
              // Log only if number of games changed
              FirebaseAnalytics.instance.logEvent(
                  name: 'games_showed',
                  parameters: <String, Object>{
                    'count': '${state.games.length}'
                  });
            }
          } else if (state is FetchTimeoutState) {
            _didTimeout = true;
          }

          Widget response;
          if (_myGames == null) {
            if (_didTimeout) {
              response = _errorText(
                msg: LocaleKeys.welcome_myGames_fetchGamesError.tr(),
                hint: LocaleKeys.welcome_myGames_fetchGamesHintError.tr(),
                image: Assets.images.errorIcon,
                buttonTitle: LocaleKeys.refresh_button.tr(),
                onButtonTap: () {
                  _didTimeout = false;
                  _startAgain();
                },
              );
            } else {
              response = _spinningLoader();
            }
          } else {
            if (_myGames!.isEmpty) {
              response = _errorText(
                msg: LocaleKeys.welcome_myGames_noYourGames.tr(),
                hint: LocaleKeys.welcome_myGames_noYourGamesHint.tr(),
                image: Assets.images.waitingIcon,
              );
            } else {
              final List<PendingGame> started =
                  List<PendingGame>.from(_myGames!)
                    ..removeWhere(
                        (final PendingGame game) => game.state == 'pending');
              final List<PendingGame> notStarted =
                  List<PendingGame>.from(_myGames!)
                    ..removeWhere(
                        (final PendingGame game) => game.state != 'pending');

              response = _GamesList(
                notStarted: notStarted,
                started: started,
                onGameTapped: _onGameTapped,
              );
            }
          }
          return response;
        },
      ),
    );
  }

  Widget _spinningLoader() {
    return SizedBox(
      width: double.infinity,
      height: _estimatedHeightWhenCollapsed(),
      child: const Center(
        child: LoadingIndicator(),
      ),
    );
  }

  Widget _errorText({
    required final String msg,
    required final String hint,
    required final AssetGenImage image,
    final String? buttonTitle,
    final Function()? onButtonTap,
  }) {
    final TextStyle? titleTheme = isSmallDevice
        ? Theme.of(context).textTheme.titleLarge
        : Theme.of(context).textTheme.headlineMedium;

    final TextStyle? subtitleTheme = isSmallDevice
        ? Theme.of(context).textTheme.bodyMedium
        : Theme.of(context).textTheme.titleMedium;

    return Container(
      width: double.infinity,
      height: _estimatedHeightWhenCollapsed(),
      margin: const EdgeInsets.fromLTRB(24, 0, 24, 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          const SizedBox(height: 24),
          Flexible(child: image.image(fit: BoxFit.contain)),
          SizedBox(height: smallIfNeeded(24)),
          Text(
            msg,
            textAlign: TextAlign.center,
            style: titleTheme?.copyWith(
              color: MyGamesSegmentViewConfig.errorTextColor,
            ),
          ),
          const SizedBox(height: 4),
          Text(
            hint,
            textAlign: TextAlign.center,
            style: subtitleTheme?.copyWith(
              color: MyGamesSegmentViewConfig.errorTextColor,
            ),
          ),
          if (buttonTitle != null) ...<Widget>[
            const SizedBox(height: 24),
            GameButton(
              title: buttonTitle,
              style: GameButtonStyle.outline(),
              height: 40,
              margin: null,
              onPressed: onButtonTap,
            ),
            const SizedBox(height: 24),
          ]
        ],
      ),
    );
  }

  double _estimatedHeightWhenCollapsed() {
    return ((MediaQuery.of(context).size.height -
                MediaQuery.of(context).padding.top) *
            WelcomePageViewConfig.bottomSheetHeight) -
        WelcomePageViewConfig.segmentsHeight -
        MediaQuery.of(context).padding.bottom;
  }

  void _onGameTapped(final String gameId) {
    widget.onGameSelected(gameId);
  }

  void _startAgain() {
    generateUniqueUserUuid(); // If user not created (first run) - generate it's id
    getIt<GamesListCubit>().reset();
    getIt<GamesListCubit>().fetchMyGames();
  }

  void _fetchGames() {
    getIt<GamesListCubit>().fetchMyGames();
  }

  @override
  void didChangeAppLifecycleState(final AppLifecycleState state) {
    if (state == AppLifecycleState.paused) {
      logger.i('app went to background');
      _refetchOnResume = true;
    } else if (state == AppLifecycleState.resumed && _refetchOnResume) {
      logger.i('app returned');
      _refetchOnResume = false;
      _fetchGames();
    }
  }
}

class _GamesList extends StatefulWidget {
  const _GamesList({
    required this.notStarted,
    required this.started,
    required this.onGameTapped,
  });

  final List<PendingGame> notStarted;
  final List<PendingGame> started;
  final Function(String) onGameTapped;

  @override
  State<_GamesList> createState() => __GamesListState();
}

class __GamesListState extends State<_GamesList> {
  /// Timer which triggers every second in order to decrease
  /// remaining time on each row.
  late Timer _refreshTimer;

  @override
  void initState() {
    super.initState();
    _refreshTimer = Timer.periodic(
      const Duration(seconds: 1),
      (final Timer timer) {
        setState(() {
          // Refreshs remaining time
        });
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
    _refreshTimer.cancel();
  }

  @override
  Widget build(final BuildContext context) {
    return Column(
      children: <Widget>[
        const SizedBox(height: 10),
        if (widget.notStarted.isNotEmpty)
          WelcomeSectionHeader(
              title: LocaleKeys.welcome_myGames_notStarted.tr()),
        ...widget.notStarted
            .mapIndexed((final int index, final PendingGame game) {
          return GameRow(
            onGameTap: widget.onGameTapped,
            entry: game,
            order: order(index, widget.notStarted.length),
          );
        }),
        if (widget.started.isNotEmpty)
          WelcomeSectionHeader(title: LocaleKeys.welcome_myGames_started.tr()),
        ...widget.started.mapIndexed((final int index, final PendingGame game) {
          return GameRow(
            onGameTap: widget.onGameTapped,
            entry: game,
            order: order(index, widget.started.length),
          );
        }),
        SizedBox(height: Defaults.bottomSpacing)
      ],
    );
  }
}

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../cubits/categories_usage_cubit.dart';
import '../../cubits/categories_usage_state.dart' as usage;
import '../../cubits/store_buy_cubit.dart';
import '../../cubits/store_cubit.dart';
import '../../cubits/store_state.dart' as store;
import '../../di/dependencies.dart';
import '../../gen/translations.gen.dart';
import '../../models/store.dart';
import '../../repositories/remote_config.dart';
import '../../routing/router.gr.dart';
import '../../utils/prompters/questions_usage_prompter.dart';
import '../bundles_grid.dart';
import '../game_button.dart';
import '../hints/info_hint.dart';
import '../loading_indicator.dart';

class StoreSegment extends StatefulWidget {
  const StoreSegment({
    final Key? key,
  }) : super(key: key);

  @override
  State<StoreSegment> createState() => _StoreSegmentState();
}

class _StoreSegmentState extends State<StoreSegment> {
  @override
  void initState() {
    super.initState();
    _fetchStore();
  }

  @override
  Widget build(final BuildContext context) {
    return BlocListener<StoreCubit, store.StoreState>(
      listenWhen:
          (final store.StoreState previous, final store.StoreState current) {
        final String currentPath = getIt<AppRouter>().currentPath;
        return currentPath == '/welcome';
      },
      listener: (final BuildContext context, final store.StoreState state) {
        if (state is store.FetchStoreState) {
          _fetchUsage(storeState: state);
        }
      },
      child: BlocBuilder<StoreCubit, store.StoreState>(
        builder: (final _, final store.StoreState state) {
          if (state is store.InitialState) {
            return Container(
              width: double.infinity,
              margin: const EdgeInsets.only(top: 32),
              child: const LoadingIndicator(),
            );
          } else if (state is store.FetchStoreState) {
            final StoreDefinition response = state.response;
            final int categoriesCount = response.all().length;
            final BundleGridConfig viewConfig =
                BundlesGrid.calculateViewSize(context, categoriesCount);

            final int boughtCount = state.unlockedProductsBundles.length;
            final bool allBought = boughtCount == categoriesCount;

            return BlocBuilder<CategoriesUsageCubit,
                usage.CategoriesUsageState>(
              builder: (final _, final usage.CategoriesUsageState usageState) {
                HintUsageModel? hintModel;
                if (usageState is usage.FetchedUsageState) {
                  hintModel = hostCreateUsageHintModel(usageState);
                }

                return Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    const SizedBox(
                        height:
                            6), // Space that overlaps with "segment controller"
                    if (hintModel != null)
                      Padding(
                        padding: const EdgeInsets.only(top: 16),
                        child: InfoHint(
                          animationAsset: hintModel.animation,
                          title: hintModel.title,
                          subtitle: hintModel.description,
                          backgroundColor: hintModel.backgroundColor,
                        ),
                      ),
                    if (state.shouldDisplayUnlockAll)
                      Padding(
                        padding: const EdgeInsets.only(
                            top: BundleGridViewConfig.verticalMargin),
                        child: Container(
                          height: viewConfig.size.width,
                          margin: const EdgeInsets.symmetric(
                              horizontal:
                                  BundleGridViewConfig.horizontalMargin),
                          child: UnlockAllProduct(
                            item: response.unlockAll,
                            onBuyTap: (final IapItem item) {
                              getIt<StoreBuyCubit>().buyProduct(
                                productId: item.iap,
                                source: BuySource.store,
                              );
                            },
                            price:
                                state.prices[response.unlockAll.iap] ?? '...',
                            fullPrice: state.priceForAllString,
                          ),
                        ),
                      ),
                    SizedBox(
                      height: viewConfig.size.height,
                      child: const BundlesGrid(),
                    ),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 24),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Text(
                            allBought
                                ? LocaleKeys.store_allBoughtHint.tr()
                                : LocaleKeys.store_boughtHint
                                    .plural(boughtCount),
                            textAlign: TextAlign.left,
                            style: Theme.of(context).textTheme.titleLarge,
                          ),
                          Text(
                            LocaleKeys.store_whyHint.tr(),
                            textAlign: TextAlign.left,
                            style: Theme.of(context).textTheme.bodyMedium,
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(height: 24),
                    GameButton(
                      title: LocaleKeys.store_restore.tr(),
                      onPressed: () => getIt<StoreCubit>().restorePurchases(),
                      textStyle: Theme.of(context).textTheme.headlineSmall,
                      throttleInterval:
                          5000, // Allow to click once per 5 seconds
                    ),
                    const SizedBox(height: 24)
                  ],
                );
              },
            );
          } else {
            return const SizedBox(
              height: 20,
            );
          }
        },
      ),
    );
  }

  void _fetchStore() {
    getIt<StoreCubit>().fetchStore();
  }

  void _fetchUsage({required final store.FetchStoreState storeState}) {
    if (RemoteConfigHelper.isUsageInfoBoxEnabled) {
      getIt<CategoriesUsageCubit>().fetchCategoriesUsage(storeState);
    }
  }
}

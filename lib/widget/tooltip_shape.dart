import 'package:flutter/material.dart';

class ToolTipCustomShape extends ShapeBorder {
  const ToolTipCustomShape({this.usePadding = true, this.arrowOnRight = true});

  final bool usePadding;
  final bool arrowOnRight;

  @override
  EdgeInsetsGeometry get dimensions =>
      EdgeInsets.only(bottom: usePadding ? 20 : 0);

  @override
  Path getInnerPath(final Rect rect, {final TextDirection? textDirection}) =>
      Path();

  @override
  Path getOuterPath(Rect rect, {final TextDirection? textDirection}) {
    final double startCornerX =
        arrowOnRight ? rect.topRight.dx - 32 : rect.topLeft.dx + 64;
    final double startCornerY =
        arrowOnRight ? rect.topRight.dy : rect.topLeft.dy;

    rect =
        Rect.fromPoints(rect.topLeft, rect.bottomRight - const Offset(0, 20));
    return Path()
      ..addRRect(
          RRect.fromRectAndRadius(rect, Radius.circular(rect.height / 3)))
      ..moveTo(startCornerX, startCornerY)
      ..relativeLineTo(-10, -10)
      ..relativeLineTo(-10, 10)
      ..close();
  }

  @override
  void paint(final Canvas canvas, final Rect rect,
      {final TextDirection? textDirection}) {}

  @override
  ShapeBorder scale(final double t) => this;
}

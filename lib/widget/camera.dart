import 'dart:async';
import 'dart:io';
import 'dart:math' as math;

import 'package:camera/camera.dart';
import 'package:collection/collection.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:image/image.dart' as img;
import 'package:image_picker/image_picker.dart';

import '../di/dependencies.dart';
import '../gen/assets.gen.dart';
import '../gen/translations.gen.dart';
import '../mixins/camera_dialog_mixin.dart';
import '../routing/router.gr.dart';
import '../scenes/alert_screen.dart';
import '../utils/app_theme.dart';
import '../utils/logger.dart';
import 'game_button.dart';
import 'image_button.dart';
import 'loading_indicator.dart';

List<CameraDescription> cameras = <CameraDescription>[];

class Camera extends StatefulWidget {
  const Camera({final Key? key, required this.controller}) : super(key: key);

  final InputCameraController controller;

  @override
  // ignore: no_logic_in_create_state
  State<Camera> createState() => _CameraState(controller);
}

class _CameraState extends State<Camera>
    with WidgetsBindingObserver, CameraPermissionDialog<Camera> {
  _CameraState(this._inputController) {
    _inputController._addState(this);
  }

  CameraController? _controller;
  final InputCameraController _inputController;

  /// If user wants to take picture we count a couple of seconds
  /// to see if it succeded.
  /// If not - we allow them to use native camera.
  Timer? _timeoutTimer;

  @override
  void initState() {
    super.initState();
    logger.d('init camera state');
    WidgetsBinding.instance.addObserver(this);

    Future<dynamic>.delayed(const Duration(seconds: 1)).then((final _) {
      _initCamera();
    });
  }

  @override
  void dispose() {
    logger.d('dispose camera state');
    WidgetsBinding.instance.removeObserver(this);
    _controller?.dispose();
    _cancelErrorTimeout();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return Stack(children: <Widget>[
      if (_controller != null && _controller!.value.isInitialized)
        CameraPreview(
          _controller!,
        )
      else
        const LoadingIndicator(
          dimension: 40,
        ),
      if (_inputController.imageTaken != null)
        Positioned.fill(
          child: Transform(
            alignment: Alignment.center,
            transform: Matrix4.rotationY(math.pi),
            child: Image.memory(
              _inputController.imageTaken!,
              fit: BoxFit.fill,
            ),
          ),
        ),
    ]);
  }

  Future<void> _initCamera() async {
    await _captureAnyError(() async {
      logger.d('init camera');
      final bool granted = await checkPermissions();
      if (!granted) {
        logger.i('persmission not granted');
        return;
      }

      if (cameras.isEmpty) {
        logger.e('there are no cameras!');
        // Condition takes place when camera is broken and image taken from library
        // Without it camera would say that camera was not detected
        // when reloaded to preview mode.
        if (_inputController.imageTaken == null) {
          _showError(LocaleKeys.alerts_camera_noDevice.tr());
        }
        return;
      }

      final CameraDescription rearCamera = cameras.firstWhereOrNull(
              (final CameraDescription c) =>
                  c.lensDirection == CameraLensDirection.front) ??
          cameras.first;
      await _onNewCameraSelected(rearCamera);
    });
  }

  Future<void> _onNewCameraSelected(final CameraDescription camera) async {
    await _captureAnyError(() async {
      logger.d('create new camera controller');

      await _disposeCurrentController();

      final CameraController controller = CameraController(
        camera,
        ResolutionPreset.high,
        enableAudio: false,
        imageFormatGroup: ImageFormatGroup.yuv420,
      );
      _controller = controller;

      // If the controller is updated then update the UI.
      controller.addListener(() {
        if (mounted) {
          setState(() {});
        }
        if (controller.value.hasError) {
          logger
              .e('camera has an error:  ${controller.value.errorDescription}');
        }
      });
      await controller.initialize();
      _updateFlashMode();
      // TODO: CameraX is not supporting it now, check newest version
      if (Platform.isIOS) {
        controller.lockCaptureOrientation(DeviceOrientation.portraitUp);
      }

      if (mounted) {
        setState(() {});
      }
    });
  }

  Future<void> _takePicture() async {
    await _captureAnyError(() async {
      logger.d('take picture');

      _startErrorTimeout();

      final CameraController? cameraController = _controller;
      if (cameraController == null || !cameraController.value.isInitialized) {
        logger.i('camera not initilized, unable to do photo');
        return;
      }

      if (cameraController.value.isTakingPicture == true) {
        // A capture is already pending, do nothing.
        return;
      }

      final XFile file = await _controller!.takePicture();
      _cancelErrorTimeout();
      await _resolveXFile(file);
    });
  }

  // If picture is not taken within a couple of seconds we will display error
  // so user can select image from gallery
  void _startErrorTimeout() {
    _cancelErrorTimeout();
    _timeoutTimer = Timer(
      const Duration(seconds: 6),
      () {
        logger.d('timeout, image not taken');
        _showError(LocaleKeys.alerts_camera_errorTitle.tr());
      },
    );
  }

  void _cancelErrorTimeout() {
    _timeoutTimer?.cancel();
  }

  Future<void> _resolveXFile(final XFile file) async {
    final img.Image? capturedImage;
    if (Platform.isIOS) {
      capturedImage = img.decodeImage(await file.readAsBytes());
    } else {
      capturedImage =
          img.decodeImage(await FlutterImageCompress.compressWithList(
        await file.readAsBytes(),
        minHeight: 1920,
        minWidth: 1080,
      ));
    }

    try {
      if (capturedImage == null) {
        throw CameraException('-100', 'Unable to save photo');
      }
      final img.Image resized = img.copyResize(capturedImage, width: 300);

      _inputController
          ._setImage(Uint8List.fromList(img.encodeJpg(resized, quality: 90)));
    } on CameraException catch (e, stacktrace) {
      logger.e('failed when taking picture: $e');
      FirebaseCrashlytics.instance
          .recordError(e, stacktrace, reason: 'Issue with taking photo');
      _showError(LocaleKeys.alerts_camera_errorTitle.tr());
    }
  }

  Future<void> _reverseCamera() async {
    await _captureAnyError(() async {
      logger.d('reverse camera');
      final CameraDescription? currentCamera = _controller?.description;
      if (cameras.isEmpty) {
        return;
      }
      final CameraDescription? otherCamera = cameras
          .firstWhereOrNull((final CameraDescription c) => c != currentCamera);

      if (otherCamera == null) {
        return;
      }

      await _onNewCameraSelected(otherCamera);
    });
  }

  void _updateFlashMode() {
    logger.d('change flash mode');
    _controller?.setFlashMode(
        _inputController._isFlashOn ? FlashMode.always : FlashMode.off);
  }

  @override
  void didChangeAppLifecycleState(final AppLifecycleState state) {
    final CameraController? cameraController = _controller;

    // App state changed before we got the chance to initialize.
    if (cameraController == null || !cameraController.value.isInitialized) {
      logger.d('camera not initialized when app status changed');
      return;
    }

    if (state == AppLifecycleState.inactive) {
      logger.d('dispose camera, app is inactive');
      cameraController.dispose();
    } else if (state == AppLifecycleState.resumed) {
      logger.d('resume camera, app is resumed');
      _onNewCameraSelected(cameraController.description);
    }
  }

  void _showError(final String msg) {
    logger.i('opening camera error dialog');
    _cancelErrorTimeout();

    final AppRouter router = getIt<AppRouter>();
    final String? currentPath = router.currentChild?.name;
    if (currentPath == InfoAlert.name) {
      return;
    }

    // Delay fixes the problem with two navigtions heppening and game room
    // being initialized a couple of times
    Future<void>.delayed(const Duration(seconds: 1), () {
      getIt<AppRouter>().push(
        InfoAlert(
          message: msg,
          subtitle: LocaleKeys.alerts_camera_errorSubtitle.tr(),
          actions: <InfoAlertAction>[
            InfoAlertAction(
              onTap: (final _) {
                getIt<AppRouter>().pop();
                FirebaseAnalytics.instance
                    .logEvent(name: 'camera_fallback_native');
                _openWithPicker(ImageSource.camera);
              },
              title: LocaleKeys.alerts_camera_openExternal.tr(),
            ),
            InfoAlertAction(
              onTap: (final _) {
                getIt<AppRouter>().pop();
                FirebaseAnalytics.instance
                    .logEvent(name: 'camera_fallback_gallery');
                _openWithPicker(ImageSource.gallery);
              },
              style: GameButtonStyle.outline(),
              title: LocaleKeys.alerts_camera_openLibrary.tr(),
            ),
          ],
        ),
      );
    });
  }

  Future<void> _openWithPicker(final ImageSource source) async {
    await _captureAnyError(() async {
      final ImagePicker picker = ImagePicker();
      final XFile? image = await picker.pickImage(source: source);
      if (image == null) {
        try {
          throw CameraException('-100', 'Unable to load photo from library');
        } catch (e, stacktrace) {
          FirebaseCrashlytics.instance.recordError(e, stacktrace,
              reason: 'Issue with loading photo from library');
        }
        return;
      }
      _resolveXFile(image);
    });
  }

  Future<void> _disposeCurrentController() async {
    await _captureAnyError(() async {
      if (_controller == null) {
        return;
      }
      final CameraController? oldController = _controller;
      // `controller` needs to be set to null before getting disposed,
      // to avoid a race condition when we use the controller that is being
      // disposed. This happens when camera permission dialog shows up,
      // which triggers `didChangeAppLifecycleState`, which disposes and
      // re-creates the controller.
      _controller = null;
      await oldController?.dispose();
    });
  }

  Future<void> _captureAnyError(final Future<dynamic> Function() body) async {
    try {
      await body();
    } catch (e, stacktrace) {
      logger.e('error when performing camera action: $e');
      if (e is CameraException && e.code == 'CameraAccessDeniedWithoutPrompt') {
        // Expected, do nothing. It is when user denied access.
        // Repro steps: deny access, hide prompt, try to revert camera.
        return;
      }
      FirebaseCrashlytics.instance.recordError(e, stacktrace,
          reason: 'Unknown error captured in camera');
    }
  }
}

class InputCameraController extends ChangeNotifier {
  Uint8List? imageTaken;
  bool _isFlashOn = false;

  _CameraState? _cameraState;

  // ignore: use_setters_to_change_properties
  void _addState(final _CameraState state) {
    _cameraState = state;
  }

  @override
  void dispose() {
    _cameraState = null;
    super.dispose();
  }

  void _setImage(final Uint8List? image) {
    imageTaken = image;
    notifyListeners();
  }

  void retakePicture() {
    _setImage(null);
  }

  void takePicture() {
    _cameraState?._takePicture();
  }

  void rotateCamera() {
    _cameraState?._reverseCamera();
  }

  void toggleFlash() {
    _isFlashOn = !_isFlashOn;
    _cameraState?._updateFlashMode();
    notifyListeners();
  }
}

class CameraButtons extends StatelessWidget {
  const CameraButtons({
    final Key? key,
    required final InputCameraController cameraController,
  })  : _cameraController = cameraController,
        super(key: key);

  final InputCameraController _cameraController;

  @override
  Widget build(final BuildContext context) {
    return Positioned(
      left: 24,
      right: 24,
      bottom: Defaults.bottomSpacing,
      child: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.bottomCenter,
            child: ImageButton(
              key: const ValueKey<String>('shutterButton'),
              image: Assets.images.shutterIcon,
              onPressed: () {
                _cameraController.takePicture();
              },
            ),
          ),
          Positioned(
            right: 0,
            bottom: 0,
            child: ImageButton(
              image: Assets.images.reverseIcon,
              onPressed: () {
                _cameraController.rotateCamera();
              },
            ),
          ),
          Positioned(
            left: 0,
            bottom: 0,
            child: ImageButton(
              image: _cameraController._isFlashOn
                  ? Assets.images.flashIcon
                  : Assets.images.flashIconInactive,
              onPressed: () {
                _cameraController.toggleFlash();
              },
            ),
          ),
        ],
      ),
    );
  }
}

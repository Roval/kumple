import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:open_store/open_store.dart';

import '../constants.dart';
import '../cubits/game_cubit.dart';
import '../cubits/game_state.dart';
import '../cubits/loading_cubit.dart';
import '../di/dependencies.dart';
import '../gen/translations.gen.dart';
import '../mixins/error_dialog_mixin.dart';
import '../models/session.dart';
import '../routing/router.gr.dart';
import '../scenes/alert_screen.dart';
import '../utils/logger.dart';
import '../utils/session_utils.dart';

class JoinListener extends StatefulWidget with ErrorDialog {
  const JoinListener({
    final Key? key,
    required this.child,
    this.onNewGame,
  }) : super(key: key);

  final Widget child;
  final Function(JoinState)? onNewGame;

  @override
  State<JoinListener> createState() => _JoinListenerState();
}

class _JoinListenerState extends State<JoinListener> {
  @override
  Widget build(final BuildContext context) {
    return BlocListener<GameCubit, GameState>(
      listener: (final _, final GameState state) {
        getIt<LoadingCubit>().hideLoading();

        if (state is JoinState) {
          if (widget.onNewGame != null) {
            // Custom routing when join received.
            // Eg. used when "Jeszcze raz" clicked - for host
            // it openes game config
            widget.onNewGame?.call(state);
          } else {
            // Go to waiting room screen
            FirebaseAnalytics.instance.logEvent(
              name: 'join_group',
              parameters: <String, Object>{'source': 'existing_game'},
            );
            getIt<AppRouter>().replaceAll(<PageRouteInfo>[
              WaitingRoom(
                session: Session(
                  state.roomKey,
                  getUserUuid(),
                ),
              ),
            ]);
          }
        } else if (state is RejoinState) {
          FirebaseAnalytics.instance.logEvent(name: 'rejoin_group');

          // Go back to the game!
          getIt<AppRouter>().replaceAll(<PageRouteInfo>[
            GameRoom(
              session: Session(
                state.roomKey,
                getUserUuid(),
              ),
              initialRounds: state.rounds,
            ),
          ]);
        } else if (state is JoinTimeout) {
          String source;
          if (state.isCreatingNewGame) {
            source = 'new_game';
            _showUnableToCreate();
          } else {
            source = 'join_game';
            _showUnableToJoin();
          }
          FirebaseAnalytics.instance.logEvent(
            name: 'join_timeout',
            parameters: <String, Object>{'source': source},
          );
        } else if (state is ErrorState) {
          _handleError(state);
        }
      },
      child: widget.child,
    );
  }

  void _showUnableToJoin() {
    widget.showErrorDialog(msg: LocaleKeys.welcome_error_joinRoom.tr());
  }

  void _showUnableToCreate() {
    widget.showErrorDialog(msg: LocaleKeys.setupGame_error_createRoom.tr());
  }

  void _handleError(final ErrorState state) {
    switch (state.error) {
      case 'max_players_count':
        FirebaseAnalytics.instance.logEvent(name: 'join_max_players_error');
        widget.showErrorDialog(
            msg: LocaleKeys.welcome_error_maxPlayersCount.tr());
        break;
      case 'update_required':
        FirebaseAnalytics.instance.logEvent(name: 'join_update_required_error');
        final AppRouter appRouter = getIt<AppRouter>();
        appRouter.push(
          InfoAlert(
            canPop: false,
            message: LocaleKeys.alerts_updateRequired_title.tr(),
            subtitle: LocaleKeys.alerts_updateRequired_subtitle.tr(),
            actions: <InfoAlertAction>[
              InfoAlertAction(
                onTap: (final _) => _navigateToShop(),
                title: LocaleKeys.alerts_updateRequired_openShop.tr(),
              ),
            ],
          ),
        );
        break;
      case 'room_not_found':
        FirebaseAnalytics.instance.logEvent(name: 'join_room_not_found_error');
        widget.showErrorDialog(msg: LocaleKeys.welcome_error_noRoom.tr());
        break;
      case 'room_started':
        FirebaseAnalytics.instance.logEvent(name: 'join_room_started_error');
        widget.showErrorDialog(msg: LocaleKeys.welcome_error_noRoom.tr());
        break;
      case 'incorrect_message':
        logger.e(
            'received incorrect message, most likely message was sent when room was cleaned on backend');
        break;
      default:
        widget.showErrorDialog(
          msg: LocaleKeys.welcome_error_unknown.tr(
            namedArgs: <String, String>{'error': state.error},
          ),
        );
    }
  }

  void _navigateToShop() {
    FirebaseAnalytics.instance.logEvent(name: 'open_app_store');
    OpenStore.instance.open(
      appStoreId: kAppStoreId,
      androidAppBundleId: 'com.kumple',
    );
  }
}

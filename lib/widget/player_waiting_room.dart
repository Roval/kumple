import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:shimmer/shimmer.dart';

import '../gen/assets.gen.dart';
import '../gen/colors.gen.dart';
import '../models/event.dart';
import '../utils/element_order.dart';
import 'avatars.dart';
import 'default_container.dart';
import 'highlight_button.dart';
import 'player_row.dart';

enum PlayerWaitingState { ready, waiting }

class PlayerWaitingRow extends StatelessWidget {
  const PlayerWaitingRow({
    final Key? key,
    required this.player,
    required this.state,
    required this.order,
    required this.subtitle,
  }) : super(key: key);

  final Player player;
  final PlayerWaitingState state;
  final ElementOrder order;
  final String subtitle;

  @override
  Widget build(final BuildContext context) {
    return PlayerRow(
      player: player,
      order: order,
      rightChild: state == PlayerWaitingState.ready
          ? Assets.images.selectedIcon.image(fit: BoxFit.fitWidth)
          : Lottie.asset(Assets.animations.spinner, height: 16, width: 16),
      subtitle: PlayerRowSubtitle(
        text: subtitle,
        style: Theme.of(context).textTheme.bodyMedium?.copyWith(
              color: state == PlayerWaitingState.ready
                  ? ColorName.text
                  : ColorName.grey,
            ),
      ),
    );
  }
}

class PlayerEmptySlot extends StatelessWidget {
  const PlayerEmptySlot({
    final Key? key,
    required this.title,
    required this.onShare,
  }) : super(key: key);

  final String title;
  final Function() onShare;

  @override
  Widget build(final BuildContext context) {
    return HighlightButton(
        onTap: onShare,
        builder: (final BuildContext context, final bool highlight) {
          return DefaultContainer(
            margin: const EdgeInsets.symmetric(vertical: 6, horizontal: 24),
            padding: const EdgeInsets.symmetric(vertical: 8, horizontal: 16),
            child: Opacity(
              opacity: highlight ? 0.5 : 1,
              child: Shimmer.fromColors(
                loop: 1,
                baseColor: Colors.black,
                highlightColor: ColorName.secondaryBackground,
                period: const Duration(milliseconds: 2500),
                child: Row(
                  children: <Widget>[
                    const EmptyAvatar(color: Colors.black),
                    const SizedBox(width: 16),
                    Expanded(
                      child: Text(
                        title,
                        textAlign: TextAlign.left,
                        style: Theme.of(context).textTheme.titleLarge,
                      ),
                    ),
                    const SizedBox(width: 16),
                    Assets.images.gameArrowIcon.image(color: Colors.black)
                  ],
                ),
              ),
            ),
          );
        });
  }
}

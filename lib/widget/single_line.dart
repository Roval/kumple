import 'package:flutter/material.dart';

import '../gen/colors.gen.dart';

class SingleLine extends StatelessWidget {
  const SingleLine({
    final Key? key,
    this.color = Colors.black,
    this.margin = EdgeInsets.zero,
    this.height = 1,
    this.roundCorners = false,
  }) : super(key: key);

  final Color color;
  final EdgeInsets margin;
  final double height;
  final bool roundCorners;

  @override
  Widget build(final BuildContext context) {
    return Container(
      height: height,
      margin: margin,
      decoration: BoxDecoration(
        color: color,
        borderRadius: roundCorners ? BorderRadius.circular(height / 2) : null,
      ),
    );
  }
}

class VerticalLine extends StatelessWidget {
  const VerticalLine({
    final Key? key,
    this.color = ColorName.grey,
    this.margin = const EdgeInsets.symmetric(vertical: 8),
  }) : super(key: key);

  final Color color;
  final EdgeInsets margin;

  @override
  Widget build(final BuildContext context) {
    return Container(
      color: color,
      width: 1,
      margin: margin,
    );
  }
}

import 'package:flutter/material.dart';

import '../gen/colors.gen.dart';
import '../utils/app_theme.dart';
import 'drag_indicator.dart';

// ignore: avoid_classes_with_only_static_members
class ModalContainerViewConfig {
  static double dragHeight = DragIndicatorViewConfig.height +
      Defaults.dockDragIndicatorTopMargin +
      Defaults.dockDragIndicatorBottomMargin;
  static const double horizontalPadding = 24;
}

class ModalContainer extends StatelessWidget {
  const ModalContainer({
    super.key,
    required this.child,
    this.showDragIndicator = true,
    this.backgroundColor = ColorName.secondaryBackground,
    this.dragOpacity = 1,
    this.padding = const EdgeInsets.symmetric(
        horizontal: ModalContainerViewConfig.horizontalPadding),
  });

  final bool showDragIndicator;
  final Color backgroundColor;
  final EdgeInsets padding;
  final double dragOpacity;
  final Widget child;

  @override
  Widget build(final BuildContext context) {
    return Container(
      padding: padding,
      decoration: BoxDecoration(
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
        color: backgroundColor,
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const SizedBox(height: Defaults.dockDragIndicatorTopMargin),
          if (showDragIndicator)
            Opacity(opacity: dragOpacity, child: const DragIndicator()),
          const SizedBox(height: Defaults.dockDragIndicatorBottomMargin),
          child,
        ],
      ),
    );
  }
}

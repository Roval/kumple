import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:perfect_freehand/perfect_freehand.dart';

import '../extensions/key_extensions.dart';
import '../gen/assets.gen.dart';
import '../utils/app_theme.dart';
import 'calculating_size.dart';
import 'image_button.dart';
import 'sketcher.dart';

// ignore: avoid_classes_with_only_static_members
class DrawQuestionViewConfig {
  static const List<Color> colors = <Color>[
    Colors.white,
    Colors.black,
    Color(0xFF5f7d8c),
    Color(0xFF9e9e9e),
    Color(0xFF795548),
    Color(0xFFff5723),
    Color(0xFFff9800),
    Color(0xFFffc107),
    Color(0xFFffec3c),
    Color(0xFFcddc3a),
    Color(0xFF8bc349),
    Color(0xFF4cb051),
    Color(0xFF009688),
    Color(0xFF01bcd6),
    Color(0xFF04a9f3),
    Color(0xFF2196f3),
    Color(0xFF3f51b6),
    Color(0xFF683ab7),
    Color(0xFF9d27b0),
    Color(0xFFe91f63),
    Color(0xFFf54336),
  ];

  static const double buttonDimension = 40;
  static const double verticalPadding = 24;
  static const double horizontalPadding = 24;
}

class DrawContainer extends StatefulWidget {
  const DrawContainer({
    final Key? key,
    required this.controller,
    this.backgroundUrl,
  }) : super(key: key);

  final DrawController controller;
  final String? backgroundUrl;

  @override
  State<DrawContainer> createState() =>
      // ignore: no_logic_in_create_state
      _DrawContainerState(controller);
}

class _DrawContainerState extends State<DrawContainer> {
  _DrawContainerState(this._drawController) {
    _drawController._addState(this);
  }

  List<Stroke> _lines = <Stroke>[];
  Stroke? _line;
  StrokeOptions _options = StrokeOptions();
  final StreamController<Stroke> _currentLineStreamController =
      StreamController<Stroke>.broadcast();
  final StreamController<List<Stroke>> _linesStreamController =
      StreamController<List<Stroke>>.broadcast();
  final GlobalKey _paintingKey = GlobalKey();

  double _paintingViewHeight = 0;

  double _currentOpacity = 1;
  double _currentSize = 5;
  Color _currentColor = Colors.black;
  bool _colorSelectionVisible = false;
  bool _opacitySelectionVisible = false;
  bool _sizeSelectionVisible = false;

  final DrawController _drawController;

  @override
  void dispose() {
    _linesStreamController.close();
    _currentLineStreamController.close();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return CalculatingSize(
      keyToCalculate: _paintingKey,
      onSizeCalculated: (final ui.Size size) {
        setState(() {
          _paintingViewHeight = size.height;
        });
      },
      child: Stack(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              const SizedBox(
                  height: DrawQuestionViewConfig.buttonDimension +
                      DrawQuestionViewConfig.verticalPadding),
              Flexible(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Flexible(
                      child: AspectRatio(
                        aspectRatio: 1,
                        child: Stack(
                          children: <Widget>[
                            RepaintBoundary(
                              key: _paintingKey,
                              child: Container(
                                clipBehavior: Clip.hardEdge,
                                decoration: BoxDecoration(
                                  borderRadius: Defaults.cornerRadius,
                                ),
                                child: Stack(
                                  children: <Widget>[
                                    Container(
                                      color: Colors.white,
                                    ),
                                    if (widget.backgroundUrl != null)
                                      CachedNetworkImage(
                                        imageUrl: widget.backgroundUrl!,
                                        fit: BoxFit.cover,
                                      ),
                                    _buildAllPaths(context),
                                  ],
                                ),
                              ),
                            ),
                            _buildCurrentPath(context),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          _buildControls(context),
        ],
      ),
    );
  }

  Widget _buildControls(final BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 24),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            children: <Widget>[
              GestureDetector(
                onTap: _toggleColors,
                child: _DrawActionButton(color: _currentColor),
              ),
              const SizedBox(height: 16),
              Visibility(
                visible: _colorSelectionVisible,
                child: _ColorsPanel(
                  onColorSelected: (final Color c) => _selectColor(c),
                  currentColor: _currentColor,
                ),
              ),
            ],
          ),
          const SizedBox(width: 16),
          Column(
            children: <Widget>[
              Container(
                width: DrawQuestionViewConfig.buttonDimension,
                height: DrawQuestionViewConfig.buttonDimension,
                clipBehavior: Clip.hardEdge,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(
                      DrawQuestionViewConfig.buttonDimension / 2),
                  border: Border.all(
                    width: 2,
                  ),
                ),
                child: Stack(
                  children: <Widget>[
                    Container(
                      width: DrawQuestionViewConfig.buttonDimension / 2,
                      height: DrawQuestionViewConfig.buttonDimension,
                      decoration: BoxDecoration(
                        color: _currentColor.withOpacity(_currentOpacity),
                        borderRadius: const BorderRadius.only(
                          topLeft: Radius.circular(
                              DrawQuestionViewConfig.buttonDimension / 2),
                          bottomLeft: Radius.circular(
                              DrawQuestionViewConfig.buttonDimension / 2),
                        ),
                      ),
                    ),
                    Center(
                      child: ImageButton(
                        image: Assets.images.eyeIcon,
                        onPressed: _toggleOpacitySlider,
                        shouldThrottle: false,
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 16),
              Visibility(
                visible: _opacitySelectionVisible,
                child: _OpacityPanel(
                  onOpacitySelected: (final double c) => _selectOpacity(c),
                  currentOpacity: _currentOpacity,
                ),
              ),
            ],
          ),
          const SizedBox(width: 16),
          Column(
            children: <Widget>[
              _DrawActionButton(
                child: ImageButton(
                  image: Assets.images.penIcon,
                  onPressed: _toggleSizeSlider,
                  shouldThrottle: false,
                ),
              ),
              const SizedBox(height: 16),
              Visibility(
                visible: _sizeSelectionVisible,
                child: _SizePanel(
                  onSizeSelected: (final double c) => _selectSize(c),
                  currentSize: _currentSize,
                ),
              ),
            ],
          ),
          const SizedBox(width: 16),
          Opacity(
            opacity: _lines.isEmpty ? 0.3 : 1,
            child: _DrawActionButton(
              child: ImageButton(
                image: Assets.images.ctrlzIcon,
                onPressed: _revert,
                shouldThrottle: false,
              ),
            ),
          ),
          const SizedBox(width: 16),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Opacity(
                  opacity: _lines.isEmpty ? 0.3 : 1,
                  child: _DrawActionButton(
                    child: ImageButton(
                      image: Assets.images.clearIcon,
                      onPressed: _clear,
                      shouldThrottle: false,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildAllPaths(final BuildContext context) {
    return RepaintBoundary(
      child: Container(
        decoration: BoxDecoration(borderRadius: Defaults.cornerRadius),
        clipBehavior: Clip.hardEdge,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: StreamBuilder<List<Stroke>>(
          stream: _linesStreamController.stream,
          builder: (final BuildContext context,
              final AsyncSnapshot<List<Stroke>> snapshot) {
            return CustomPaint(
              painter: Sketcher(
                lines: _lines,
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _buildCurrentPath(final BuildContext context) {
    return Listener(
      onPointerDown: onPointerDown,
      onPointerMove: onPointerMove,
      onPointerUp: onPointerUp,
      child: RepaintBoundary(
        child: Container(
          color: Colors.transparent,
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: StreamBuilder<Stroke>(
            stream: _currentLineStreamController.stream,
            builder: (final BuildContext context,
                final AsyncSnapshot<Stroke> snapshot) {
              return CustomPaint(
                painter: Sketcher(
                  lines: _line == null ? <Stroke>[] : <Stroke>[_line!],
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  void _clear() {
    setState(() {
      _lines = <Stroke>[];
      _line = null;
    });
    widget.controller._setPaintingState(false);
  }

  void _toggleColors() {
    FirebaseAnalytics.instance.logEvent(name: 'drawColorToggleClicked');
    setState(() {
      _colorSelectionVisible = !_colorSelectionVisible;
    });
  }

  void _toggleOpacitySlider() {
    FirebaseAnalytics.instance.logEvent(name: 'drawOpacityToggleClicked');
    setState(() {
      _opacitySelectionVisible = !_opacitySelectionVisible;
    });
  }

  void _toggleSizeSlider() {
    FirebaseAnalytics.instance.logEvent(name: 'drawSizeToggleClicked');
    setState(() {
      _sizeSelectionVisible = !_sizeSelectionVisible;
    });
  }

  void _selectColor(final Color color) {
    setState(() {
      _currentColor = color;
      _colorSelectionVisible = false;
    });
  }

  void _selectSize(final double size) {
    setState(() {
      _currentSize = size;
    });
  }

  void _selectOpacity(final double opacity) {
    setState(() {
      _currentOpacity = opacity;
    });
  }

  void _hidePanels() {
    if (!_colorSelectionVisible &&
        !_sizeSelectionVisible &&
        !_opacitySelectionVisible) {
      return;
    }
    setState(() {
      _colorSelectionVisible = false;
      _opacitySelectionVisible = false;
      _sizeSelectionVisible = false;
    });
  }

  void _revert() {
    if (_lines.isNotEmpty) {
      _lines.removeLast();
    }
    if (_lines.isEmpty) {
      widget.controller._setPaintingState(false);
    }
    setState(() {});
  }

  double _normalizeDy(final double dy) {
    return dy -
        (DrawQuestionViewConfig.buttonDimension +
            DrawQuestionViewConfig.verticalPadding);
  }

  double _normalizeDx(final double dx) {
    final double width = MediaQuery.of(context).size.width -
        (2 * DrawQuestionViewConfig.horizontalPadding);
    return dx - ((width - _paintingViewHeight) / 2);
  }

  double _restrictToBoundries(final double offset) {
    return offset < 0
        ? (_currentSize / 2).ceilToDouble()
        : offset > _paintingViewHeight
            ? _paintingViewHeight - (_currentSize / 2).ceilToDouble()
            : offset;
  }

  void onPointerDown(final PointerDownEvent details) {
    _options = StrokeOptions(
      color: _currentColor.withOpacity(_currentOpacity),
      size: _currentSize,
    );

    // ignore: cast_nullable_to_non_nullable
    final RenderBox box = context.findRenderObject() as RenderBox;
    final Offset offset = box.globalToLocal(details.position);
    final Point point = Point(
      _restrictToBoundries(_normalizeDx(offset.dx)),
      _restrictToBoundries(_normalizeDy(offset.dy)),
    );
    final List<Point> points = <Point>[point];
    _line = Stroke(points, _options);
    _currentLineStreamController.add(_line!);
  }

  void onPointerMove(final PointerMoveEvent details) {
    if (_line == null) {
      // Possible when clear clicked during drawing
      return;
    }
    _hidePanels();

    // ignore: cast_nullable_to_non_nullable
    final RenderBox box = context.findRenderObject() as RenderBox;
    final Offset offset = box.globalToLocal(details.position);
    final double dy = _normalizeDy(offset.dy);
    final double dx = _normalizeDx(offset.dx);
    final Point point = Point(
      _restrictToBoundries(dx),
      _restrictToBoundries(dy),
    );
    final List<Point> points = <Point>[..._line!.points, point];
    _line = Stroke(points, _options);
    _currentLineStreamController.add(_line!);
  }

  void onPointerUp(final PointerUpEvent details) {
    if (_line == null) {
      // Possible when clear clicked during drawing
      return;
    }
    _hidePanels();

    _lines = List<Stroke>.from(_lines)..add(_line!);
    _linesStreamController.add(_lines);
    setState(() {
      _line = null;
    });
    widget.controller._setPaintingState(true);
  }
}

class _DrawActionButton extends StatelessWidget {
  const _DrawActionButton({
    final Key? key,
    this.color = Colors.white,
    this.child,
  }) : super(key: key);

  final Color color;
  final Widget? child;

  @override
  Widget build(final BuildContext context) {
    return Container(
      height: DrawQuestionViewConfig.buttonDimension,
      width: DrawQuestionViewConfig.buttonDimension,
      decoration: BoxDecoration(
        borderRadius:
            BorderRadius.circular(DrawQuestionViewConfig.buttonDimension / 2),
        border: Border.all(
          width: 2,
        ),
        color: color,
      ),
      child: child,
    );
  }
}

class _ColorDot extends StatelessWidget {
  const _ColorDot({
    final Key? key,
    required this.color,
    required this.isSelected,
  }) : super(key: key);

  final Color color;
  final bool isSelected;

  @override
  Widget build(final BuildContext context) {
    return Container(
      height: 30,
      width: 30,
      decoration: BoxDecoration(
        border: Border.all(
          width: 2,
          color: isSelected ? Colors.white.withAlpha(80) : color,
        ),
        borderRadius: BorderRadius.circular(15),
        color: color,
      ),
    );
  }
}

class _ColorsPanel extends StatelessWidget {
  const _ColorsPanel({
    final Key? key,
    required this.onColorSelected,
    required this.currentColor,
  }) : super(key: key);

  final Function(Color) onColorSelected;
  final Color? currentColor;

  @override
  Widget build(final BuildContext context) {
    return Container(
      width: 40,
      height: 210,
      clipBehavior: Clip.hardEdge,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.black.withAlpha(200),
      ),
      child: ListView.separated(
        padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 16),
        itemCount: DrawQuestionViewConfig.colors.length,
        itemBuilder: (final BuildContext context, final int index) {
          final Color color = DrawQuestionViewConfig.colors[index];
          return GestureDetector(
            onTap: () {
              onColorSelected(color);
            },
            child: _ColorDot(
              isSelected: color == currentColor,
              color: color,
            ),
          );
        },
        separatorBuilder: (final BuildContext context, final int index) {
          return const SizedBox(height: 16);
        },
      ),
    );
  }
}

class _SizePanel extends StatelessWidget {
  const _SizePanel(
      {final Key? key, required this.onSizeSelected, required this.currentSize})
      : super(key: key);

  final Function(double) onSizeSelected;
  final double currentSize;

  @override
  Widget build(final BuildContext context) {
    return Container(
      width: 40,
      height: 210,
      clipBehavior: Clip.hardEdge,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.black.withAlpha(200),
      ),
      child: RotatedBox(
        quarterTurns: 1,
        child: Slider(
          min: 2,
          max: 12,
          value: currentSize,
          activeColor: Colors.white,
          inactiveColor: Colors.white38,
          onChanged: (final double value) {
            onSizeSelected(value);
          },
        ),
      ),
    );
  }
}

class _OpacityPanel extends StatelessWidget {
  const _OpacityPanel(
      {final Key? key,
      required this.onOpacitySelected,
      required this.currentOpacity})
      : super(key: key);

  final Function(double) onOpacitySelected;
  final double currentOpacity;

  @override
  Widget build(final BuildContext context) {
    return Container(
      width: 40,
      height: 210,
      clipBehavior: Clip.hardEdge,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.black.withAlpha(200),
      ),
      child: RotatedBox(
        quarterTurns: 1,
        child: Slider(
          value: currentOpacity,
          activeColor: Colors.white,
          inactiveColor: Colors.white38,
          onChanged: (final double value) {
            onOpacitySelected(value);
          },
        ),
      ),
    );
  }
}

class DrawController extends ChangeNotifier {
  bool paintingExists = false;

  late _DrawContainerState _state;

  // ignore: use_setters_to_change_properties
  void _addState(final _DrawContainerState state) {
    _state = state;
  }

  void _setPaintingState(final bool exists) {
    paintingExists = exists;
    notifyListeners();
  }

  Future<Uint8List?> takeScreenshot() async {
    return _state._paintingKey.capturePng();
  }
}

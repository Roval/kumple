import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class ImageInGrid extends StatelessWidget {
  const ImageInGrid({
    final Key? key,
    required this.url,
    required this.isHighlighted,
  }) : super(key: key);

  final String url;
  final bool isHighlighted;

  @override
  Widget build(final BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        image: DecorationImage(
          fit: BoxFit.cover,
          image: CachedNetworkImageProvider(url),
        ),
      ),
    );
  }
}

import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';

import '../gen/colors.gen.dart';

const double _kAvatarState = 40;

class Avatar extends StatelessWidget {
  const Avatar({
    final Key? key,
    this.imageUrl,
    this.imageData,
    this.dimension = _kAvatarState,
  }) : super(key: key);

  final double dimension;
  final String? imageUrl;
  final Uint8List? imageData;

  @override
  Widget build(final BuildContext context) {
    return SizedBox(
      height: dimension,
      width: dimension,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(dimension / 2),
        child: imageData != null
            ? Image.memory(
                imageData!,
                fit: BoxFit.cover,
              )
            : CachedNetworkImage(
                imageUrl: imageUrl!,
                fit: BoxFit.cover,
                placeholder: (final BuildContext context, final _) =>
                    Container(color: ColorName.lightGrey),
              ),
      ),
    );
  }
}

class EmptyAvatar extends StatelessWidget {
  const EmptyAvatar({
    final Key? key,
    this.dimension = _kAvatarState,
    this.dashPattern = const <double>[4, 4],
    this.strokeWidth = 2,
    this.color = ColorName.grey,
  }) : super(key: key);

  final double dimension;
  final double strokeWidth;
  final Color color;
  final List<double> dashPattern;

  @override
  Widget build(final BuildContext context) {
    return SizedBox(
      height: dimension,
      width: dimension,
      child: Center(
        child: SizedBox(
          height: dimension - strokeWidth,
          width: dimension - strokeWidth,
          child: DottedBorder(
            padding: EdgeInsets.zero,
            color: color,
            dashPattern: dashPattern,
            strokeWidth: strokeWidth,
            borderType: BorderType.Oval,
            child: Container(),
          ),
        ),
      ),
    );
  }
}

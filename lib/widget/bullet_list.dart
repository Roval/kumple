import 'dart:math';

import 'package:flutter/material.dart';

class BulletList extends StatelessWidget {
  const BulletList({
    final Key? key,
    required this.strings,
  }) : super(key: key);

  final List<String> strings;

  @override
  Widget build(final BuildContext context) {
    final int maxLinesPerAnswer = max(4 / strings.length, 1).toInt();

    return Container(
      alignment: Alignment.centerLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: strings.map((final String str) {
          return Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              if (strings.length > 1) ...<Widget>[
                Text(
                  '\u2022',
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
                const SizedBox(width: 8),
              ],
              Expanded(
                child: Text(
                  str,
                  maxLines: maxLinesPerAnswer,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.headlineMedium,
                ),
              ),
            ],
          );
        }).toList(),
      ),
    );
  }
}

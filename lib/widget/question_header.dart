import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../extensions/string_extensions.dart';
import '../utils/device_util.dart';
import 'game_header.dart';

class QuestionHeader extends StatelessWidget {
  const QuestionHeader({
    final Key? key,
    required this.color,
    required this.question,
    this.smallFont = false,
    this.imageUrl,
    this.onImageTap,
  }) : super(key: key);

  final String question;
  final bool smallFont;
  final Color color;
  final String? imageUrl;
  final Function()? onImageTap;

  @override
  Widget build(final BuildContext context) {
    final TextStyle? bigStyle = isSmallDevice
        ? Theme.of(context).textTheme.headlineLarge
        : Theme.of(context).textTheme.displaySmall;
    final TextStyle? smallStyle = isSmallDevice
        ? Theme.of(context).textTheme.bodyMedium
        : Theme.of(context).textTheme.headlineMedium;

    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: color,
        borderRadius: const BorderRadius.only(
          bottomLeft: Radius.circular(20),
          bottomRight: Radius.circular(20),
        ),
      ),
      padding: const EdgeInsets.fromLTRB(24, 8, 24, 18),
      child: Row(
        children: <Widget>[
          Expanded(
            child: AnimatedDefaultTextStyle(
              duration: const Duration(milliseconds: 100),
              curve: Curves.bounceIn,
              style: smallFont
                  ? smallStyle!.copyWith(color: Colors.white)
                  : bigStyle!.copyWith(color: Colors.white),
              child: Text(
                question.breaking,
                textAlign: TextAlign.left,
              ),
            ),
          ),
          if (imageUrl != null)
            Row(
              children: <Widget>[
                const SizedBox(width: 16),
                GestureDetector(
                  onTap: onImageTap,
                  child: Container(
                      clipBehavior: Clip.hardEdge,
                      decoration: BoxDecoration(
                        color: color,
                        borderRadius: BorderRadius.circular(5),
                      ),
                      height: isSmallDevice ? 50 : 80,
                      width: isSmallDevice ? 50 : 80,
                      child: CachedNetworkImage(imageUrl: imageUrl!)),
                ),
              ],
            )
        ],
      ),
    );
  }
}

class BlankQuestionHeader extends StatelessWidget {
  const BlankQuestionHeader({
    final Key? key,
    required this.color,
  }) : super(key: key);

  final Color color;

  @override
  Widget build(final BuildContext context) {
    return AnimatedContainer(
      duration: const Duration(
          milliseconds: GameHeaderViewConfig.colorChangeDuration),
      curve: Curves.fastOutSlowIn,
      width: double.infinity,
      decoration: BoxDecoration(
        color: color,
        borderRadius: const BorderRadius.only(
          bottomLeft: Radius.circular(20),
          bottomRight: Radius.circular(20),
        ),
      ),
      padding: const EdgeInsets.fromLTRB(24, 16, 24, 32),
      child: const Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _BlankAnswer(
            fillPercentage: 0.67,
          ),
          SizedBox(height: 10),
          _BlankAnswer(
            fillPercentage: 0.81,
          ),
          SizedBox(height: 10),
          _BlankAnswer(
            fillPercentage: 0.42,
          )
        ],
      ),
    );
  }
}

class _BlankAnswer extends StatelessWidget {
  const _BlankAnswer({
    final Key? key,
    required this.fillPercentage,
  }) : super(key: key);

  final double fillPercentage;

  @override
  Widget build(final BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.white.withAlpha(76),
      ),
      height: 25,
      width: MediaQuery.of(context).size.width * fillPercentage,
    );
  }
}

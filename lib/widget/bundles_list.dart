import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubits/store_buy_cubit.dart';
import '../cubits/store_cubit.dart';
import '../cubits/store_state.dart';
import '../di/dependencies.dart';
import '../gen/assets.gen.dart';
import '../gen/colors.gen.dart';
import '../gen/translations.gen.dart';
import '../models/store.dart';
import '../utils/app_theme.dart';
import '../utils/element_order.dart';
import '../utils/locale_utils.dart';
import 'bundles_grid.dart';
import 'checkbox.dart';
import 'default_container.dart';
import 'scroll_with_line.dart';

class BundlesList extends StatefulWidget {
  const BundlesList({
    final Key? key,
    required this.selectedBundles,
    required this.onBundleToggle,
  }) : super(key: key);

  final List<String> selectedBundles;
  final Function(String)? onBundleToggle;

  @override
  State<BundlesList> createState() => _BundlesListState();
}

class _BundlesListState extends State<BundlesList> {
  @override
  Widget build(final BuildContext context) {
    return BlocBuilder<StoreCubit, StoreState>(
      builder: (final _, final StoreState state) {
        if (state is FetchStoreState) {
          final List<StoreItem> items = state.response.all();

          if (items.isEmpty) {
            return Container();
          }

          return ScrollWithLine(
            child: ListView.separated(
              padding: EdgeInsets.only(bottom: Defaults.buttomSpacingScroll),
              itemCount: items.isEmpty
                  ? 0
                  : items.length + (state.shouldDisplayUnlockAll ? 1 : 0),
              separatorBuilder: (final BuildContext context, final int index) =>
                  const SizedBox(height: 0),
              itemBuilder: (final BuildContext context, int index) {
                if (index == 0 && state.shouldDisplayUnlockAll) {
                  // Buy all row
                  return _UnlockAllProduct(
                    item: state.response.unlockAll,
                    onBuyTap: _buyProduct,
                    price: state.prices[state.response.unlockAll.iap] ?? '...',
                    fullPrice: state.priceForAllString,
                  );
                } else if (state.shouldDisplayUnlockAll) {
                  index -= 1;
                }

                final StoreItem item = items[index];
                return _StoreProduct(
                  order: index == 0
                      ? (state.shouldDisplayUnlockAll
                          ? ElementOrder.middle
                          : ElementOrder.first)
                      : index == items.length - 1
                          ? ElementOrder.last
                          : ElementOrder.middle,
                  item: item,
                  price: state.prices[item.iap] ?? '...',
                  onBuyTap: _buyProduct,
                  onTap: (final StoreItem item) {
                    widget.onBundleToggle?.call(item.bundle);
                  },
                  isSeasonal: state.response.seasonal.contains(item),
                  isTemporaryFree:
                      state.temporaryFreeInStoreIaps.contains(item.iap),
                  isEnabled: widget.selectedBundles.contains(item.bundle),
                  isBought: state.unlockedProductsIaps.contains(item.iap),
                );
              },
            ),
          );
        }
        return Container();
      },
    );
  }

  void _buyProduct(final IapItem item) {
    getIt<StoreBuyCubit>().buyProduct(
      productId: item.iap,
      source: BuySource.newGame,
    );
  }
}

class _StoreProduct extends StatelessWidget {
  const _StoreProduct({
    final Key? key,
    required this.item,
    required this.onBuyTap,
    required this.isEnabled,
    required this.isBought,
    required this.isSeasonal,
    required this.isTemporaryFree,
    required this.price,
    required this.onTap,
    required this.order,
  }) : super(key: key);

  final Function(StoreItem) onBuyTap;
  final Function(StoreItem)? onTap;
  final StoreItem item;
  final bool isBought;
  final bool isSeasonal;
  final bool isTemporaryFree;
  final bool isEnabled;
  final String price;
  final ElementOrder order;

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (isBought) {
          onTap?.call(item);
        } else {
          onBuyTap(item);
        }
      },
      child: DefaultContainer(
        padding: const EdgeInsets.symmetric(
          horizontal: DefaultContainerConfig.horizontalPadding,
          vertical: 12,
        ),
        order: order,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Row(
                children: <Widget>[
                  SizedBox(height: item.backgroundUrl != null ? 0 : 16),
                  Stack(
                    children: <Widget>[
                      Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                          color: item.color,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        padding: const EdgeInsets.all(8),
                        child: CachedNetworkImage(
                          imageUrl: item.imageUrl,
                          fit: BoxFit.contain,
                        ),
                      ),
                      if (isTemporaryFree)
                        Positioned(
                          right: 0,
                          bottom: 0,
                          child: Assets.images.freeIcon.image(),
                        ),
                    ],
                  ),
                  const SizedBox(width: 16),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Text(
                          localisedStoreItemTitle(item),
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: Theme.of(context).textTheme.titleLarge,
                        ),
                        if (isTemporaryFree || isSeasonal)
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Assets.images.timerIcon
                                  .image(color: ColorName.grey),
                              const SizedBox(width: 8),
                              Padding(
                                padding: const EdgeInsets.only(top: 1),
                                child: Text(
                                  hintForItem(item, isSeasonal: isSeasonal),
                                  textAlign: TextAlign.left,
                                  overflow: TextOverflow.ellipsis,
                                  style: Theme.of(context)
                                      .textTheme
                                      .labelLarge
                                      ?.copyWith(color: ColorName.grey),
                                ),
                              ),
                            ],
                          )
                        else if (!isBought)
                          Text(price,
                              textAlign: TextAlign.left,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context).textTheme.bodyMedium),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            if (isBought)
              GameCheckbox(isSelected: isEnabled)
            else
              Assets.images.cartListIcon.image(fit: BoxFit.none)
          ],
        ),
      ),
    );
  }
}

class _UnlockAllProduct extends StatelessWidget {
  const _UnlockAllProduct({
    final Key? key,
    required this.item,
    required this.onBuyTap,
    required this.price,
    required this.fullPrice,
  }) : super(key: key);

  final Function(IapItem) onBuyTap;
  final IapItem item;
  final String fullPrice;
  final String price;

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
      onTap: () {
        onBuyTap(item);
      },
      child: DefaultContainer(
        padding: EdgeInsets.zero,
        color: ColorName.questionColor5,
        order: ElementOrder.first,
        // margin: const EdgeInsets.all(12),
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: DefaultContainerConfig.horizontalPadding,
                vertical: 12,
              ),
              child: Row(
                children: <Widget>[
                  const SizedBox(height: 16),
                  Stack(
                    children: <Widget>[
                      Container(
                        width: 50,
                        height: 50,
                        decoration: BoxDecoration(
                          color: ColorName.paleRed,
                          borderRadius: BorderRadius.circular(5),
                        ),
                        padding: const EdgeInsets.all(8),
                        child: Assets.images.unlockAllIcon.image(),
                      ),
                    ],
                  ),
                  const SizedBox(width: 16),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Text(
                          LocaleKeys.store_unlockAllShort.tr(),
                          textAlign: TextAlign.left,
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          style: Theme.of(context)
                              .textTheme
                              .headlineMedium
                              ?.copyWith(
                                color: Colors.white,
                              ),
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Stack(children: <Widget>[
                              Text(
                                fullPrice,
                                textAlign: TextAlign.center,
                                overflow: TextOverflow.ellipsis,
                                style: Theme.of(context)
                                    .textTheme
                                    .bodyLarge
                                    ?.copyWith(
                                      color: Colors.white,
                                    ),
                              ),
                              Positioned(
                                left: -2,
                                right: -2,
                                top: 0,
                                bottom: 0,
                                child: Center(
                                  child:
                                      Container(color: Colors.white, height: 2),
                                ),
                              )
                            ]),
                            const SizedBox(width: 8),
                            Text(
                              price,
                              textAlign: TextAlign.center,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context)
                                  .textTheme
                                  .bodyLarge
                                  ?.copyWith(
                                    color: Colors.white,
                                  ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
                top: 0,
                bottom: 0,
                right: 0,
                child:
                    Assets.images.unlockAllBgSmall.image(fit: BoxFit.fitHeight))
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import '../extensions/key_extensions.dart';

class CalculatingSize extends StatefulWidget {
  const CalculatingSize({
    super.key,
    required this.keyToCalculate,
    required this.onSizeCalculated,
    required this.child,
    this.startCalculating = true,
  });

  final GlobalKey keyToCalculate;
  final Function(Size) onSizeCalculated;
  final Widget child;
  final bool startCalculating;

  @override
  State<CalculatingSize> createState() => _CalculatingSizeState();
}

class _CalculatingSizeState extends State<CalculatingSize> {
  bool _firstRun = true;

  @override
  Widget build(final BuildContext context) {
    _calculateSizes();
    return widget.child;
  }

  void _calculateSizes() {
    if (_firstRun && widget.startCalculating) {
      SchedulerBinding.instance.addPostFrameCallback((final _) {
        final Size size = widget.keyToCalculate.size;

        widget.onSizeCalculated(size);
      });
      _firstRun = false;
    }
  }
}

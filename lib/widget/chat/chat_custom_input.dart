import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;
import 'package:flutter_chat_ui/flutter_chat_ui.dart';

import '../../gen/assets.gen.dart';
import '../../gen/colors.gen.dart';
import '../../gen/translations.gen.dart';
import '../../utils/app_theme.dart';
import '../image_button.dart';

/// A class that represents bottom bar widget with a text field, attachment and
/// send buttons inside. By default hides send button when text field is empty.
class CustomChatInput extends StatefulWidget {
  /// Creates [CustomChatInput] widget.
  const CustomChatInput({
    super.key,
    required this.onSendPressed,
  });

  /// Will be called on [SendButton] tap. Has [types.PartialText] which can
  /// be transformed to [types.TextMessage] and added to the messages list.
  final void Function(types.PartialText) onSendPressed;

  @override
  State<CustomChatInput> createState() => _CustomChatInputState();
}

/// [CustomChatInput] widget state.
class _CustomChatInputState extends State<CustomChatInput> {
  late final FocusNode _inputFocusNode = FocusNode(
    onKeyEvent: (final FocusNode node, final KeyEvent event) {
      if (event.physicalKey == PhysicalKeyboardKey.enter &&
          !HardwareKeyboard.instance.physicalKeysPressed.any(
            (final PhysicalKeyboardKey el) => <PhysicalKeyboardKey>{
              PhysicalKeyboardKey.shiftLeft,
              PhysicalKeyboardKey.shiftRight,
            }.contains(el),
          )) {
        if (event is KeyDownEvent) {
          _handleSendPressed();
        }
        return KeyEventResult.handled;
      } else {
        return KeyEventResult.ignored;
      }
    },
  );

  bool _sendButtonVisible = false;
  late TextEditingController _textController;

  @override
  void initState() {
    super.initState();

    _textController = InputTextFieldController();
    _textController.addListener(_handleTextControllerChange);
  }

  @override
  void dispose() {
    _inputFocusNode.dispose();
    _textController.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) => GestureDetector(
        onTap: () => _inputFocusNode.requestFocus(),
        child: _inputBuilder(),
      );

  void _handleSendPressed() {
    final String trimmedText = _textController.text.trim();
    if (trimmedText != '') {
      final types.PartialText partialText =
          types.PartialText(text: trimmedText);
      widget.onSendPressed(partialText);

      _textController.clear();
    }
  }

  Widget _inputBuilder() {
    final MediaQueryData query = MediaQuery.of(context);
    final EdgeInsets safeAreaInsets = EdgeInsets.fromLTRB(
        12, 12, 12, 12 + query.padding.bottom + query.viewInsets.bottom);

    return Focus(
      autofocus: true,
      child: Material(
        color: ColorName.lightGrey,
        child: Container(
          margin: safeAreaInsets,
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: Defaults.cornerRadius,
                  ),
                  padding: const EdgeInsets.all(10),
                  child: TextField(
                    controller: _textController,
                    inputFormatters: <TextInputFormatter>[
                      LengthLimitingTextInputFormatter(400),
                    ],
                    cursorColor: ColorName.grey,
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      contentPadding: EdgeInsets.zero,
                      isCollapsed: true,
                      hintStyle: Theme.of(context)
                          .textTheme
                          .titleMedium
                          ?.copyWith(color: ColorName.grey),
                      hintText: LocaleKeys.chat_inputHint.tr(),
                    ),
                    focusNode: _inputFocusNode,
                    keyboardType: TextInputType.multiline,
                    maxLines: 5,
                    minLines: 1,
                    autocorrect: false,
                    style: Theme.of(context).textTheme.titleMedium,
                    textCapitalization: TextCapitalization.sentences,
                  ),
                ),
              ),
              if (_sendButtonVisible) const SizedBox(width: 12),
              ConstrainedBox(
                constraints: const BoxConstraints(
                  minHeight: 42,
                ),
                child: Visibility(
                  visible: _sendButtonVisible,
                  child: Container(
                    width: 42,
                    height: 42,
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: Defaults.cornerRadius,
                    ),
                    child: ImageButton(
                      onPressed: _handleSendPressed,
                      image: Assets.images.sendIcon,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _handleTextControllerChange() {
    setState(() {
      _sendButtonVisible = _textController.text.trim() != '';
    });
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../cubits/chat_cubit.dart';
import '../../cubits/chat_state.dart';
import '../../di/dependencies.dart';
import '../../gen/assets.gen.dart';
import '../../gen/colors.gen.dart';
import '../animations.dart';
import '../game_header.dart';
import '../image_button.dart';
import 'chat_module.dart';

// ignore: avoid_classes_with_only_static_members
class _ChatButtonViewConfig {
  static const int animationBadgeDuration = 200;
}

class ChatButton extends StatefulWidget {
  const ChatButton({
    super.key,
    required this.controller,
    required this.roomKey,
    this.shouldBeWhite = true,
  });

  final String roomKey;
  final ChatModuleController controller;
  final bool shouldBeWhite;

  @override
  State<ChatButton> createState() => _ChatButtonState();
}

class _ChatButtonState extends State<ChatButton> with TickerProviderStateMixin {
  late Animation<double> _badgeAnimation;
  late AnimationController _badgeAnimationController;
  bool _hasUnreadMessages = false;
  @override
  void initState() {
    super.initState();

    getIt<ChatCubit>()
        .loadLastSeenMessageInChannel(widget.roomKey)
        .then((final _) => _refreshUnreadState());
    _badgeAnimationController = AnimationController(
      duration: const Duration(
          milliseconds: _ChatButtonViewConfig.animationBadgeDuration),
      vsync: this,
    );
    _badgeAnimation = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(_badgeAnimationController)
      ..addListener(() {
        if (!mounted) {
          return;
        }

        setState(() {});
      });

    widget.controller.addListener(_refreshUnreadState);
  }

  @override
  void dispose() {
    _badgeAnimationController.dispose();
    widget.controller.removeListener(_refreshUnreadState);
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return BlocListener<ChatCubit, ChatState>(
      listenWhen: (final ChatState _, final ChatState current) =>
          current is ChannelState && current.channel == widget.roomKey,
      listener: (final _, final ChatState state) {
        _refreshUnreadState();
      },
      child: Stack(
        clipBehavior: Clip.none,
        children: <Widget>[
          ImageButton(
            onPressed: () {
              setState(() {
                _hasUnreadMessages = false;
              });
              // Start again if it failed on init
              getIt<ChatCubit>().startObservingChannel(widget.roomKey);
              widget.controller.open();
            },
            height: GameHeaderViewConfig.minHeight,
            image: Assets.images.chatIcon,
            tintColor: widget.shouldBeWhite ? Colors.white : Colors.black,
            padding: const EdgeInsets.symmetric(horizontal: 24),
          ),
          if (_hasUnreadMessages)
            Positioned.fill(
              top: -18,
              right: -20,
              child: Align(
                child: PopAnimation(
                  animation: _badgeAnimation,
                  scale: 0.5,
                  child: Container(
                    decoration: BoxDecoration(
                      color: ColorName.red,
                      borderRadius: BorderRadius.circular(6),
                    ),
                    width: 12,
                    height: 12,
                  ),
                ),
              ),
            ),
        ],
      ),
    );
  }

  Future<void> _refreshUnreadState() async {
    if (widget.controller.isOpened) {
      if (_hasUnreadMessages) {
        setState(() {
          _hasUnreadMessages = false;
        });
      }
      return;
    }
    final bool hasNewMessages =
        getIt<ChatCubit>().hasNewMassagedInChannel(widget.roomKey);

    _badgeAnimationController.reset();
    _badgeAnimationController.forward();
    setState(() {
      _hasUnreadMessages = hasNewMessages;
    });
  }
}

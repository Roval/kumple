import 'dart:async';

import 'package:collection/collection.dart';
import 'package:dart_emoji/dart_emoji.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as chat_types;
import 'package:flutter_chat_types/flutter_chat_types.dart';
import 'package:flutter_chat_ui/flutter_chat_ui.dart' as chat;

import '../../cubits/chat_cubit.dart';
import '../../cubits/chat_state.dart';
import '../../cubits/loading_cubit.dart';
import '../../cubits/notifications_cubit.dart';
import '../../di/dependencies.dart';
import '../../gen/colors.gen.dart';
import '../../gen/translations.gen.dart';
import '../../models/event.dart';
import '../../repositories/remote_config.dart';
import '../../utils/app_theme.dart';
import '../../utils/camera_util.dart';
import '../../utils/logger.dart';
import '../../utils/notifications_service.dart';
import '../../utils/session_utils.dart';
import '../animated_draggable.dart';
import '../avatars.dart';
import '../game_header.dart';
import '../modal_container.dart';
import 'chat_custom_input.dart';

class ChatModule extends StatefulWidget {
  const ChatModule({
    super.key,
    required this.controller,
    required this.roomKey,
    required this.child,
    required this.onClose,
    required this.players,
    this.waitBeforeLaunch,
  });

  final ChatModuleController controller;
  final String roomKey;
  final Widget child;
  final Function() onClose;
  final Players players;

  /// Sometimes we don't want to start chat immidiatly (eg. when creating room)
  /// But wait a couple of seconds before the chat will be created in Firebase
  final Duration? waitBeforeLaunch;

  @override
  // ignore: no_logic_in_create_state
  State<ChatModule> createState() => _ChatModuleState(controller);
}

class _ChatModuleState extends State<ChatModule> {
  _ChatModuleState(this._controller) {
    _controller._addState(this);
  }

  final ChatModuleController _controller;
  final AnimatedDraggableController _draggableController =
      AnimatedDraggableController();
  bool _isChatDisplayed = false;
  late chat_types.User _user;
  StreamSubscription<String>? _notificationsClickedSubscription;

  List<chat_types.Message> get _messages {
    return getIt<ChatCubit>().chats[widget.roomKey] ?? <chat_types.Message>[];
  }

  /// Caches info if message is an emoji, to not calculate it every time
  final Map<String, bool> _isEmoji = <String, bool>{};

  Map<String, String> get _names {
    final Map<String, String> names = <String, String>{};
    for (final Player player in widget.players) {
      names[player.id] = player.name;
    }
    return names;
  }

  bool get _isChatEnabled {
    return RemoteConfigHelper.isChatEnabled;
  }

  late DateFormat _timeDateFormat;
  bool _timeFormatSet = false;

  @override
  void initState() {
    super.initState();

    if (!_isChatEnabled) {
      logger.i('chat disabled, skip init');
      return;
    }

    _user = chat_types.User(id: getUserUuid());
    logger.i('[chat] chat initilised');
    final List<chat_types.Message> messages =
        getIt<ChatCubit>().messagesForChannel(widget.roomKey);

    // Check if chat should be opened on start
    final String? lastRoom =
        getIt<NotificationsService>().consumeLastNotificationRoom();
    final bool shouldOpenChat = lastRoom != null && lastRoom == widget.roomKey;

    // We store the variable outside this Future
    // to cache it because sometimes chat
    // receives the new message earlier than starting load the difference
    // (eg. when we have to wait before launching it)
    final chat_types.Message? newestMessageWhenStarted =
        messages.firstWhereOrNull(
            (final chat_types.Message element) => element.remoteId != null);
    Future<dynamic>.delayed(widget.waitBeforeLaunch ?? Duration.zero)
        .then((final _) async {
      if (newestMessageWhenStarted != null) {
        // Triggers loading of the newest messages
        // TODO: This is not efficiant, it should be loaded after chat is clicked, not initilised, but if chat opened twice this will cause that another message can be new and we loose all the messages in between
        getIt<ChatCubit>()
            .loadNew(widget.roomKey, after: newestMessageWhenStarted.remoteId!);
      }

      // Start receiving new messages
      await getIt<ChatCubit>().startObservingChannel(widget.roomKey);

      // Start observing notifications click (inside the gameroom)
      _notificationsClickedSubscription = getIt<NotificationsService>()
          .notificationClickedStream
          .listen((final String roomKey) {
        // Notification clicked
        if (mounted) {
          setState(() {
            if (roomKey == widget.roomKey && !_isChatDisplayed) {
              _controller.open();
            }
          });
        }
      });

      if (shouldOpenChat) {
        logger.i('[chat] open from notification');
        Future<dynamic>.delayed(const Duration(milliseconds: 100))
            .then((final _) => _controller.open());
        getIt<LoadingCubit>().hideLoading();
      }
    });
  }

  @override
  void dispose() {
    logger.d('[chat] chat disposed');
    // If chat disposed by changing screens
    getIt<NotificationsCubit>().openedChatChannel = null;
    _draggableController.dispose();
    _notificationsClickedSubscription?.cancel();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    final double height = MediaQuery.of(context).size.height -
        GameHeaderViewConfig.minHeight -
        MediaQuery.of(context).padding.top -
        ModalContainerViewConfig.dragHeight;

    if (!_timeFormatSet) {
      _timeDateFormat = MediaQuery.of(context).alwaysUse24HourFormat
          ? DateFormat('HH:mm')
          : DateFormat('hh:mm a');
      _timeFormatSet = true;
    }

    return Stack(
      children: <Widget>[
        widget.child,
        if (_isChatEnabled && _isChatDisplayed)
          AnimatedDraggable(
            allowCloseOnBackground: true,
            controller: _draggableController,
            onDraggedDown: () {
              getIt<NotificationsCubit>().openedChatChannel = null;
              widget.onClose();

              if (mounted) {
                setState(() {
                  _isChatDisplayed = false;
                });
              }
            },
            child: ModalContainer(
              padding: EdgeInsets.zero,
              child: SizedBox(
                height: height,
                child: BlocBuilder<ChatCubit, ChatState>(
                  buildWhen: (final ChatState _, final ChatState current) =>
                      current is ChannelState &&
                      current.channel == widget.roomKey,
                  builder: (final _, final ChatState state) {
                    final ChatCubit chatCubit = getIt<ChatCubit>();

                    if (_isChatDisplayed) {
                      // Mark chat as seen
                      chatCubit.saveLastSeen(widget.roomKey);
                    }

                    if (state is ChannelState) {
                      return NotificationListener<UserScrollNotification>(
                        onNotification:
                            (final UserScrollNotification notification) {
                          final ScrollDirection direction =
                              notification.direction;
                          if (direction == ScrollDirection.reverse) {
                            // Hide keyboard on scroll
                            FocusManager.instance.primaryFocus?.unfocus();
                          }
                          return true;
                        },
                        child: chat.Chat(
                          useTopSafeAreaInset: false,
                          showUserAvatars: true,
                          theme: chat.DefaultChatTheme(
                            // Bubbles
                            messageBorderRadius: Defaults.cornerRadiusValue,
                            userAvatarImageBackgroundColor: ColorName.lightGrey,
                            // My messages
                            primaryColor: ColorName.lightGrey,
                            // Other's messages
                            secondaryColor: Colors.white,
                            // Date divider
                            dateDividerTextStyle: Theme.of(context)
                                .textTheme
                                .labelMedium!
                                .copyWith(
                                  color: ColorName.grey,
                                ),
                            dateDividerMargin:
                                const EdgeInsets.symmetric(vertical: 14),
                            backgroundColor: ColorName.secondaryBackground,
                          ),
                          emptyState: _emptyState(context),
                          emojiEnlargementBehavior:
                              chat.EmojiEnlargementBehavior.never,
                          messages: _messages,
                          onSendPressed: _sendMessage,
                          textMessageBuilder: (
                            final chat_types.TextMessage msg, {
                            final int? messageWidth,
                            final bool? showName,
                          }) =>
                              _textWidget(msg, showName ?? false),
                          user: _user,
                          showUserNames: true,
                          customBottomWidget: CustomChatInput(
                            onSendPressed: _sendMessage,
                          ),
                          dateHeaderThreshold: 24 * 60 * 60 * 1000,
                          isLastPage: chatCubit.isFullyFetched(widget.roomKey),
                          onEndReached: () async {
                            logger.d('start loading next page');
                            await _nextPage();
                          },
                          // Statuses will be empty
                          customStatusBuilder:
                              (final chat_types.Message message,
                                      {final BuildContext? context}) =>
                                  Container(),
                          timeFormat: _timeDateFormat,
                          customDateHeaderText: (final DateTime p0) =>
                              DateFormat.MMMd().format(p0),
                        ),
                      );
                    }
                    return Container();
                  },
                ),
              ),
            ),
          ),
      ],
    );
  }

  Column _emptyState(final BuildContext context) {
    return Column(
      children: <Widget>[
        Wrap(
          spacing: -11,
          children: <Widget>[
            ...widget.players.map(
              (final Player e) => Avatar(
                imageUrl: avatarUrl(player: e.id),
              ),
            )
          ],
        ),
        const SizedBox(height: 16),
        Text(LocaleKeys.chat_emptyHint.tr(),
            style: Theme.of(context).textTheme.titleLarge),
        const SizedBox(height: 4),
        Text(
          LocaleKeys.waitingRoom_id.tr(
            namedArgs: <String, String>{'id': widget.roomKey},
          ).toUpperCase(),
          style: Theme.of(context)
              .textTheme
              .labelMedium!
              .copyWith(color: ColorName.grey),
        ),
      ],
    );
  }

  Widget _textWidget(final chat_types.TextMessage msg, final bool showName) {
    final String? dateStr;
    if (msg.createdAt != null) {
      final DateTime date = DateTime.fromMillisecondsSinceEpoch(msg.createdAt!);
      dateStr = _timeDateFormat.format(date);
    } else {
      dateStr = null;
    }
    // Check if bubble should be enlarged
    bool? isEmoji = _isEmoji[msg.id];
    if (isEmoji == null) {
      isEmoji =
          EmojiUtil.hasOnlyEmojis(msg.text) && msg.text.characters.length <= 4;
      _isEmoji[msg.id] = isEmoji;
    }

    return Opacity(
      opacity: msg.status == Status.sending ? 0.3 : 1,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 10),
        child: IntrinsicWidth(
          child: Column(
            crossAxisAlignment: msg.author.id == getUserUuid()
                ? CrossAxisAlignment.end
                : CrossAxisAlignment.stretch,
            children: <Widget>[
              if (showName)
                Text(
                  _names[msg.author.id] ?? LocaleKeys.chat_unknownPlayer.tr(),
                  style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                      color:
                          getIt<ChatCubit>().nameColorForPlayer(msg.author.id)),
                ),
              Text(
                msg.text,
                style: isEmoji
                    ? Theme.of(context).textTheme.displayLarge
                    : Theme.of(context).textTheme.titleMedium,
              ),
              if (dateStr != null)
                Text(
                  dateStr,
                  textAlign: TextAlign.end,
                  style: Theme.of(context)
                      .textTheme
                      .labelMedium!
                      .copyWith(color: ColorName.grey),
                ),
            ],
          ),
        ),
      ),
    );
  }

  void _sendMessage(final chat_types.PartialText msg) {
    logger.i('[chat] send message');
    FirebaseAnalytics.instance.logEvent(name: 'send_message');
    // We need to pass name because it can change between games,
    // so we send the one associated with this game
    getIt<ChatCubit>()
        .sendMessage(widget.roomKey, msg.text, _names[getUserUuid()]);
  }

  void _close() {
    logger.i('[chat] closing chat');
    _draggableController.close();
  }

  void _open() {
    // Fixes bug BUD-367 when chat was reopened when the new game started and notification was clicked during the game
    getIt<NotificationsService>().consumeLastNotificationRoom();

    FirebaseAnalytics.instance.logEvent(name: 'open_chat');
    FirebaseAnalytics.instance.setCurrentScreen(
      screenName: 'Chat',
      screenClassOverride: 'chat_screen',
    );

    getIt<ChatCubit>().loadHistory(widget.roomKey);
    getIt<NotificationsCubit>().openedChatChannel = widget.roomKey;
    logger.i('[chat] opening chat');
    if (mounted) {
      setState(() {
        _isChatDisplayed = true;
      });
    }
  }

  Future<void> _nextPage() async {
    await Future<dynamic>.delayed(const Duration(seconds: 1));
    await getIt<ChatCubit>().loadHistory(widget.roomKey, nextPage: true);
  }
}

class ChatModuleController extends ChangeNotifier {
  _ChatModuleState? _state;

  // ignore: use_setters_to_change_properties
  void _addState(final _ChatModuleState state) {
    _state = state;
  }

  @override
  void dispose() {
    _state = null;
    super.dispose();
  }

  void close() {
    _state?._close();
    notifyListeners();
  }

  void open() {
    _state?._open();
    notifyListeners();
  }

  bool get isOpened {
    return _state?._isChatDisplayed ?? false;
  }
}

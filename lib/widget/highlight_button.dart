import 'package:flutter/material.dart';

typedef HighlightButtonChildBuilder = Widget Function(
    BuildContext context, bool highlight);

class HighlightButton extends StatefulWidget {
  const HighlightButton({
    super.key,
    required this.onTap,
    required this.builder,
  });

  final Function() onTap;
  final HighlightButtonChildBuilder builder;

  @override
  State<HighlightButton> createState() => _HighlightButtonState();
}

class _HighlightButtonState extends State<HighlightButton> {
  bool _pressed = false;

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
      onTapUp: (final _) {
        _highlight(false);
        widget.onTap();
      },
      onTapCancel: () => _highlight(false),
      onTapDown: (final _) => _highlight(true),
      onPanDown: (final _) => _highlight(true),
      onPanEnd: (final _) => _highlight(false),
      onPanCancel: () => _highlight(false),
      child: widget.builder(context, _pressed),
    );
  }

  void _highlight(final bool highlight) {
    setState(() {
      _pressed = highlight;
    });
  }
}

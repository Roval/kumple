import 'package:flutter/material.dart';

import '../gen/assets.gen.dart';
import '../utils/throttler.dart';

class ImageButton extends StatefulWidget {
  const ImageButton({
    final Key? key,
    required this.onPressed,
    required this.image,
    this.tintColor,
    this.height,
    this.padding = EdgeInsets.zero,
    this.shouldThrottle = true,
  }) : super(key: key);

  final Function() onPressed;
  final AssetGenImage image;
  final EdgeInsets padding;
  final double? height;
  final Color? tintColor;
  final bool shouldThrottle;

  @override
  State<ImageButton> createState() => _ImageButtonState();
}

class _ImageButtonState extends State<ImageButton> {
  bool _pressed = false;
  late final Throttler _throttler =
      Throttler(throttleGapInMillis: widget.shouldThrottle ? 1000 : 0);

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
      onTapUp: (final _) {
        _highlight(false);
        _onPressed();
      },
      onTapCancel: () => _highlight(false),
      onTapDown: (final _) => _highlight(true),
      onPanDown: (final _) => _highlight(true),
      onPanEnd: (final _) => _highlight(false),
      onPanCancel: () => _highlight(false),
      child: Opacity(
        opacity: _pressed ? 0.3 : 1,
        child: Container(
          color: Colors.transparent,
          padding: widget.padding,
          height: widget.height,
          child: Center(child: widget.image.image(color: widget.tintColor)),
        ),
      ),
    );
  }

  void _onPressed() {
    _throttler.run(() => widget.onPressed.call());
  }

  void _highlight(final bool highlight) {
    setState(() {
      _pressed = highlight;
    });
  }
}

import 'package:flutter/material.dart';

import '../models/event.dart';
import 'image_in_grid.dart';

enum SelectState { selected, deselected, toSelect }

class ImageToSelect extends StatelessWidget {
  const ImageToSelect({
    final Key? key,
    required this.player,
    required this.url,
    required this.state,
    required this.onPlayerSelect,
  }) : super(key: key);

  final Player player;
  final String url;
  final SelectState state;
  final Function(Player) onPlayerSelect;

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {
          onPlayerSelect(player);
        },
        child: Opacity(
          opacity: state == SelectState.deselected ? 0.5 : 1,
          child: ImageInGrid(
            url: url,
            isHighlighted: state == SelectState.selected,
          ),
        ));
  }
}

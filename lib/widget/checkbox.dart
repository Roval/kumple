import 'package:flutter/material.dart';

import '../gen/assets.gen.dart';
import '../gen/colors.gen.dart';

class GameCheckbox extends StatelessWidget {
  const GameCheckbox({
    super.key,
    required this.isSelected,
  });

  final bool isSelected;

  @override
  Widget build(final BuildContext context) {
    if (isSelected)
      return Assets.images.selectedIcon.image();
    else
      return Container(
        width: 16,
        height: 16,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(color: ColorName.grey),
        ),
      );
  }
}

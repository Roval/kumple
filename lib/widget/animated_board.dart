import 'package:flutter/material.dart';

import 'delayed_tween_animation_builder.dart';

// ignore: avoid_classes_with_only_static_members
class _AnimatedBoardViewConfig {
  static const Duration topChildDuration = Duration(milliseconds: 700);
  static const Duration bottomChildDelay = Duration(milliseconds: 900);
  static const Duration bottomChildDuration = Duration(milliseconds: 400);
}

class AnimatedBoard extends StatefulWidget {
  const AnimatedBoard({
    super.key,
    required this.topChild,
    required this.bottomChild,
    this.onEnd,
    this.shouldExpand = true,
    this.spaceBetweenComponents = 16,
  });

  final Widget topChild;
  final Widget bottomChild;
  final Function()? onEnd;
  final bool shouldExpand;
  final double spaceBetweenComponents;

  @override
  State<AnimatedBoard> createState() => _AnimatedBoardState();
}

class _AnimatedBoardState extends State<AnimatedBoard> {
  @override
  Widget build(final BuildContext context) {
    return _wrapInExpand(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          TweenAnimationBuilder<double>(
              tween: Tween<double>(begin: 0, end: 1),
              duration: _AnimatedBoardViewConfig.topChildDuration,
              builder: (final BuildContext context, final double value,
                  final Widget? child) {
                return Transform.translate(
                    offset: Offset(0, 20 * (1 - value)),
                    child: Opacity(opacity: value, child: child));
              },
              child: widget.topChild),
          SizedBox(height: widget.spaceBetweenComponents),
          _wrapInExpand(
            child: DelayedTweenAnimationBuilder<double>(
              delay: _AnimatedBoardViewConfig.bottomChildDelay,
              duration: _AnimatedBoardViewConfig.bottomChildDuration,
              curve: Curves.easeOut,
              tween: Tween<double>(begin: 0, end: 1),
              builder: (final BuildContext context, final double value,
                  final Widget? child) {
                return Transform.translate(
                    offset: Offset(0, 50 * (1 - value)),
                    child: Opacity(opacity: value, child: child));
              },
              onEnd: widget.onEnd,
              child: widget.bottomChild,
            ),
          )
        ],
      ),
    );
  }

  Widget _wrapInExpand({required final Widget child}) {
    if (widget.shouldExpand) {
      return Expanded(child: child);
    }
    return child;
  }
}

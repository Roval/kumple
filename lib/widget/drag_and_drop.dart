import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../gen/assets.gen.dart';
import '../gen/colors.gen.dart';
import '../models/event.dart';
import '../utils/app_theme.dart';
import '../utils/camera_util.dart';
import '../utils/device_util.dart';
import '../utils/element_order.dart';
import 'avatars.dart';
import 'checkbox.dart';
import 'default_container.dart';

class DragAndDropUsers extends StatelessWidget {
  const DragAndDropUsers({
    final Key? key,
    required this.players,
    required final GlobalKey<State<StatefulWidget>> draggableKey,
    this.displayInOneLine = true,
  })  : _draggableKey = draggableKey,
        super(key: key);

  final Players players;
  final GlobalKey<State<StatefulWidget>> _draggableKey;
  final bool displayInOneLine;

  @override
  Widget build(final BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: displayInOneLine ? Axis.horizontal : Axis.vertical,
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Wrap(
        spacing: 8,
        children: List<Widget>.generate(players.length, (final int index) {
          final Player player = players[index];
          return _PlayerDragTag(
            player: player,
            draggableKey: _draggableKey,
          );
        }),
      ),
    );
  }
}

class _DraggingListItem extends StatelessWidget {
  const _DraggingListItem({
    final Key? key,
    required this.dragKey,
    required this.photoProvider,
  }) : super(key: key);

  final GlobalKey dragKey;
  final ImageProvider photoProvider;

  @override
  Widget build(final BuildContext context) {
    return FractionalTranslation(
      translation: const Offset(-0.5, -0.5),
      child: ClipRRect(
        key: dragKey,
        borderRadius: BorderRadius.circular(50.0),
        child: SizedBox(
          height: 90,
          width: 90,
          child: Opacity(
            opacity: 0.85,
            child: Image(
              image: photoProvider,
              fit: BoxFit.cover,
            ),
          ),
        ),
      ),
    );
  }
}

class DragAndDropBoxAnswer extends StatelessWidget {
  const DragAndDropBoxAnswer({
    final Key? key,
    required this.answer,
    required this.assigned,
    required this.areAllAssigned,
    required this.answerIndex,
    required this.onPlayerAssigned,
    this.onSecondaryActionTap,
    this.draggableKey,
  }) : super(key: key);

  final String answer;
  final Players assigned;
  final int answerIndex;
  final bool areAllAssigned;
  final Function(Player, int) onPlayerAssigned;
  final Function(int)? onSecondaryActionTap;
  final GlobalKey<State<StatefulWidget>>? draggableKey;

  @override
  Widget build(final BuildContext context) {
    final TextStyle? titleFont = isSmallDevice
        ? Theme.of(context).textTheme.titleLarge
        : Theme.of(context).textTheme.headlineMedium;

    return Padding(
      padding: const EdgeInsets.only(bottom: Defaults.buttonsSpacing),
      child: DragTarget<Player>(
        builder: (final BuildContext context, final List<Player?> candidateData,
            final List<dynamic> rejectedData) {
          return DefaultContainer(
            margin: draggableKey == null
                ? const EdgeInsets.symmetric(horizontal: 6)
                : null,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                GestureDetector(
                  onTap: _onBoxTap,
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Text(
                          answer.toUpperCase(),
                          textAlign: TextAlign.left,
                          style: titleFont,
                        ),
                      ),
                      if (onSecondaryActionTap != null)
                        Assets.images.plusIcon.image(),
                    ],
                  ),
                ),
                const SizedBox(height: 16),
                Wrap(
                  spacing: 12,
                  runSpacing: Defaults.buttonsSpacing,
                  children: List<Widget>.generate(
                      assigned.length + (areAllAssigned ? 0 : 1),
                      (final int index) {
                    if (index >= assigned.length) {
                      // Last item
                      return const _PlayerEmptyDragTag();
                    }
                    final Player player = assigned[index];
                    return _PlayerAssaignedDragTag(
                      player: player,
                      draggableKey: draggableKey,
                    );
                  }),
                ),
              ],
            ),
          );
        },
        onAccept: (final Player data) {
          onPlayerAssigned(data, answerIndex);
        },
      ),
    );
  }

  void _onBoxTap() {
    onSecondaryActionTap?.call(answerIndex);
  }
}

class DragAndDropAnswer extends StatelessWidget {
  const DragAndDropAnswer({
    final Key? key,
    required this.answer,
    required this.assigned,
    required this.answerIndex,
    required this.onPlayerAssigned,
    this.draggableKey,
    this.onSecondaryActionTap,
  }) : super(key: key);

  final String answer;
  final Player? assigned;
  final int answerIndex;
  final Function(Player, int) onPlayerAssigned;
  final Function(int)? onSecondaryActionTap;
  final GlobalKey<State<StatefulWidget>>? draggableKey;

  @override
  Widget build(final BuildContext context) {
    final TextStyle? titleFont = isSmallDevice
        ? Theme.of(context).textTheme.titleLarge
        : Theme.of(context).textTheme.headlineMedium;

    return Padding(
      padding: const EdgeInsets.only(bottom: Defaults.buttonsSpacing),
      child: DragTarget<Player>(
        builder: (final BuildContext context, final List<Player?> candidateData,
            final List<dynamic> rejectedData) {
          return GestureDetector(
            onTap: _onBoxTap,
            child: DefaultContainer(
              margin: draggableKey == null
                  ? const EdgeInsets.symmetric(horizontal: 6)
                  : null,
              child: ConstrainedBox(
                constraints: const BoxConstraints(minHeight: 41),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    if (assigned != null)
                      _PlayerAssaignedDragTag(
                        player: assigned!,
                        draggableKey: draggableKey,
                      )
                    else
                      const _PlayerEmptyDragTag(),
                    const SizedBox(width: 16),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            answer,
                            style: titleFont,
                          ),
                          if (assigned != null)
                            Text(assigned!.name,
                                maxLines: 1,
                                overflow: TextOverflow.ellipsis,
                                style: Theme.of(context).textTheme.bodySmall),
                        ],
                      ),
                    ),
                    if (onSecondaryActionTap != null)
                      Assets.images.plusIcon.image(),
                  ],
                ),
              ),
            ),
          );
        },
        onAccept: (final Player data) {
          onPlayerAssigned(data, answerIndex);
        },
      ),
    );
  }

  void _onBoxTap() {
    onSecondaryActionTap?.call(answerIndex);
  }
}

class _PlayerEmptyDragTag extends StatelessWidget {
  const _PlayerEmptyDragTag();

  @override
  Widget build(final BuildContext context) {
    return EmptyAvatar(
      color: Colors.black,
      dimension: isSmallDevice ? 30 : 40,
    );
  }
}

class _PlayerAssaignedDragTag extends StatelessWidget {
  const _PlayerAssaignedDragTag({
    final Key? key,
    required this.player,
    this.draggableKey,
  }) : super(key: key);

  final Player player;
  final GlobalKey<State<StatefulWidget>>? draggableKey;

  @override
  Widget build(final BuildContext context) {
    final Widget tag = Avatar(
      imageUrl: player.avatar,
      dimension: isSmallDevice ? 30 : 40,
    );
    if (draggableKey != null) {
      return Draggable<Player>(
        data: player,
        dragAnchorStrategy: pointerDragAnchorStrategy,
        feedback: _DraggingListItem(
          dragKey: draggableKey!,
          photoProvider: CachedNetworkImageProvider(player.avatar),
        ),
        onDragStarted: () {
          HapticFeedback.lightImpact();
        },
        onDragCompleted: () {
          HapticFeedback.heavyImpact();
        },
        child: tag,
      );
    }
    return tag;
  }
}

class _PlayerDragTag extends StatelessWidget {
  const _PlayerDragTag({
    final Key? key,
    required this.player,
    required final GlobalKey<State<StatefulWidget>> draggableKey,
  })  : _draggableKey = draggableKey,
        super(key: key);

  final Player player;
  final GlobalKey<State<StatefulWidget>> _draggableKey;

  @override
  Widget build(final BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          height: 50,
          decoration: BoxDecoration(
            borderRadius: Defaults.cornerRadius,
            color: Colors.white,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              const SizedBox(width: 5),
              Assets.images.gripIcon.image(),
              const SizedBox(width: 5),
              Avatar(
                dimension: 32,
                imageUrl: avatarUrl(player: player.id),
              ),
              const SizedBox(width: 8),
              ConstrainedBox(
                constraints: const BoxConstraints(maxWidth: 100),
                child: Text(
                  player.name,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context).textTheme.bodySmall,
                ),
              ),
              const SizedBox(width: 12),
            ],
          ),
        ),
        Positioned.fill(
          child: Row(
            children: <Widget>[
              Expanded(
                child: Draggable<Player>(
                  data: player,
                  dragAnchorStrategy: pointerDragAnchorStrategy,
                  feedback: _DraggingListItem(
                    dragKey: _draggableKey,
                    photoProvider: CachedNetworkImageProvider(player.avatar),
                  ),
                  onDragStarted: () {
                    HapticFeedback.lightImpact();
                  },
                  onDragCompleted: () {
                    HapticFeedback.heavyImpact();
                  },
                  child: Container(
                    color: Colors.transparent,
                  ),
                ),
              ),
              Expanded(child: Container()),
            ],
          ),
        ),
      ],
    );
  }
}

class PlayersDragSelectList extends StatelessWidget {
  const PlayersDragSelectList({
    final Key? key,
    required this.players,
    required this.tag,
    required this.answers,
    required this.onPlayerToggle,
    required this.currentlyAssigned,
  }) : super(key: key);

  final Players players;
  final String tag;
  final Answers answers;
  final List<Player> currentlyAssigned;
  final Function(Player) onPlayerToggle;

  @override
  Widget build(final BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.only(
        bottom: Defaults.bottomSpacing,
      ),
      itemCount: players.length,
      itemBuilder: (final BuildContext context, final int index) {
        final Player player = players[index];
        final String? answer = answers[player.id] as String?;
        return _PlayerToSelectDrag(
          order: order(index, players.length),
          state: answer == null
              ? PlayerToSelectDragState.notSelected
              : currentlyAssigned.contains(player)
                  ? PlayerToSelectDragState.selected
                  : PlayerToSelectDragState.other,
          answer: answer,
          player: player,
          onPlayerSelect: () {
            onPlayerToggle(player);
          },
        );
      },
      separatorBuilder: (final BuildContext context, final int index) =>
          const SizedBox(),
    );
  }
}

enum PlayerToSelectDragState { selected, notSelected, other }

class _PlayerToSelectDrag extends StatelessWidget {
  const _PlayerToSelectDrag({
    final Key? key,
    required this.player,
    required this.order,
    required this.state,
    required this.onPlayerSelect,
    this.answer,
  }) : super(key: key);

  final ElementOrder order;
  final Player player;
  final PlayerToSelectDragState state;
  final String? answer;
  final Function() onPlayerSelect;

  @override
  Widget build(final BuildContext context) {
    final Color fontColor = state == PlayerToSelectDragState.other
        ? ColorName.grey
        : ColorName.text;

    return GestureDetector(
      behavior: HitTestBehavior.opaque,
      onTap: onPlayerSelect,
      child: DefaultContainer(
        order: order,
        margin: const EdgeInsets.symmetric(horizontal: 6),
        child: ConstrainedBox(
          constraints: const BoxConstraints(minHeight: 41),
          child: Row(
            children: <Widget>[
              Avatar(
                dimension: isSmallDevice ? 30 : 40,
                imageUrl: avatarUrl(player: player.id),
              ),
              const SizedBox(width: 16),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      player.name,
                      textAlign: TextAlign.left,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context)
                          .textTheme
                          .titleLarge
                          ?.copyWith(color: fontColor),
                    ),
                    if (state == PlayerToSelectDragState.other &&
                        answer != null)
                      Text(
                        answer!,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context)
                            .textTheme
                            .bodySmall
                            ?.copyWith(color: fontColor),
                      ),
                  ],
                ),
              ),
              const SizedBox(width: 16),
              GameCheckbox(
                isSelected: state == PlayerToSelectDragState.selected,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

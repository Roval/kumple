import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../cubits/loading_cubit.dart';
import '../../cubits/store_buy_cubit.dart';
import '../../cubits/store_buy_state.dart';
import '../../cubits/store_cubit.dart';
import '../../di/dependencies.dart';
import '../../gen/translations.gen.dart';
import '../../mixins/error_dialog_mixin.dart';

class StoreActionsOverlay extends StatelessWidget with ErrorDialog {
  const StoreActionsOverlay({super.key, required this.child});

  final Widget child;

  @override
  Widget build(final BuildContext context) {
    return BlocListener<StoreBuyCubit, StoreBuyState>(
      listener: (final BuildContext context, final StoreBuyState state) async {
        if (state is PurchaseStartedState) {
          getIt<LoadingCubit>().showLoading();
          return;
        }
        getIt<LoadingCubit>().hideLoading();

        if (state is PurchaseErrorState) {
          _handleError(state.value);
        } else if (state is PurchaseSuccessState) {
          _fetchStore();
        }
      },
      child: child,
    );
  }

  void _handleError(final PurchaseError error) {
    switch (error) {
      case PurchaseError.productNotFound:
        showErrorDialog(msg: LocaleKeys.store_error_notFound.tr());
        break;
      case PurchaseError.storeError:
        showErrorDialog(msg: LocaleKeys.store_error.tr());
        break;
      case PurchaseError.storeUnavailable:
        showErrorDialog(msg: LocaleKeys.store_error_storeUnavailable.tr());
        break;
      case PurchaseError.notVerified:
        showErrorDialog(msg: LocaleKeys.store_error_notVerified.tr());
        break;
    }
  }

  void _fetchStore() {
    getIt<StoreCubit>().fetchStore();
  }
}

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../cubits/loading_cubit.dart';
import '../../cubits/loading_state.dart';
import '../loading_indicator.dart';

class LoadingOverlay extends StatefulWidget {
  const LoadingOverlay({super.key, required this.child});

  final Widget child;

  @override
  State<LoadingOverlay> createState() => _LoadingOverlayState();
}

class _LoadingOverlayState extends State<LoadingOverlay> {
  @override
  Widget build(final BuildContext context) {
    return BlocBuilder<LoadingCubit, LoadingState>(
      builder: (final BuildContext context, final LoadingState state) {
        return Stack(
          children: <Widget>[
            widget.child,
            if (state is LoadingVisible)
              BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
                child: Container(
                  color: Colors.black54,
                  child: const LoadingIndicator(),
                ),
              ),
          ],
        );
      },
    );
  }
}

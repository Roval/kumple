import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../cubits/notifications_cubit.dart';
import '../../cubits/notifications_state.dart';
import '../../di/dependencies.dart';
import '../../gen/colors.gen.dart';
import '../../repositories/local_storage.dart';
import '../../utils/app_theme.dart';
import '../../utils/audio_player.dart';
import '../../utils/camera_util.dart';
import '../../utils/logger.dart';
import '../../utils/notifications_service.dart';
import '../avatars.dart';

class _NotificationsOverlayViewConfig {
  static const double insidePadding = 15;
  static const double externalPadding = 16;
  static const double spaceBetweenTexts = 10;
}

class NotificationsOverlay extends StatefulWidget {
  const NotificationsOverlay({super.key, required this.child});

  final Widget child;

  @override
  State<NotificationsOverlay> createState() => _NotificationsOverlayState();
}

class _NotificationsOverlayState extends State<NotificationsOverlay>
    with TickerProviderStateMixin {
  MessageNotificationState? _currentToast;

  late AnimationController _controller;
  Animation<Offset>? _offsetFloat;

  bool _isSoundOn = false;

  /// Subscription for all the changes of isOn value for sound
  late StreamSubscription<bool> _isSoundOnSubscription;

  @override
  void initState() {
    super.initState();

    LocalStorage.isSoundOn().then((final bool value) => _isSoundOn = value);
    _isSoundOnSubscription =
        LocalStorage.isSoundOnStream.listen((final bool newValue) {
      logger.i('new value for sound setting $newValue');
      _isSoundOn = newValue;
    });

    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 300),
    );

    _controller.addStatusListener((final AnimationStatus status) async {
      if (status == AnimationStatus.completed) {
        await Future<dynamic>.delayed(const Duration(seconds: 2));
        _controller.reverse();
      } else if (status == AnimationStatus.dismissed) {
        setState(() {
          _currentToast = null;
        });
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    _isSoundOnSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    if (_offsetFloat == null) {
      _offsetFloat = Tween<Offset>(
              begin: Offset(
                0,
                -(_NotificationsOverlayViewConfig.externalPadding +
                    _NotificationsOverlayViewConfig.insidePadding * 2 +
                    MediaQuery.of(context).padding.top +
                    appTheme.textTheme.titleLarge!.fontSize! +
                    appTheme.textTheme.titleMedium!.fontSize! +
                    _NotificationsOverlayViewConfig.spaceBetweenTexts +
                    3), // + 3 to make sure it is above
              ),
              end: Offset.zero)
          .animate(
        CurvedAnimation(
          parent: _controller,
          curve: Curves.linearToEaseOut,
        ),
      );

      _offsetFloat?.addListener(() {
        setState(() {});
      });
    }

    return BlocListener<NotificationsCubit, NotificationsState>(
      listener: (final BuildContext context, final NotificationsState state) {
        if (state is MessageNotificationState) {
          _playSoundIfNeeded();
          final bool shouldStartAnimation = _currentToast == null;

          setState(() {
            _currentToast = state;
            if (shouldStartAnimation) {
              _controller.forward();
            }
          });
        }
      },
      child: Stack(
        children: <Widget>[
          widget.child,
          if (_currentToast != null)
            SafeArea(
              child: Transform.translate(
                offset: _offsetFloat!.value,
                child: Container(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 16,
                      vertical: _NotificationsOverlayViewConfig.insidePadding),
                  margin: const EdgeInsets.symmetric(
                      horizontal: 10,
                      vertical:
                          _NotificationsOverlayViewConfig.externalPadding),
                  decoration: BoxDecoration(
                      borderRadius: Defaults.cornerRadius,
                      color: ColorName.secondaryBackground,
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                          color: Colors.black.withOpacity(0.15),
                          spreadRadius: 1,
                          blurRadius: 20,
                          offset: const Offset(0, 4),
                        ),
                      ]),
                  child: GestureDetector(
                    onTap: _onNotificationClicked,
                    behavior: HitTestBehavior.opaque,
                    child: Row(
                      children: <Widget>[
                        Avatar(
                          imageUrl: avatarUrl(player: _currentToast!.player),
                        ),
                        const SizedBox(width: 12),
                        Expanded(
                          child: Column(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.baseline,
                                textBaseline: TextBaseline.alphabetic,
                                children: <Widget>[
                                  Text(
                                    _currentToast!.title,
                                    textAlign: TextAlign.left,
                                    maxLines: 1,
                                    style:
                                        Theme.of(context).textTheme.titleLarge,
                                  ),
                                  const SizedBox(width: 10),
                                  Expanded(
                                    child: Text(
                                      _currentToast!.room,
                                      textAlign: TextAlign.left,
                                      maxLines: 1,
                                      style: Theme.of(context)
                                          .textTheme
                                          .labelMedium
                                          ?.copyWith(color: ColorName.grey),
                                    ),
                                  ),
                                ],
                              ),
                              Text(_currentToast!.body,
                                  textAlign: TextAlign.left,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                  style:
                                      Theme.of(context).textTheme.titleSmall),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            )
        ],
      ),
    );
  }

  Future<void> _playSoundIfNeeded() async {
    if (_isSoundOn) {
      playIncomingMessageSound();
    }
  }

  void _onNotificationClicked() {
    if (_currentToast != null) {
      _controller.reverse();
      getIt<NotificationsService>().publishNotificationClicked(
          _currentToast!.room, _currentToast!.action);
    }
  }
}

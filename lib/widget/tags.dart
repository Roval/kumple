import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../gen/assets.gen.dart';
import '../gen/translations.gen.dart';

class HostTag extends StatelessWidget {
  const HostTag({
    final Key? key,
  }) : super(key: key);

  @override
  Widget build(final BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.black,
        borderRadius: BorderRadius.circular(4),
      ),
      padding: const EdgeInsets.fromLTRB(5, 1, 5, 0),
      child: Text(
        LocaleKeys.host.tr(),
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.bodyLarge?.copyWith(
              color: Colors.white,
            ),
      ),
    );
  }
}

class CloseTag extends StatelessWidget {
  const CloseTag({super.key});

  @override
  Widget build(final BuildContext context) {
    return Container(
      color: Colors.white, // Otherwise padding not clickable
      padding: const EdgeInsets.fromLTRB(20, 10, 16, 10),
      child: Assets.images.closeIconBlack.image(width: 12, height: 12),
    );
  }
}

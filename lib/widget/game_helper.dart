import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../cubits/game_state.dart';
import '../gen/assets.gen.dart';
import '../gen/translations.gen.dart';
import '../models/event.dart';
import '../models/store.dart';
import '../utils/app_theme.dart';
import '../utils/locale_utils.dart';
import 'default_container.dart';
import 'game_button.dart';
import 'single_line.dart';

class GameHelper extends StatefulWidget {
  const GameHelper(
      {super.key,
      required this.state,
      required this.storeItem,
      required this.onReportQuestion});

  final QuestionState state;
  final StoreItem? storeItem;
  final Function() onReportQuestion;

  @override
  State<GameHelper> createState() => _GameHelperState();
}

class _GameHelperState extends State<GameHelper> {
  @override
  Widget build(final BuildContext context) {
    final List<String> content = _questionTypeContentTexts();

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Text(
          LocaleKeys.gameboards_helper_title.tr(),
          textAlign: TextAlign.left,
          maxLines: 1,
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        const SizedBox(height: 16),
        DefaultContainer(
          margin: EdgeInsets.zero,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Assets.images.infoIcon.image(),
              const SizedBox(width: 16),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      content[0],
                      textAlign: TextAlign.left,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    Text(
                      content[1],
                      textAlign: TextAlign.left,
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 24),
        Text(
          LocaleKeys.gameboards_helper_otherInfo.tr(),
          textAlign: TextAlign.left,
          maxLines: 1,
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        ..._categoryWidgetStack(),
        const SizedBox(height: 16),
        GameButton(
          title: LocaleKeys.gameboards_helper_reportQuestion_button.tr(),
          margin: EdgeInsets.zero,
          style: const GameButtonStyle.whiteWithBorder(),
          onPressed: widget.onReportQuestion,
        ),
        SizedBox(height: Defaults.bottomSpacing),
      ],
    );
  }

  List<Widget> _categoryWidgetStack() {
    if (widget.storeItem != null) {
      return <Widget>[
        const SizedBox(height: 16),
        DefaultContainer(
          margin: EdgeInsets.zero,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              CachedNetworkImage(
                imageUrl: widget.storeItem!.imageUrl,
                width: 22,
                height: 22,
              ),
              const SizedBox(width: 16),
              Expanded(
                child: Text(
                  LocaleKeys.gameboards_helper_questionOrigin.tr(
                      namedArgs: <String, String>{
                        'category': localisedStoreItemTitle(widget.storeItem!)
                      }),
                  textAlign: TextAlign.left,
                  style: Theme.of(context).textTheme.titleLarge,
                ),
              ),
            ],
          ),
        ),
      ];
    }
    return <Widget>[];
  }

  List<String> _questionTypeContentTexts() {
    switch (widget.state.data.type) {
      case QuestionType.oneUser:
        return <String>[
          LocaleKeys.gameboards_helper_howTo_oneUser_title.tr(),
          LocaleKeys.gameboards_helper_howTo_oneUser_content.tr(),
        ];
      case QuestionType.oneOf:
        return <String>[
          LocaleKeys.gameboards_helper_howTo_oneOf_title.tr(),
          LocaleKeys.gameboards_helper_howTo_oneOf_content.tr(),
        ];
      case QuestionType.bestOf:
        return <String>[
          LocaleKeys.gameboards_helper_howTo_bestOf_title.tr(),
          LocaleKeys.gameboards_helper_howTo_bestOf_content.tr(),
        ];
      case QuestionType.bestOfOneUser:
        return <String>[
          LocaleKeys.gameboards_helper_howTo_bestOfAnswer_title.tr(),
          LocaleKeys.gameboards_helper_howTo_bestOfAnswer_content.tr(),
        ];
      case QuestionType.orOr:
      case QuestionType.dragOrOr:
        return <String>[
          LocaleKeys.gameboards_helper_howTo_orOr_title.tr(),
          LocaleKeys.gameboards_helper_howTo_orOr_content.tr(),
        ];
      case QuestionType.text:
        return <String>[
          LocaleKeys.gameboards_helper_howTo_text_title.tr(),
          LocaleKeys.gameboards_helper_howTo_text_content.tr(),
        ];
      case QuestionType.assign:
        return <String>[
          LocaleKeys.gameboards_helper_howTo_assign_title.tr(),
          LocaleKeys.gameboards_helper_howTo_assign_content.tr(),
        ];
      case QuestionType.assignOne:
        return <String>[
          LocaleKeys.gameboards_helper_howTo_assignOne_title.tr(),
          LocaleKeys.gameboards_helper_howTo_assignOne_content.tr(),
        ];
      case QuestionType.bestOfImage:
        return <String>[
          LocaleKeys.gameboards_helper_howTo_photo_title.tr(),
          LocaleKeys.gameboards_helper_howTo_photo_content.tr(),
        ];
      case QuestionType.bestOfImageOneUser:
        return <String>[
          LocaleKeys.gameboards_helper_howTo_photoAnswer_title.tr(),
          LocaleKeys.gameboards_helper_howTo_photoAnswer_content.tr(),
        ];
      case QuestionType.draw:
        return <String>[
          LocaleKeys.gameboards_helper_howTo_draw_title.tr(),
          LocaleKeys.gameboards_helper_howTo_draw_content.tr(),
        ];
      case QuestionType.drawOneUser:
        return <String>[
          LocaleKeys.gameboards_helper_howTo_drawAnswer_title.tr(),
          LocaleKeys.gameboards_helper_howTo_drawAnswer_content.tr(),
        ];
    }
  }
}

class GameHelperQuestionReport extends StatefulWidget {
  const GameHelperQuestionReport({
    super.key,
    required this.onReportQuestion,
  });

  final Function(String) onReportQuestion;

  @override
  State<GameHelperQuestionReport> createState() =>
      _GameHelperQuestionReportState();
}

class _GameHelperQuestionReportState extends State<GameHelperQuestionReport> {
  final FocusNode _focusNode = FocusNode();
  final TextEditingController _textFieldController = TextEditingController();
  bool get _isTextFieldedFocused => _focusNode.hasFocus;

  /// Answer from text field
  String get _report => _textFieldController.text.trim();

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() {
      if (!_isTextFieldedFocused) {
        Future<void>.delayed(const Duration(seconds: 2))
            .then((final _) => SystemChrome.restoreSystemUIOverlays());
      }
      setState(() {
        // Refresh
      });
    });

    _textFieldController.addListener(() {
      // Refreshes send button
      setState(() {});
    });
  }

  @override
  void dispose() {
    _textFieldController.dispose();
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height - 120,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(
            LocaleKeys.gameboards_helper_reportQuestion_title.tr(),
            textAlign: TextAlign.left,
            maxLines: 1,
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          const SizedBox(height: 16),
          Column(
            children: <Widget>[
              TextField(
                focusNode: _focusNode,
                inputFormatters: <TextInputFormatter>[
                  LengthLimitingTextInputFormatter(400),
                ],
                enableSuggestions: false,
                autocorrect: false,
                textInputAction: TextInputAction.done,
                onSubmitted: (final String value) =>
                    FocusManager.instance.primaryFocus?.unfocus(),
                controller: _textFieldController,
                keyboardType: TextInputType.multiline,
                maxLines: 4,
                minLines: 1,
                decoration: const InputDecoration(
                  border: InputBorder.none,
                ),
                textAlign: TextAlign.left,
                style: Theme.of(context).textTheme.displayMedium,
              ),
              const SingleLine(height: 2, roundCorners: true),
            ],
          ),
          const SizedBox(height: 16),
          GameButton(
            title: LocaleKeys.gameboards_helper_reportQuestion_confirm.tr(),
            margin: EdgeInsets.zero,
            onPressed:
                _report.isEmpty ? null : () => widget.onReportQuestion(_report),
          ),
          SizedBox(height: Defaults.bottomSpacing),
        ],
      ),
    );
  }
}

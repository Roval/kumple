import 'dart:math';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:simple_animations/simple_animations.dart';

import '../utils/logger.dart';
import 'calculating_size.dart';

// ignore: avoid_classes_with_only_static_members
class _AnimatedDraggableViewConfig {
  static const int animationDuration = 150;
}

class AnimatedDraggable extends StatefulWidget {
  const AnimatedDraggable({
    super.key,
    required this.onDraggedDown,
    required this.child,
    required this.controller,
    this.allowSnap = true,
    this.allowCloseOnBackground = false,
    this.animationDelay = 0,
    this.minHeight,
    this.maxHeight,
  });

  final AnimatedDraggableController controller;
  final Function() onDraggedDown;
  final Widget child;
  final bool allowSnap;
  final int animationDelay;
  final bool allowCloseOnBackground;
  final double? minHeight;
  final double? maxHeight;

  @override
  // ignore: no_logic_in_create_state
  State<AnimatedDraggable> createState() => _AnimatedDraggableState(controller);
}

class _AnimatedDraggableState extends State<AnimatedDraggable>
    with TickerProviderStateMixin, AnimationMixin<AnimatedDraggable> {
  _AnimatedDraggableState(this._inputController) {
    _inputController._addState(this);
  }

  final AnimatedDraggableController _inputController;

  // Animation fields
  late Animation<double> _animation;
  bool _isClosedCalled = false;

  // Drag controller
  final DraggableScrollableController _dragController =
      DraggableScrollableController();

  // Content height calcualtion (wrap content)
  final GlobalKey _contentKey = GlobalKey();
  double _contentHeight = 0;

  /// Current height of the drag controller, from 0 to 1 in collelation
  /// to view height
  double _currentDragHeight = 0;

  @override
  void initState() {
    super.initState();
    // If controller dragged down - dismiss it
    _dragController.addListener(() {
      if (!_dragController.isAttached) {
        return;
      }
      // Sometimes it stops at very small number like 0.000002
      // so we have to check if it is lower than 1
      if (_dragController.pixels <= 1) {
        _close();
      }

      // If controller was moved up - decrese "tappable" space above controller
      if (controller.isCompleted)
        // Even thou we are waiting for all animations to be over - it still crashed
        // so we give the screen some time to finish all
        Future<dynamic>.delayed(const Duration(milliseconds: 100)).then(
          (final _) {
            if (!mounted) {
              return;
            }
            setState(() {
              _currentDragHeight = _dragController.size;
            });
          },
        );
    });

    // Start appear animation
    controller.duration = const Duration(
        milliseconds: _AnimatedDraggableViewConfig.animationDuration);
    _startAppearAnimations();
  }

  @override
  void dispose() {
    _dragController.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    final double desiredHeight =
        _animation.value * (widget.minHeight ?? _calculateViewPort());
    return CalculatingSize(
      keyToCalculate: _contentKey,
      onSizeCalculated: (final Size size) {
        setState(() {
          _contentHeight = size.height;
        });
      },
      child: Stack(
        children: <Widget>[
          BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 5, sigmaY: 5),
            child: Container(
              color: Colors.black54,
              child: Opacity(
                opacity: _contentHeight != 0 ? 1 : 0,
                child: DraggableScrollableSheet(
                    initialChildSize: desiredHeight,
                    minChildSize: widget.allowSnap
                        ? 0
                        : widget.minHeight ?? desiredHeight,
                    maxChildSize: (widget.minHeight != null)
                        ? (widget.maxHeight ?? 0.8)
                        : desiredHeight,
                    controller: _dragController,
                    snap: true,
                    builder: (final BuildContext context,
                        final ScrollController scrollController) {
                      return SingleChildScrollView(
                        controller: scrollController,
                        child: Container(
                          key: _contentKey,
                          child: widget.child,
                        ),
                      );
                    }),
              ),
            ),
          ),
          if (widget.allowCloseOnBackground)
            GestureDetector(
              onTap: () {
                _startCloseAnimations();
              },
              child: Container(
                color: Colors.transparent,
                height: MediaQuery.of(context).size.height *
                    (1 - max(_currentDragHeight, desiredHeight)),
              ),
            ),
        ],
      ),
    );
  }

  void _startAppearAnimations() {
    _tween(up: true);
  }

  void _startCloseAnimations() {
    _tween(up: false);
  }

  void _tween({required final bool up}) {
    _animation = Tween<double>(
      begin: up ? 0 : 1,
      end: up ? 1 : 0,
    ).animate(controller)
      ..addListener(() {
        if (_animation.isCompleted && !up) {
          // Animated down
          _close();
          return;
        }
        setState(() {
          // The state that has changed here is the animation object’s value.
        });
      });
    controller.reset();

    Future<dynamic>.delayed(
            Duration(milliseconds: up ? widget.animationDelay : 0))
        .then((final _) => controller.play());
  }

  double _calculateViewPort() {
    if (_contentHeight == 0) {
      // For first frame, so height can be calculated properly
      return 1;
    }
    return _contentHeight / MediaQuery.of(context).size.height;
  }

  void _close() {
    if (_isClosedCalled) {
      return;
    }
    logger.i('closing drag container');
    _isClosedCalled = true;
    widget.onDraggedDown();
  }
}

class AnimatedDraggableController extends ChangeNotifier {
  _AnimatedDraggableState? _state;

  // ignore: use_setters_to_change_properties
  void _addState(final _AnimatedDraggableState state) {
    _state = state;
  }

  @override
  void dispose() {
    _state = null;
    super.dispose();
  }

  void close() {
    _state?._startCloseAnimations();
  }
}

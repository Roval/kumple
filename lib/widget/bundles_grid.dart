import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubits/store_buy_cubit.dart';
import '../cubits/store_cubit.dart';
import '../cubits/store_state.dart';
import '../di/dependencies.dart';
import '../gen/assets.gen.dart';
import '../gen/colors.gen.dart';
import '../gen/translations.gen.dart';
import '../models/store.dart';
import '../utils/locale_utils.dart';
import 'badge.dart';
import 'loading_indicator.dart';

// ignore: avoid_classes_with_only_static_members
class BundleGridViewConfig {
  static const double horizontalMargin = 24;
  static const double verticalMargin = 16;
  static const double spaceBetweenCells = 16;
}

class BundleGridConfig {
  BundleGridConfig({required this.rows, required this.size});

  final int rows;
  final Size size;
}

class BundlesGrid extends StatefulWidget {
  const BundlesGrid({
    final Key? key,
    this.showNotBoughtOnly = false,
    this.showNotBoughtVisials = true,
    this.source = BuySource.newGame,
  }) : super(key: key);

  final bool showNotBoughtOnly;
  final bool showNotBoughtVisials;
  final BuySource source;

  static BundleGridConfig calculateViewSize(
      final BuildContext context, final int bundlesCount) {
    final int rows = _numberOfRows(bundlesCount);
    final double width = (MediaQuery.of(context).size.width -
            BundleGridViewConfig.horizontalMargin * 2 - // Horizontal margin
            BundleGridViewConfig
                .spaceBetweenCells) / // Between cells (horizontally)
        2;
    final double gridHeight = rows * width +
        (BundleGridViewConfig.spaceBetweenCells *
            (rows - 1)) + // Between cells (vertically)
        BundleGridViewConfig.verticalMargin * 2; // Vertical margin
    return BundleGridConfig(rows: rows, size: Size(width, gridHeight));
  }

  static int _numberOfRows(final int bundlesCount) {
    return (bundlesCount / 2).ceil();
  }

  @override
  State<BundlesGrid> createState() => _BundlesGridState();
}

class _BundlesGridState extends State<BundlesGrid> {
  @override
  Widget build(final BuildContext context) {
    return BlocBuilder<StoreCubit, StoreState>(
      builder: (final _, final StoreState state) {
        if (state is InitialState) {
          return const SizedBox(
            width: double.infinity,
            child: LoadingIndicator(),
          );
        } else if (state is FetchStoreState) {
          List<StoreItem> items = state.response.all(vertical: true);
          if (widget.showNotBoughtOnly) {
            items = items
                .where((final StoreItem element) =>
                    state.notBoughtInStoreIaps.contains(element.iap))
                .toList();
          }

          if (items.isEmpty) {
            return Container();
          }

          return GridView.count(
            scrollDirection: Axis.horizontal,
            crossAxisCount: BundlesGrid._numberOfRows(items.length),
            mainAxisSpacing: BundleGridViewConfig.spaceBetweenCells,
            crossAxisSpacing: BundleGridViewConfig.spaceBetweenCells,
            padding: const EdgeInsets.symmetric(
              horizontal: BundleGridViewConfig.horizontalMargin,
              vertical: BundleGridViewConfig.verticalMargin,
            ),
            children: List<Widget>.generate(
              items.length,
              (final int index) {
                final StoreItem item = items[index];
                return _StoreProduct(
                  item: item,
                  price: state.prices[item.iap] ?? '...',
                  onBuyTap: (final StoreItem item) {
                    getIt<StoreBuyCubit>().buyProduct(
                      productId: item.iap,
                      source: widget.source,
                    );
                  },
                  isSeasonal: state.response.seasonal.contains(item),
                  isTemporaryFree:
                      state.temporaryFreeInStoreIaps.contains(item.iap),
                  isBought: state.unlockedProductsBundles.contains(item.bundle),
                  showNotBoughtVisials: widget.showNotBoughtVisials,
                );
              },
            ),
          );
        }
        return Container();
      },
    );
  }
}

class _StoreProduct extends StatelessWidget {
  const _StoreProduct({
    final Key? key,
    required this.item,
    required this.onBuyTap,
    required this.isBought,
    required this.isTemporaryFree,
    required this.isSeasonal,
    required this.price,
    required this.showNotBoughtVisials,
  }) : super(key: key);

  final Function(StoreItem) onBuyTap;
  final StoreItem item;
  final bool isBought;
  final bool isTemporaryFree;
  final bool isSeasonal;
  final bool showNotBoughtVisials;
  final String price;

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (!isBought) {
          onBuyTap(item);
        }
      },
      child: Stack(
        clipBehavior: Clip.none,
        children: <Widget>[
          Opacity(
            opacity: showNotBoughtVisials
                ? isBought
                    ? 1
                    : 0.4
                : 1,
            child: Container(
              padding: const EdgeInsets.symmetric(
                horizontal: 10,
                vertical: 10,
              ),
              decoration: BoxDecoration(
                color: item.color,
                borderRadius: BorderRadius.circular(5),
              ),
              child: Column(
                children: <Widget>[
                  SizedBox(height: item.backgroundUrl != null ? 0 : 16),
                  Expanded(
                    child: Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        _wrapImageIfNeeded(item),
                        if (isTemporaryFree)
                          AspectRatio(
                            aspectRatio: 1,
                            child: Container(
                              alignment: Alignment.bottomRight,
                              child: FractionallySizedBox(
                                  widthFactor: 1,
                                  heightFactor: 0.6,
                                  child: Transform.translate(
                                      offset: const Offset(16, 10),
                                      child:
                                          Assets.images.freeIconBig.image())),
                            ),
                          ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 16),
                  Text(
                    localisedStoreItemTitle(item),
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: Theme.of(context)
                        .textTheme
                        .headlineMedium
                        ?.copyWith(color: Colors.white),
                  ),
                  if (isTemporaryFree || isSeasonal)
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Assets.images.timerIcon.image(),
                        const SizedBox(width: 8),
                        Text(
                          hintForItem(item, isSeasonal: isSeasonal),
                          textAlign: TextAlign.center,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context)
                              .textTheme
                              .labelLarge
                              ?.copyWith(color: Colors.white),
                        ),
                      ],
                    )
                  else
                    Opacity(
                      opacity: !isBought ? 1 : 0,
                      child: Text(
                        price,
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context)
                            .textTheme
                            .labelLarge
                            ?.copyWith(color: Colors.white),
                      ),
                    ),
                ],
              ),
            ),
          ),
          if (showNotBoughtVisials && !isBought)
            SelectedBadge(
              color: ColorName.darkRed,
              backgroundColor: ColorName.secondaryBackground,
              image: Assets.images.cartIcon.image(fit: BoxFit.none),
            )
        ],
      ),
    );
  }

  Widget _wrapImageIfNeeded(final StoreItem item) {
    final CachedNetworkImage image = CachedNetworkImage(
      imageUrl: item.backgroundUrl ?? item.imageUrl,
      fit: BoxFit.contain,
    );
    if (item.backgroundUrl == null) {
      return Center(
        child: AspectRatio(
          aspectRatio: 1,
          child: image,
        ),
      );
    }
    return image;
  }
}

String hintForItem(final StoreItem item, {required final bool isSeasonal}) {
  if (item.daysRemainingForPromo >= 1) {
    return LocaleKeys.store_offer_daysToEnd
        .plural(item.daysRemainingForPromo.toInt());
  }
  return isSeasonal
      ? LocaleKeys.store_offer_disappearSoon.tr()
      : LocaleKeys.store_offer_endsSoon.tr();
}

class UnlockAllProduct extends StatelessWidget {
  const UnlockAllProduct({
    final Key? key,
    required this.item,
    required this.onBuyTap,
    required this.price,
    required this.fullPrice,
  }) : super(key: key);

  final Function(IapItem) onBuyTap;
  final IapItem item;
  final String fullPrice;
  final String price;

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
      onTap: () {
        onBuyTap(item);
      },
      child: Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 10,
        ),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: Assets.images.unlockAllBg.provider(),
            fit: BoxFit.cover,
          ),
          color: ColorName.questionColor5,
          borderRadius: BorderRadius.circular(5),
        ),
        child: Column(
          children: <Widget>[
            const SizedBox(height: 16),
            Expanded(
              child: Center(
                child: AspectRatio(
                  aspectRatio: 1,
                  child: Assets.images.unlockAllIcon.image(),
                ),
              ),
            ),
            const SizedBox(height: 16),
            Text(
              LocaleKeys.store_unlockAll.tr(),
              textAlign: TextAlign.center,
              overflow: TextOverflow.ellipsis,
              maxLines: 1,
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium
                  ?.copyWith(color: Colors.white),
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Stack(children: <Widget>[
                  Text(
                    fullPrice,
                    textAlign: TextAlign.center,
                    overflow: TextOverflow.ellipsis,
                    style: Theme.of(context)
                        .textTheme
                        .labelLarge
                        ?.copyWith(color: Colors.white),
                  ),
                  Positioned(
                    left: -2,
                    right: -2,
                    top: 0,
                    bottom: 0,
                    child: Center(
                      child: Container(color: Colors.white, height: 2),
                    ),
                  )
                ]),
                const SizedBox(width: 8),
                Text(
                  price,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  style: Theme.of(context)
                      .textTheme
                      .labelLarge
                      ?.copyWith(color: Colors.white),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

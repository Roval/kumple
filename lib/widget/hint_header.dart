import 'package:flutter/material.dart';

import '../models/event.dart';
import '../utils/device_util.dart';
import 'default_container.dart';
import 'player_row.dart';

class HintHeader extends StatelessWidget {
  const HintHeader(
    this.text, {
    final Key? key,
    this.topPadding = 24,
    this.player,
    this.answer,
  }) : super(key: key);

  final String text;
  final double topPadding;
  final Player? player;
  final String? answer;

  @override
  Widget build(final BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        SizedBox(height: topPadding),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Text(
            text,
            textAlign: TextAlign.left,
            style: Theme.of(context).textTheme.headlineMedium,
          ),
        ),
        if (player != null) ...<Widget>[
          const SizedBox(height: 10),
          PlayerRow(player: player!),
        ],
        if (answer != null) ...<Widget>[
          const SizedBox(height: 10),
          _HintHeaderAnswer(answer: answer!),
        ],
      ],
    );
  }
}

class _HintHeaderAnswer extends StatelessWidget {
  const _HintHeaderAnswer({final Key? key, required this.answer})
      : super(key: key);

  final String answer;

  @override
  Widget build(final BuildContext context) {
    final TextStyle? titleFont = isSmallDevice
        ? Theme.of(context).textTheme.titleLarge
        : Theme.of(context).textTheme.headlineMedium;

    return DefaultContainer(
      child: Text(
        answer,
        style: titleFont,
      ),
    );
  }
}

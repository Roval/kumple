import 'package:flutter/material.dart';
import 'package:perfect_freehand/perfect_freehand.dart';

class Stroke {
  const Stroke(this.points, this.options);
  final List<Point> points;
  final StrokeOptions options;
}

class StrokeOptions {
  StrokeOptions({
    this.size = 10,
    this.color = Colors.black,
    this.isComplete = false,
  });

  /// The base size (diameter) of the stroke.
  double size;

  /// The color of stroke
  Color color;

  // Whether the line is complete.
  final bool isComplete;
}

class Sketcher extends CustomPainter {
  Sketcher({required this.lines});

  final List<Stroke> lines;

  @override
  void paint(final Canvas canvas, final Size size) {
    for (int i = 0; i < lines.length; ++i) {
      final StrokeOptions options = lines[i].options;
      final Paint paint = Paint()..color = options.color;
      final List<Point> outlinePoints = getStroke(
        lines[i].points,
        size: options.size,
        thinning: 0.4, // Responsible for pressure, when -0.99 then strict line
        simulatePressure: false,
        isComplete: options.isComplete,
      );

      final Path path = Path();

      if (outlinePoints.isEmpty) {
        return;
      } else if (outlinePoints.length < 2) {
        // If the path only has one line, draw a dot.
        path.addOval(Rect.fromCircle(
            center: Offset(outlinePoints[0].x, outlinePoints[0].y), radius: 1));
      } else {
        // Otherwise, draw a line that connects each point with a curve.
        path.moveTo(outlinePoints[0].x, outlinePoints[0].y);

        for (int i = 1; i < outlinePoints.length - 1; ++i) {
          final Point p0 = outlinePoints[i];
          final Point p1 = outlinePoints[i + 1];
          path.quadraticBezierTo(
              p0.x, p0.y, (p0.x + p1.x) / 2, (p0.y + p1.y) / 2);
        }
      }

      canvas.drawPath(path, paint);
    }
  }

  @override
  bool shouldRepaint(final Sketcher oldDelegate) {
    return true;
  }
}

import 'package:flutter/material.dart';

import '../gen/colors.gen.dart';

class PositionBadge extends StatelessWidget {
  const PositionBadge({
    final Key? key,
    required this.position,
  }) : super(key: key);

  final int position;

  @override
  Widget build(final BuildContext context) {
    return Container(
      height: 24,
      width: 24,
      padding: const EdgeInsets.only(top: 1),
      decoration: BoxDecoration(
        color: _bgColorPosition(),
        borderRadius: BorderRadius.circular(14),
        border: Border.all(
          color: _borderColorPosition(),
          width: 2,
        ),
      ),
      child: Text(
        '$position',
        textAlign: TextAlign.center,
        style: Theme.of(context).textTheme.bodyMedium?.copyWith(
              color: _textColorPosition(),
            ),
      ),
    );
  }

  Color _textColorPosition() {
    switch (position) {
      case 1:
        return const Color.fromRGBO(193, 105, 79, 1);
      case 2:
        return const Color.fromRGBO(75, 75, 75, 1);
      case 3:
        return const Color.fromRGBO(103, 60, 31, 1);
      default:
        return ColorName.text;
    }
  }

  Color _borderColorPosition() {
    switch (position) {
      case 1:
        return const Color.fromRGBO(193, 105, 79, 1);
      case 2:
        return const Color.fromRGBO(75, 75, 75, 1);
      case 3:
        return const Color.fromRGBO(103, 60, 31, 1);
      default:
        return Colors.white;
    }
  }

  Color _bgColorPosition() {
    switch (position) {
      case 1:
        return const Color.fromRGBO(255, 204, 77, 1);
      case 2:
        return const Color.fromRGBO(242, 242, 242, 1);
      case 3:
        return const Color.fromRGBO(225, 176, 105, 1);
      default:
        return Colors.white;
    }
  }
}

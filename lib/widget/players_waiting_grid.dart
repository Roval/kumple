import 'dart:math';

import 'package:collection/collection.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../gen/translations.gen.dart';
import '../models/event.dart';
import '../utils/app_theme.dart';
import '../utils/element_order.dart';
import 'player_row.dart';
import 'player_waiting_room.dart';
import 'tags.dart';

enum _PlayersWiatingListMode { joined, host }

class PlayersWaitingConfig {
  PlayersWaitingConfig.host({required final Function(int) onPlayerRemoved})
      : _mode = _PlayersWiatingListMode.host,
        _onPlayerRemoved = onPlayerRemoved,
        showEmptySlots = true;

  PlayersWaitingConfig.joined()
      : _mode = _PlayersWiatingListMode.joined,
        _onPlayerRemoved = null,
        showEmptySlots = true;

  final _PlayersWiatingListMode _mode;
  final Function(int)? _onPlayerRemoved;
  final bool showEmptySlots;
}

class PlayersWaitingRoomList extends StatefulWidget {
  const PlayersWaitingRoomList({
    final Key? key,
    required this.host,
    required this.players,
    required this.config,
    required this.bottomMargin,
    required this.maxPlayers,
    required this.onShare,
  }) : super(key: key);

  final int maxPlayers;
  final Players players;
  final String host;
  final double bottomMargin;
  final PlayersWaitingConfig config;
  final Function() onShare;

  @override
  State<PlayersWaitingRoomList> createState() => _PlayersWaitingRoomListState();
}

class _PlayersWaitingRoomListState extends State<PlayersWaitingRoomList> {
  @override
  Widget build(final BuildContext context) {
    final int nextPlayerCount =
        max(min(widget.players.length + 1, widget.maxPlayers), 3);

    return Column(
      children: <Widget>[
        ...widget.players.mapIndexed(
          (final int index, final Player player) => player.id == widget.host
              ? PlayerRowV2(
                  player: player,
                  expandName: false,
                  rightChild: const Padding(
                      padding: EdgeInsets.only(right: 16), child: HostTag()),
                )
              : PlayerRowV2(
                  player: player,
                  rightChild:
                      widget.config._mode == _PlayersWiatingListMode.host
                          ? GestureDetector(
                              onTap: () {
                                widget.config._onPlayerRemoved?.call(index);
                              },
                              child: const CloseTag(),
                            )
                          : null,
                ),
        ),
        if (widget.config.showEmptySlots &&
            nextPlayerCount > widget.players.length)
          PlayerEmptySlot(
            title: _emptySlotTilte(nextPlayerCount),
            onShare: widget.onShare,
          ),
        SizedBox(
            height: 16 +
                widget.bottomMargin +
                MediaQuery.of(context).viewPadding.bottom),
      ],
    );
  }

  String _emptySlotTilte(final int index) {
    return index == 3
        ? LocaleKeys.waitingRoom_list_requirePlayer3.tr()
        : LocaleKeys.waitingRoom_list_addPlayer.tr();
  }
}

class PlayersWaitingAnswerList extends StatelessWidget {
  const PlayersWaitingAnswerList({
    final Key? key,
    required this.players,
    required this.readyPlayers,
    required this.readyString,
  }) : super(key: key);

  final Players players;
  final List<String> readyPlayers;
  final String readyString;

  @override
  Widget build(final BuildContext context) {
    return ListView.separated(
      padding: EdgeInsets.only(bottom: Defaults.bottomSpacing),
      itemCount: players.length,
      itemBuilder: (final BuildContext context, final int index) {
        final Player player = players[index];
        final bool isReady = readyPlayers.contains(player.id);
        return PlayerWaitingRow(
          order: order(index, players.length),
          player: player,
          subtitle: isReady
              ? readyString
              : LocaleKeys.gameboards_awaitingOthers_pendingState.tr(),
          state:
              isReady ? PlayerWaitingState.ready : PlayerWaitingState.waiting,
        );
      },
      separatorBuilder: (final BuildContext context, final int index) =>
          const SizedBox(),
    );
  }
}

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../gen/assets.gen.dart';
import '../../gen/translations.gen.dart';
import '../../utils/app_theme.dart';
import '../../utils/element_order.dart';
import '../checkbox.dart';
import '../default_container.dart';
import '../game_button.dart';

enum Language {
  polish,
  ukrainian,
  german,
  english;

  String get value {
    switch (this) {
      case Language.polish:
        return 'polish';
      case Language.english:
        return 'english';
      case Language.ukrainian:
        return 'ukrainian';
      case Language.german:
        return 'german';
    }
  }

  static Language valueForStr(final String raw) {
    switch (raw) {
      case 'polish':
        return Language.polish;
      case 'english':
        return Language.english;
      case 'ukrainian':
        return Language.ukrainian;
      case 'german':
        return Language.german;
      default:
        return Language.polish;
    }
  }

  String get title {
    switch (this) {
      case Language.polish:
        return LocaleKeys.setupGame_settings_language_polish.tr();
      case Language.english:
        return LocaleKeys.setupGame_settings_language_english.tr();
      case Language.ukrainian:
        return LocaleKeys.setupGame_settings_language_ukrainian.tr();
      case Language.german:
        return LocaleKeys.setupGame_settings_language_german.tr();
    }
  }

  Widget get icon {
    switch (this) {
      case Language.polish:
        return Assets.images.polandIcon.image();
      case Language.english:
        return Assets.images.englishIcon.image();
      case Language.ukrainian:
        return Assets.images.ukraineIcon.image();
      case Language.german:
        return Assets.images.germanIcon.image();
    }
  }
}

class ChangeLanguageSettings extends StatefulWidget {
  const ChangeLanguageSettings({
    super.key,
    required this.initialLanguage,
    required this.onSave,
  });

  final Function(Language) onSave;
  final Language initialLanguage;

  @override
  State<ChangeLanguageSettings> createState() => _ChangeLanguageSettingsState();
}

class _ChangeLanguageSettingsState extends State<ChangeLanguageSettings> {
  late Language _currentLanguage;

  @override
  void initState() {
    super.initState();
    _currentLanguage = widget.initialLanguage;
  }

  @override
  Widget build(final BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Text(
          LocaleKeys.setupGame_settings_language_title.tr(),
          textAlign: TextAlign.left,
          maxLines: 1,
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        const SizedBox(height: 16),
        _LanguageRow(
          order: ElementOrder.first,
          language: Language.polish,
          isSelected: _currentLanguage == Language.polish,
          onTap: () {
            setState(() {
              _currentLanguage = Language.polish;
            });
          },
        ),
        _LanguageRow(
          order: ElementOrder.middle,
          language: Language.english,
          isSelected: _currentLanguage == Language.english,
          onTap: () {
            setState(() {
              _currentLanguage = Language.english;
            });
          },
        ),
        _LanguageRow(
          order: ElementOrder.middle,
          language: Language.german,
          isSelected: _currentLanguage == Language.german,
          onTap: () {
            setState(() {
              _currentLanguage = Language.german;
            });
          },
        ),
        _LanguageRow(
          order: ElementOrder.last,
          language: Language.ukrainian,
          isSelected: _currentLanguage == Language.ukrainian,
          onTap: () {
            setState(() {
              _currentLanguage = Language.ukrainian;
            });
          },
        ),
        const SizedBox(height: 24),
        GameButton(
          title: LocaleKeys.save.tr(),
          margin: EdgeInsets.zero,
          onPressed: () => widget.onSave(_currentLanguage),
        ),
        SizedBox(height: Defaults.bottomSpacing),
      ],
    );
  }
}

class _LanguageRow extends StatelessWidget {
  const _LanguageRow(
      {required this.language,
      required this.onTap,
      required this.order,
      required this.isSelected});

  final Language language;
  final ElementOrder order;
  final bool isSelected;
  final Function() onTap;

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: DefaultContainer(
        margin: EdgeInsets.zero,
        padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 16),
        order: order,
        child: Row(
          children: <Widget>[
            language.icon,
            const SizedBox(width: 12),
            Expanded(
              child: Text(
                language.title,
                textAlign: TextAlign.left,
                maxLines: 1,
                style: Theme.of(context).textTheme.titleLarge,
              ),
            ),
            GameCheckbox(isSelected: isSelected),
          ],
        ),
      ),
    );
  }
}

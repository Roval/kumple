import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../../gen/assets.gen.dart';
import '../../gen/translations.gen.dart';
import '../../utils/app_theme.dart';
import '../../utils/element_order.dart';
import '../checkbox.dart';
import '../default_container.dart';
import '../game_button.dart';

enum QuestionSetting {
  drag,
  photo;

  String get title {
    switch (this) {
      case QuestionSetting.drag:
        return LocaleKeys.setupGame_settings_quesitonTypes_noDrag.tr();
      case QuestionSetting.photo:
        return LocaleKeys.setupGame_settings_quesitonTypes_noImage.tr();
    }
  }

  String get value {
    switch (this) {
      case QuestionSetting.drag:
        return 'drag';
      case QuestionSetting.photo:
        return 'photo';
    }
  }

  static QuestionSetting? valueForStr(final String raw) {
    switch (raw) {
      case 'drag':
        return QuestionSetting.drag;
      case 'photo':
        return QuestionSetting.photo;
      default:
        return null;
    }
  }
}

class ChangeQuestionsSettings extends StatefulWidget {
  const ChangeQuestionsSettings({
    super.key,
    required this.initialExcludedQuestions,
    required this.onSave,
  });

  final Function(Set<QuestionSetting>) onSave;
  final Set<QuestionSetting> initialExcludedQuestions;

  @override
  State<ChangeQuestionsSettings> createState() =>
      _ChangeQuestionsSettingsState();
}

class _ChangeQuestionsSettingsState extends State<ChangeQuestionsSettings> {
  late Set<QuestionSetting> _currentSettings;

  @override
  void initState() {
    super.initState();
    _currentSettings =
        Set<QuestionSetting>.from(widget.initialExcludedQuestions); // Copy
  }

  @override
  Widget build(final BuildContext context) {
    final bool isPhotoExcluded =
        _currentSettings.contains(QuestionSetting.photo);
    final bool isDragExcluded = _currentSettings.contains(QuestionSetting.drag);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Text(
          LocaleKeys.setupGame_settings_quesitonTypes_title.tr(),
          textAlign: TextAlign.left,
          maxLines: 1,
          style: Theme.of(context).textTheme.headlineMedium,
        ),
        const SizedBox(height: 16),
        _QuestionSettingRow(
          order: ElementOrder.first,
          setting: QuestionSetting.drag,
          isSelected: isDragExcluded,
          onTap: () {
            _toggle(QuestionSetting.drag);
          },
        ),
        _QuestionSettingRow(
          order: ElementOrder.last,
          setting: QuestionSetting.photo,
          isSelected: isPhotoExcluded,
          onTap: () {
            _toggle(QuestionSetting.photo);
          },
        ),
        const SizedBox(height: 24),
        DefaultContainer(
          margin: EdgeInsets.zero,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Assets.images.infoIcon.image(),
              const SizedBox(width: 16),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Text(
                      LocaleKeys
                          .setupGame_settings_quesitonTypes_noQuestionHint1
                          .tr(),
                      textAlign: TextAlign.left,
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    Text(
                      LocaleKeys
                          .setupGame_settings_quesitonTypes_noQuestionHint2
                          .tr(),
                      textAlign: TextAlign.left,
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        const SizedBox(height: 24),
        GameButton(
          title: LocaleKeys.save.tr(),
          margin: EdgeInsets.zero,
          onPressed: () => widget.onSave(_currentSettings),
        ),
        SizedBox(height: Defaults.bottomSpacing),
      ],
    );
  }

  void _toggle(final QuestionSetting setting) {
    final bool isIncluded = _currentSettings.contains(setting);
    setState(() {
      if (isIncluded) {
        _currentSettings.remove(setting);
      } else {
        _currentSettings.add(setting);
      }
    });
  }
}

class _QuestionSettingRow extends StatelessWidget {
  const _QuestionSettingRow({
    required this.setting,
    required this.onTap,
    required this.order,
    required this.isSelected,
  });

  final QuestionSetting setting;
  final ElementOrder order;
  final bool isSelected;
  final Function() onTap;

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: DefaultContainer(
        margin: EdgeInsets.zero,
        padding: const EdgeInsets.symmetric(vertical: 14, horizontal: 16),
        order: order,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                setting.title,
                textAlign: TextAlign.left,
                maxLines: 1,
                style: Theme.of(context).textTheme.titleLarge,
              ),
            ),
            GameCheckbox(isSelected: isSelected),
          ],
        ),
      ),
    );
  }
}

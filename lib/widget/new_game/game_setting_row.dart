import 'package:flutter/material.dart';

import '../../gen/assets.gen.dart';
import '../../gen/colors.gen.dart';
import '../../utils/element_order.dart';
import '../default_container.dart';

class GameSettingRow extends StatelessWidget {
  const GameSettingRow({
    final Key? key,
    required this.title,
    required this.value,
    required this.order,
    required this.onTap,
    this.image,
  }) : super(key: key);

  final String title;
  final String value;
  final ElementOrder order;
  final Widget? image;
  final Function() onTap;

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: DefaultContainer(
        order: order,
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                title,
                textAlign: TextAlign.left,
                maxLines: 1,
                style: Theme.of(context).textTheme.titleLarge,
              ),
            ),
            if (image != null)
              Padding(padding: const EdgeInsets.only(right: 8), child: image),
            Text(
              value,
              textAlign: TextAlign.left,
              maxLines: 1,
              style: Theme.of(context)
                  .textTheme
                  .titleMedium
                  ?.copyWith(color: Colors.black.withOpacity(0.5)),
            ),
            const SizedBox(width: 14),
            Assets.images.gameArrowIcon.image(color: ColorName.text)
          ],
        ),
      ),
    );
  }
}

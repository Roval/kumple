import 'package:flutter/material.dart';

class SelectedBadge extends StatelessWidget {
  const SelectedBadge({
    final Key? key,
    required this.color,
    this.backgroundColor = Colors.white,
    required this.image,
  }) : super(key: key);

  final Color color;
  final Color backgroundColor;
  final Widget image;

  @override
  Widget build(final BuildContext context) {
    return Positioned(
      right: -10,
      top: -10,
      child: Container(
        width: 28,
        height: 28,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(14),
          color: backgroundColor,
        ),
        child: Center(
          // Must be done like this (circle in circle) because white boarder had red 1px line around view
          child: Container(
              width: 22,
              height: 22,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(11),
                color: color,
              ),
              child: image),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vector_graphics/vector_graphics.dart';

import '../../scenes/edit_profile_screen.dart';
import '../../utils/camera_util.dart';
import '../../utils/session_utils.dart';
import '../avatars.dart';
import '../loading_indicator.dart';
import 'edit_profile_emoji_setup.dart';

class AvatarSelectorViewConfig {
  static const double dimension = 150;
}

class EditProfileAvatarSelector extends StatelessWidget {
  const EditProfileAvatarSelector({super.key, required this.config});

  final EditAvatarConfig config;

  @override
  Widget build(final BuildContext context) {
    return Stack(children: <Widget>[
      if (config.avatarData != null)
        // From camera
        Center(
          child: Avatar(
            dimension: AvatarSelectorViewConfig.dimension,
            imageData: config.avatarData,
          ),
        )
      else if (config.mode == EditAvatarMode.fromWebCamera)
        // From web, already saved
        Center(
          child: Avatar(
            dimension: AvatarSelectorViewConfig.dimension,
            imageUrl: avatarUrl(player: getUserUuid()),
          ),
        )
      else if (config.emojiData != null)
        // Emoji configuration
        EmojiAvatar(config: config.emojiData!)
      else
        const SizedBox(
          height: AvatarSelectorViewConfig.dimension,
          child: LoadingIndicator(),
        ),
    ]);
  }
}

class EmojiAvatar extends StatelessWidget {
  const EmojiAvatar({
    super.key,
    required this.config,
  });

  final EmojiConfig config;

  @override
  Widget build(final BuildContext context) {
    return Center(
      child: Stack(
        alignment: Alignment.center,
        children: <Widget>[
          Container(
            width: AvatarSelectorViewConfig.dimension,
            height: AvatarSelectorViewConfig.dimension,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: EditProfileEmojiEditorViewConfig.colors[config.color] ??
                    EditProfileEmojiEditorViewConfig
                        .colors[EditProfileEmojiEditorViewConfig.defaultColor]!,
              ),
              borderRadius:
                  BorderRadius.circular(AvatarSelectorViewConfig.dimension / 2),
            ),
          ),
          SvgPicture(
            AssetBytesLoader(config.image),
            height: 85,
          ),
        ],
      ),
    );
  }
}

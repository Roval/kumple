import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../gen/colors.gen.dart';
import '../../gen/translations.gen.dart';
import '../../utils/upper_case_formatter.dart';
import '../default_container.dart';
import '../single_line.dart';

class EditProfileInputField extends StatefulWidget {
  const EditProfileInputField({
    super.key,
    required this.textFieldController,
  });

  final TextEditingController textFieldController;

  @override
  State<EditProfileInputField> createState() => _EditProfileInputFieldState();
}

class _EditProfileInputFieldState extends State<EditProfileInputField> {
  final FocusNode _focusNode = FocusNode();

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() {
      if (!_focusNode.hasFocus) {
        Future<void>.delayed(const Duration(seconds: 2))
            .then((final _) => SystemChrome.restoreSystemUIOverlays());
      }
    });
  }

  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 24),
          child: Text(
            LocaleKeys.editProfile_name.tr(),
            textAlign: TextAlign.left,
            maxLines: 1,
            style: Theme.of(context).textTheme.headlineMedium,
          ),
        ),
        const SizedBox(height: 16),
        DefaultContainer(
          child: Column(
            children: <Widget>[
              TextField(
                  key: const ValueKey<String>('nameInput'),
                  focusNode: _focusNode,
                  inputFormatters: <TextInputFormatter>[
                    UpperCaseTextFormatter(),
                    LengthLimitingTextInputFormatter(20),
                  ],
                  textCapitalization: TextCapitalization.characters,
                  enableSuggestions: false,
                  autocorrect: false,
                  textInputAction: TextInputAction.done,
                  onSubmitted: (final String value) =>
                      FocusManager.instance.primaryFocus?.unfocus(),
                  controller: widget.textFieldController,
                  maxLines: null,
                  minLines: 1,
                  decoration: InputDecoration(
                    isDense: true,
                    contentPadding: EdgeInsets.zero,
                    hintStyle:
                        Theme.of(context).textTheme.displaySmall?.copyWith(
                              color: ColorName.text50,
                            ),
                    hintText: LocaleKeys.editProfile_insertName.tr(),
                    border: InputBorder.none,
                  ),
                  textAlign: TextAlign.left,
                  style: Theme.of(context).textTheme.displaySmall),
              const SizedBox(height: 8),
              const SingleLine(height: 2, roundCorners: true),
            ],
          ),
        ),
      ],
    );
  }
}

import 'dart:typed_data';

import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:vector_graphics/vector_graphics.dart';

import '../../di/dependencies.dart';
import '../../extensions/list_extensions.dart';
import '../../gen/translations.gen.dart';
import '../../mixins/camera_dialog_mixin.dart';
import '../../routing/router.gr.dart';
import '../../scenes/edit_profile_screen.dart';
import '../../utils/app_theme.dart';
import '../default_container.dart';
import '../game_button.dart';
import '../game_header.dart';
import '../highlight_button.dart';
import '../modal_container.dart';
import '../scroll_with_line.dart';
import 'edit_profile_avatar_selector.dart';

// ignore: avoid_classes_with_only_static_members
class EditProfileEmojiEditorViewConfig {
  static const double verticalColorSpacing = 16;
  static const double emojiSpacing = 16;

  static const Map<String, List<Color>> colors = <String, List<Color>>{
    defaultColor: <Color>[Color(0xffc7f2ce), Color(0xff9be9a7)],
    'lightPurple': <Color>[Color(0xffe5ebff), Color(0xffc9d5ff)],
    'yellow': <Color>[Color(0xfffff6d2), Color(0xffffe697)],
    'pink': <Color>[Color(0xffffe1e9), Color(0xfffec1cf), Color(0xffffb2c4)],
    'lightBlue': <Color>[Color(0xffe1f5ff), Color(0xffbbeaff)],
    'red': <Color>[Color(0xffffd4d1), Color(0xfff5aeaa)],
    'lightGreen': <Color>[Color(0xffebffdb), Color(0xffd1fc9a)],
    'blue': <Color>[Color(0xffe0fafb), Color(0xff9ff3f8)],
    'orange': <Color>[Color(0xffffefe1), Color(0xffffe0c5)],
    'purple': <Color>[Color(0xffe7e2ff), Color(0xffd6cbfc)],
  };

  static const String defaultColor = 'green';

  static List<String> avatars = List<String>.generate(
      72, (final int index) => 'assets/svg/emoji${index + 1}.svg.vec');
}

class EditProfileEmojiEditor extends StatefulWidget {
  const EditProfileEmojiEditor({
    super.key,
    required this.config,
    required this.onSave,
  });

  final EditAvatarConfig config;
  final Function(EditAvatarConfig) onSave;

  @override
  State<EditProfileEmojiEditor> createState() => _EditProfileEmojiEditorState();
}

class _EditProfileEmojiEditorState extends State<EditProfileEmojiEditor>
    with CameraPermissionDialog<EditProfileEmojiEditor> {
  late EditAvatarConfig _config;

  @override
  void initState() {
    super.initState();

    // Create deep copy (except for avatar data, it cannot be changed here)
    _config = EditAvatarConfig(
        mode: widget.config.mode,
        avatarData: widget.config.avatarData,
        emojiData: widget.config.emojiData != null
            ? EmojiConfig(
                widget.config.emojiData!.color, widget.config.emojiData!.image)
            : null);
  }

  @override
  Widget build(final BuildContext context) {
    final double itemDimension = (MediaQuery.of(context).size.width -
            (ModalContainerViewConfig.horizontalPadding * 2) -
            DefaultContainerConfig.horizontalPadding * 2 -
            EditProfileEmojiEditorViewConfig.emojiSpacing * 5) /
        6;
    final double colorHorizontalSpacing = (MediaQuery.of(context).size.width -
            (ModalContainerViewConfig.horizontalPadding * 2) -
            DefaultContainerConfig.horizontalPadding * 2 -
            itemDimension * 5) /
        4;

    final double height = MediaQuery.of(context).size.height -
        GameHeaderViewConfig.minHeight -
        MediaQuery.of(context).padding.top -
        ModalContainerViewConfig.dragHeight;

    return SizedBox(
      height: height,
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              DefaultContainer(
                margin: EdgeInsets.zero,
                child: EditProfileAvatarSelector(
                  config: _config,
                ),
              ),
              const SizedBox(height: 8),
              Expanded(
                child: ScrollWithLine(
                  child: SingleChildScrollView(
                    child: Column(
                      children: <Widget>[
                        const SizedBox(height: 8),
                        DefaultContainer(
                          margin: EdgeInsets.zero,
                          child: Wrap(
                            spacing: colorHorizontalSpacing,
                            runSpacing: EditProfileEmojiEditorViewConfig
                                .verticalColorSpacing,
                            children: EditProfileEmojiEditorViewConfig
                                .colors.entries
                                .map(
                                  (final MapEntry<String, List<Color>> entry) =>
                                      HighlightButton(
                                          onTap: () => _selectColor(entry.key),
                                          builder: (final BuildContext context,
                                              final bool highlight) {
                                            return Opacity(
                                              opacity: highlight ? 0.5 : 1,
                                              child: Container(
                                                width: itemDimension,
                                                height: itemDimension,
                                                decoration: BoxDecoration(
                                                  gradient: LinearGradient(
                                                    begin: Alignment.topCenter,
                                                    end: Alignment.bottomCenter,
                                                    colors: entry.value,
                                                  ),
                                                  border: entry.key ==
                                                          _config
                                                              .emojiData?.color
                                                      ? Border.all(width: 2)
                                                      : null,
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          itemDimension / 2),
                                                ),
                                              ),
                                            );
                                          }),
                                )
                                .toList(),
                          ),
                        ),
                        const SizedBox(height: 16),
                        DefaultContainer(
                          margin: EdgeInsets.zero,
                          child: Wrap(
                            spacing:
                                EditProfileEmojiEditorViewConfig.emojiSpacing,
                            runSpacing:
                                EditProfileEmojiEditorViewConfig.emojiSpacing,
                            children: EditProfileEmojiEditorViewConfig.avatars
                                .map(
                                  (final String avatar) => HighlightButton(
                                    onTap: () => _selectImage(avatar),
                                    builder: (final BuildContext context,
                                        final bool highlight) {
                                      return Opacity(
                                        opacity: highlight ? 0.5 : 1,
                                        child: Container(
                                          width: itemDimension,
                                          height: itemDimension,
                                          padding: const EdgeInsets.all(7),
                                          decoration: BoxDecoration(
                                            color: const Color(0xfff4f4f5),
                                            border: avatar ==
                                                    _config.emojiData?.image
                                                ? Border.all(width: 2)
                                                : null,
                                            borderRadius: BorderRadius.circular(
                                                itemDimension / 2),
                                          ),
                                          child: SvgPicture(
                                              AssetBytesLoader(avatar)),
                                        ),
                                      );
                                    },
                                  ),
                                )
                                .toList(),
                          ),
                        ),
                        SizedBox(
                            height: Defaults.buttomSpacingScroll +
                                16 +
                                GameButtonViewConfig.height),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
          Positioned(
            bottom: Defaults.bottomSpacing,
            left: 0,
            right: 0,
            child: Column(
              children: <Widget>[
                GameButton(
                  key: const ValueKey<String>('cameraButton'),
                  margin: EdgeInsets.zero,
                  style: const GameButtonStyle.whiteWithBorder(),
                  title: LocaleKeys.editProfile_takePicture.tr(),
                  dropShadow: true,
                  onPressed: _openCamera,
                ),
                const SizedBox(height: 16),
                GameButton(
                  key: const ValueKey<String>('readyButton'),
                  margin: EdgeInsets.zero,
                  title: LocaleKeys.editProfile_readyButton.tr(),
                  dropShadow: true,
                  onPressed: _acceptData,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _acceptData() {
    FirebaseAnalytics.instance.logEvent(name: 'avatar_selector_ready');
    widget.onSave(_config);
  }

  Future<void> _openCamera() async {
    FirebaseAnalytics.instance.logEvent(name: 'edit_avatar_open_camera');
    final bool granted = await checkPermissions();
    if (!granted) {
      return;
    }

    getIt<AppRouter>().push(Camera(onImageTaken: (final Uint8List bytes) {
      setState(() {
        _config = EditAvatarConfig.fromCamera(bytes);
      });
    }));
  }

  void _selectImage(final String avatar) {
    final String currentColor = _config.emojiData?.color ??
        EditProfileEmojiEditorViewConfig.colors.keys.toList().randomItem();
    setState(() {
      _config = EditAvatarConfig.fromEmoji(currentColor, avatar);
    });
  }

  void _selectColor(final String color) {
    final String currentImage = _config.emojiData?.image ??
        EditProfileEmojiEditorViewConfig.avatars.randomItem();
    setState(() {
      _config = EditAvatarConfig.fromEmoji(color, currentImage);
    });
  }
}

import 'package:flutter/material.dart';

import '../gen/assets.gen.dart';
import '../gen/colors.gen.dart';

class StarWithPointsViewConfig {
  static const double horizontalSpace = 8;
  static const double textWidth = 24;
  static const double width =
      textWidth + horizontalSpace + 16; // 16 is start width
}

class StarWithPoints extends StatelessWidget {
  const StarWithPoints({
    final Key? key,
    required this.points,
  }) : super(key: key);

  final int points;

  @override
  Widget build(final BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        ConstrainedBox(
          constraints: const BoxConstraints(
            minWidth: StarWithPointsViewConfig.textWidth,
            maxWidth: StarWithPointsViewConfig.textWidth,
          ),
          child: Text(
            '$points',
            textAlign: TextAlign.right,
            style: Theme.of(context).textTheme.titleLarge?.copyWith(
                  color: ColorName.orange,
                ),
          ),
        ),
        const SizedBox(width: StarWithPointsViewConfig.horizontalSpace),
        Transform.translate(
            offset: const Offset(0, -1), child: Assets.images.starIcon.image()),
      ],
    );
  }
}

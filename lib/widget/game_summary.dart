import 'package:cached_network_image/cached_network_image.dart';
import 'package:collection/collection.dart';
import 'package:easy_localization/easy_localization.dart' as localization;
import 'package:flutter/material.dart';

import '../cubits/game_state.dart';
import '../di/dependencies.dart';
import '../gen/assets.gen.dart';
import '../gen/colors.gen.dart';
import '../gen/translations.gen.dart';
import '../models/store.dart';
import '../repositories/remote_config.dart';
import '../routing/router.gr.dart';
import '../scenes/new_game_screen.dart';
import '../utils/app_theme.dart';
import '../utils/locale_utils.dart';
import 'default_container.dart';
import 'image_button.dart';
import 'new_game/change_questions_setting.dart';

// ignore: avoid_classes_with_only_static_members
class _GameSummaryViewConfig {
  static final TextStyle textStyle = appTheme.textTheme.bodyMedium!;
  static const double horizontalMargin = 10;
  static const double iconDimension = 16;
  static const double iconMargin = 8;
  static const int maxRowsCount = 2;
  static const double componentSpacing = 8;
}

class GameSummary extends StatefulWidget {
  const GameSummary({
    final Key? key,
    required this.lastState,
    required this.isMyGame,
    required this.roomKey,
    required this.showAllItems,
    required this.onItemsToggle,
  }) : super(key: key);

  final JoinState lastState;
  final bool isMyGame;
  final String roomKey;
  final bool showAllItems;
  final Function() onItemsToggle;

  @override
  State<GameSummary> createState() => _GameSummaryState();
}

class _Component {
  _Component({
    required this.title,
    this.imageUrl,
    this.imageWidget,
    this.action,
  });

  final String title;
  final String? imageUrl;
  final Widget? imageWidget;
  final Function()? action;
}

class _GameSummaryState extends State<GameSummary> {
  /// Required to correctly map icon from category name
  StoreDefinition? _storeResponse;
  List<_Component> _components = <_Component>[];
  int _wrapItemsCount = 0;

  @override
  void initState() {
    super.initState();

    // Load store content
    _storeResponse = RemoteConfigHelper.lastFetchedStoreResponse;
    RemoteConfigHelper.storeContent().then((final StoreDefinition value) {
      _storeResponse = value;
      // Recalculate components
      if (mounted) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(final BuildContext context) {
    _calculateComponents();

    return DefaultContainer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            children: <Widget>[
              Expanded(
                child: Text(
                  LocaleKeys.waitingRoom_id.tr(
                    namedArgs: <String, String>{'id': widget.roomKey},
                  ).toUpperCase(),
                  textAlign: TextAlign.left,
                  maxLines: 1,
                  style: Theme.of(context).textTheme.displayMedium,
                ),
              ),
              if (widget.isMyGame)
                ImageButton(
                  image: Assets.images.settingsSmallIcon,
                  onPressed: () {
                    getIt<AppRouter>().push(
                      NewGame(
                        config: NewGameConfig.edit(lastState: widget.lastState),
                      ),
                    );
                  },
                ),
            ],
          ),
          const SizedBox(height: 16),
          Wrap(
            spacing: _GameSummaryViewConfig.componentSpacing,
            runSpacing: 12,
            children: _components
                .sublist(0,
                    widget.showAllItems ? _components.length : _wrapItemsCount)
                .map((final _Component e) => _GameAttribiute(component: e))
                .toList(),
          ),
        ],
      ),
    );
  }

  void _calculateComponents() {
    _components = <_Component>[
      // language
      _Component(
          title: widget.lastState.language.title,
          imageWidget: widget.lastState.language.icon),

      // excluded questions
      ...widget.lastState.excludedQuestions.map(
        (final QuestionSetting e) => _Component(title: e.title),
      ),

      // categories
      if (_storeResponse != null)
        ...widget.lastState.categories.map((final String category) {
          final List<StoreItem> categories =
              _storeResponse?.all() ?? <StoreItem>[];
          final StoreItem? matching = categories.firstWhereOrNull(
              (final StoreItem item) => item.bundle == category);

          return _Component(
              title: matching != null
                  ? localisedStoreItemTitle(matching)
                  : category,
              imageUrl: matching?.imageUrl);
        }),

      // zip all
      if (widget.showAllItems)
        _Component(
          title: LocaleKeys.waitingRoom_tags_showLess.tr(),
          action: widget.onItemsToggle,
        )
    ];

    _wrapItemsCount = _calculateWrapLimit();
  }

  int _calculateWrapLimit() {
    final double availableWidth = MediaQuery.of(context).size.width -
        (2 * DefaultContainerConfig.horizontalMargin) -
        (2 * DefaultContainerConfig.horizontalPadding);

    final List<double> allWidths =
        _components.map((final _Component e) => _widthForComponent(e)).toList();

    int itemCount = 0;
    int currentIndex = 0;
    for (int row = 0; row < _GameSummaryViewConfig.maxRowsCount; row++) {
      double rowWidth = availableWidth;
      bool firstItem = true;
      final bool isLastRowForCollapsed = !widget.showAllItems &&
          row + 1 == _GameSummaryViewConfig.maxRowsCount;
      _Component? previousExpandRow;
      // ignore: unnecessary_statements
      for (currentIndex; currentIndex < _components.length; currentIndex++) {
        double nextButtonWidth = 0;
        _Component? lastItem;
        if (isLastRowForCollapsed) {
          // Last row, we need to start calculating "Expand" button width
          final int remainingItems = _components.length - itemCount - 1;
          if (remainingItems > 0) {
            lastItem = _Component(
              title: LocaleKeys.waitingRoom_tags_remainingBundlesCount
                  .tr(namedArgs: <String, String>{'count': '$remainingItems'}),
              action: widget.onItemsToggle,
            );
            nextButtonWidth = _widthForComponent(lastItem) +
                _GameSummaryViewConfig.componentSpacing;
          }
        }

        final double remainingWidth = currentIndex + 1 < _components.length
            ? allWidths.sublist(currentIndex + 1).reduce(
                (final double value, final double element) =>
                    value + element + _GameSummaryViewConfig.componentSpacing)
            : 0;

        final double itemWidth = allWidths[currentIndex];
        rowWidth -= itemWidth +
            (firstItem ? 0 : _GameSummaryViewConfig.componentSpacing);
        // If remaining space for row will be enough for all buttons - skip "expand"
        // This can be an issue if last button is smaller than "expand" button
        if (remainingWidth > rowWidth) {
          rowWidth -= nextButtonWidth;
        } else {
          nextButtonWidth = 0;
          lastItem = null;
        }
        if (rowWidth < 0) {
          if (isLastRowForCollapsed && previousExpandRow != null) {
            _components.insert(currentIndex, previousExpandRow);
            itemCount += 1;
          }
          break;
        }
        // Restore row width, so it doesn't have "expand" button inside of it
        rowWidth += nextButtonWidth;
        previousExpandRow = lastItem;
        itemCount += 1;
        firstItem = false;
      }
    }

    return itemCount;
  }

  double _widthForComponent(final _Component component) {
    final TextPainter textPainter = TextPainter(
      text: TextSpan(
          text: component.title, style: _GameSummaryViewConfig.textStyle),
      textDirection: TextDirection.ltr,
      textScaleFactor:
          WidgetsBinding.instance.platformDispatcher.textScaleFactor,
    )..layout();
    return textPainter.size.width +
        (2 * _GameSummaryViewConfig.horizontalMargin) +
        (component.imageUrl != null || component.imageWidget != null
            ? _GameSummaryViewConfig.iconDimension +
                _GameSummaryViewConfig.iconMargin
            : 0);
  }
}

class _GameAttribiute extends StatelessWidget {
  const _GameAttribiute({required this.component});

  final _Component component;

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
      onTap: component.action,
      child: Container(
        height: 24,
        padding: const EdgeInsets.symmetric(
            horizontal: _GameSummaryViewConfig.horizontalMargin),
        decoration: BoxDecoration(
          color: component.action != null ? Colors.white : ColorName.lightGrey,
          borderRadius: BorderRadius.circular(12),
          border: component.action != null ? Border.all() : null,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            if (component.imageUrl != null) ...<Widget>[
              CachedNetworkImage(
                imageUrl: component.imageUrl!,
                width: _GameSummaryViewConfig.iconDimension,
                height: _GameSummaryViewConfig.iconDimension,
              ),
              const SizedBox(width: _GameSummaryViewConfig.iconMargin),
            ],
            if (component.imageWidget != null) ...<Widget>[
              component.imageWidget!,
              const SizedBox(width: _GameSummaryViewConfig.iconMargin),
            ],
            Text(
              component.title,
              textAlign: TextAlign.center,
              maxLines: 1,
              style: _GameSummaryViewConfig.textStyle,
            ),
          ],
        ),
      ),
    );
  }
}

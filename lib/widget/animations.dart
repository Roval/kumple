import 'package:flutter/material.dart';

class PopAnimation extends StatelessWidget {
  const PopAnimation({
    final Key? key,
    required this.child,
    required final Animation<double> animation,
    this.scale = 0.3,
  })  : _animation = animation,
        super(key: key);

  final Widget child;
  final double scale;
  final Animation<double> _animation;

  @override
  Widget build(final BuildContext context) {
    final bool reverse = _animation.value > 0.5;

    return Transform.scale(
      scale: 1 + (scale * (((reverse ? 1 : 0) - _animation.value) / 0.5)),
      child: child,
    );
  }
}

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

import '../di/dependencies.dart';
import '../gen/assets.gen.dart';
import '../gen/colors.gen.dart';
import '../mixins/close_dialog_mixin.dart';
import '../models/event.dart';
import '../repositories/remote_config.dart';
import '../routing/router.gr.dart';
import '../scenes/game_screen.dart';
import 'chat/chat_button.dart';
import 'chat/chat_module.dart';
import 'delayed_tween_animation_builder.dart';
import 'image_button.dart';

class NewGameHeader extends _GameHeader {
  const NewGameHeader({
    final Key? key,
    final Color color = ColorName.secondaryBackground,
    this.backImage,
  }) : super(key: key, color: color);

  final AssetGenImage? backImage;

  @override
  List<Widget> rightWidgets(final BuildContext context) {
    return <Widget>[];
  }

  @override
  List<Widget> leftWidgets(final BuildContext context) {
    return <Widget>[
      ImageButton(
        image: backImage ?? Assets.images.backSmallIcon,
        padding: const EdgeInsets.symmetric(horizontal: 24),
        height: GameHeaderViewConfig.minHeight,
        onPressed: () {
          getIt<AppRouter>().popForced();
        },
      )
    ];
  }
}

class WaitingRoomHeader extends _GameHeader {
  const WaitingRoomHeader({
    final Key? key,
    required final String roomKey,
    required this.isMyGame,
    required final ChatModuleController chatController,
  }) : super(
          key: key,
          roomKey: roomKey,
          chatController: chatController,
        );

  final bool isMyGame;

  @override
  List<Widget> rightWidgets(final BuildContext context) {
    final Widget? chatButton = _chatButton(shouldBeWhite: false);
    return <Widget>[
      if (chatButton != null) chatButton,
    ];
  }

  @override
  List<Widget> leftWidgets(final BuildContext context) {
    return <Widget>[
      _closeButton(context, Assets.images.backSmallIcon),
    ];
  }

  @override
  void close({final String? roomKey}) {
    if (isMyGame) {
      super.close(roomKey: roomKey);
    } else {
      showCloseOrLeaveDialog(roomKey: roomKey);
    }
  }
}

class GameplayHeader extends StatefulWidget {
  const GameplayHeader({
    final Key? key,
    required this.rounds,
    required this.colorController,
    required this.roomKey,
    required this.chatController,
    this.onHelperTap,
    this.categoryImageUrl,
  }) : super(key: key);

  final Rounds rounds;
  final String roomKey;
  final String? categoryImageUrl;
  final GameplayHeaderController colorController;
  final ChatModuleController chatController;
  final Function()? onHelperTap;

  @override
  State<GameplayHeader> createState() => _GameplayHeaderState();
}

class _GameplayHeaderState extends State<GameplayHeader> {
  Color color = ColorName.questionColor1;

  /// Set by listening to changes in color controller
  bool _triggerBgColorAnimate = false;

  String? _lastPublishedIconUrl;

  @override
  void initState() {
    super.initState();
    widget.colorController.addListener(controllerCallback);
  }

  @override
  void dispose() {
    widget.colorController.removeListener(controllerCallback);
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    final bool shouldAnimateBgColor = _triggerBgColorAnimate;
    _triggerBgColorAnimate = false;

    final _GameplayHeader result = _GameplayHeader(
      shouldAnimateBgColor: shouldAnimateBgColor,
      rounds: widget.rounds,
      color: widget.colorController.color,
      roomKey: widget.roomKey,
      iconUrl: widget.categoryImageUrl,
      shouldAnimateIcon: _lastPublishedIconUrl != widget.categoryImageUrl,
      onHelperTap: widget.onHelperTap,
      chatController: widget.chatController,
    );
    _lastPublishedIconUrl = widget.categoryImageUrl;

    return result;
  }

  void controllerCallback() {
    setState(() {
      _triggerBgColorAnimate = true;
    });
  }
}

class _GameplayHeader extends _GameHeader {
  const _GameplayHeader({
    final Key? key,
    required final Rounds rounds,
    required final Color color,
    required final String roomKey,
    required final String? iconUrl,
    required final bool shouldAnimateBgColor,
    required final bool shouldAnimateIcon,
    required final ChatModuleController chatController,
    this.onHelperTap,
  }) : super(
            key: key,
            roomKey: roomKey,
            color: color,
            iconUrl: iconUrl,
            shouldAnimateBgColor: shouldAnimateBgColor,
            shouldAnimateIcon: shouldAnimateIcon,
            chatController: chatController,
            rounds: rounds);

  final Function()? onHelperTap;

  @override
  List<Widget> leftWidgets(final BuildContext context) {
    return <Widget>[
      _closeButton(
        context,
        Assets.images.closeIcon,
      ),
      if (onHelperTap != null)
        ImageButton(
          padding: const EdgeInsets.only(right: 24),
          onPressed: onHelperTap!,
          height: GameHeaderViewConfig.minHeight,
          image: Assets.images.questionIcon,
        )
    ];
  }

  @override
  List<Widget> rightWidgets(final BuildContext context) {
    final Widget? chatButton = _chatButton(shouldBeWhite: true);
    return <Widget>[
      if (chatButton != null) chatButton,
    ];
  }
}

// ignore: avoid_classes_with_only_static_members
class GameHeaderViewConfig {
  static const double minHeight = 55;
  static const double progressBarHeight = 6;
  static const double progressBarHeightTopMargin = 10;
  static const double progressBarHeightBottomMargin = 10;
  static const double minHeightWithRounds = minHeight +
      progressBarHeight +
      progressBarHeightTopMargin +
      progressBarHeightBottomMargin;
  static const int colorChangeDuration = 500;
  static const int nextRoundAnimation = 500;
  static const int iconChangeDuration = 300;
  static const int iconShowDuration = 3500;
}

abstract class _GameHeader extends StatelessWidget with CloseDialog {
  const _GameHeader({
    final Key? key,
    this.roomKey,
    this.iconUrl,
    this.shouldAnimateBgColor = false,
    this.shouldAnimateIcon = false,
    this.color = ColorName.secondaryBackground,
    this.chatController,
    this.rounds,
  }) : super(key: key);

  final String? roomKey;
  final Color color;
  final String? iconUrl;
  final bool shouldAnimateBgColor;
  final bool shouldAnimateIcon;
  final ChatModuleController? chatController;
  final Rounds? rounds;

  @override
  Widget build(final BuildContext context) {
    final double topPadding = MediaQuery.of(context).padding.top;
    return Stack(
      clipBehavior: Clip.none,
      children: <Widget>[
        ConstrainedBox(
          constraints: BoxConstraints(
              minHeight: GameHeaderViewConfig.minHeight + topPadding),
          child: Stack(
            alignment: Alignment.bottomCenter,
            children: <Widget>[
              Positioned.fill(
                child: AnimatedContainer(
                    duration: Duration(
                        milliseconds: shouldAnimateBgColor
                            ? GameHeaderViewConfig.colorChangeDuration
                            : 0),
                    curve: Curves.fastOutSlowIn,
                    color: color),
              ),
              Container(
                padding: EdgeInsets.only(top: topPadding),
                child: Column(
                  children: <Widget>[
                    Stack(
                      alignment: Alignment.center,
                      children: <Widget>[
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Row(
                            children: leftWidgets(context),
                          ),
                        ),
                        _middleWidget(context),
                        Align(
                          alignment: Alignment.centerRight,
                          child: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: rightWidgets(context),
                          ),
                        ),
                      ],
                    ),
                    if (rounds != null)
                      Container(
                        margin: const EdgeInsets.only(
                          top: GameHeaderViewConfig.progressBarHeightTopMargin,
                          bottom: GameHeaderViewConfig
                              .progressBarHeightBottomMargin,
                        ),
                        padding: const EdgeInsets.symmetric(
                            horizontal: 22), // 2 points are in progress bar
                        height: GameHeaderViewConfig.progressBarHeight,
                        child: Row(
                          children: List<Widget>.generate(
                            rounds!.maxRounds,
                            (final int index) => Expanded(
                              child: Container(
                                clipBehavior: Clip.hardEdge,
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 2),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(
                                        GameHeaderViewConfig.progressBarHeight /
                                            2)),
                                child: AnimatedContainer(
                                  duration: const Duration(
                                      milliseconds: GameHeaderViewConfig
                                          .nextRoundAnimation),
                                  curve: Curves.bounceInOut,
                                  color: index < rounds!.current
                                      ? Colors.white
                                      : Colors.white.withOpacity(0.4),
                                ),
                              ),
                            ),
                          ),
                        ),
                      )
                    else
                      Container(),
                  ],
                ),
              ),
            ],
          ),
        ),
        Positioned(
          // Fix for one pixel line
          bottom: -0.5,
          left: 0,
          right: 0,
          child: AnimatedContainer(
            duration: Duration(
                milliseconds: shouldAnimateBgColor
                    ? GameHeaderViewConfig.colorChangeDuration
                    : 0),
            curve: Curves.fastOutSlowIn,
            height: 1,
            color: color,
          ),
        ),
      ],
    );
  }

  List<Widget> leftWidgets(final BuildContext context);

  List<Widget> rightWidgets(final BuildContext context);

  Widget _middleWidget(final BuildContext context) {
    if (iconUrl == null) {
      return Center(child: Assets.images.logoSmall.image());
    }
    return Center(
      child: SizedBox(
          width: 40,
          height: 40,
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              DelayedTweenAnimationBuilder<double>(
                tween: Tween<double>(begin: 1, end: 0),
                duration: const Duration(
                    milliseconds: GameHeaderViewConfig.iconChangeDuration),
                delay: const Duration(
                    milliseconds: GameHeaderViewConfig.iconShowDuration),
                child: CachedNetworkImage(imageUrl: iconUrl!),
                builder: (final BuildContext context, final double value,
                    final Widget? child) {
                  return Opacity(opacity: value, child: child);
                },
              ),
              DelayedTweenAnimationBuilder<double>(
                tween: Tween<double>(begin: 0, end: 1),
                duration: const Duration(
                    milliseconds: GameHeaderViewConfig.iconChangeDuration),
                delay: const Duration(
                  milliseconds: GameHeaderViewConfig.iconShowDuration +
                      GameHeaderViewConfig.iconChangeDuration,
                ),
                child: Assets.images.logoSmall.image(),
                builder: (final BuildContext context, final double value,
                    final Widget? child) {
                  return Opacity(opacity: value, child: child);
                },
              )
            ],
          )),
    );
  }

  Widget _closeButton(final BuildContext context, final AssetGenImage image) {
    return ImageButton(
      image: image,
      padding: const EdgeInsets.symmetric(horizontal: 24),
      height: GameHeaderViewConfig.minHeight,
      onPressed: () {
        close(roomKey: roomKey);
      },
    );
  }

  Widget? _chatButton({required final bool shouldBeWhite}) {
    if (RemoteConfigHelper.isChatEnabled &&
        roomKey != null &&
        chatController != null) {
      return ChatButton(
        controller: chatController!,
        roomKey: roomKey!,
        shouldBeWhite: shouldBeWhite,
      );
    }
    return null;
  }
}

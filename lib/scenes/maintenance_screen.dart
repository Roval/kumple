import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../cubits/chat_cubit.dart';
import '../di/dependencies.dart';
import '../gen/colors.gen.dart';
import '../gen/translations.gen.dart';
import '../utils/locale_utils.dart';
import '../utils/route_aware_observer.dart';

class MaintenancePage extends StatefulWidget {
  const MaintenancePage({super.key});

  @override
  State<MaintenancePage> createState() => _MaintenancePageState();
}

class _MaintenancePageState extends State<MaintenancePage>
    with RouteAwareAnalytics<MaintenancePage> {
  @override
  String get screenClass => 'maintenance_screen';
  @override
  String get screenName => 'Maintenance';

  @override
  void initState() {
    super.initState();

    // Stop observing every channel when maintenance displayed
    getIt<ChatCubit>().stopObservingAllChannels();
  }

  @override
  Widget build(final BuildContext context) {
    return Scaffold(
      backgroundColor: ColorName.primaryBackground,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            localisedLogo.image(width: 146, height: 180),
            const SizedBox(height: 40),
            Text(
              LocaleKeys.maintenance.tr(),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ],
        ),
      ),
    );
  }
}

import 'package:applovin_max/applovin_max.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:transparent_pointer/transparent_pointer.dart';

import '../cubits/chat_cubit.dart';
import '../cubits/game_cubit.dart';
import '../cubits/loading_cubit.dart';
import '../di/dependencies.dart';
import '../environment_config.dart';
import '../gen/colors.gen.dart';
import '../gen/translations.gen.dart';
import '../mixins/join_action_mixin.dart';
import '../plugins/facebook_plugin.dart';
import '../repositories/remote_config.dart';
import '../routing/router.gr.dart';
import '../utils/app_theme.dart';
import '../utils/dynamic_link_service.dart';
import '../utils/locale_utils.dart';
import '../utils/logger.dart';
import '../utils/notifications_service.dart';
import '../utils/route_aware_observer.dart';
import '../widget/drag_indicator.dart';
import '../widget/game_button.dart';
import '../widget/join_listener.dart';
import '../widget/new_game/change_language_setting.dart';
import '../widget/new_game/change_questions_setting.dart';
import '../widget/single_line.dart';
import '../widget/welcome_segments/my_games_segment.dart';
import '../widget/welcome_segments/settings_segment.dart';
import '../widget/welcome_segments/store_segment.dart';
import 'new_game_screen.dart';

bool _adsInitialised = false;

class WelcomeSegments {
  static const int yourGames = 0;
  static const int store = 1;
  static const int settings = 2;
}

// ignore: avoid_classes_with_only_static_members
class WelcomePageViewConfig {
  static const double bottomSheetHeight = 0.45;

  static double segmentsHeight = Defaults.dockDragIndicatorBottomMargin +
      DragIndicatorViewConfig.height +
      Defaults.dockDragIndicatorTopMargin +
      _SegmentTitleViewConfig.titleBottomMargin +
      _SegmentTitleViewConfig.indicatorHeight +
      appTheme.textTheme.titleLarge!.fontSize!;
}

class WelcomePage extends StatefulWidget {
  const WelcomePage({final Key? key}) : super(key: key);

  @override
  State<WelcomePage> createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage>
    with
        JoinAction,
        TickerProviderStateMixin,
        RouteAwareAnalytics<WelcomePage> {
  @override
  String get screenClass => 'welcome_screen';
  @override
  String get screenName => 'Welcome';

  int _selectedSegment = WelcomeSegments.yourGames;
  ScrollController? _controller;

  @override
  void initState() {
    super.initState();
    DynamicLinkService().startListeniningForDynamicLinks();
    getIt<NotificationsService>().checkInitialPush();
    getIt<NotificationsService>().startListeniningNotifications();

    // Stop observing every channel when back to welcome screan
    getIt<ChatCubit>().stopObservingAllChannels();

    _startAdsAndTracking();

    // Load content needed for game
    Future<void>.delayed(const Duration(seconds: 2))
        .then((final _) => RemoteConfigHelper.loadFriendlyHints());

    // TODO: Start asking user if they want to change avatar because it is very dark
    // migrateAvatarIfNeeded().then((final bool shouldMigrateAvatar) {});
  }

  Future<void> _startAdsAndTracking() async {
    if (_adsInitialised) {
      return;
    }

    _adsInitialised = true;
    // Init AppLovin
    if (!EnvironmentConfig.isTestPackage) {
      AppLovinMAX.setPrivacyPolicyUrl('https://buddies.fun/privacy-policy');
      AppLovinMAX.setConsentFlowEnabled(true);
      AppLovinMAX.setConsentFlowDebugUserGeography(
          ConsentFlowUserGeography.other);
      final Map<dynamic, dynamic>? config = await AppLovinMAX.initialize(
          '9BKZsa7_CTiGWgfncaFB_ECcwkfsUaGLv0JRabEO1o3a7nTVndbZP9Mn5qy8O9PUVvpnefTbuTtJ9bNEB5FwLC');
      logger.i('[ATT] AppLoving configured $config');

      final bool granted =
          (config?['appTrackingTransparencyStatus'] == 'granted');
      FacebookPlugin.updateAttStatus(isEnabled: granted);

      // AppLovinMAX.showMediationDebugger();
    }
  }

  @override
  void dispose() {
    logger.d('welcome screen disposed');
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return JoinListener(
      child: WillPopScope(
        onWillPop: () async => false,
        child: Scaffold(
          backgroundColor: ColorName.primaryBackground,
          resizeToAvoidBottomInset: false,
          body: SafeArea(
            bottom: false,
            child: Stack(
              children: <Widget>[
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    const SizedBox(height: 24),
                    Expanded(
                      child: Container(
                        child: localisedLogo.image(fit: BoxFit.scaleDown),
                      ),
                    ),
                    const SizedBox(height: 40),
                    GameButton(
                      key: const ValueKey<String>('createGame'),
                      title: LocaleKeys.setupGame_title_newGame.tr(),
                      onPressed: _onCreatePressed,
                    ),
                    const SizedBox(height: 16),
                    GameButton(
                      title: LocaleKeys.joinGame_title.tr(),
                      onPressed: _onJoinPressed,
                      style: GameButtonStyle.outline(),
                    ),
                    const SizedBox(height: 32),
                    SizedBox(
                      height: (MediaQuery.of(context).size.height -
                              MediaQuery.of(context).padding.top) *
                          WelcomePageViewConfig.bottomSheetHeight,
                    ),
                  ],
                ),
                DraggableScrollableSheet(
                  initialChildSize: WelcomePageViewConfig.bottomSheetHeight,
                  minChildSize: WelcomePageViewConfig.bottomSheetHeight,
                  maxChildSize: 0.8,
                  snap: true,
                  builder: (final BuildContext context,
                      final ScrollController scrollController) {
                    _controller = scrollController;
                    return Container(
                      decoration: const BoxDecoration(
                        color: ColorName.secondaryBackground,
                        borderRadius: BorderRadius.only(
                          topLeft: Defaults.bottomSheetCornerRadius,
                          topRight: Defaults.bottomSheetCornerRadius,
                        ),
                      ),
                      child: Stack(
                        children: <Widget>[
                          Positioned.fill(
                              child: _bottomSheetContent(scrollController)),
                          TransparentPointer(
                            child: Container(
                              decoration: const BoxDecoration(
                                color: ColorName.secondaryBackground,
                                borderRadius: BorderRadius.only(
                                  topLeft: Defaults.bottomSheetCornerRadius,
                                  topRight: Defaults.bottomSheetCornerRadius,
                                ),
                              ),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  const SizedBox(
                                      height:
                                          Defaults.dockDragIndicatorTopMargin),
                                  const DragIndicator(),
                                  const SizedBox(
                                      height: Defaults
                                          .dockDragIndicatorBottomMargin),
                                  _DockSegments(
                                    selectedSegment: _selectedSegment,
                                    onSegmentChange:
                                        _changeBottomSheetToSegment,
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _changeBottomSheetToSegment(final int segment) {
    if (_controller != null && _controller!.positions.isNotEmpty) {
      _controller!.jumpTo(0);
    }
    setState(() {
      _selectedSegment = segment;
    });
  }

  Widget _bottomSheetContent(final ScrollController controller) {
    return SingleChildScrollView(
      padding: EdgeInsets.only(
          bottom: MediaQuery.of(context).padding.bottom,
          top: WelcomePageViewConfig.segmentsHeight),
      controller: controller,
      child: Stack(
        children: <Widget>[
          Offstage(
            offstage: _selectedSegment != WelcomeSegments.yourGames,
            child: MyGamesSegment(
              onGameSelected: (final String game) {
                // Consume remaining notification
                // It fixed below bug:
                // 1. Have chat opened
                // 2. Go to background and click notification
                // 3. We store last opened channel to open chat next time it is entered
                // 4. Close the game, open it again and bam - chat is displayed
                final String? _ =
                    getIt<NotificationsService>().consumeLastNotificationRoom();
                joinRoom(game, isJoiningForTheFirstTime: false);
              },
            ),
          ),
          Offstage(
            offstage: _selectedSegment != WelcomeSegments.store,
            child: const StoreSegment(),
          ),
          Offstage(
            offstage: _selectedSegment != WelcomeSegments.settings,
            child: const SettingsSegment(),
          ),
        ],
      ),
    );
  }

  Future<void> _onCreatePressed() async {
    logger.i('user clicked create');
    FirebaseAnalytics.instance.logEvent(name: 'create_game');
    FacebookPlugin.logEvent(name: 'create_game');

    await _requestNotificationsSettigns();

    await verifyIfNameAndPhotoExistsThenProceed(
      success: (final bool isNewUser, final String name) {
        if (isNewUser) {
          _createGameForNewPlayer(name);
        } else {
          _createGameForExistingPlayer();
        }
      },
    );
  }

  void _createGameForNewPlayer(final String name) {
    final Language language = defaultQuestionsLanguage;
    FirebaseAnalytics.instance.logEvent(
      name: 'create_game_new_player',
      parameters: <String, Object>{
        'rounds': '12',
        'language': language.value,
        'excluded': ''
      },
    );
    getIt<LoadingCubit>().showLoading();

    // We can force unwrap here, name will be set on previous screen
    getIt<GameCubit>().createGameRoom(
      name: name,
      iaps: <String>[], // Default will be applied on backend
      rounds: 12,
      language: defaultQuestionsLanguage,
      excludedCategories: <QuestionSetting>{},
    );
  }

  void _createGameForExistingPlayer() {
    getIt<AppRouter>().pushAndPopUntil(
        NewGame(
          config: NewGameConfig.create(),
        ),
        predicate: (final Route<dynamic> route) =>
            route.settings.name == Welcome.name);
  }

  Future<void> _onJoinPressed() async {
    logger.i('user clicked join');
    FirebaseAnalytics.instance.logEvent(name: 'join_game');
    FacebookPlugin.logEvent(name: 'join_game');

    await _requestNotificationsSettigns();
    await verifyIfNameAndPhotoExistsThenProceed(
      success: (final bool isNewUser, final String name) async {
        getIt<AppRouter>().pushAndPopUntil(const JoinGame(),
            predicate: (final Route<dynamic> route) =>
                route.settings.name == Welcome.name);
      },
    );
  }

  Future<void> _requestNotificationsSettigns() async {
    final NotificationsService notificationService =
        getIt<NotificationsService>();
    final bool wasPermissionAskedBefore =
        await notificationService.wasPermissionAsked();
    if (!wasPermissionAskedBefore) {
      await notificationService.requestPermission();
    }
  }
}

// ignore: avoid_classes_with_only_static_members
class _SegmentTitleViewConfig {
  static const double titleBottomMargin = 8;
  static const double indicatorHeight = 2;
  static Color highlightColor = Colors.black.withAlpha(10);
}

class _SegmentTitle extends StatelessWidget {
  const _SegmentTitle({
    final Key? key,
    required this.title,
    required this.isHighlighted,
    required this.onSegmentTap,
  }) : super(key: key);

  final String title;
  final bool isHighlighted;
  final Function() onSegmentTap;

  @override
  Widget build(final BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: () => onSegmentTap(),
        child: Container(
          margin: const EdgeInsets.all(2),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(7),
            boxShadow: <BoxShadow>[
              if (isHighlighted)
                BoxShadow(
                  color: _SegmentTitleViewConfig.highlightColor,
                  spreadRadius: 1,
                  blurRadius: 8,
                  offset: const Offset(0, 3), // changes position of shadow
                ),
            ],
            border: isHighlighted
                ? Border.all(
                    width: 0.5,
                    color: _SegmentTitleViewConfig.highlightColor,
                  )
                : null,
            color: isHighlighted ? Colors.white : Colors.transparent,
          ),
          width: double.infinity,
          child: Center(
            child: Text(title,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.titleLarge),
          ),
        ),
      ),
    );
  }
}

class _DockSegmentsViewConfig {
  static const double height = 32;
}

class _DockSegments extends StatelessWidget {
  const _DockSegments({
    final Key? key,
    required this.selectedSegment,
    required this.onSegmentChange,
  }) : super(key: key);

  final int selectedSegment;
  final Function(int) onSegmentChange;

  @override
  Widget build(final BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 24),
      height: _DockSegmentsViewConfig.height,
      decoration: BoxDecoration(
        borderRadius: Defaults.cornerRadius,
        color: ColorName.lightGrey,
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          _SegmentTitle(
            title: LocaleKeys.welcome_segments_games.tr(),
            isHighlighted: selectedSegment == WelcomeSegments.yourGames,
            onSegmentTap: () => onSegmentChange(WelcomeSegments.yourGames),
          ),
          VerticalLine(
              color: selectedSegment == WelcomeSegments.settings
                  ? ColorName.grey.withOpacity(0.3)
                  : Colors.transparent),
          _SegmentTitle(
            title: LocaleKeys.welcome_segments_store.tr(),
            isHighlighted: selectedSegment == WelcomeSegments.store,
            onSegmentTap: () => onSegmentChange(WelcomeSegments.store),
          ),
          VerticalLine(
              color: selectedSegment == WelcomeSegments.yourGames
                  ? ColorName.grey.withOpacity(0.3)
                  : Colors.transparent),
          _SegmentTitle(
            title: LocaleKeys.welcome_segments_settings.tr(),
            isHighlighted: selectedSegment == WelcomeSegments.settings,
            onSegmentTap: () => onSegmentChange(WelcomeSegments.settings),
          ),
        ],
      ),
    );
  }
}

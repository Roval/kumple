import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';

import '../gen/assets.gen.dart';
import '../utils/route_aware_observer.dart';
import '../widget/camera.dart';
import '../widget/game_header.dart';

class CameraPage extends StatefulWidget {
  const CameraPage({
    final Key? key,
    required this.onImageTaken,
  }) : super(key: key);

  final Function(Uint8List) onImageTaken;

  @override
  State<CameraPage> createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage>
    with RouteAwareAnalytics<CameraPage> {
  @override
  String get screenClass => 'camera_screen';
  @override
  String get screenName => 'Camera';

  final InputCameraController _cameraController = InputCameraController();

  @override
  void initState() {
    super.initState();
    _cameraController.addListener(() {
      _controllerChanged();
    });
  }

  @override
  void dispose() {
    _cameraController.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    // Android (AndroidX) library has ratio 4 / 3
    // iOS has ratio 16 / 9
    final double ratio = Platform.isIOS ? 16 / 9 : 4 / 3;
    return Scaffold(
      backgroundColor: Colors.black,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            NewGameHeader(
              color: Colors.black,
              backImage: Assets.images.closeIcon,
            ),
            const SizedBox(height: 40),
            AspectRatio(
              aspectRatio: 1,
              child: FittedBox(
                clipBehavior: Clip.hardEdge,
                fit: BoxFit.fitWidth,
                child: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.width * ratio,
                  child: Camera(controller: _cameraController),
                ),
              ),
            ),
            const SizedBox(height: 32),
            Expanded(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 24),
                child: Stack(
                  children: <Widget>[
                    CameraButtons(
                      cameraController: _cameraController,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _controllerChanged() async {
    final Uint8List? file = _cameraController.imageTaken;
    if (file == null) {
      setState(() {}); // May be triggered by changing flash mode
      return;
    }
    setState(() {
      widget.onImageTaken(file);
      Navigator.of(context).pop();
    });
  }
}

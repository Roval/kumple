import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../cubits/store_buy_cubit.dart';
import '../di/dependencies.dart';
import '../gen/colors.gen.dart';
import '../gen/translations.gen.dart';
import '../models/store.dart';
import '../routing/router.gr.dart';
import '../utils/app_theme.dart';
import '../utils/locale_utils.dart';
import '../utils/logger.dart';
import '../utils/route_aware_observer.dart';
import '../widget/animated_draggable.dart';
import '../widget/game_button.dart';
import '../widget/modal_container.dart';

class OfferPage extends StatefulWidget {
  const OfferPage({
    final Key? key,
    required this.price,
    required this.item,
  }) : super(key: key);

  final String price;
  final StoreItem item;

  @override
  State<OfferPage> createState() => _OfferPageState();
}

class _OfferPageState extends State<OfferPage>
    with RouteAwareAnalytics<OfferPage> {
  @override
  String get screenClass => 'offer_screen';
  @override
  String get screenName => 'Offer';

  final AnimatedDraggableController _draggableController =
      AnimatedDraggableController();

  @override
  void dispose() {
    logger.d('dispose offer');
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    final StoreItem item = widget.item;
    final String name = localisedStoreItemTitle(item);
    final String hint = localisedStoreItemHint(item);

    return WillPopScope(
      onWillPop: () async {
        _close();
        return false;
      },
      child: AnimatedDraggable(
        onDraggedDown: () {
          getIt<AppRouter>().popForced();
        },
        controller: _draggableController,
        child: ModalContainer(
          backgroundColor: item.color,
          dragOpacity: 0.4,
          padding: EdgeInsets.zero,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              CachedNetworkImage(
                height: 66,
                imageUrl: item.imageUrl,
              ),
              const SizedBox(height: 14),
              Text(
                name,
                textAlign: TextAlign.center,
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: Theme.of(context)
                    .textTheme
                    .headlineMedium
                    ?.copyWith(color: Colors.white),
              ),
              const SizedBox(height: 4),
              Text(
                widget.price,
                textAlign: TextAlign.left,
                overflow: TextOverflow.ellipsis,
                style: Theme.of(context)
                    .textTheme
                    .bodyLarge
                    ?.copyWith(color: Colors.white),
              ),
              const SizedBox(height: 20),
              Container(
                padding: const EdgeInsets.symmetric(horizontal: 18),
                color: ColorName.secondaryBackground,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    const SizedBox(height: 20),
                    Text(
                      LocaleKeys.offer_title.tr(
                        namedArgs: <String, String>{
                          'name': name,
                        },
                      ),
                      textAlign: TextAlign.left,
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                    const SizedBox(height: 8),
                    Text(
                      LocaleKeys.offer_subtitle.tr(
                        namedArgs: <String, String>{'example': hint},
                      ),
                      textAlign: TextAlign.left,
                      style: Theme.of(context).textTheme.titleMedium,
                    ),
                    const SizedBox(height: 24),
                    GameButton(
                      title: LocaleKeys.offer_buttons_buy.tr(
                        namedArgs: <String, String>{
                          'name': name,
                          'price': widget.price,
                        },
                      ),
                      margin: EdgeInsets.zero,
                      onPressed: () async {
                        _close();
                        await Future<dynamic>.delayed(
                            const Duration(seconds: 1));
                        getIt<StoreBuyCubit>().buyProduct(
                          productId: item.iap,
                          source: BuySource.offer,
                        );
                      },
                    ),
                    const SizedBox(height: 16),
                    GameButton(
                      title: LocaleKeys.offer_buttons_close.tr().toUpperCase(),
                      margin: EdgeInsets.zero,
                      style: const GameButtonStyle.none(),
                      onPressed: () {
                        _close();
                      },
                    ),
                    SizedBox(height: Defaults.bottomSpacing),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _close() {
    _draggableController.close();
  }
}

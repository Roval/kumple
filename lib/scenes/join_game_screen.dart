import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../gen/colors.gen.dart';
import '../gen/translations.gen.dart';
import '../mixins/join_action_mixin.dart';
import '../utils/app_theme.dart';
import '../utils/route_aware_observer.dart';
import '../utils/upper_case_formatter.dart';
import '../widget/game_button.dart';
import '../widget/game_header.dart';
import '../widget/single_line.dart';

class JoinGamePage extends StatefulWidget {
  const JoinGamePage({final Key? key}) : super(key: key);

  @override
  State<JoinGamePage> createState() => _JoinGamePageState();
}

class _JoinGamePageState extends State<JoinGamePage>
    with JoinAction, RouteAwareAnalytics<JoinGamePage> {
  @override
  String get screenClass => 'join_game_screen';
  @override
  String get screenName => 'JoinGame';

  final TextEditingController _textFieldController = TextEditingController();
  final FocusNode _focusNode = FocusNode();

  String get _id => _textFieldController.text.trim();

  @override
  void initState() {
    super.initState();
    _textFieldController.addListener(() {
      setState(() {});
    });
    _focusNode.addListener(() {
      if (!_focusNode.hasFocus) {
        Future<void>.delayed(const Duration(seconds: 2))
            .then((final _) => SystemChrome.restoreSystemUIOverlays());
      }
    });
  }

  @override
  void dispose() {
    _focusNode.dispose();
    _textFieldController.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return Scaffold(
      backgroundColor: ColorName.secondaryBackground,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            const NewGameHeader(),
            const SizedBox(height: 8),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 24),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Text(
                    LocaleKeys.joinGame_title.tr(),
                    textAlign: TextAlign.left,
                    maxLines: 1,
                    style: Theme.of(context).textTheme.displayMedium,
                  ),
                  const SizedBox(height: 24),
                  Text(
                    LocaleKeys.joinGame_roomId.tr(),
                    textAlign: TextAlign.left,
                    maxLines: 1,
                    style: Theme.of(context).textTheme.headlineMedium,
                  ),
                  TextField(
                    autofocus: true,
                    focusNode: _focusNode,
                    inputFormatters: <TextInputFormatter>[
                      UpperCaseTextFormatter(),
                      LengthLimitingTextInputFormatter(20),
                    ],
                    enableSuggestions: false,
                    autocorrect: false,
                    textInputAction: TextInputAction.done,
                    onSubmitted: (final String value) =>
                        FocusManager.instance.primaryFocus?.unfocus(),
                    controller: _textFieldController,
                    keyboardType: TextInputType.number,
                    maxLines: null,
                    minLines: 1,
                    decoration: InputDecoration(
                      hintStyle: Theme.of(context)
                          .textTheme
                          .displayMedium
                          ?.copyWith(color: ColorName.text50),
                      hintText: LocaleKeys.joinGame_insertRoomId.tr(),
                      border: InputBorder.none,
                    ),
                    textAlign: TextAlign.left,
                    style: Theme.of(context).textTheme.displayMedium,
                  ),
                  const SingleLine(height: 2, roundCorners: true),
                ],
              ),
            ),
            Expanded(child: Container()),
            const SizedBox(height: 16),
            GameButton(
              title: LocaleKeys.joinGame_button.tr(),
              onPressed: _id.isEmpty ? null : _acceptJoin,
            ),
            SizedBox(height: Defaults.bottomSpacing),
          ],
        ),
      ),
    );
  }

  void _acceptJoin() {
    FirebaseAnalytics.instance.logEvent(name: 'join_game_confirmed');
    joinRoom(_id, isJoiningForTheFirstTime: true);
  }
}

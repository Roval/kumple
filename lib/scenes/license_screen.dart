import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import '../di/dependencies.dart';
import '../gen/assets.gen.dart';
import '../gen/colors.gen.dart';
import '../gen/translations.gen.dart';
import '../routing/router.gr.dart';
import '../utils/app_theme.dart';
import '../utils/route_aware_observer.dart';

// Taken from: https://github.com/noxasch/custom_license_page/blob/master/lib/license_screen.dart

class LicensesPage extends StatefulWidget {
  const LicensesPage({
    final Key? key,
  }) : super(key: key);

  @override
  State<LicensesPage> createState() => _LicensesPageState();
}

class _LicensesPageState extends State<LicensesPage>
    with RouteAwareAnalytics<LicensesPage> {
  @override
  String get screenClass => 'license_screen';
  @override
  String get screenName => 'License';

  final List<Widget> _licenses = <Widget>[];
  final Map<String, List<Widget>> _licenseContent = <String, List<Widget>>{};
  bool _loaded = false;

  @override
  void initState() {
    super.initState();
    _initLicenses();
  }

  Future<void> _initLicenses() async {
    // Most of these part are taken from flutter showLicensePage
    await for (final LicenseEntry license in LicenseRegistry.licenses) {
      List<Widget> tempSubWidget = <Widget>[];
      final List<LicenseParagraph> paragraphs =
          await SchedulerBinding.instance.scheduleTask<List<LicenseParagraph>>(
        license.paragraphs.toList,
        Priority.animation,
        debugLabel: 'License',
      );

      if (_licenseContent.containsKey(license.packages.join(', '))) {
        tempSubWidget = _licenseContent[license.packages.join(', ')]!;
      }

      for (final LicenseParagraph paragraph in paragraphs) {
        if (paragraph.indent == LicenseParagraph.centeredIndent) {
          tempSubWidget.add(Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              paragraph.text,
              style: appTheme.textTheme.bodyLarge,
            ),
          ));
        } else {
          tempSubWidget.add(Padding(
            padding: const EdgeInsets.all(8),
            child: Text(
              paragraph.text,
              style: appTheme.textTheme.bodyLarge,
            ),
          ));
        }
      }
      tempSubWidget.add(const Divider());
      _licenseContent[license.packages.join(', ')] = tempSubWidget;
    }

    _licenseContent.forEach((final String key, final List<Widget> value) {
      int count = 0;
      for (final Widget element in value) {
        if (element.runtimeType == Divider) {
          count += 1;
        }
      }
      // Replace ExpansionTile with any widget that suits you
      _licenses.add(ExpansionTile(
        title: Text(key, style: appTheme.textTheme.headlineSmall),
        subtitle: Text(
          '$count licenses',
          style: appTheme.textTheme.bodyLarge,
        ),
        trailing: const Text('🔽'),
        children: value,
      ));
    });

    setState(() {
      _loaded = true;
    });
  }

  @override
  Widget build(final BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: ColorName.secondaryBackground,
        shadowColor: Colors.transparent,
        leading: IconButton(
          icon: Assets.images.backSmallIcon.image(),
          onPressed: () {
            getIt<AppRouter>().pop();
          },
        ),
        centerTitle: true,
        title: ConstrainedBox(
          constraints: const BoxConstraints(maxWidth: 200),
          child: Text(LocaleKeys.settings_license.tr(),
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.headlineMedium),
        ),
      ),
      body: Container(
        decoration: const BoxDecoration(
          border: Border(top: BorderSide(color: Colors.grey)),
        ),
        child: SafeArea(
          child: !_loaded
              ? const Center(child: CircularProgressIndicator())
              : ListView.separated(
                  itemCount: _licenses.length,
                  separatorBuilder:
                      (final BuildContext context, final int index) =>
                          const Divider(),
                  itemBuilder: (final BuildContext context, final int index) {
                    return _licenses.elementAt(index);
                  },
                ),
        ),
      ),
    );
  }
}

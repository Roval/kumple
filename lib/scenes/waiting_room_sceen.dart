import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubits/categories_usage_cubit.dart';
import '../cubits/categories_usage_state.dart';
import '../cubits/game_cubit.dart';
import '../cubits/game_state.dart';
import '../di/dependencies.dart';
import '../gen/colors.gen.dart';
import '../gen/translations.gen.dart';
import '../mixins/camera_dialog_mixin.dart';
import '../mixins/close_dialog_mixin.dart';
import '../models/event.dart';
import '../models/session.dart';
import '../plugins/notifications_plugin.dart';
import '../repositories/remote_config.dart';
import '../routing/router.gr.dart';
import '../utils/app_theme.dart';
import '../utils/appsflyer_helper.dart';
import '../utils/logger.dart';
import '../utils/prompters/questions_usage_prompter.dart';
import '../utils/route_aware_observer.dart';
import '../utils/session_utils.dart';
import '../utils/share_util.dart';
import '../widget/animated_board.dart';
import '../widget/chat/chat_module.dart';
import '../widget/default_container.dart';
import '../widget/game_button.dart';
import '../widget/game_header.dart';
import '../widget/game_summary.dart';
import '../widget/hints/info_hint.dart';
import '../widget/players_waiting_grid.dart';
import '../widget/scroll_with_line.dart';
import '../widget/waiting_room/waiting_room_empty_list.dart';
import '../widget/waiting_room/waiting_room_no_one_to_play.dart';
import '../widget/waiting_room/waiting_room_share_row.dart';

class WaitingRoomPage extends StatefulWidget {
  const WaitingRoomPage({final Key? key, required this.session})
      : super(key: key);

  final Session session;

  @override
  State<WaitingRoomPage> createState() => _WaitingRoomPageState();
}

class _WaitingRoomPageState extends State<WaitingRoomPage>
    with
        WidgetsBindingObserver,
        CameraPermissionDialog<WaitingRoomPage>,
        CloseDialog,
        RouteAwareAnalytics<WaitingRoomPage> {
  @override
  String get screenClass => 'waiting_room_screen';
  @override
  String get screenName => 'WaitingRoom';

  /// Latest state fetched, used to check ownership and ready players
  JoinState? _lastState;

  /// Shows all items in summary
  bool _showAllItems = false;

  bool get _isMyGame => _lastState!.owner == getUserUuid();
  bool get _allExceptHostReady =>
      _lastState!.players.length > 2 &&
      (_lastState!.players.length - _lastState!.readyPlayers.length) <= 1;

  final ChatModuleController _chatController = ChatModuleController();

  /// If true we should show dialog that player has no one to play with
  bool shouldShowNoInviteOverlay = false;

  bool get _isOnlyHost => _lastState!.players.length <= 1;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addObserver(this);

    // Clear notifications
    NotificationsPlugin.removeNotificationsWithTag(widget.session.roomKey);
  }

  @override
  void dispose() {
    logger.d('waiting room screen disposed');
    WidgetsBinding.instance.removeObserver(this);
    _chatController.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return BlocListener<GameCubit, GameState>(
      listener: (final _, final GameState state) {
        if (state is DisconnectState) {
          // Go back to welcome screen
          getIt<AppRouter>().replaceAll(<PageRouteInfo>[const Welcome()]);
        }
        if (state is KickedState && state.roomKey == widget.session.roomKey) {
          logger.i('kicked from room ${state.roomKey}');
          // Go back to welcom screen
          getIt<AppRouter>().replaceAll(<PageRouteInfo>[const Welcome()]);
        }
        if (state is QuestionState) {
          // TODO: Co jak jestem w INNYM waiting roomie, czy przypadkiem nie przeskocze tam? mozliwe jak socket nie bedzie trzymal info w ktorym pokoju jestem, teraz jest ok
          FirebaseAnalytics.instance.logLevelStart(
            levelName: 'round ${state.rounds.current}',
          );
          logAppsFlyerEvent(
            'af_level_achieved',
            params: <String, dynamic>{'af_level': state.rounds.current},
          );

          getIt<AppRouter>().replaceAll(<PageRouteInfo>[
            GameRoom(
              session: widget.session,
              initialRounds: state.rounds,
            ),
          ]);
        }
      },
      child: BlocBuilder<GameCubit, GameState>(
        buildWhen: (final GameState previous, final GameState current) =>
            current is JoinState,
        builder: (final _, final GameState s) {
          final JoinState state = s as JoinState;
          _lastState = state;
          final bool isPlayerReady = state.readyPlayers.contains(getUserUuid());
          return ChatModule(
            roomKey: widget.session.roomKey,
            controller: _chatController,
            players: _lastState?.players ?? <Player>[],
            onClose: () {
              logCurrentScreen();
            },
            waitBeforeLaunch: const Duration(seconds: 3),
            child: WillPopScope(
              onWillPop: () async {
                if (_chatController.isOpened) {
                  _chatController.close();
                } else {
                  showCloseDialog(roomKey: widget.session.roomKey);
                }
                return false;
              },
              child: Scaffold(
                  backgroundColor: ColorName.secondaryBackground,
                  body: SafeArea(
                    bottom: false,
                    top: false,
                    child: Stack(
                      children: <Widget>[
                        _contentList(context, state),
                        if (_allExceptHostReady) _bottomButtons(isPlayerReady),
                        if (shouldShowNoInviteOverlay)
                          WaitingRoomNoOneToPlayWith(onShare: () {
                            _shareGame(eventName: 'share_no_one');
                          }, onDragDown: () {
                            setState(() {
                              shouldShowNoInviteOverlay = false;
                            });
                          }),
                      ],
                    ),
                  )),
            ),
          );
        },
      ),
    );
  }

  BlocBuilder<CategoriesUsageCubit, CategoriesUsageState> _contentList(
      final BuildContext context, final JoinState state) {
    return BlocBuilder<CategoriesUsageCubit, CategoriesUsageState>(
      builder: (final _, final CategoriesUsageState usageState) {
        HintUsageModel? hintModel;
        if (usageState is FetchedUsageState) {
          hintModel = hostCreateUsageHintModel(usageState);
        }

        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            WaitingRoomHeader(
              roomKey: widget.session.roomKey,
              isMyGame: _isMyGame,
              chatController: _chatController,
            ),
            Expanded(
              child: ScrollWithLine(
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      const SizedBox(height: 8),
                      if (_isMyGame && hintModel != null)
                        Padding(
                          padding: const EdgeInsets.only(bottom: 24),
                          child: InfoHint(
                            animationAsset: hintModel.animation,
                            title: hintModel.title,
                            subtitle: hintModel.description,
                            backgroundColor: hintModel.backgroundColor,
                          ),
                        ),
                      Stack(
                        children: <Widget>[
                          // Background color, to be visible between summary and invite buttons
                          if (!_isOnlyHost)
                            Positioned(
                              top: 40,
                              bottom: 40,
                              left: 24,
                              right: 24,
                              child:
                                  Container(color: ColorName.primaryBackground),
                            ),
                          Column(
                            children: <Widget>[
                              GameSummary(
                                lastState: _lastState!,
                                isMyGame: _isMyGame,
                                roomKey: widget.session.roomKey,
                                showAllItems: _showAllItems,
                                onItemsToggle: () {
                                  setState(() {
                                    _showAllItems = !_showAllItems;
                                    FirebaseAnalytics.instance.logEvent(
                                        name: _showAllItems
                                            ? 'game_summary_expanded'
                                            : 'games_summary_collapse');
                                  });
                                },
                              ),
                              if (!_isOnlyHost)
                                DefaultContainer(
                                  color: ColorName.primaryBackground,
                                  child: Column(
                                    children: <Widget>[
                                      WaitingRoomShareRow(
                                          roomKey: widget.session.roomKey),
                                      GameButton(
                                        key: const ValueKey<String>(
                                            'inviteFriendsButton'),
                                        title: LocaleKeys
                                            .waitingRoom_button_inviteFriends
                                            .tr(),
                                        onPressed: () {
                                          _shareGame(
                                              eventName: 'share_summary');
                                        },
                                        margin: EdgeInsets.zero,
                                        style: const GameButtonStyle
                                            .whiteWithBorder(),
                                      ),
                                    ],
                                  ),
                                ),
                            ],
                          ),
                        ],
                      ),
                      if (!_isOnlyHost)
                        _playersList(context, state)
                      else
                        WaitingRoomEmptyList(
                          roomKey: widget.session.roomKey,
                          onScroll: () {
                            setState(() {
                              _showAllItems = false;
                            });
                          },
                          onNoOneToPlay: _noFriends,
                          onShare: () {
                            _shareGame(eventName: 'share_empty');
                          },
                        )
                    ],
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Widget _playersList(final BuildContext context, final JoinState state) {
    final int maxPlayers = RemoteConfigHelper.maxPlayers;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        AnimatedBoard(
          shouldExpand: false,
          spaceBetweenComponents: 0,
          topChild: Padding(
            padding: const EdgeInsets.fromLTRB(24, 16, 24, 10),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Text(
                    LocaleKeys.waitingRoom_status_playersList.tr(),
                    textAlign: TextAlign.left,
                    style: Theme.of(context).textTheme.titleLarge,
                  ),
                ),
                const SizedBox(width: 16),
                Text(
                  '${state.players.length}/${RemoteConfigHelper.maxPlayers}',
                  textAlign: TextAlign.left,
                  style: Theme.of(context).textTheme.titleLarge,
                ),
              ],
            ),
          ),
          bottomChild: PlayersWaitingRoomList(
            players: state.players,
            host: state.owner,
            maxPlayers: maxPlayers,
            bottomMargin: _isMyGame ? (GameButtonViewConfig.height + 24) : 0,
            onShare: () {
              _shareGame(eventName: 'share_list');
            },
            config: getUserUuid() == state.owner
                ? PlayersWaitingConfig.host(onPlayerRemoved: (final int index) {
                    FirebaseAnalytics.instance.logEvent(name: 'kick_user');
                    getIt<GameCubit>().removePlayer(
                        room: widget.session.roomKey,
                        userId: state.players[index].id);
                    setState(() {
                      state.players.removeAt(index);
                    });
                  })
                : PlayersWaitingConfig.joined(),
          ),
        ),
      ],
    );
  }

  Widget _bottomButtons(final bool isPlayerReady) {
    return Positioned(
      left: 0,
      right: 0,
      bottom: 0,
      child: Column(
        children: <Widget>[
          if (_isMyGame) ...<Widget>[
            GameButton(
              dropShadow: true,
              title: !_allExceptHostReady
                  ? LocaleKeys.waitingRoom_button_waitForPlayers.tr()
                  : LocaleKeys.waitingRoom_button_startGame.tr(),
              style: _allExceptHostReady
                  ? const GameButtonStyle.black()
                  : GameButtonStyle.grey(),
              isUserInteractionEnabled: _allExceptHostReady,
              onPressed: !_allExceptHostReady ? () {} : _onReady,
            ),
          ],
          SizedBox(height: Defaults.bottomSpacing),
        ],
      ),
    );
  }

  @override
  void showCloseDialog({final String? roomKey}) {
    if (_isMyGame) {
      super.showCloseDialog(roomKey: roomKey);
    } else {
      showCloseOrLeaveDialog(roomKey: roomKey);
    }
  }

  Future<void> _onReady() async {
    final bool granted = await checkPermissions();
    if (!granted) {
      return;
    }

    FirebaseAnalytics.instance.logEvent(name: 'start_game');

    getIt<GameCubit>().reportPlayerReadyInRoom(
      room: widget.session.roomKey,
    );
  }

  Future<void> _shareGame({required final String eventName}) async {
    FirebaseAnalytics.instance.logEvent(
      name: eventName,
      parameters: <String, String>{'type': _isMyGame ? 'host' : 'player'},
    );
    await getIt<ShareUtil>().shareByExtension(widget.session.roomKey);
  }

  void _noFriends() {
    FirebaseAnalytics.instance.logEvent(name: 'no_fiends_selected');
    setState(() {
      shouldShowNoInviteOverlay = true;
    });
  }

  @override
  void didChangeAppLifecycleState(final AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.paused:
        logger.i('app went to background');
        break;
      case AppLifecycleState.resumed:
        logger.i('app returned');
        // Clear corresponding notifications
        NotificationsPlugin.removeNotificationsWithTag(widget.session.roomKey);
        break;
      case AppLifecycleState.inactive:
        logger.i('app inactive');
        break;
      case AppLifecycleState.detached:
        logger.i('app detached');
        break;
      case AppLifecycleState.hidden:
        logger.i('app hidden');
        break;
    }
  }
}

import 'package:collection/collection.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubits/categories_usage_cubit.dart';
import '../cubits/categories_usage_state.dart';
import '../cubits/game_cubit.dart';
import '../cubits/game_state.dart';
import '../cubits/loading_cubit.dart';
import '../cubits/store_buy_cubit.dart';
import '../cubits/store_buy_state.dart';
import '../cubits/store_cubit.dart';
import '../cubits/store_state.dart';
import '../di/dependencies.dart';
import '../gen/colors.gen.dart';
import '../gen/translations.gen.dart';
import '../mixins/join_action_mixin.dart';
import '../models/store.dart';
import '../repositories/local_storage.dart';
import '../repositories/remote_config.dart';
import '../routing/router.gr.dart';
import '../utils/app_theme.dart';
import '../utils/element_order.dart';
import '../utils/locale_utils.dart';
import '../utils/logger.dart';
import '../utils/prompters/hardcore_prompter.dart';
import '../utils/prompters/questions_usage_prompter.dart';
import '../utils/route_aware_observer.dart';
import '../widget/animated_draggable.dart';
import '../widget/bundles_list.dart';
import '../widget/game_button.dart';
import '../widget/game_header.dart';
import '../widget/hints/info_hint.dart';
import '../widget/loading_indicator.dart';
import '../widget/modal_container.dart';
import '../widget/new_game/change_language_setting.dart';
import '../widget/new_game/change_questions_setting.dart';
import '../widget/new_game/game_setting_row.dart';

enum _NewGameMode {
  edit,
  create,
  resume;

  String get title {
    switch (this) {
      case _NewGameMode.edit:
        return LocaleKeys.setupGame_title_editGame.tr();
      case _NewGameMode.create:
        return LocaleKeys.setupGame_title_newGame.tr();
      case _NewGameMode.resume:
        return LocaleKeys.setupGame_title_nextGame.tr();
    }
  }
}

enum _NewGameOverlay {
  changeQuestions,
  changeLanguage;
}

class NewGameConfig {
  NewGameConfig.create()
      : _mode = _NewGameMode.create,
        _lastState = null;

  NewGameConfig.edit({required final JoinState lastState})
      : _mode = _NewGameMode.edit,
        _lastState = lastState;

  NewGameConfig.resume({required final JoinState nextGameState})
      : _mode = _NewGameMode.resume,
        _lastState = nextGameState;

  final _NewGameMode _mode;
  final JoinState? _lastState;
}

class NewGamePage extends StatefulWidget {
  const NewGamePage({
    final Key? key,
    required this.config,
  }) : super(key: key);

  final NewGameConfig config;

  @override
  State<NewGamePage> createState() => _NewGamePageState();
}

class _NewGamePageState extends State<NewGamePage>
    with JoinAction, RouteAwareAnalytics<NewGamePage> {
  @override
  String get screenClass => 'new_game_screen';
  @override
  String get screenName => 'NewGame';

  final AnimatedDraggableController _draggableController =
      AnimatedDraggableController();

  /// Number of selected roundes
  int _roundsCount = 12;

  /// Selected game language
  Language _language = Language.polish;

  /// Excluded game question types
  Set<QuestionSetting> _excludedQuestions = <QuestionSetting>{};

  /// Currently selected bundles
  List<String>? _activeBundles;

  /// Response from store, with all bundles
  FetchStoreState? _storeResponse;

  /// Screen is considered loaded when avaliable bunldes are fetched
  bool get _isScreenLoaded => _storeResponse != null;

  /// At least one bundle has to be selected to proceed
  bool get _atLeastOneBundleSelected =>
      _isScreenLoaded && _activeBundles!.isNotEmpty;

  /// Currently displayed overlay
  _NewGameOverlay? _overlay;

  /// In order to avoid displaying usage alert more than once we change this value to true
  bool _usageAlertWasShown = false;

  /// Model of the usage dialog, filled when usage fetched
  HintUsageModel? hintModel;

  /// Data sent with Firebase event
  Map<String, Object> get _eventParams {
    return <String, Object>{
      'rounds': '$_roundsCount',
      'language': _language.value,
      'excluded': _excludedQuestions
          .map((final QuestionSetting e) => e.value)
          .sorted((final String a, final String b) => a.compareTo(b))
          .join(',')
    };
  }

  @override
  void initState() {
    super.initState();

    // Apply default language based on locale
    _language = defaultQuestionsLanguage;

    // Apply default settings if in edit mode / resume mode
    final JoinState? lastState = widget.config._lastState;
    if (lastState != null) {
      _roundsCount = lastState.rounds.maxRounds;
      _language = lastState.language;
      _excludedQuestions = lastState.excludedQuestions;
      _activeBundles = List<String>.from(
          lastState.categories); // Make copy so we don't change it in state
    }

    _fetchStore();
  }

  @override
  void dispose() {
    logger.d('new game screen disposed');
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    final _NewGameMode mode = widget.config._mode;

    return MultiBlocListener(
      // ignore: always_specify_types
      listeners: [
        BlocListener<StoreCubit, StoreState>(
          listener: (final _, final StoreState state) async {
            if (state is FetchStoreState) {
              await _applyStoreBundles(state, mode);
            }
          },
        ),
        BlocListener<StoreBuyCubit, StoreBuyState>(
            listener: (final _, final StoreBuyState state) {
          _applyNewBoughtBundle(state);
        }),
        BlocListener<CategoriesUsageCubit, CategoriesUsageState>(
            listener: (final _, final CategoriesUsageState state) {
          _applyNewUsage(state);
        }),
      ],
      child: Scaffold(
        backgroundColor: ColorName.secondaryBackground,
        body: SafeArea(
          bottom: false,
          top: false,
          child: Stack(
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  const NewGameHeader(),
                  const SizedBox(height: 16),
                  if (hintModel != null)
                    Padding(
                      padding: const EdgeInsets.only(bottom: 24),
                      child: InfoHint(
                        animationAsset: hintModel!.animation,
                        title: hintModel!.title,
                        subtitle: hintModel!.description,
                        backgroundColor: hintModel!.backgroundColor,
                      ),
                    ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 24),
                    child: Text(
                      mode.title,
                      textAlign: TextAlign.left,
                      maxLines: 1,
                      style: Theme.of(context).textTheme.headlineMedium,
                    ),
                  ),
                  const SizedBox(height: 16),
                  _newGameCreatorHeader(context),
                  const SizedBox(height: 24),
                  ..._bundles(context),
                ],
              ),
              Positioned(
                left: 0,
                right: 0,
                bottom: Defaults.bottomSpacing,
                child: mode == _NewGameMode.create
                    ? GameButton(
                        key: const ValueKey<String>('createButton'),
                        dropShadow: true,
                        title: LocaleKeys.next.tr(),
                        isUserInteractionEnabled: _atLeastOneBundleSelected,
                        style: _atLeastOneBundleSelected
                            ? const GameButtonStyle.black()
                            : GameButtonStyle.grey(),
                        onPressed:
                            _atLeastOneBundleSelected ? _onNextPressed : () {},
                      )
                    : GameButton(
                        key: const ValueKey<String>('saveButton'),
                        dropShadow: true,
                        title: LocaleKeys.save.tr(),
                        isUserInteractionEnabled: _atLeastOneBundleSelected,
                        style: _atLeastOneBundleSelected
                            ? const GameButtonStyle.black()
                            : GameButtonStyle.grey(),
                        onPressed:
                            _atLeastOneBundleSelected ? _onEditPressed : () {},
                      ),
              ),
              if (_overlay != null)
                AnimatedDraggable(
                  allowCloseOnBackground: true,
                  onDraggedDown: () {
                    setState(() {
                      _overlay = null;
                    });
                  },
                  controller: _draggableController,
                  child: ModalContainer(
                    child: _widgetForOverlaySetting(),
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }

  List<Widget> _bundles(final BuildContext context) {
    return <Widget>[
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                LocaleKeys.setupGame_bundles.tr(),
                textAlign: TextAlign.left,
                maxLines: 1,
                style: Theme.of(context).textTheme.headlineMedium,
              ),
            ),
            if (_isScreenLoaded)
              Text(
                '${_activeBundles!.length}/${_storeResponse!.response.all().length}',
                textAlign: TextAlign.left,
                maxLines: 1,
                style: Theme.of(context).textTheme.headlineMedium,
              ),
          ],
        ),
      ),
      const SizedBox(height: 16),
      Expanded(
        child: _storeResponse == null
            ? const LoadingIndicator()
            : BundlesList(
                onBundleToggle: _onBundleTap,
                selectedBundles: _activeBundles!,
              ),
      ),
    ];
  }

  Widget _newGameCreatorHeader(final BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        GameSettingRow(
          title: LocaleKeys.setupGame_settings_quesitonTypes_title.tr(),
          value: _excludedCategoriesTitle(),
          order: ElementOrder.first,
          onTap: () {
            setState(() {
              FirebaseAnalytics.instance
                  .logEvent(name: 'game_settings_opened_change_question');
              _overlay = _NewGameOverlay.changeQuestions;
            });
          },
        ),
        GameSettingRow(
          title: LocaleKeys.setupGame_settings_language_title.tr(),
          value: _language.title,
          order: ElementOrder.last,
          image: _language.icon,
          onTap: () {
            setState(() {
              FirebaseAnalytics.instance
                  .logEvent(name: 'game_settings_opened_change_language');
              _overlay = _NewGameOverlay.changeLanguage;
            });
          },
        ),
      ],
    );
  }

  Widget _widgetForOverlaySetting() {
    switch (_overlay!) {
      case _NewGameOverlay.changeQuestions:
        return ChangeQuestionsSettings(
          initialExcludedQuestions: _excludedQuestions,
          onSave: (final Set<QuestionSetting> newValue) {
            _excludedQuestions = newValue;
            _draggableController.close();
          },
        );
      case _NewGameOverlay.changeLanguage:
        return ChangeLanguageSettings(
          initialLanguage: _language,
          onSave: (final Language newValue) {
            _language = newValue;
            _draggableController.close();
          },
        );
    }
  }

  void _applyNewUsage(final CategoriesUsageState state) {
    if (state is FetchedUsageState) {
      setState(() {
        hintModel = hostCreateUsageHintModel(state);
      });

      if (!_usageAlertWasShown && RemoteConfigHelper.isUsageAlertsEnabled) {
        FirebaseAnalytics.instance.logEvent(name: 'show_usage_offer');
        showQuestionUsageAlertIfNeeded(state);
        _usageAlertWasShown = true;
      }
    }
  }

  void _applyNewBoughtBundle(final StoreBuyState state) {
    if (state is PurchaseSuccessState) {
      if (state.iap == _storeResponse?.response.unlockAll.iap) {
        logger.i('add all bundles, except hardcore, to game');
        _activeBundles?.addAll(_storeResponse?.response.store
                .where(
                    (final StoreItem item) => !item.bundle.contains('HARDCORE'))
                .map((final StoreItem item) => item.bundle) ??
            <String>[]);
        setState(() {
          _activeBundles = _activeBundles?.toSet().toList();
        });
        return;
      }

      logger.i('add bought bundle to game');
      // Find product by iap name and add it to active boundles
      final StoreItem? product = _storeResponse?.response.store
          .firstWhereOrNull((final StoreItem e) => e.iap == state.iap);
      setState(() {
        if (product != null) {
          _activeBundles?.add(product.bundle);
        }
      });
    }
  }

  Future<void> _applyStoreBundles(
      final StoreState state, final _NewGameMode mode) async {
    if (state is FetchStoreState) {
      if (_storeResponse == null) {
        if (mode == _NewGameMode.create && _activeBundles == null) {
          // Call it only once when store fetched
          final List<String> unlockedBundles = state.unlockedProductsBundles;
          _activeBundles = List<String>.from(
              unlockedBundles); // Make copy so we don't change it in state
          await showHardcoreDialogIfNeeded(state);
          _activeBundles?.remove('HARDCORE');
        } else if (mode == _NewGameMode.resume) {
          // TODO: Disable offers for now
          // await showOfferIfNeeded(state);
        }
        setState(() {
          _storeResponse = state;
        });
      }
      // Fetch usage for available categories
      // (will be called when screen initilise & when new bundle bought)
      _fetchUsage(storeState: state);
    }
  }

  String _excludedCategoriesTitle() {
    if (_excludedQuestions.isEmpty) {
      return LocaleKeys.setupGame_settings_quesitonTypes_all.tr();
    }
    if (_excludedQuestions.length == 1) {
      return _excludedQuestions.first.title;
    }
    return LocaleKeys.setupGame_settings_quesitonTypes_moreDisabled.tr(
      namedArgs: <String, String>{'count': '${_excludedQuestions.length}'},
    );
  }

  void _onBundleTap(final String bundle) {
    setState(() {
      if (_activeBundles == null) {
        return;
      }

      if (_activeBundles!.contains(bundle)) {
        _activeBundles!.remove(bundle);
      } else {
        _activeBundles!.add(bundle);
      }
    });
  }

  void _fetchStore() {
    getIt<StoreCubit>().fetchStore();
  }

  void _fetchUsage({required final FetchStoreState storeState}) {
    if (RemoteConfigHelper.isUsageInfoBoxEnabled) {
      getIt<CategoriesUsageCubit>().fetchCategoriesUsage(storeState);
    }
  }

  Future<void> _onNextPressed() async {
    logger.i('user clicked next');
    getIt<LoadingCubit>().showLoading();
    FirebaseAnalytics.instance
        .logEvent(name: 'create_game_confirmed', parameters: _eventParams);
    _logGameSettings();

    final String? name = await LocalStorage.getUsername();

    // We can force unwrap here, name will be set on previous screen
    getIt<GameCubit>().createGameRoom(
      name: name!,
      iaps: await _mapBundlesToIaps(),
      rounds: _roundsCount,
      language: _language,
      excludedCategories: _excludedQuestions,
    );
  }

  Future<void> _onEditPressed() async {
    logger.i('user clicked save');
    FirebaseAnalytics.instance
        .logEvent(name: 'update_game_config', parameters: _eventParams);
    _logGameSettings();

    getIt<GameCubit>().updateGameRoom(
      maxRounds: _roundsCount,
      categories: await _mapBundlesToIaps(),
      language: _language,
      excludedCategories: _excludedQuestions,
    );
    getIt<AppRouter>().popForced();
  }

  Future<void> _logGameSettings() async {
    FirebaseAnalytics.instance
        .logEvent(name: 'game_settings', parameters: _eventParams);
  }

  Future<List<String>> _mapBundlesToIaps() async {
    List<String> iaps;
    if (_storeResponse != null) {
      iaps = _storeResponse!.response
          .all()
          .where((final StoreItem element) =>
              (_activeBundles ?? <String>[]).contains(element.bundle))
          .map((final StoreItem e) {
            String base = e.iap;
            if (_storeResponse!.temporaryFreeInStoreIaps.contains(e.iap) ||
                _storeResponse!.response.seasonal.contains(e)) {
              // For free or seasonal add free suffix
              base += '.free';
            }
            return base;
          })
          .whereType<String>()
          .toList();
    } else {
      iaps = await LocalStorage.getBoughtProducts();
    }
    return iaps;
  }
}

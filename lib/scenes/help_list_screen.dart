import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../di/dependencies.dart';
import '../gen/assets.gen.dart';
import '../gen/colors.gen.dart';
import '../gen/translations.gen.dart';
import '../mixins/camera_dialog_mixin.dart';
import '../routing/router.gr.dart';
import '../utils/logger.dart';
import '../utils/route_aware_observer.dart';
import '../utils/share_util.dart';
import '../widget/default_container.dart';
import '../widget/game_button.dart';
import '../widget/game_header.dart';

class HelpListPage extends StatefulWidget {
  const HelpListPage({
    final Key? key,
  }) : super(key: key);

  @override
  State<HelpListPage> createState() => _HelpListPageState();
}

class _HelpListPageState extends State<HelpListPage>
    with
        CameraPermissionDialog<HelpListPage>,
        RouteAwareAnalytics<HelpListPage> {
  @override
  String get screenClass => 'help_list_screen';
  @override
  String get screenName => 'HelpList';

  @override
  Widget build(final BuildContext context) {
    return Scaffold(
      backgroundColor: ColorName.secondaryBackground,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            const NewGameHeader(),
            const SizedBox(height: 8),
            Expanded(
              child: SingleChildScrollView(
                physics: const NeverScrollableScrollPhysics(),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Text(
                        LocaleKeys.settings_help_title.tr(),
                        textAlign: TextAlign.left,
                        maxLines: 1,
                        style: Theme.of(context).textTheme.displayMedium,
                      ),
                      const SizedBox(height: 16),
                      DefaultContainer(
                        margin: EdgeInsets.zero,
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Column(
                          children: <Widget>[
                            _QuestionRow(
                              title:
                                  LocaleKeys.settings_help_question1_title.tr(),
                              onTap: () => _openHelpDetails(
                                title: LocaleKeys.settings_help_question1_title
                                    .tr(),
                                content: LocaleKeys
                                    .settings_help_question1_content
                                    .tr(),
                                id: 'new_phone',
                              ),
                            ),
                            _QuestionRow(
                              title:
                                  LocaleKeys.settings_help_question2_title.tr(),
                              onTap: () => _openHelpDetails(
                                title: LocaleKeys.settings_help_question2_title
                                    .tr(),
                                content: LocaleKeys
                                    .settings_help_question2_content
                                    .tr(),
                                id: 'buy_error',
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            const HelpBottomButtons(),
            const SizedBox(height: 24),
          ],
        ),
      ),
    );
  }

  void _openHelpDetails(
      {required final String title,
      required final String content,
      required final String id}) {
    FirebaseAnalytics.instance.logEvent(name: 'open_help_details_$id');
    getIt<AppRouter>().push(HelpDetails(title: title, content: content));
  }
}

class HelpBottomButtons extends StatelessWidget {
  const HelpBottomButtons({super.key});

  @override
  Widget build(final BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24),
      child: Row(
        children: <Widget>[
          Expanded(
            child: GameButton(
              title: LocaleKeys.settings_help_contact.tr(),
              onPressed: _openContact,
              style: const GameButtonStyle.whiteWithBorder(),
              dropShadow: true,
              margin: EdgeInsets.zero,
            ),
          ),
          if (getIt<ShareUtil>().possibleApps.contains(ShareApp.fbMessanger))
            Container(
              margin: const EdgeInsets.only(left: 16),
              width: 50,
              child: GameButton(
                onPressed: () {
                  getIt<ShareUtil>().shareContactOnFb();
                },
                style: const GameButtonStyle.whiteWithBorder(),
                image: ShareApp.fbMessanger.image,
                margin: EdgeInsets.zero,
              ),
            )
        ],
      ),
    );
  }

  Future<void> _openContact() async {
    final String email = LocaleKeys.settings_email.tr();
    final bool opened = await launchUrl(Uri.parse('mailto:$email'));
    logger.i('openining mail result: $opened');
    if (!opened) {
      FirebaseAnalytics.instance.logEvent(name: 'contact_fallback');
      getIt<AppRouter>().push(
        InfoAlert(
          message: LocaleKeys.settings_emailFallbackTitle.tr(
            namedArgs: <String, String>{'email': email},
          ),
          subtitle: LocaleKeys.settings_emailFallbackSubtitle.tr(),
        ),
      );
    }
  }
}

class _QuestionRow extends StatelessWidget {
  const _QuestionRow({
    required this.title,
    required this.onTap,
  });

  final String title;
  final Function() onTap;

  @override
  Widget build(final BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.symmetric(vertical: 16),
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text(
                title,
                textAlign: TextAlign.left,
                style: Theme.of(context).textTheme.titleMedium,
              ),
            ),
            const SizedBox(width: 16),
            Assets.images.gameArrowIcon.image(color: Colors.black)
          ],
        ),
      ),
    );
  }
}

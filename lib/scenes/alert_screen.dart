import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../di/dependencies.dart';
import '../gen/translations.gen.dart';
import '../routing/router.gr.dart';
import '../utils/app_theme.dart';
import '../utils/logger.dart';
import '../utils/route_aware_observer.dart';
import '../widget/animated_draggable.dart';
import '../widget/game_button.dart';
import '../widget/modal_container.dart';

class InfoAlertAction {
  const InfoAlertAction({
    required this.onTap,
    required this.title,
    this.smaller = false,
    this.style = const GameButtonStyle.black(),
  });

  final bool smaller;
  final Function(Function()) onTap;
  final String title;
  final GameButtonStyle style;
}

class InfoAlertPage extends StatefulWidget {
  const InfoAlertPage({
    final Key? key,
    required this.message,
    this.subtitle,
    this.actions,
    this.canPop = true,
  }) : super(key: key);

  final String message;
  final String? subtitle;
  final List<InfoAlertAction>? actions;
  final bool canPop;

  @override
  State<InfoAlertPage> createState() => _InfoAlertPageState();
}

class _InfoAlertPageState extends State<InfoAlertPage>
    with RouteAwareAnalytics<InfoAlertPage> {
  @override
  String get screenClass => 'alert_screen';
  @override
  String get screenName => 'Alert';

  final AnimatedDraggableController _draggableController =
      AnimatedDraggableController();

  @override
  void dispose() {
    logger.d('dispose info alert');
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (widget.canPop) {
          _close();
        }
        return false;
      },
      child: AnimatedDraggable(
        allowSnap: widget.canPop,
        onDraggedDown: () {
          getIt<AppRouter>().popForced();
        },
        controller: _draggableController,
        child: ModalContainer(
          showDragIndicator: widget.canPop,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Text(
                    widget.message,
                    style: Theme.of(context).textTheme.headlineMedium,
                    textAlign: TextAlign.left,
                  ),
                  if (widget.subtitle != null) ...<Widget>[
                    const SizedBox(height: 8),
                    Text(
                      widget.subtitle!,
                      style: Theme.of(context).textTheme.titleMedium,
                      textAlign: TextAlign.left,
                    ),
                  ],
                ],
              ),
              const SizedBox(height: 24),
              if (widget.actions == null)
                GameButton(
                  title: LocaleKeys.alerts_ok.tr(),
                  margin: EdgeInsets.zero,
                  onPressed: () {
                    _close();
                  },
                )
              else
                Column(
                  children: <Widget>[
                    ...Iterable<int>.generate(widget.actions!.length +
                            (widget.actions!.length - 1))
                        .map((final int index) {
                      if (index.isEven) {
                        final InfoAlertAction e = widget.actions![index ~/ 2];
                        return GameButton(
                          title: e.title,
                          onPressed: () {
                            e.onTap(_close);
                          },
                          style: e.style,
                          margin: EdgeInsets.zero,
                          height: e.smaller ? 30 : GameButtonViewConfig.height,
                        );
                      } else {
                        return const SizedBox(height: 16);
                      }
                    }),
                  ],
                ),
              SizedBox(height: Defaults.bottomSpacing),
            ],
          ),
        ),
      ),
    );
  }

  void _close() {
    _draggableController.close();
  }
}

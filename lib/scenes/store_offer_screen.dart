import 'dart:math';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:lottie/lottie.dart';
import 'package:vector_graphics/vector_graphics.dart';

import '../../cubits/store_state.dart' as store;
import '../cubits/store_buy_cubit.dart';
import '../cubits/store_cubit.dart';
import '../di/dependencies.dart';
import '../gen/assets.gen.dart';
import '../gen/colors.gen.dart';
import '../gen/translations.gen.dart';
import '../routing/router.dart';
import '../routing/router.gr.dart';
import '../utils/logger.dart';
import '../utils/route_aware_observer.dart';
import '../widget/animated_draggable.dart';
import '../widget/bundles_grid.dart';
import '../widget/loading_indicator.dart';
import '../widget/modal_container.dart';

// ignore: avoid_classes_with_only_static_members
class _StoreOfferViewConfig {
  static const double minBottomSheetHeight = 0.60;
  static const double maxBottomSheetHeight = 0.90;
}

enum StoreOfferMode { freeHalf, freeDone, allDone, random }

class StoreOfferPage extends StatefulWidget {
  const StoreOfferPage({
    final Key? key,
    required this.mode,
  }) : super(key: key);

  final StoreOfferMode mode;

  @override
  State<StoreOfferPage> createState() => _StoreOfferPageState();
}

class _StoreOfferPageState extends State<StoreOfferPage>
    with RouteAwareAnalytics<StoreOfferPage> {
  @override
  String get screenClass => 'store_offer_screen';
  @override
  String get screenName => 'store_offer';

  // We store random text index so it correlates title and image, even if we don't use it;
  final int _randomOfferIndex = Random().nextInt(3);

  final AnimatedDraggableController _draggableController =
      AnimatedDraggableController();

  String get _title {
    switch (widget.mode) {
      case StoreOfferMode.freeHalf:
        return LocaleKeys.questionUsage_hint_host_title_freeCategory_duplicates
            .tr();
      case StoreOfferMode.freeDone:
        return LocaleKeys.questionUsage_offer_title_allDone.tr();
      case StoreOfferMode.allDone:
        return LocaleKeys.questionUsage_offer_title_allDone.tr();
      case StoreOfferMode.random:
        return <String>[
          LocaleKeys.questionUsage_offer_title_example1.tr(),
          LocaleKeys.questionUsage_offer_title_example2.tr(),
          LocaleKeys.questionUsage_offer_title_example3.tr(),
        ][_randomOfferIndex];
    }
  }

  String get _image {
    switch (widget.mode) {
      case StoreOfferMode.freeHalf:
        return Assets.svg.timerSvg;
      case StoreOfferMode.freeDone:
        return Assets.animations.sadface;
      case StoreOfferMode.allDone:
        return Assets.animations.crying;
      case StoreOfferMode.random:
        return <String>[
          Assets.animations.yawn,
          Assets.animations.stars,
          Assets.animations.glasses,
        ][_randomOfferIndex];
    }
  }

  double get _scale {
    switch (widget.mode) {
      case StoreOfferMode.freeDone:
        return 1.14;
      case StoreOfferMode.allDone:
        return 1.42;
      case StoreOfferMode.random:
        return <double>[1.42, 1.18, 1.52][_randomOfferIndex];
      // ignore: no_default_cases
      default:
        return 1;
    }
  }

  Color get _backgroundColor {
    switch (widget.mode) {
      case StoreOfferMode.freeDone:
      case StoreOfferMode.freeHalf:
        return ColorName.primaryBackground;
      // ignore: no_default_cases
      default:
        return ColorName.pink;
    }
  }

  @override
  void dispose() {
    logger.d('dispose store offer');
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        _close();
        return false;
      },
      child: AnimatedDraggable(
        onDraggedDown: () {
          getIt<AppRouter>().popForced();
        },
        controller: _draggableController,
        minHeight: _StoreOfferViewConfig.minBottomSheetHeight,
        maxHeight: _StoreOfferViewConfig.maxBottomSheetHeight,
        animationDelay: RoutesDuration.modal,
        allowCloseOnBackground: true,
        child: ModalContainer(
          backgroundColor: _backgroundColor,
          padding: EdgeInsets.zero,
          child: Stack(
            children: <Widget>[
              // Backgroud if content is too small
              Container(
                  height: (MediaQuery.of(context).size.height *
                          _StoreOfferViewConfig.maxBottomSheetHeight) -
                      ModalContainerViewConfig.dragHeight,
                  width: MediaQuery.of(context).size.width,
                  color: ColorName.secondaryBackground),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                    color: _backgroundColor,
                    padding: const EdgeInsets.only(top: 4, bottom: 28),
                    child: (widget.mode != StoreOfferMode.freeHalf)
                        ? Transform.scale(
                            scale: _scale,
                            child: Lottie.asset(_image, height: 70, width: 70),
                          )
                        : SvgPicture(
                            AssetBytesLoader(_image),
                            height: 70,
                          ),
                  ),
                  Container(
                    padding: const EdgeInsets.fromLTRB(24, 16, 24, 0),
                    color: ColorName.secondaryBackground,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        Text(
                          _title,
                          textAlign: TextAlign.left,
                          style: Theme.of(context).textTheme.titleLarge,
                        ),
                        const SizedBox(height: 8),
                        Text(
                          LocaleKeys.store_whyHint.tr(),
                          textAlign: TextAlign.left,
                          style: Theme.of(context).textTheme.bodyMedium,
                        ),
                      ],
                    ),
                  ),
                  Container(
                    color: ColorName.secondaryBackground,
                    child: _storeContent(),
                  ),
                  Container(
                    color: ColorName.secondaryBackground,
                    height: MediaQuery.of(context).viewPadding.bottom,
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _storeContent() {
    return BlocBuilder<StoreCubit, store.StoreState>(
        builder: (final BuildContext context, final store.StoreState state) {
      if (state is store.InitialState) {
        return Container(
          width: double.infinity,
          margin: const EdgeInsets.only(top: 32),
          child: const LoadingIndicator(),
        );
      } else if (state is store.FetchStoreState) {
        final int categoriesCount = state.notBoughtInStoreIaps.length;
        final BundleGridConfig viewConfig =
            BundlesGrid.calculateViewSize(context, categoriesCount);

        return SizedBox(
          height: viewConfig.size.height,
          child: const BundlesGrid(
            showNotBoughtOnly: true,
            showNotBoughtVisials: false,
            source: BuySource.usageOffer,
          ),
        );
      } else {
        return const SizedBox(
          height: 20,
        );
      }
    });
  }

  void _close() {
    _draggableController.close();
  }
}

import 'package:flutter/material.dart';
import 'package:styled_text/styled_text.dart';

import '../gen/colors.gen.dart';
import '../mixins/camera_dialog_mixin.dart';
import '../utils/app_theme.dart';
import '../utils/route_aware_observer.dart';
import '../widget/default_container.dart';
import '../widget/game_header.dart';
import 'help_list_screen.dart';

class HelpDetailsPage extends StatefulWidget {
  const HelpDetailsPage({
    final Key? key,
    required this.title,
    required this.content,
  }) : super(key: key);

  final String title;
  final String content;

  @override
  State<HelpDetailsPage> createState() => _HelpDetailsPageState();
}

class _HelpDetailsPageState extends State<HelpDetailsPage>
    with
        CameraPermissionDialog<HelpDetailsPage>,
        RouteAwareAnalytics<HelpDetailsPage> {
  @override
  String get screenClass => 'help_details_screen';
  @override
  String get screenName => 'HelpDetails';

  @override
  Widget build(final BuildContext context) {
    return Scaffold(
      backgroundColor: ColorName.secondaryBackground,
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            const NewGameHeader(),
            const SizedBox(height: 8),
            Expanded(
              child: SingleChildScrollView(
                physics: const NeverScrollableScrollPhysics(),
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 24),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Text(
                        widget.title,
                        textAlign: TextAlign.left,
                        style: Theme.of(context).textTheme.displayMedium,
                      ),
                      const SizedBox(height: 16),
                      DefaultContainer(
                        margin: EdgeInsets.zero,
                        child: StyledText(
                          text: widget.content,
                          textAlign: TextAlign.left,
                          style: Theme.of(context).textTheme.titleMedium,
                          tags: <String, StyledTextTagBase>{
                            'bold': StyledTextTag(
                                style: appTheme.textTheme.titleLarge)
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
            const HelpBottomButtons(),
            const SizedBox(height: 24),
          ],
        ),
      ),
    );
  }
}

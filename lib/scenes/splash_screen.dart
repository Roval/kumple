import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:simple_animations/animation_builder/play_animation_builder.dart';

import '../cubits/maintenence_cubit.dart';
import '../di/dependencies.dart';
import '../entities/base_request.dart';
import '../gen/colors.gen.dart';
import '../repositories/local_storage.dart';
import '../routing/router.gr.dart';
import '../utils/appsflyer_helper.dart';
import '../utils/device_util.dart';
import '../utils/locale_utils.dart';
import '../utils/route_aware_observer.dart';
import '../utils/session_utils.dart';
import '../utils/share_util.dart';

class SplashPage extends StatefulWidget {
  const SplashPage({final Key? key}) : super(key: key);

  @override
  State<SplashPage> createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage>
    with RouteAwareAnalytics<SplashPage> {
  @override
  String get screenClass => 'splash_screen';
  @override
  String get screenName => 'Splash';

  @override
  void initState() {
    super.initState();
    generateUniqueUserUuid();
    BaseRequest.loadAppVersion();
    getIt<ShareUtil>().checkPossibleApps();

    LocalStorage.getUsername().then((final String? name) {
      if (name != null) {
        // user was already created, send event he comes back to the app
        logAppsFlyerEvent('af_login');
      }
    });
  }

  @override
  Widget build(final BuildContext context) {
    calculateDeviceMetrics(context);
    setupLocale(context.locale);
    return Scaffold(
      backgroundColor: ColorName.primaryBackground,
      body: Center(
        child: PlayAnimationBuilder<double>(
          delay: const Duration(seconds: 1),
          tween: Tween<double>(begin: 1, end: 0),
          duration: const Duration(milliseconds: 170),
          curve: Curves.easeInCubic,
          child: localisedLogo.image(),
          builder: (final BuildContext context, final double value,
              final Widget? child) {
            return Transform.scale(
                scale: 1 + (1 - value),
                child: Opacity(opacity: value, child: child));
          },
          onCompleted: () {
            Future<dynamic>.delayed(const Duration(milliseconds: 50))
                .then((final _) {
              getIt<AppRouter>().replace(getIt<MaintenenceCubit>().isEnabled
                  ? const Maintenance()
                  : const Welcome());
            });
          },
        ),
      ),
    );
  }
}

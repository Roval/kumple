// ignore_for_file: depend_on_referenced_packages, implementation_imports

import 'package:cached_network_image/cached_network_image.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_storage_platform_interface/src/full_metadata.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';

import '../cubits/loading_cubit.dart';
import '../di/dependencies.dart';
import '../extensions/key_extensions.dart';
import '../extensions/list_extensions.dart';
import '../gen/assets.gen.dart';
import '../gen/colors.gen.dart';
import '../gen/translations.gen.dart';
import '../mixins/camera_dialog_mixin.dart';
import '../repositories/local_storage.dart';
import '../routing/router.gr.dart';
import '../utils/app_theme.dart';
import '../utils/appsflyer_helper.dart';
import '../utils/camera_util.dart';
import '../utils/logger.dart';
import '../utils/route_aware_observer.dart';
import '../utils/session_utils.dart';
import '../widget/animated_draggable.dart';
import '../widget/default_container.dart';
import '../widget/edit_profile/edit_profile_avatar_selector.dart';
import '../widget/edit_profile/edit_profile_emoji_setup.dart';
import '../widget/edit_profile/edit_profile_input_field.dart';
import '../widget/game_button.dart';
import '../widget/game_header.dart';
import '../widget/highlight_button.dart';
import '../widget/modal_container.dart';

enum EditAvatarMode {
  loading,
  fromWebCamera,
  fromWebEmoji,
  fromCamera,
  fromEmoji
}

class EditAvatarConfig extends Equatable {
  const EditAvatarConfig(
      {required this.mode, required this.avatarData, required this.emojiData});

  const EditAvatarConfig.loading()
      : mode = EditAvatarMode.loading,
        avatarData = null,
        emojiData = null;

  const EditAvatarConfig.fromWebCamera()
      : mode = EditAvatarMode.fromWebCamera,
        avatarData = null,
        emojiData = null;

  const EditAvatarConfig.fromCamera(final Uint8List data)
      : mode = EditAvatarMode.fromCamera,
        avatarData = data,
        emojiData = null;

  EditAvatarConfig.fromEmoji(final String color, final String image)
      : mode = EditAvatarMode.fromEmoji,
        avatarData = null,
        emojiData = EmojiConfig(color, image);

  EditAvatarConfig.fromWebEmoji(final String color, final String image)
      : mode = EditAvatarMode.fromWebEmoji,
        avatarData = null,
        emojiData = EmojiConfig(color, image);

  final EditAvatarMode mode;

  // Set if `fromCamera` mode
  final Uint8List? avatarData;

  // Set if `fromEmoji` mode
  final EmojiConfig? emojiData;

  @override
  List<Object?> get props =>
      <dynamic>[mode, avatarData, emojiData?.color, emojiData?.image];
}

class EmojiConfig {
  EmojiConfig(this.color, this.image);
  String color;
  String image;
}

class EditProfilePage extends StatefulWidget {
  const EditProfilePage({
    final Key? key,
    required this.onSave,
    this.editMode = false,
  }) : super(key: key);

  /// Called after sucessful save
  final Function(String) onSave;

  /// If true - user opened profile from settings
  final bool editMode;

  @override
  State<EditProfilePage> createState() => _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfilePage>
    with
        CameraPermissionDialog<EditProfilePage>,
        RouteAwareAnalytics<EditProfilePage> {
  @override
  String get screenClass => 'edit_profile_screen';
  @override
  String get screenName => 'EditProfile';

  final TextEditingController _textFieldController = TextEditingController();
  final GlobalKey _paintingKey = GlobalKey();

  /// Currently set name of the user, for new user it is blank value
  String _originalName = '';

  /// Name currecntly writted in text field
  String get _name => _textFieldController.text.trim();

  /// Current state of the avatar
  EditAvatarConfig _avatarConfig = const EditAvatarConfig.loading();

  /// True if user added new photo or changed emoji
  bool get _avatarWasChanged =>
      _avatarConfig.mode == EditAvatarMode.fromCamera ||
      _avatarConfig.mode == EditAvatarMode.fromEmoji;

  /// If true - modal to edit emoji is visible
  bool _isEmojiEditEnabled = false;

  final AnimatedDraggableController _draggableController =
      AnimatedDraggableController();

  @override
  void initState() {
    super.initState();
    myAvatarMetadata().then(
      (final FullMetadata? metadata) => setState(() {
        // Set current state that avatar should be displayed from web
        if (metadata != null) {
          final String? color = metadata.customMetadata?['color'];
          final String? image = metadata.customMetadata?['image'];

          if (color != null && image != null) {
            // Restore current emoji config
            _avatarConfig = EditAvatarConfig.fromWebEmoji(color, image);
          } else {
            _avatarConfig = const EditAvatarConfig.fromWebCamera();
          }
        } else {
          _avatarConfig = EditAvatarConfig.fromEmoji(
            EditProfileEmojiEditorViewConfig.colors.keys.toList().randomItem(),
            EditProfileEmojiEditorViewConfig.avatars.randomItem(),
          );
        }
      }),
    );
    LocalStorage.getUsername().then((final String? value) {
      if (value != null) {
        _textFieldController.text = value;
        // Original name should be only compared in edit mode
        _originalName = widget.editMode ? value : '';
      }
    });
    _textFieldController.addListener(() {
      _sendDebugReportIfProperName();
      setState(() {});
    });
  }

  @override
  void dispose() {
    logger.d('edit profile screen disposed');
    _textFieldController.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return Scaffold(
      backgroundColor: ColorName.secondaryBackground,
      body: SafeArea(
        top: false,
        bottom: false,
        child: Stack(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                const NewGameHeader(),
                const SizedBox(height: 8),
                Expanded(
                  child: SingleChildScrollView(
                    physics: const NeverScrollableScrollPhysics(),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: <Widget>[
                        ..._nameWidgets(),
                        const SizedBox(height: 24),
                        ..._avatarWidgets()
                      ],
                    ),
                  ),
                ),
              ],
            ),
            if (_checkIfNameAndAvatarFilled())
              KeyboardVisibilityBuilder(
                builder:
                    (final BuildContext context, final bool isKeyboardVisible) {
                  return Positioned(
                    bottom: isKeyboardVisible ? 24 : Defaults.bottomSpacing,
                    left: 0,
                    right: 0,
                    child: GameButton(
                      key: const ValueKey<String>('saveButton'),
                      title: LocaleKeys.save.tr(),
                      dropShadow: true,
                      onPressed: _acceptData,
                    ),
                  );
                },
              ),
            if (_isEmojiEditEnabled)
              AnimatedDraggable(
                allowCloseOnBackground: true,
                onDraggedDown: () {
                  setState(() {
                    _isEmojiEditEnabled = false;
                  });
                },
                controller: _draggableController,
                child: ModalContainer(
                  child: EditProfileEmojiEditor(
                    config: _avatarConfig,
                    onSave: (final EditAvatarConfig newConfig) {
                      _avatarConfig = newConfig;
                      // Close will call setState
                      _draggableController.close();
                    },
                  ),
                ),
              ),

            // Temporary view, which is hidden for a viewer, where the avatar to capture is displayed
            // We capture this view instead of the one within the circle
            if (_avatarConfig.mode == EditAvatarMode.fromEmoji)
              Positioned(
                bottom: -230,
                left: 0,
                child: RepaintBoundary(
                  key: _paintingKey,
                  child: Container(
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: EditProfileEmojiEditorViewConfig
                                .colors[_avatarConfig.emojiData!.color] ??
                            EditProfileEmojiEditorViewConfig.colors[
                                EditProfileEmojiEditorViewConfig.defaultColor]!,
                      ),
                    ),
                    child: EmojiAvatar(
                      config: _avatarConfig.emojiData!,
                    ),
                  ),
                ),
              )
          ],
        ),
      ),
    );
  }

  List<Widget> _avatarWidgets() {
    return <Widget>[
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Text(
          LocaleKeys.editProfile_avatar.tr(),
          textAlign: TextAlign.left,
          maxLines: 1,
          style: Theme.of(context).textTheme.headlineMedium,
        ),
      ),
      const SizedBox(height: 16),
      GestureDetector(
        onTap: () {
          setState(() {
            FocusScope.of(context).unfocus();
            _isEmojiEditEnabled = true;
          });
        },
        child: DefaultContainer(
          child: Center(
            child: SizedBox(
              width: AvatarSelectorViewConfig.dimension,
              child: Stack(
                children: <Widget>[
                  EditProfileAvatarSelector(
                    config: _avatarConfig,
                  ),
                  Positioned(
                    top: 3,
                    right: 3,
                    child: Assets.images.editAvatarIcon.image(),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
      const SizedBox(height: 16),
      HighlightButton(
        onTap: _openCamera,
        builder: (final BuildContext context, final bool highlight) {
          return Opacity(
            opacity: highlight ? 0.5 : 1,
            child: DefaultContainer(
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: Text(
                    LocaleKeys.editProfile_takePicture.tr(),
                    textAlign: TextAlign.left,
                    maxLines: 1,
                    style: Theme.of(context).textTheme.headlineMedium,
                  )),
                  Assets.images.gameArrowIcon.image(color: Colors.black),
                ],
              ),
            ),
          );
        },
      ),
    ];
  }

  List<Widget> _nameWidgets() {
    return <Widget>[
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24),
        child: Text(
          LocaleKeys.editProfile_title.tr(),
          textAlign: TextAlign.left,
          maxLines: 1,
          style: Theme.of(context).textTheme.displayMedium,
        ),
      ),
      const SizedBox(height: 24),
      EditProfileInputField(
        textFieldController: _textFieldController,
      ),
    ];
  }

  bool _checkIfNameAndAvatarFilled() {
    if (widget.editMode) {
      // Save only if one of the field changed
      return _avatarWasChanged || _verifyNameExists();
    }
    return _verifyNameExists() &&
        // Avatar already existed ...
        (_avatarConfig.mode == EditAvatarMode.fromWebCamera ||
            _avatarConfig.mode == EditAvatarMode.fromWebEmoji ||
            _avatarWasChanged || // or was just generated
            kTestAccounts.contains(
                _name)); // or is one of the test accounts where image already exists
  }

  bool _verifyNameExists() {
    return _name.isNotEmpty && _name != _originalName;
  }

  Future<void> _acceptData() async {
    FocusScope.of(context).unfocus();

    if (widget.editMode) {
      FirebaseAnalytics.instance.logEvent(
        name: 'update_player',
      );
    } else {
      FirebaseAnalytics.instance
          .logEvent(name: 'create_player', parameters: <String, String>{
        'content': (_avatarConfig.mode == EditAvatarMode.fromWebCamera ||
                _avatarConfig.mode == EditAvatarMode.fromCamera)
            ? 'with_camera'
            : 'with_emoji'
      });
      logAppsFlyerEvent('af_complete_registration');
    }
    getIt<LoadingCubit>().showLoading();
    LocalStorage.saveUsername(_name);
    // Update name in Firebase
    FirebaseAuth.instance.currentUser?.updateDisplayName(_name);

    if (_avatarWasChanged) {
      final bool hasInternet = await InternetConnectionChecker().hasConnection;
      if (!hasInternet) {
        getIt<AppRouter>().push(InfoAlert(
          message: LocaleKeys.alerts_error_ups.tr(),
          subtitle: LocaleKeys.alerts_error_noInternet.tr(),
        ));
        return;
      }

      if (!isUserUuidGenerated) {
        logger.i('user not created, wait for it to be created');
        try {
          await FirebaseAuth.instance.signInAnonymously();
        } catch (e, stacktrace) {
          logger.e('unable to sign in user');
          FirebaseCrashlytics.instance.recordError(e, stacktrace,
              reason: 'Unable to create user account!');
        }
      }

      // Clear cache
      final String playerUuid = getUserUuid();
      await CachedNetworkImage.evictFromCache(avatarUrl(player: playerUuid));
      if (_avatarConfig.avatarData != null) {
        // From camera
        await saveAvatar(_avatarConfig.avatarData!);
      } else {
        // From emoji
        await saveAvatar((await _paintingKey.capturePng())!,
            additionalMetadata: <String, String>{
              'color': _avatarConfig.emojiData!.color,
              'image': _avatarConfig.emojiData!.image,
            });
      }
    } else if (kTestAccounts.contains(_name)) {
      setAsTestAccount(name: _name);
    }
    getIt<LoadingCubit>().hideLoading();

    widget.onSave(_name);
  }

  Future<void> _openCamera() async {
    FirebaseAnalytics.instance.logEvent(name: 'edit_profile_open_camera');
    FocusManager.instance.primaryFocus?.unfocus();
    final bool granted = await checkPermissions();
    if (!granted) {
      return;
    }

    getIt<AppRouter>().push(Camera(onImageTaken: (final Uint8List bytes) {
      setState(() {
        _avatarConfig = EditAvatarConfig.fromCamera(bytes);
      });
    }));
  }

  void _sendDebugReportIfProperName() {
    if (_name == 'SENDREPORT') {
      _textFieldController.text = _originalName;
      try {
        throw Exception('user send manual report');
      } catch (e, stacktrace) {
        FirebaseCrashlytics.instance
            .recordError(e, stacktrace, reason: 'manual report!');
      }
    }
  }
}

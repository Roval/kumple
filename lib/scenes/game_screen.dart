import 'package:auto_route/auto_route.dart';
import 'package:collection/collection.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../cubits/game_cubit.dart';
import '../cubits/game_state.dart';
import '../cubits/loading_cubit.dart';
import '../di/dependencies.dart';
import '../environment_config.dart';
import '../extensions/string_extensions.dart';
import '../gen/colors.gen.dart';
import '../gen/translations.gen.dart';
import '../mixins/close_dialog_mixin.dart';
import '../models/event.dart';
import '../models/session.dart';
import '../models/store.dart';
import '../plugins/notifications_plugin.dart';
import '../repositories/remote_config.dart';
import '../routing/router.gr.dart';
import '../utils/appsflyer_helper.dart';
import '../utils/database_helper.dart';
import '../utils/logger.dart';
import '../utils/route_aware_observer.dart';
import '../utils/session_utils.dart';
import '../widget/animated_draggable.dart';
import '../widget/boards/assign_answers.dart';
import '../widget/boards/awaiting_next_question.dart';
import '../widget/boards/awaiting_others.dart';
import '../widget/boards/best_of_camera_question.dart';
import '../widget/boards/best_of_image_one_user_question.dart';
import '../widget/boards/drag_or_or_question.dart';
import '../widget/boards/draw_question.dart';
import '../widget/boards/finish_winner.dart';
import '../widget/boards/image_present_question.dart';
import '../widget/boards/keyboard_question.dart';
import '../widget/boards/one_user_question.dart';
import '../widget/boards/or_or_question.dart';
import '../widget/boards/pick_answer.dart';
import '../widget/boards/question_result.dart';
import '../widget/boards/refresh_board.dart';
import '../widget/chat/chat_module.dart';
import '../widget/game_header.dart';
import '../widget/game_helper.dart';
import '../widget/modal_container.dart';

class GamePage extends StatefulWidget {
  const GamePage({
    final Key? key,
    required this.session,
    required this.initialRounds,
  }) : super(key: key);

  final Session session;
  final Rounds initialRounds;

  @override
  State<GamePage> createState() => _GamePageState();
}

enum _GamePageDialogs {
  helper,
  report;
}

class _GamePageState extends State<GamePage>
    with WidgetsBindingObserver, CloseDialog, RouteAwareAnalytics<GamePage> {
  @override
  String get screenClass => 'game_screen';
  @override
  String get screenName => 'Game';

  final String _logTag = '[gamescreen]';

  /// Caches some of the boards in order to preserve their state
  final Map<String, Widget> _boardCache = <String, Widget>{};

  /// Set to true when state for finished game received.
  /// Disables bloc builder.
  bool _gameFinished = false;

  /// Used in gameplay header to display current round
  late Rounds _rounds;

  /// Used in gameplay header to display current category icon
  String? _currentCategoryImageUrl;

  /// Required to correctly map icon from category name
  StoreDefinition? _storeResponse;

  /// Represents helper dialog which should be displayed to user;
  _GamePageDialogs? _displayedDialog;

  /// Last game state, used for chat module to have players list
  PlayersState? _lastReceivedState;

  final GameplayHeaderController _colorController = GameplayHeaderController();

  final AnimatedDraggableController _draggableController =
      AnimatedDraggableController();

  final ChatModuleController _chatController = ChatModuleController();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);

    // Load store content
    _storeResponse = RemoteConfigHelper.lastFetchedStoreResponse;
    RemoteConfigHelper.storeContent()
        .then((final StoreDefinition value) => _storeResponse = value);

    // Clear notifications
    NotificationsPlugin.removeNotificationsWithTag(widget.session.roomKey);

    _rounds = widget.initialRounds;
  }

  @override
  void dispose() {
    logger.d('$_logTag game screen disposed');
    WidgetsBinding.instance.removeObserver(this);
    _colorController.dispose();
    _chatController.dispose();
    super.dispose();
  }

  @override
  Widget build(final BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (_chatController.isOpened) {
          _chatController.close();
        } else {
          showCloseDialog(roomKey: widget.session.roomKey);
        }
        return false;
      },
      child: Scaffold(
        backgroundColor: ColorName.secondaryBackground,
        resizeToAvoidBottomInset: false,
        body: SafeArea(
          top: false,
          bottom: false,
          child: BlocListener<GameCubit, GameState>(
            listenWhen: (final GameState previous, final GameState current) =>
                current is ErrorState,
            listener: (final BuildContext context, final GameState state) {
              _detectIfRoomDidntFinish(state);
            },
            child: BlocBuilder<GameCubit, GameState>(
              buildWhen: (final GameState previous, final GameState current) =>
                  !_gameFinished && current is! RejoinState,
              builder: (final BuildContext context, final GameState state) {
                logger.i('$_logTag new state for board $state');

                if (state is PlayersState) {
                  if (_rounds.current != state.rounds.current) {
                    logger.i('$_logTag detected new round');
                    _rounds = state.rounds;
                    _displayedDialog = null;

                    FirebaseAnalytics.instance.logLevelStart(
                      levelName: 'round ${_rounds.current}',
                    );
                    // Lets not send it to apps flyer do decrease number of events
                    // logAppsFlyerEvent(
                    //   'af_level_achieved',
                    //   params: <String, dynamic>{'af_level': _rounds.current},
                    // );
                  }
                  _lastReceivedState = state;
                }
                _updateLastCategoryIfNeeded(state);
                // Change "in place", this will not call callbacks
                _colorController.color = _mapStateToColor(state);

                if (state is FinishState) {
                  _gameFinished = true;
                  _displayedDialog = null;

                  FirebaseAnalytics.instance.logEvent(name: 'game_finished');
                  logAppsFlyerEvent('game_finished');
                }

                return ChatModule(
                  roomKey: widget.session.roomKey,
                  controller: _chatController,
                  players: _lastReceivedState?.players ?? <Player>[],
                  onClose: () {
                    logCurrentScreen();
                  },
                  child: Stack(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          if (!_gameFinished)
                            GameplayHeader(
                              colorController: _colorController,
                              chatController: _chatController,
                              roomKey: widget.session.roomKey,
                              rounds: _rounds,
                              categoryImageUrl: _currentCategoryImageUrl,
                              onHelperTap: (state is QuestionState)
                                  ? _onHelperTap
                                  : null,
                            ),
                          _adoptGameScreen(state),
                        ],
                      ),
                      if (_displayedDialog == _GamePageDialogs.helper)
                        _helperWidget(state),
                      if (_displayedDialog == _GamePageDialogs.report)
                        _reportWidget(state),
                    ],
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget _helperWidget(final GameState state) {
    if (state is QuestionState) {
      return AnimatedDraggable(
        allowCloseOnBackground: true,
        controller: _draggableController,
        onDraggedDown: () {
          setState(() {
            _displayedDialog = null;
          });
        },
        child: ModalContainer(
            child: GameHelper(
          state: state,
          storeItem: _storeItemForCategory(state.category),
          onReportQuestion: () async {
            logger.i('$_logTag showing report dialog');
            _draggableController.close();
            await Future<dynamic>.delayed(const Duration(seconds: 1));
            setState(() {
              _displayedDialog = _GamePageDialogs.report;
            });
          },
        )),
      );
    }
    return Container();
  }

  Widget _reportWidget(final GameState state) {
    if (state is QuestionState) {
      return AnimatedDraggable(
        allowCloseOnBackground: true,
        controller: _draggableController,
        onDraggedDown: () {
          setState(() {
            _displayedDialog = null;
          });
        },
        child: ModalContainer(
            child: GameHelperQuestionReport(
          onReportQuestion: (final String report) =>
              _reportQuestion(state, report),
        )),
      );
    }
    return Container();
  }

  Future<void> _reportQuestion(
      final QuestionState state, final String report) async {
    logger.i('$_logTag reporing incorrect question');
    FirebaseAnalytics.instance.logEvent(name: 'report_sent');

    _draggableController.close();
    getIt<LoadingCubit>().showLoading();
    await DatabaseHelper.addNew(
        path:
            'reports/question/${EnvironmentConfig.isTestPackage ? 'test' : 'prod'}',
        data: <String, String>{
          'id': state.data.id,
          'text': state.data.question,
          'player': getUserUuid(),
          'report': report,
          'room': widget.session.roomKey,
        });
    await Future<dynamic>.delayed(const Duration(seconds: 1));
    getIt<LoadingCubit>().hideLoading();
    await Future<dynamic>.delayed(const Duration(milliseconds: 500));
    getIt<AppRouter>().push(
      InfoAlert(
        message: LocaleKeys.gameboards_helper_reportQuestion_confirmation.tr(),
        subtitle: LocaleKeys
            .gameboards_helper_reportQuestion_confirmationSubtitle
            .tr(),
      ),
    );
  }

  /// This is possible when eg. user lost connection just before
  /// showing result screen. Game is already gone and refresh button
  /// will just return that room not found
  void _detectIfRoomDidntFinish(final GameState state) {
    if (state is ErrorState) {
      if (state.error == 'room_not_found') {
        getIt<AppRouter>().replaceAll(<PageRouteInfo>[const Welcome()]);
      }
    }
  }

  Widget _adoptGameScreen(final GameState state) {
    void answerWrapper(final Object answer) {
      logger.i('$_logTag user wants to send answer to question');
      FocusScope.of(context).unfocus();
      getIt<GameCubit>().sendAnswer(
        room: widget.session.roomKey,
        questionId: (state as QuestionState).data.id,
        answer: answer,
      );
    }

    Widget? toReturn;
    final String myPlayerId = widget.session.myPlayerId;
    if (state is QuestionState) {
      final String question = state.data.question;

      if (state.data.answers.containsKey(myPlayerId)) {
        return AwaitingOthers(
          boardColor: _colorController.color,
          players: state.players,
          question: question,
          answered: _mapToAnswerUuids(state.data),
        );
      }
      if (_boardCache.containsKey(state.data.id)) {
        // Return cached board
        toReturn = _boardCache[state.data.id];
      } else {
        Widget board;
        switch (state.data.type) {
          case QuestionType.oneUser:
            board = OneUserQuestion(
              color: _colorController.color,
              hint: LocaleKeys.gameboards_predictAnswer.tr(),
              question: question,
              players: state.players,
              onStringAnswer: answerWrapper,
            );
            break;
          case QuestionType.text:
            board = KeyboardQuestion(
              color: _colorController.color,
              question: question,
              hint: LocaleKeys.gameboards_sayTruthHint.tr(),
              onSend: answerWrapper,
            );
            break;
          case QuestionType.assignOne:
            final Map<String, Object> map =
                (state.data.addons! as Map<dynamic, dynamic>).cast();
            final Map<String, Object> answers =
                (map['answers']! as Map<dynamic, dynamic>).cast();
            answers.remove(myPlayerId); // Remove my answer, so it doesn't show

            final Map<String, String> assignment =
                (map['assign_to']! as Map<dynamic, dynamic>).cast();
            final String myAssignmentId = assignment[getUserUuid()]!;

            // This question can have two modes
            final String mode = map['form']! as String;

            if (mode == 'answer') {
              final Player? selectedPlayer = state.players
                  .firstWhereOrNull((final Player u) => u.id == myAssignmentId);
              board = PickAnswer(
                color: _colorController.color,
                question: question,
                answers: answers,
                hint: LocaleKeys.gameboards_assign_onePlayerHint.tr(),
                assignedPlayer: selectedPlayer,
                onMapAnswer: answerWrapper,
              );
            } else {
              final MapEntry<String, Object>? selectedAnswer = answers.entries
                  .firstWhereOrNull((final MapEntry<String, Object> element) =>
                      element.key == myAssignmentId);
              board = OneUserQuestion(
                color: _colorController.color,
                hint: LocaleKeys.gameboards_assign_oneAnswerHint.tr(),
                question: question,
                players: state.players,
                onMapAnswer: answerWrapper,
                assignedAnswer: selectedAnswer,
              );
            }
            break;
          case QuestionType.assign:
            final Map<String, Object> answers =
                (state.data.addons! as Map<dynamic, dynamic>).cast();
            answers.remove(myPlayerId); // Remove my answer, so it doesn't show
            board = AssignAnswers(
              color: _colorController.color,
              question: question,
              correctAnswers: answers,
              players: state.players
                  .where((final Player element) => element.id != myPlayerId)
                  .toList(),
              onAllAssigned: answerWrapper,
            );
            break;
          case QuestionType.bestOf:
            board = KeyboardQuestion(
              color: _colorController.color,
              hint: LocaleKeys.gameboards_bestOf_answerHint.tr(),
              question: question,
              onSend: answerWrapper,
            );
            break;
          case QuestionType.bestOfOneUser:
            final Map<String, Object> map =
                (state.data.addons! as Map<dynamic, dynamic>).cast();
            final Map<String, Object> answers =
                (map['answers']! as Map<dynamic, dynamic>).cast();
            board = PickAnswer(
              color: _colorController.color,
              hint: LocaleKeys.gameboards_predictAnswer.tr(),
              question: question,
              answers: answers,
              onStringAnswer: answerWrapper,
            );
            break;
          case QuestionType.oneOf:
            final List<String> options = (state.data.data!).split('|');
            // PickAnswer accepts answers in format <user>:<answer>
            // so create map of <answer>:<answer> in order to pass it
            final Map<String, String> answers = <String, String>{
              for (final String answer in options) answer: answer
            };
            final String? selectedPlayerId = state.data.addons as String?;
            final Player? selectedPlayer = state.players
                .firstWhereOrNull((final Player u) => u.id == selectedPlayerId);
            final String hint = selectedPlayer?.id == myPlayerId
                ? LocaleKeys.gameboards_sayTruthHint.tr()
                : LocaleKeys.gameboards_assign_onePlayerHint.tr();

            board = PickAnswer(
              color: _colorController.color,
              hint: hint,
              question: question,
              answers: answers,
              onStringAnswer: answerWrapper,
              assignedPlayer: selectedPlayer,
            );
            break;
          case QuestionType.orOr:
            board = OrOrQuestion(
              color: _colorController.color,
              question: question,
              tags: state.data.rawQuestion.tags,
              onAswer: answerWrapper,
            );
            break;
          case QuestionType.dragOrOr:
            board = DragOrOrQuestion(
                question: question,
                color: _colorController.color,
                tags: state.data.rawQuestion.tags,
                players: state.players
                    .where((final Player element) => element.id != myPlayerId)
                    .toList(),
                onAllAssigned: answerWrapper);
            break;
          case QuestionType.bestOfImage:
            final String? patternUrl = state.data.data?.toLowerCase();
            if (patternUrl != null) {
              board = _imageBoard(state, patternUrl, answerWrapper);
            } else {
              board = _cameraBoard(state, patternUrl, answerWrapper);
            }
            break;
          case QuestionType.bestOfImageOneUser:
          case QuestionType.drawOneUser:
            board = BestOfImageOneUserQuestion(
              color: _colorController.color,
              players: state.players,
              question: state.data.question,
              patternUrl: state.data.data?.toLowerCase(),
              answers: (state.data.addons! as Map<dynamic, dynamic>).cast(),
              onSend: answerWrapper,
            );
            break;
          case QuestionType.draw:
            board = DrawQuestion(
              color: _colorController.color,
              question: state.data.question,
              onSend: answerWrapper,
              roomKey: widget.session.roomKey,
              questionKey: state.data.id,
              backgroundUrl: state.data.data?.toLowerCase(),
            );
            break;
        }

        _boardCache[state.data.id] = board;
        toReturn = board;
      }
    } else if (state is QuestionResultState) {
      if (state.readyPlayers.contains(myPlayerId)) {
        return AwaitingNextQuestion(
          players: state.players,
          readyPlayers: state.readyPlayers,
          colorController: _colorController,
        );
      } else {
        toReturn = QuestionResult(
          roomKey: widget.session.roomKey,
          color: _colorController.color,
          question: state.data.question,
          result: state.data,
          players: state.players,
          isFirstRound: state.rounds.current == 1,
          onContinue: () {
            getIt<GameCubit>().reportPlayerWantsToContinue(
              room: widget.session.roomKey,
            );
          },
        );
      }
    } else if (state is FinishState) {
      toReturn = FinishWinner(
        host: state.host,
        players: state.players,
        nextRoomKey: state.nextRoomKey,
      );
    }
    return Expanded(
      child: toReturn ??
          RefreshBoard(
            onRefresh: () async {
              logger.i('$_logTag refresh tapped');
              getIt<GameCubit>().rejoin(widget.session.roomKey);
            },
          ),
    );
  }

  void _onHelperTap() {
    logger.i('$_logTag user wants to see question helper');
    FirebaseAnalytics.instance.logEvent(name: 'show_helper');
    FocusManager.instance.primaryFocus?.unfocus();

    setState(() {
      _displayedDialog = _GamePageDialogs.helper;
    });
  }

  void _updateLastCategoryIfNeeded(final GameState state) {
    String currentQuestionCategory;
    if (state is QuestionState) {
      currentQuestionCategory = state.category;
    } else if (state is QuestionResultState) {
      _currentCategoryImageUrl = null;
      return;
    } else {
      return;
    }

    // Find correct category in store response
    final StoreItem? matching = _storeItemForCategory(currentQuestionCategory);
    if (matching == null || matching.bundle == _storeResponse?.free.bundle) {
      _currentCategoryImageUrl = null;
    } else if (matching.imageUrl != _currentCategoryImageUrl) {
      _currentCategoryImageUrl = matching.imageUrl;
    }
  }

  StoreItem? _storeItemForCategory(final String category) {
    final List<StoreItem> categories = _storeResponse?.all() ?? <StoreItem>[];
    return categories
        .firstWhereOrNull((final StoreItem item) => item.bundle == category);
  }

  Widget _cameraBoard(
    final QuestionState state,
    final String? patternUrl,
    final Function(String) onSend,
  ) {
    return BestOfCameraQuestion(
      color: _colorController.color,
      roomKey: widget.session.roomKey,
      questionKey: state.data.id,
      question: state.data.question,
      onSend: onSend,
      patternUrl: patternUrl,
      onImageTap: () {
        setState(() {
          _boardCache[state.data.id] = _imageBoard(state, patternUrl!, onSend);
        });
      },
    );
  }

  Widget _imageBoard(
    final QuestionState state,
    final String patternUrl,
    final Function(String) onSend,
  ) {
    return ImagePresentBoard(
      color: _colorController.color,
      question: state.data.question,
      imageUrl: patternUrl,
      onNext: () {
        setState(() {
          _boardCache[state.data.id] = _cameraBoard(state, patternUrl, onSend);
        });
      },
    );
  }

  List<String> _mapToAnswerUuids(final QuestionData question) {
    final List<String> uuids = question.answers.keys.toList();
    return uuids;
  }

  Color _mapStateToColor(final GameState state) {
    QuestionType? type;
    if (state is QuestionResultState) {
      if (!state.data.readyPlayers.contains(getUserUuid())) {
        type = state.data.type;
      }
    } else if (state is QuestionState) {
      type = state.data.type;
    } else if (state is FinishState) {
      return Colors.white;
    }

    if (type == null) {
      return _colorController.color; // Return the same, do not change color
    }

    switch (type) {
      case QuestionType.oneOf:
        return ColorName.questionColor1;

      case QuestionType.bestOf:
      case QuestionType.bestOfOneUser:
        return ColorName.questionColor2;

      case QuestionType.bestOfImage:
      case QuestionType.bestOfImageOneUser:
        return ColorName.questionColor3;

      case QuestionType.oneUser:
        return ColorName.questionColor4;

      case QuestionType.text:
      case QuestionType.assign:
      case QuestionType.assignOne:
        return ColorName.questionColor5;

      case QuestionType.orOr:
      case QuestionType.dragOrOr:
        return ColorName.questionColor6;

      case QuestionType.draw:
      case QuestionType.drawOneUser:
        return ColorName.questionColor7;
    }
  }

  @override
  void didChangeAppLifecycleState(final AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.paused:
        logger.i('$_logTag app went to background');
        _gameGoingToBackground();
        break;
      case AppLifecycleState.resumed:
        logger.i('$_logTag app returned');
        _gameResumedFromBackground();
        break;
      case AppLifecycleState.inactive:
        logger.i('$_logTag app inactive');
        break;
      case AppLifecycleState.detached:
        logger.i('$_logTag app detached');
        break;
      case AppLifecycleState.hidden:
        logger.i('$_logTag app hidden');
        break;
    }
  }

  void _gameGoingToBackground() {
    getIt<GameCubit>().disconnect('background');
  }

  /// Returning from background, call rejoin to get new game state
  /// (but do not call it when game is finished as that will tirgger
  /// an error dialog that room doesn't exist)
  void _gameResumedFromBackground() {
    if (!_gameFinished) {
      getIt<GameCubit>().rejoin(widget.session.roomKey);
    }

    // Clear notifications related to this room
    NotificationsPlugin.removeNotificationsWithTag(widget.session.roomKey);
  }
}

class GameplayHeaderController extends ChangeNotifier {
  Color color = ColorName.questionColor1;

  void publishColor(final Color color) {
    this.color = color;
    notifyListeners();
  }
}

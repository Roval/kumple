import 'package:camera/camera.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'app.dart';
import 'cubits/categories_usage_cubit.dart';
import 'cubits/chat_cubit.dart';
import 'cubits/game_cubit.dart';
import 'cubits/games_list_cubit.dart';
import 'cubits/loading_cubit.dart';
import 'cubits/maintenence_cubit.dart';
import 'cubits/notifications_cubit.dart';
import 'cubits/store_buy_cubit.dart';
import 'cubits/store_cubit.dart';
import 'di/dependencies.dart';
import 'repositories/remote_config.dart';
import 'routing/router.gr.dart';
import 'utils/appsflyer_helper.dart';
import 'utils/logger.dart';
import 'widget/camera.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp();

  // Configure dependencies
  configureDependencies();
  getIt.registerSingleton<AppRouter>(AppRouter());

  // Hide navigation
  SystemChrome.setEnabledSystemUIMode(
    SystemUiMode.manual,
    overlays: <SystemUiOverlay>[],
  );

  if (kDebugMode) {
    // Force disable Crashlytics collection while doing every day development.
    await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(false);
    await RemoteConfigHelper.configureRemoteConfigForDevelopment();
  }

  // Disables EasyLocalization logs
  // ignore: always_specify_types
  EasyLocalization.logger.enableBuildModes = [];
  await EasyLocalization.ensureInitialized();

  // Init AppsFlyer
  configureAppsFlyer();

  // Fetch the available cameras before initializing the app.
  try {
    cameras = await availableCameras();
  } on CameraException {
    logger.e('unable to fetch available cameras');
  }

  // Set defaults
  RemoteConfigHelper.setDefaults();
  debugPaintSizeEnabled = false;

  _verifyLocale();

  runApp(
    MultiBlocProvider(
      // ignore: always_specify_types
      providers: [
        BlocProvider<GameCubit>(
          create: (final BuildContext context) => getIt<GameCubit>(),
        ),
        BlocProvider<CategoriesUsageCubit>(
          create: (final BuildContext context) => getIt<CategoriesUsageCubit>(),
        ),
        BlocProvider<GamesListCubit>(
          create: (final BuildContext context) => getIt<GamesListCubit>(),
        ),
        BlocProvider<StoreCubit>(
          lazy: false,
          create: (final BuildContext context) => getIt<StoreCubit>(),
        ),
        BlocProvider<StoreBuyCubit>(
          lazy: false,
          create: (final BuildContext context) => getIt<StoreBuyCubit>(),
        ),
        BlocProvider<NotificationsCubit>(
          lazy: false,
          create: (final BuildContext context) => getIt<NotificationsCubit>(),
        ),
        BlocProvider<MaintenenceCubit>(
          lazy: false,
          create: (final BuildContext context) => getIt<MaintenenceCubit>(),
        ),
        BlocProvider<ChatCubit>(
          lazy: false,
          create: (final BuildContext context) => getIt<ChatCubit>(),
        ),
        BlocProvider<LoadingCubit>(
          lazy: false,
          create: (final BuildContext context) => getIt<LoadingCubit>(),
        ),
      ],
      child: EasyLocalization(
        supportedLocales: const <Locale>[
          Locale('en'),
          Locale('pl'),
          Locale('uk'),
          Locale('de'),
        ],
        path: 'assets/translations',
        fallbackLocale: const Locale('en'),
        child: const App(),
      ),
    ),
  );
}

void _verifyLocale() {
  // If we can create DateFormat it means that localse is supported
  // In same rare cases it will throw an error
  try {
    final DateFormat _ = DateFormat.Hm();
  } catch (e) {
    Intl.defaultLocale = 'en';
  }
}

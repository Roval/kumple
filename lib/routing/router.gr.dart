// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'dart:typed_data' as _i21;

import 'package:auto_route/auto_route.dart' as _i16;
import 'package:flutter/material.dart' as _i17;

import '../models/event.dart' as _i19;
import '../models/session.dart' as _i18;
import '../models/store.dart' as _i20;
import '../scenes/alert_screen.dart' as _i8;
import '../scenes/camera_screen.dart' as _i11;
import '../scenes/edit_profile_screen.dart' as _i7;
import '../scenes/game_screen.dart' as _i4;
import '../scenes/help_details_screen.dart' as _i14;
import '../scenes/help_list_screen.dart' as _i13;
import '../scenes/join_game_screen.dart' as _i6;
import '../scenes/license_screen.dart' as _i12;
import '../scenes/maintenance_screen.dart' as _i15;
import '../scenes/new_game_screen.dart' as _i5;
import '../scenes/offer_screen.dart' as _i9;
import '../scenes/splash_screen.dart' as _i1;
import '../scenes/store_offer_screen.dart' as _i10;
import '../scenes/waiting_room_sceen.dart' as _i3;
import '../scenes/welcome_screen.dart' as _i2;

class AppRouter extends _i16.RootStackRouter {
  AppRouter([_i17.GlobalKey<_i17.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i16.PageFactory> pagesMap = {
    Splash.name: (routeData) {
      return _i16.CustomPage<dynamic>(
        routeData: routeData,
        child: const _i1.SplashPage(),
        transitionsBuilder: _i16.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 200,
        opaque: true,
        barrierDismissible: false,
      );
    },
    Welcome.name: (routeData) {
      return _i16.CustomPage<dynamic>(
        routeData: routeData,
        child: const _i2.WelcomePage(),
        transitionsBuilder: _i16.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 200,
        opaque: true,
        barrierDismissible: false,
      );
    },
    WaitingRoom.name: (routeData) {
      final args = routeData.argsAs<WaitingRoomArgs>();
      return _i16.CustomPage<dynamic>(
        routeData: routeData,
        child: _i3.WaitingRoomPage(
          key: args.key,
          session: args.session,
        ),
        transitionsBuilder: _i16.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 200,
        opaque: true,
        barrierDismissible: false,
      );
    },
    GameRoom.name: (routeData) {
      final args = routeData.argsAs<GameRoomArgs>();
      return _i16.CustomPage<dynamic>(
        routeData: routeData,
        child: _i4.GamePage(
          key: args.key,
          session: args.session,
          initialRounds: args.initialRounds,
        ),
        transitionsBuilder: _i16.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 200,
        opaque: true,
        barrierDismissible: false,
      );
    },
    NewGame.name: (routeData) {
      final args = routeData.argsAs<NewGameArgs>();
      return _i16.CustomPage<dynamic>(
        routeData: routeData,
        child: _i5.NewGamePage(
          key: args.key,
          config: args.config,
        ),
        transitionsBuilder: _i16.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 200,
        opaque: true,
        barrierDismissible: false,
      );
    },
    JoinGame.name: (routeData) {
      return _i16.CustomPage<dynamic>(
        routeData: routeData,
        child: const _i6.JoinGamePage(),
        transitionsBuilder: _i16.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 200,
        opaque: true,
        barrierDismissible: false,
      );
    },
    EditProfile.name: (routeData) {
      final args = routeData.argsAs<EditProfileArgs>();
      return _i16.CustomPage<dynamic>(
        routeData: routeData,
        child: _i7.EditProfilePage(
          key: args.key,
          onSave: args.onSave,
          editMode: args.editMode,
        ),
        transitionsBuilder: _i16.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 200,
        opaque: true,
        barrierDismissible: false,
      );
    },
    InfoAlert.name: (routeData) {
      final args = routeData.argsAs<InfoAlertArgs>();
      return _i16.CustomPage<dynamic>(
        routeData: routeData,
        child: _i8.InfoAlertPage(
          key: args.key,
          message: args.message,
          subtitle: args.subtitle,
          actions: args.actions,
          canPop: args.canPop,
        ),
        transitionsBuilder: _i16.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 120,
        opaque: false,
        barrierDismissible: false,
      );
    },
    Offer.name: (routeData) {
      final args = routeData.argsAs<OfferArgs>();
      return _i16.CustomPage<dynamic>(
        routeData: routeData,
        child: _i9.OfferPage(
          key: args.key,
          price: args.price,
          item: args.item,
        ),
        transitionsBuilder: _i16.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 120,
        opaque: false,
        barrierDismissible: false,
      );
    },
    StoreOffer.name: (routeData) {
      final args = routeData.argsAs<StoreOfferArgs>();
      return _i16.CustomPage<dynamic>(
        routeData: routeData,
        child: _i10.StoreOfferPage(
          key: args.key,
          mode: args.mode,
        ),
        transitionsBuilder: _i16.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 120,
        opaque: false,
        barrierDismissible: false,
      );
    },
    Camera.name: (routeData) {
      final args = routeData.argsAs<CameraArgs>();
      return _i16.CustomPage<dynamic>(
        routeData: routeData,
        child: _i11.CameraPage(
          key: args.key,
          onImageTaken: args.onImageTaken,
        ),
        transitionsBuilder: _i16.TransitionsBuilders.slideBottom,
        durationInMilliseconds: 150,
        opaque: true,
        barrierDismissible: false,
      );
    },
    Licenses.name: (routeData) {
      return _i16.CustomPage<dynamic>(
        routeData: routeData,
        child: const _i12.LicensesPage(),
        transitionsBuilder: _i16.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 200,
        opaque: true,
        barrierDismissible: false,
      );
    },
    HelpList.name: (routeData) {
      return _i16.CustomPage<dynamic>(
        routeData: routeData,
        child: const _i13.HelpListPage(),
        transitionsBuilder: _i16.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 200,
        opaque: true,
        barrierDismissible: false,
      );
    },
    HelpDetails.name: (routeData) {
      final args = routeData.argsAs<HelpDetailsArgs>();
      return _i16.CustomPage<dynamic>(
        routeData: routeData,
        child: _i14.HelpDetailsPage(
          key: args.key,
          title: args.title,
          content: args.content,
        ),
        transitionsBuilder: _i16.TransitionsBuilders.fadeIn,
        durationInMilliseconds: 200,
        opaque: true,
        barrierDismissible: false,
      );
    },
    Maintenance.name: (routeData) {
      return _i16.CustomPage<dynamic>(
        routeData: routeData,
        child: const _i15.MaintenancePage(),
        transitionsBuilder: _i16.TransitionsBuilders.noTransition,
        durationInMilliseconds: 0,
        opaque: true,
        barrierDismissible: false,
      );
    },
  };

  @override
  List<_i16.RouteConfig> get routes => [
        _i16.RouteConfig(
          Splash.name,
          path: '/',
        ),
        _i16.RouteConfig(
          Welcome.name,
          path: '/welcome',
        ),
        _i16.RouteConfig(
          WaitingRoom.name,
          path: '/waiting_room',
        ),
        _i16.RouteConfig(
          GameRoom.name,
          path: '/game_room',
        ),
        _i16.RouteConfig(
          NewGame.name,
          path: '/new_game',
        ),
        _i16.RouteConfig(
          JoinGame.name,
          path: '/join_game',
        ),
        _i16.RouteConfig(
          EditProfile.name,
          path: '/edit_profile',
        ),
        _i16.RouteConfig(
          InfoAlert.name,
          path: '/alert',
        ),
        _i16.RouteConfig(
          Offer.name,
          path: '/offer',
        ),
        _i16.RouteConfig(
          StoreOffer.name,
          path: '/storeOffer',
        ),
        _i16.RouteConfig(
          Camera.name,
          path: '/camera',
        ),
        _i16.RouteConfig(
          Licenses.name,
          path: '/licenses',
        ),
        _i16.RouteConfig(
          HelpList.name,
          path: '/help',
        ),
        _i16.RouteConfig(
          HelpDetails.name,
          path: '/help/details',
        ),
        _i16.RouteConfig(
          Maintenance.name,
          path: '/maintenance',
        ),
      ];
}

/// generated route for
/// [_i1.SplashPage]
class Splash extends _i16.PageRouteInfo<void> {
  const Splash()
      : super(
          Splash.name,
          path: '/',
        );

  static const String name = 'Splash';
}

/// generated route for
/// [_i2.WelcomePage]
class Welcome extends _i16.PageRouteInfo<void> {
  const Welcome()
      : super(
          Welcome.name,
          path: '/welcome',
        );

  static const String name = 'Welcome';
}

/// generated route for
/// [_i3.WaitingRoomPage]
class WaitingRoom extends _i16.PageRouteInfo<WaitingRoomArgs> {
  WaitingRoom({
    _i17.Key? key,
    required _i18.Session session,
  }) : super(
          WaitingRoom.name,
          path: '/waiting_room',
          args: WaitingRoomArgs(
            key: key,
            session: session,
          ),
        );

  static const String name = 'WaitingRoom';
}

class WaitingRoomArgs {
  const WaitingRoomArgs({
    this.key,
    required this.session,
  });

  final _i17.Key? key;

  final _i18.Session session;

  @override
  String toString() {
    return 'WaitingRoomArgs{key: $key, session: $session}';
  }
}

/// generated route for
/// [_i4.GamePage]
class GameRoom extends _i16.PageRouteInfo<GameRoomArgs> {
  GameRoom({
    _i17.Key? key,
    required _i18.Session session,
    required _i19.Rounds initialRounds,
  }) : super(
          GameRoom.name,
          path: '/game_room',
          args: GameRoomArgs(
            key: key,
            session: session,
            initialRounds: initialRounds,
          ),
        );

  static const String name = 'GameRoom';
}

class GameRoomArgs {
  const GameRoomArgs({
    this.key,
    required this.session,
    required this.initialRounds,
  });

  final _i17.Key? key;

  final _i18.Session session;

  final _i19.Rounds initialRounds;

  @override
  String toString() {
    return 'GameRoomArgs{key: $key, session: $session, initialRounds: $initialRounds}';
  }
}

/// generated route for
/// [_i5.NewGamePage]
class NewGame extends _i16.PageRouteInfo<NewGameArgs> {
  NewGame({
    _i17.Key? key,
    required _i5.NewGameConfig config,
  }) : super(
          NewGame.name,
          path: '/new_game',
          args: NewGameArgs(
            key: key,
            config: config,
          ),
        );

  static const String name = 'NewGame';
}

class NewGameArgs {
  const NewGameArgs({
    this.key,
    required this.config,
  });

  final _i17.Key? key;

  final _i5.NewGameConfig config;

  @override
  String toString() {
    return 'NewGameArgs{key: $key, config: $config}';
  }
}

/// generated route for
/// [_i6.JoinGamePage]
class JoinGame extends _i16.PageRouteInfo<void> {
  const JoinGame()
      : super(
          JoinGame.name,
          path: '/join_game',
        );

  static const String name = 'JoinGame';
}

/// generated route for
/// [_i7.EditProfilePage]
class EditProfile extends _i16.PageRouteInfo<EditProfileArgs> {
  EditProfile({
    _i17.Key? key,
    required dynamic Function(String) onSave,
    bool editMode = false,
  }) : super(
          EditProfile.name,
          path: '/edit_profile',
          args: EditProfileArgs(
            key: key,
            onSave: onSave,
            editMode: editMode,
          ),
        );

  static const String name = 'EditProfile';
}

class EditProfileArgs {
  const EditProfileArgs({
    this.key,
    required this.onSave,
    this.editMode = false,
  });

  final _i17.Key? key;

  final dynamic Function(String) onSave;

  final bool editMode;

  @override
  String toString() {
    return 'EditProfileArgs{key: $key, onSave: $onSave, editMode: $editMode}';
  }
}

/// generated route for
/// [_i8.InfoAlertPage]
class InfoAlert extends _i16.PageRouteInfo<InfoAlertArgs> {
  InfoAlert({
    _i17.Key? key,
    required String message,
    String? subtitle,
    List<_i8.InfoAlertAction>? actions,
    bool canPop = true,
  }) : super(
          InfoAlert.name,
          path: '/alert',
          args: InfoAlertArgs(
            key: key,
            message: message,
            subtitle: subtitle,
            actions: actions,
            canPop: canPop,
          ),
        );

  static const String name = 'InfoAlert';
}

class InfoAlertArgs {
  const InfoAlertArgs({
    this.key,
    required this.message,
    this.subtitle,
    this.actions,
    this.canPop = true,
  });

  final _i17.Key? key;

  final String message;

  final String? subtitle;

  final List<_i8.InfoAlertAction>? actions;

  final bool canPop;

  @override
  String toString() {
    return 'InfoAlertArgs{key: $key, message: $message, subtitle: $subtitle, actions: $actions, canPop: $canPop}';
  }
}

/// generated route for
/// [_i9.OfferPage]
class Offer extends _i16.PageRouteInfo<OfferArgs> {
  Offer({
    _i17.Key? key,
    required String price,
    required _i20.StoreItem item,
  }) : super(
          Offer.name,
          path: '/offer',
          args: OfferArgs(
            key: key,
            price: price,
            item: item,
          ),
        );

  static const String name = 'Offer';
}

class OfferArgs {
  const OfferArgs({
    this.key,
    required this.price,
    required this.item,
  });

  final _i17.Key? key;

  final String price;

  final _i20.StoreItem item;

  @override
  String toString() {
    return 'OfferArgs{key: $key, price: $price, item: $item}';
  }
}

/// generated route for
/// [_i10.StoreOfferPage]
class StoreOffer extends _i16.PageRouteInfo<StoreOfferArgs> {
  StoreOffer({
    _i17.Key? key,
    required _i10.StoreOfferMode mode,
  }) : super(
          StoreOffer.name,
          path: '/storeOffer',
          args: StoreOfferArgs(
            key: key,
            mode: mode,
          ),
        );

  static const String name = 'StoreOffer';
}

class StoreOfferArgs {
  const StoreOfferArgs({
    this.key,
    required this.mode,
  });

  final _i17.Key? key;

  final _i10.StoreOfferMode mode;

  @override
  String toString() {
    return 'StoreOfferArgs{key: $key, mode: $mode}';
  }
}

/// generated route for
/// [_i11.CameraPage]
class Camera extends _i16.PageRouteInfo<CameraArgs> {
  Camera({
    _i17.Key? key,
    required dynamic Function(_i21.Uint8List) onImageTaken,
  }) : super(
          Camera.name,
          path: '/camera',
          args: CameraArgs(
            key: key,
            onImageTaken: onImageTaken,
          ),
        );

  static const String name = 'Camera';
}

class CameraArgs {
  const CameraArgs({
    this.key,
    required this.onImageTaken,
  });

  final _i17.Key? key;

  final dynamic Function(_i21.Uint8List) onImageTaken;

  @override
  String toString() {
    return 'CameraArgs{key: $key, onImageTaken: $onImageTaken}';
  }
}

/// generated route for
/// [_i12.LicensesPage]
class Licenses extends _i16.PageRouteInfo<void> {
  const Licenses()
      : super(
          Licenses.name,
          path: '/licenses',
        );

  static const String name = 'Licenses';
}

/// generated route for
/// [_i13.HelpListPage]
class HelpList extends _i16.PageRouteInfo<void> {
  const HelpList()
      : super(
          HelpList.name,
          path: '/help',
        );

  static const String name = 'HelpList';
}

/// generated route for
/// [_i14.HelpDetailsPage]
class HelpDetails extends _i16.PageRouteInfo<HelpDetailsArgs> {
  HelpDetails({
    _i17.Key? key,
    required String title,
    required String content,
  }) : super(
          HelpDetails.name,
          path: '/help/details',
          args: HelpDetailsArgs(
            key: key,
            title: title,
            content: content,
          ),
        );

  static const String name = 'HelpDetails';
}

class HelpDetailsArgs {
  const HelpDetailsArgs({
    this.key,
    required this.title,
    required this.content,
  });

  final _i17.Key? key;

  final String title;

  final String content;

  @override
  String toString() {
    return 'HelpDetailsArgs{key: $key, title: $title, content: $content}';
  }
}

/// generated route for
/// [_i15.MaintenancePage]
class Maintenance extends _i16.PageRouteInfo<void> {
  const Maintenance()
      : super(
          Maintenance.name,
          path: '/maintenance',
        );

  static const String name = 'Maintenance';
}

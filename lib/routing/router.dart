import 'package:auto_route/auto_route.dart';

import '../scenes/alert_screen.dart';
import '../scenes/camera_screen.dart';
import '../scenes/edit_profile_screen.dart';
import '../scenes/game_screen.dart';
import '../scenes/help_details_screen.dart';
import '../scenes/help_list_screen.dart';
import '../scenes/join_game_screen.dart';
import '../scenes/license_screen.dart';
import '../scenes/maintenance_screen.dart';
import '../scenes/new_game_screen.dart';
import '../scenes/offer_screen.dart';
import '../scenes/splash_screen.dart';
import '../scenes/store_offer_screen.dart';
import '../scenes/waiting_room_sceen.dart';
import '../scenes/welcome_screen.dart';

// ignore: avoid_classes_with_only_static_members
class RoutesDuration {
  static const int regular = 200;
  static const int modal = 120;
}

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route,Dialog',
  routes: <AutoRoute>[
    CustomRoute(
      name: 'Splash',
      page: SplashPage,
      initial: true,
      durationInMilliseconds: RoutesDuration.regular,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      name: 'Welcome',
      page: WelcomePage,
      path: '/welcome',
      durationInMilliseconds: RoutesDuration.regular,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      name: 'WaitingRoom',
      page: WaitingRoomPage,
      path: '/waiting_room',
      durationInMilliseconds: RoutesDuration.regular,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      name: 'GameRoom',
      page: GamePage,
      path: '/game_room',
      durationInMilliseconds: RoutesDuration.regular,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      name: 'NewGame',
      page: NewGamePage,
      path: '/new_game',
      durationInMilliseconds: RoutesDuration.regular,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      name: 'JoinGame',
      page: JoinGamePage,
      path: '/join_game',
      durationInMilliseconds: RoutesDuration.regular,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      name: 'EditProfile',
      page: EditProfilePage,
      path: '/edit_profile',
      durationInMilliseconds: RoutesDuration.regular,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      name: 'InfoAlert',
      page: InfoAlertPage,
      path: '/alert',
      // fullscreenDialog: true,
      durationInMilliseconds: RoutesDuration.modal,
      opaque: false,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      name: 'Offer',
      page: OfferPage,
      path: '/offer',
      // fullscreenDialog: true,
      durationInMilliseconds: RoutesDuration.modal,
      opaque: false,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      name: 'StoreOffer',
      page: StoreOfferPage,
      path: '/storeOffer',
      // fullscreenDialog: true,
      durationInMilliseconds: RoutesDuration.modal,
      opaque: false,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      name: 'Camera',
      page: CameraPage,
      path: '/camera',
      durationInMilliseconds: 150,
      transitionsBuilder: TransitionsBuilders.slideBottom,
    ),
    CustomRoute(
      name: 'Licenses',
      page: LicensesPage,
      path: '/licenses',
      durationInMilliseconds: RoutesDuration.regular,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      name: 'HelpList',
      page: HelpListPage,
      path: '/help',
      durationInMilliseconds: RoutesDuration.regular,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      name: 'HelpDetails',
      page: HelpDetailsPage,
      path: '/help/details',
      durationInMilliseconds: RoutesDuration.regular,
      transitionsBuilder: TransitionsBuilders.fadeIn,
    ),
    CustomRoute(
      name: 'Maintenance',
      page: MaintenancePage,
      path: '/maintenance',
      durationInMilliseconds: 0,
      transitionsBuilder: TransitionsBuilders.noTransition,
    ),
  ],
)
class $AppRouter {}

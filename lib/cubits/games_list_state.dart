import 'package:equatable/equatable.dart';

import '../models/event.dart';

abstract class GameListState extends Equatable {}

class InitialState extends GameListState {
  @override
  List<Object> get props => <Object>[];
}

class FetchedState extends GameListState {
  FetchedState(this.games);

  final List<PendingGame> games;

  @override
  List<Object> get props => <Object>[games];
}

class FetchTimeoutState extends GameListState {
  @override
  List<Object> get props => <Object>[];
}

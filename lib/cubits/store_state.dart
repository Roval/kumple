import 'package:equatable/equatable.dart';

import '../models/store.dart';

abstract class StoreState extends Equatable {}

class InitialState extends StoreState {
  @override
  List<Object> get props => <Object>[];
}

class FetchStoreState extends StoreState {
  FetchStoreState(
    this.response,
    this.prices,
    this.unlockedProductsIaps,
    this.temporaryFreeInStoreIaps,
    this.shouldDisplayUnlockAll,
    this.priceForAllString,
    this.boughtInStoreIaps,
    this.notBoughtInStoreIaps,
  );

  final StoreDefinition response;
  final Map<String, String> prices;
  final List<String> unlockedProductsIaps;
  final List<String> temporaryFreeInStoreIaps;
  final String priceForAllString;
  final bool shouldDisplayUnlockAll;
  final List<String> boughtInStoreIaps;
  final List<String> notBoughtInStoreIaps;

  List<String> get unlockedProductsBundles => response
      .all()
      .where((final StoreItem element) =>
          unlockedProductsIaps.contains(element.iap))
      .map((final StoreItem e) => e.bundle)
      .toList();

  @override
  List<Object> get props => <Object>[
        response,
        unlockedProductsIaps,
        prices,
        temporaryFreeInStoreIaps,
        priceForAllString,
        shouldDisplayUnlockAll,
        boughtInStoreIaps,
        notBoughtInStoreIaps
      ];
}

import 'package:equatable/equatable.dart';

abstract class NotificationsState extends Equatable {}

class InitialState extends NotificationsState {
  @override
  List<Object> get props => <Object>[];
}

class SilentNotificaitonState extends NotificationsState {
  SilentNotificaitonState(this.action, this.room);

  final String action;
  final String room;
  final DateTime _timestamp = DateTime.now();

  @override
  List<Object> get props => <Object>[action, room, _timestamp];
}

class MessageNotificationState extends NotificationsState {
  MessageNotificationState(
      this.title, this.body, this.room, this.player, this.action);

  final String title;
  final String body;
  final String room;
  final String player;
  final String action;
  final DateTime _timestamp = DateTime.now();

  @override
  List<Object> get props =>
      <Object>[title, body, room, player, action, _timestamp];
}

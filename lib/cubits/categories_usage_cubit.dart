import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import '../entities/fetch_games_request.dart';
import '../models/event.dart';
import '../repositories/server_repository.dart';
import '../utils/logger.dart';
import '../utils/session_utils.dart';
import 'categories_usage_state.dart';
import 'store_state.dart' as store;

@singleton
class CategoriesUsageCubit extends Cubit<CategoriesUsageState> {
  CategoriesUsageCubit({required this.repository}) : super(InitialState()) {
    _connectToGameRooms();
  }

  final ServerRepository repository;
  StreamSubscription<Event>? _subscription;

  @override
  Future<void> close() {
    _subscription?.cancel();
    return super.close();
  }

  void _connectToGameRooms() {
    _subscription = repository.observeState().listen((final Event event) {
      if (event is FetchUsageEvent) {
        emit(FetchedUsageState(event.usage));
      }
    });
  }

  void fetchCategoriesUsage(final store.FetchStoreState storeState) {
    if (!isUserUuidGenerated) {
      logger.i('user not authorised yet, do not ask for usage');
      return;
    }

    // Remove unlock all iap
    final List<String> categoriesIaps = storeState.unlockedProductsIaps
        .where((final String element) =>
            element != storeState.response.unlockAll.iap)
        .toList();

    repository.fetchUsage(FetchUsageRequest(getUserUuid(), categoriesIaps));
  }
}

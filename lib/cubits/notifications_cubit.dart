import 'dart:async';

import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import '../utils/session_utils.dart';
import 'notifications_state.dart';

// TODO: Merge with NotificationService, we don't need cubit for it
@singleton
class NotificationsCubit extends Cubit<NotificationsState> {
  NotificationsCubit() : super(InitialState());

  /// If chat for given channel is opened - avoid displaying notifications
  String? openedChatChannel;

  Future<void> receivedNotification(final RemoteMessage event) async {
    final Map<String, dynamic> data = event.data;
    final String? action = data['action'] as String?;
    final String? room = data['room'] as String?;
    if (room != null) {
      if (action == 'refresh') {
        emit(SilentNotificaitonState(action!, room));
      } else {
        final String? title = event.notification?.title;
        final String? body = event.notification?.body;
        final String? player = data['player'] as String?;

        // Right now no ohter as 'chat' push will pass as it is the only one which has player
        // TODO: ^ make state push also be visible
        if (title != null &&
            body != null &&
            player != null &&
            player != getUserUuid() &&
            openedChatChannel != room) {
          // Display only messages from another users and chat not opened
          emit(MessageNotificationState(
              title, body, room, player, action ?? ''));
        }
      }
    }
  }
}

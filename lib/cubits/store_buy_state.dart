import 'package:equatable/equatable.dart';

abstract class StoreBuyState extends Equatable {}

enum PurchaseError {
  productNotFound,
  storeError,
  storeUnavailable,
  notVerified
}

class InitialBuyState extends StoreBuyState {
  @override
  List<Object> get props => <Object>[];
}

class PurchaseStartedState extends StoreBuyState {
  @override
  List<Object> get props => <Object>[];
}

class PurchaseCancaledState extends StoreBuyState {
  @override
  List<Object> get props => <Object>[];
}

class PurchaseSuccessState extends StoreBuyState {
  PurchaseSuccessState(this.iap);

  final String iap;

  @override
  List<Object> get props => <Object>[iap];
}

class PurchaseErrorState extends StoreBuyState {
  PurchaseErrorState(this.value);

  final PurchaseError value;
  final DateTime _timestamp = DateTime.now();

  @override
  List<Object> get props => <Object>[value, _timestamp];
}

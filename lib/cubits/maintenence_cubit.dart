import 'dart:async';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import '../environment_config.dart';
import '../utils/database_helper.dart';
import '../utils/logger.dart';
import 'maintenence_state.dart';

@singleton
class MaintenenceCubit extends Cubit<MaintenenceState> {
  MaintenenceCubit() : super(NoMaintenenceState()) {
    startListeniningForMaintenance();
  }
  bool isEnabled = false;
  StreamSubscription<DatabaseEvent>? _subscription;

  @override
  Future<void> close() {
    _subscription?.cancel();
    return super.close();
  }

  Future<void> startListeniningForMaintenance() async {
    final String path =
        'maintenance/${EnvironmentConfig.isTestPackage ? 'test' : 'prod'}';

    _subscription = await DatabaseHelper.startObserving(
      path: path,
      onValue: _resolveValue,
    );
  }

  void _resolveValue(final bool? value) {
    final bool newValue = value ?? false;
    logger.i('maintenece state: $newValue');
    isEnabled = newValue;
    switch (newValue) {
      case true:
        emit(ActiveMaintenenceState());
        break;
      case false:
        emit(NoMaintenenceState());
        break;
    }
  }
}

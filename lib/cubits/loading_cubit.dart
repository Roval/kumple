import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import 'loading_state.dart';

@singleton
class LoadingCubit extends Cubit<LoadingState> {
  LoadingCubit() : super(LoadingHidden());

  void hideLoading() {
    emit(LoadingHidden());
  }

  void showLoading() {
    emit(LoadingVisible());
  }
}

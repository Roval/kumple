import 'dart:async';

import 'package:collection/collection.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import '../entities/fetch_games_request.dart';
import '../models/event.dart';
import '../repositories/server_repository.dart';
import '../utils/logger.dart';
import '../utils/session_utils.dart';
import 'games_list_state.dart';

@singleton
class GamesListCubit extends Cubit<GameListState> {
  GamesListCubit({required this.repository}) : super(InitialState()) {
    _connectToGameRooms();
  }

  final ServerRepository repository;
  Timer? _fetchGamesTimeout;
  StreamSubscription<Event>? _subscription;

  @override
  Future<void> close() {
    _subscription?.cancel();
    return super.close();
  }

  void _connectToGameRooms() {
    _subscription = repository.observeState().listen((final Event event) {
      if (event is FetchGamesEvent) {
        _fetchGamesTimeout?.cancel();
        _fetchGamesTimeout = null;

        emit(FetchedState(event.games.sorted(
            (final PendingGame a, final PendingGame b) =>
                a.expiresAt.compareTo(b.expiresAt))));
      } else if (event is ErrorEvent) {
        // Clear all timers
        _fetchGamesTimeout?.cancel();
        _fetchGamesTimeout = null;
      }
    });
  }

  /// Called eg. when user doesn't click "Play again" so Welcome screen refetches games.
  /// Otherwise the user would see old games, containing just finished game.
  void reset() {
    emit(InitialState());
  }

  void fetchMyGames() {
    if (!isUserUuidGenerated) {
      logger.i('user not authorised yet, do not ask for games');
      emit(FetchedState(const <PendingGame>[]));
      return;
    }

    repository.fetchGames(FetchGamesRequest(getUserUuid()));

    _fetchGamesTimeout =
        Timer(const Duration(seconds: 10), () => emit(FetchTimeoutState()));
  }
}

import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:cloud_functions/cloud_functions.dart';
import 'package:collection/collection.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as chat_types;
import 'package:flutter_chat_types/flutter_chat_types.dart';
import 'package:injectable/injectable.dart';

import '../di/dependencies.dart';
import '../repositories/local_storage.dart';
import '../utils/camera_util.dart';
import '../utils/database_helper.dart';
import '../utils/logger.dart';
import '../utils/question_colors.dart';
import '../utils/session_utils.dart';
import 'chat_state.dart';
import 'game_cubit.dart';

class _ChatCubitConfig {
  static const int numberOfMessagesInPack = 25;
}

@singleton
class ChatCubit extends Cubit<ChatState> {
  ChatCubit() : super(InitialState());

  /// Holds info about each opened stream.
  /// Steam is opened when room entered and closed when left.
  final Map<String, DatabaseSubscription> _openedStreams =
      <String, DatabaseSubscription>{};

  /// Holds info about each message in the channel in the chat
  final Map<String, List<chat_types.TextMessage>> chats =
      <String, List<chat_types.TextMessage>>{};

  /// Holds set of all the messages in the channel,
  /// used to verify that messages should not be added twice
  final Map<String, Set<String>> _chatsMessagesKeys = <String, Set<String>>{};

  /// Holds info when user last viewed the chat
  final Map<String, int> _chatsLastSeens = <String, int>{};

  /// Holds info if history for the chat was loaded
  final Set<String> _chatsLoaded = <String>{};

  /// Holdes info if there is no more history to load for given chat
  final Set<String> _chatsFetchedToStart = <String>{};

  final Map<String, Color> _userColors = <String, Color>{};

  final List<chat_types.TextMessage> _messagesWaitingForDeliver =
      <chat_types.TextMessage>[];

  List<chat_types.TextMessage> messagesForChannel(final String channel) {
    return chats[channel] ?? <chat_types.TextMessage>[];
  }

  Future<void> sendMessage(
      final String channel, final String message, final String? name) async {
    // Send it locally
    final String id = _randomString();
    final chat_types.TextMessage entity = chat_types.TextMessage(
        author: chat_types.User(
            id: getUserUuid(), imageUrl: avatarUrl(player: getUserUuid())),
        id: id,
        status: Status.sending,
        createdAt: DateTime.now().millisecondsSinceEpoch,
        text: message);
    chats[channel]!.insert(0, entity);
    _messagesWaitingForDeliver.add(entity);

    final ChannelState newState = ChannelState(channel, chats[channel]!.length);
    emit(newState);

    // Send to remote
    final FirebaseFunctions functions = FirebaseFunctions.instance;
    final HttpsCallable callable = functions.httpsCallable('sendMessage');
    try {
      await callable.call(<String, String>{
        'id': id,
        'name': name ?? '',
        'channel': channel,
        'message': message,
      });
    } on FirebaseFunctionsException catch (error) {
      logger.e(error.message);
    }
    logger.i('[chat] message sent');
  }

  void saveLastSeen(final String channel) {
    // Save in both local storage (disc) and memory.
    final int time = DateTime.now().millisecondsSinceEpoch;
    _chatsLastSeens[channel] = time;
    LocalStorage.markChannelAsSeen(channel, time);
  }

  /// Fetch only once, when chat started, to minimise number of calls to local storage
  Future<void> loadLastSeenMessageInChannel(final String channel) async {
    if (_chatsLastSeens[channel] == null) {
      _chatsLastSeens[channel] =
          await LocalStorage.getChannelLastSeen(channel) ?? 0;
    }
  }

  bool hasNewMassagedInChannel(final String channel) {
    final int timestamp = _chatsLastSeens[channel] ?? 0;
    logger.i(
        '[chat] checking for latest message for $channel, last visited $timestamp');
    final chat_types.TextMessage? lastMessage = chats[channel]?.firstOrNull;
    final int? lastMessageTimestamp = lastMessage?.createdAt;
    final chat_types.User? lastMessageAuthor = lastMessage?.author;
    logger.i(
        '[chat] latest message for $channel: $lastMessageTimestamp, user: ${lastMessageAuthor?.id}');
    if (lastMessageTimestamp == null) {
      return false;
    }
    // If my own message received after delay when chat closed
    if (lastMessageAuthor?.id == getUserUuid()) {
      return false;
    }
    return lastMessageTimestamp > timestamp;
  }

  bool isFullyFetched(final String channel) {
    return _chatsFetchedToStart.contains(channel);
  }

  Future<void> loadNew(final String channel,
      {required final String after}) async {
    if (!_chatsLoaded.contains(channel)) {
      // Only proceed if history previously loaded
      return;
    }

    logger.i('[chat] start loading new messages for $channel, after: $after');

    final String key = 'chat/$channel/messages';
    try {
      final Map<Object?, Object?> messagesData =
          await DatabaseHelper.getValue(path: key, after: after, count: 100) ??
              <Object?, Object?>{};

      addNewMessages(channel, messagesData);
    } on FirebaseException catch (e) {
      logger.e('[chat] error when sloading new messages $channel, $e');
    }
  }

  Future<void> loadHistory(
    final String channel, {
    final bool nextPage = false,
  }) async {
    if (_chatsLoaded.contains(channel) && !nextPage) {
      // Discard if we want to load first page again
      return;
    }

    if (!_chatsLoaded.contains(channel) && nextPage) {
      // Discard if not loaded and next page requested
      return;
    }

    final List<chat_types.TextMessage> chatMessages = chats[channel]!;
    final String? lastMessage = chatMessages.lastOrNull?.remoteId;
    logger.i('[chat] start loading history for $channel, before: $lastMessage');

    final String key = 'chat/$channel/messages';
    try {
      final Map<Object?, Object?> messagesData = await DatabaseHelper.getValue(
            path: key,
            before: lastMessage,
            count: _ChatCubitConfig.numberOfMessagesInPack,
          ) ??
          <Object?, Object?>{};

      _chatsLoaded.add(channel);

      logger.i(
          '[chat] loaded history for $channel, count ${messagesData.length}');

      if (messagesData.length < _ChatCubitConfig.numberOfMessagesInPack) {
        _chatsFetchedToStart.add(channel);
      }

      addNewMessages(channel, messagesData);
    } on FirebaseException catch (e) {
      logger.e('[chat] error when loading new messages $channel, $e');
    }
  }

  void addNewMessages(
      final String channel, final Map<Object?, Object?> messagesData) {
    final List<chat_types.TextMessage> chatMessages = chats[channel]!;

    final List<chat_types.TextMessage> messages =
        _mapFromFirebaseSnapshot(messagesData)
            // Discard already added elements
            .where((final chat_types.TextMessage element) =>
                !_chatsMessagesKeys[channel]!.contains(element.remoteId))
            .toList();
    chatMessages.addAll(messages);
    chatMessages.sort(
        (final chat_types.TextMessage a, final chat_types.TextMessage b) =>
            b.createdAt!.compareTo(a.createdAt!));

    _chatsMessagesKeys[channel]!.addAll(messagesData.keys.cast<String>());

    logger
        .i('[chat] loaded ${messagesData.length} messages in channel $channel');

    emit(ChannelState(channel, chatMessages.length));
  }

  Future<void> startObservingChannel(
    final String channel,
  ) async {
    if (_openedStreams.containsKey(channel)) {
      logger.i('[chat] channel $channel already observed');
      return;
    }

    if (!chats.containsKey(channel)) {
      logger.i('[chat] create state for channel $channel');
      chats[channel] = <chat_types.TextMessage>[];
      _chatsMessagesKeys[channel] = <String>{};
    }

    logger.i('[chat] start observing $channel');
    final String key = 'chat/$channel/messages';

    // ignore: cancel_subscriptions
    final DatabaseSubscription? newStream =
        await DatabaseHelper.startObservingChildAdded(
      path: key,
      onValue: (final String key, final Object? message) {
        if (_chatsMessagesKeys[channel]!.contains(key)) {
          return;
        }

        logger.i('[chat] new message $key in channel: $message');
        if (message is Map) {
          final List<chat_types.TextMessage> chatMessages = chats[channel]!;
          final chat_types.TextMessage entity =
              _mapFromFirebaseObject(key, message);
          if (_chatsMessagesKeys[channel]!.contains(entity.remoteId)) {
            // Message already exist, skip
            return;
          }
          if (entity.author.id == getUserUuid()) {
            // Find message to be delivered and change its content, do not add it again
            final chat_types.TextMessage? awaitingMessage =
                _messagesWaitingForDeliver.firstWhereOrNull(
                    (final chat_types.TextMessage msgWaiting) {
              return msgWaiting.id == entity.id;
            });
            if (awaitingMessage != null) {
              final int index = chatMessages.indexOf(awaitingMessage);
              chatMessages[index] = entity;
              _messagesWaitingForDeliver.remove(awaitingMessage);
            } else {
              chatMessages.insert(0, entity);
            }
          } else {
            chatMessages.insert(0, entity);
          }
          _chatsMessagesKeys[channel]!.add(key);

          final ChannelState newState =
              ChannelState(channel, chatMessages.length);
          emit(newState);
        }
      },
      onError: (final dynamic error) {
        logger
            .e('[chat] error when starting observing channel $channel, $error');

        if (error is FirebaseException) {
          if (error.code == 'permission-denied') {
            logger.i('[chat] reconfigure chat, permission denied');
            getIt<GameCubit>().reconfigureChatForPlayer(room: channel);
          }
        }
        stopObservingChannel(channel);
      },
    );
    if (newStream != null) {
      logger.i('[chat] started observing channel $channel');
      _openedStreams[channel] = newStream;
    }
  }

  Future<void> stopObservingAllChannels() async {
    // Create copy of the opened channels list
    // (copy is needed otherwise we get exception that we modify array during for loop)
    final List<String> openedChannels = List<String>.from(_openedStreams.keys);
    for (final String observedChannel in openedChannels) {
      await stopObservingChannel(observedChannel);
    }
  }

  Future<void> stopObservingChannel(final String channel) async {
    final StreamSubscription<DatabaseEvent>? stream = _openedStreams[channel];
    if (stream == null) {
      logger.i('[chat] no channel opened for $channel, skip closing');
      return;
    }

    await stream.cancel();
    _openedStreams.remove(channel);
    logger.i('[chat] channel $channel stream cancelled');
  }

  List<chat_types.TextMessage> _mapFromFirebaseSnapshot(
      final Map<Object?, Object?> snapshot) {
    return snapshot
        .cast<String, Map<dynamic, dynamic>>()
        .entries
        .map(
          (final MapEntry<String, Map<dynamic, dynamic>> e) =>
              _mapFromFirebaseObject(e.key, e.value),
        )
        .toList();
  }

  chat_types.TextMessage _mapFromFirebaseObject(
    final String key,
    final Map<dynamic, dynamic> data,
  ) {
    return chat_types.TextMessage(
      id: data['id']
          as String, // using just a key crashes on iOS for some f**ing weird reason, so we use UUID and has key in remoteId
      author: chat_types.User(
          id: data['player'] as String,
          imageUrl: avatarUrl(player: data['player'] as String)),
      text: data['message']! as String,
      createdAt: data['date'] as int,
      remoteId: key,
    );
  }

  // According to doc we should use UUID, but random string is ok for now
  String _randomString() {
    final Random random = Random.secure();
    final List<int> values =
        List<int>.generate(16, (final int i) => random.nextInt(255));
    return base64UrlEncode(values);
  }

  /// We hold global map of users colors across all the chats to never change
  /// whenever chat is reopened
  Color nameColorForPlayer(final String author) {
    if (_userColors[author] != null) {
      return _userColors[author]!;
    }

    // Generate next available color
    final int currentColorCount = _userColors.length;
    final int colorIndex = currentColorCount % questionColors.length;

    _userColors[author] = questionColors[colorIndex];
    return _userColors[author]!;
  }
}

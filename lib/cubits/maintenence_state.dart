import 'package:equatable/equatable.dart';

abstract class MaintenenceState extends Equatable {}

class NoMaintenenceState extends MaintenenceState {
  @override
  List<Object> get props => <Object>[];
}

class ActiveMaintenenceState extends MaintenenceState {
  ActiveMaintenenceState();

  @override
  List<Object> get props => <Object>[];
}

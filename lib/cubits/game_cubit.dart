import 'dart:async';

import 'package:collection/collection.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

import '../entities/answer_request.dart';
import '../entities/create_game_request.dart';
import '../models/event.dart';
import '../repositories/local_storage.dart';
import '../repositories/server_repository.dart';
import '../utils/logger.dart';
import '../utils/session_utils.dart';
import '../widget/new_game/change_language_setting.dart';
import '../widget/new_game/change_questions_setting.dart';
import 'game_state.dart';

enum _JoinEnum { create, join, rejoin }

class _JoinIntent {
  _JoinIntent(this.timer, this.reason);

  Timer timer;
  _JoinEnum reason;
}

@singleton
class GameCubit extends Cubit<GameState> {
  GameCubit({required this.repository}) : super(InitialState()) {
    _connectToGameRooms();
  }

  final String _logTag = '[game_cubit]';

  final ServerRepository repository;
  StreamSubscription<Event>? _subscription;

  // When calling `rejoin` method we want to also receive the game state
  // (for other users this event is discarded but for me it is when I restore connection).
  // So when we get this we send two events: Rejoin and GameState
  // This flag is set to true when rejoin called so we know to send game state after getting the response
  bool _rejoinReceived = false;

  /// Responsible for storing info what is the last reason user is trying to join
  /// the game and starts timeout timer;
  _JoinIntent? _joinIntent;

  @override
  Future<void> close() {
    _subscription?.cancel();
    return super.close();
  }

  void _connectToGameRooms() {
    logger.i('$_logTag connect to events');
    _rejoinReceived = false;
    _subscription = repository.observeState().listen((final Event event) {
      logger.i('$_logTag received new event, ${event.type}');
      if (event is GameEvent) {
        final GameData data = event.game.data;
        final Players players = event.game.players;

        // Move host to top of players
        final Player? host = players
            .firstWhereOrNull((final Player e) => e.id == event.game.owner);
        if (host != null) {
          players.remove(host);
          players.insert(0, host);
        }

        switch (event.type) {
          case 'joined':
            final _JoinEnum? reason = _joinIntent?.reason;
            _stopTimeout();

            if (data is PendingData) {
              if (reason == _JoinEnum.create) {
                _markGameCreated();
              } else if (reason == _JoinEnum.join) {
                _markUserJoinedTheGame();
              }

              logger.i('$_logTag emit join event');
              emit(JoinState(
                // Join when on waiting screen
                players,
                event.game.rounds,
                event.game.room,
                data.readyPlayers,
                event.game.owner,
                data.categories,
                data.language,
                data.excludedQuestions,
              ));
            } else {
              logger.i('$_logTag emit rejoin event');
              emit(RejoinState(
                // Join state when game is not pending
                players,
                event.game.rounds,
                event.game.room,
              ));

              // If I'm the rejoined user - I have to consider this as a state
              if (!_rejoinReceived) {
                logger.i('$_logTag add game state to rejoin event');
                _handleState(event.game);
              }
            }
            _rejoinReceived = true;
            break;
          case 'state':
            logger.i('$_logTag emit state event');
            _handleState(event.game);
            break;
          default:
            logger.i('$_logTag not processed event with type: ${event.type}');
            break;
        }
      } else if (event is ErrorEvent) {
        _stopTimeout();

        emit(ErrorState(event.error));
      } else if (event is KickedEvent) {
        emit(KickedState(event.room));
      } else if (event is DisconnectEvent) {
        if (event.reason == 'background') {
          // When in game screen - we don't want to send DisconnectState because
          // the screen would show "Refresh screen" immidiatly after entering background
          // Instead we do not send it it game stays in "question screen" and connection will be
          // restored immidiatly after user reenters the app.
          // Hopefully he will not notice that connection was not there and will contiue playing.
          logger.i(
              '$_logTag ommit disconnected, app will restore connection immidiatly');
          return;
        }
        emit(DisconnectState());
      }
    });
  }

  void _handleState(final Game game) {
    final GameData data = game.data;
    final Players players = game.players;
    final Rounds rounds = game.rounds;
    if (data is QuestionData) {
      emit(QuestionState(players, rounds, data, game.owner));
    } else if (data is QuestionResultData) {
      emit(QuestionResultState(
        players,
        rounds,
        data,
        data.readyPlayers,
      ));
    } else if (data is FinishData) {
      emit(FinishState(players, rounds, game.owner, data.nextRoomKey));
    }
  }

  void _timeoutHit(final GameState stateToSend) {
    logger.e(
        '$_logTag timeout hit with state $stateToSend for intent ${_joinIntent?.reason.name}');
    _joinIntent = null;
    emit(stateToSend);
    disconnect('timeout');
  }

  /// Stops timeout
  void _stopTimeout() {
    if (_joinIntent != null) {
      logger.i(
          '$_logTag stop timeout counter for intent ${_joinIntent!.reason.name}');
      // Clear all timers
      _joinIntent?.timer.cancel();
      _joinIntent = null;
    }
  }

  Future<void> createGameRoom({
    required final String name,
    required final int rounds,
    required final List<String> iaps,
    required final Language language,
    required final Set<QuestionSetting> excludedCategories,
  }) async {
    final String? token = await _pushToken();
    final UserJoinRequest user = UserJoinRequest(getUserUuid(), name, token);
    repository.createGame(
      CreateGameRequest(
          user,
          iaps,
          rounds,
          language.value,
          excludedCategories
              .map((final QuestionSetting e) => e.value)
              .toList()),
    );

    _joinIntent = _JoinIntent(
      Timer(
        const Duration(seconds: 10),
        () => _timeoutHit(
          JoinTimeout(true),
        ),
      ),
      _JoinEnum.create,
    );
  }

  Future<void> joinGameRoom({
    required final String name,
    required final String room,
    required final bool isJoiningForTheFirstTime,
  }) async {
    final String? token = await _pushToken();

    final UserJoinRequest user = UserJoinRequest(getUserUuid(), name, token);
    // TODO: Previously the app had some try/catch here do get all the errors
    // It was said that socket "was stuck" but I don't remember what was that :/
    // Check the previous commits to see how it was done
    repository.joinGame(JoinGameRequest(room, user));

    _joinIntent = _JoinIntent(
      Timer(
        const Duration(seconds: 10),
        () => _timeoutHit(
          JoinTimeout(false),
        ),
      ),
      isJoiningForTheFirstTime ? _JoinEnum.join : _JoinEnum.rejoin,
    );
  }

  Future<void> leaveGameRoom({required final String room}) async {
    repository.leaveGame(LeaveGameRequest(room, getUserUuid()));
    _rejoinReceived = false;
  }

  Future<void> exitGameRoom({required final String room}) async {
    repository.exitGame(ExitGameRequest(room, getUserUuid()));
    _rejoinReceived = false;
  }

  Future<void> removePlayer({
    required final String room,
    required final String userId,
  }) async {
    repository.removePlayer(RemovePlayerRequest(room, userId));
  }

  Future<void> reportPlayerReadyInRoom({required final String room}) async {
    repository.reportReady();
  }

  Future<void> reportPlayerWantsToContinue({required final String room}) async {
    repository.reportContinue();
  }

  Future<void> reconfigureChatForPlayer({required final String room}) async {
    repository.reconfigureChat();
  }

  Future<void> updateGameRoom({
    required final int maxRounds,
    required final List<String> categories,
    required final Language language,
    required final Set<QuestionSetting> excludedCategories,
  }) async {
    repository.updateGame(UpdateGameRequest(
      maxRounds,
      categories,
      language.value,
      excludedCategories.map((final QuestionSetting e) => e.value).toList(),
    ));
  }

  Future<void> sendAnswer({
    required final String room,
    required final String questionId,
    required final Object answer,
  }) async {
    repository.sendAnswer(AnswerQuestionRequest(questionId, answer));
  }

  Future<void> disconnect(final String closeReason) async {
    await repository.closeWithCode(closeReason);
  }

  Future<void> rejoin(final String room) async {
    final String? name = await LocalStorage.getUsername();
    if (name != null) {
      logger.i('$_logTag sending join request');
      final UserJoinRequest user = UserJoinRequest(getUserUuid(), name, null);
      _rejoinReceived = false;
      repository.joinGame(JoinGameRequest(room, user));
    }
  }

  Future<void> _markUserJoinedTheGame() async {
    final int howManyTimesUserJoined =
        await LocalStorage.getHowManyGamesJoined();
    final int howManyTimesUserCreatedTheGame =
        await LocalStorage.getHowManyGamesCreated();

    if (howManyTimesUserCreatedTheGame == 0 && howManyTimesUserJoined == 0) {
      logger.i('users first interaction is joining the game');
      FirebaseAnalytics.instance.logEvent(name: 'joined_as_first_action');
    }

    LocalStorage.markGameJoined();
  }

  Future<void> _markGameCreated() async {
    final int howManyTimesUserCreatedTheGame =
        await LocalStorage.getHowManyGamesCreated();

    FirebaseAnalytics.instance.logEvent(
        name: 'created_game',
        parameters: <String, int>{'count': howManyTimesUserCreatedTheGame});

    LocalStorage.markGameCreated();
  }

  Future<String?> _pushToken() async {
    try {
      return await FirebaseMessaging.instance.getToken();
    } catch (e) {
      logger.e('failed getting token for user: (e)');
      return null;
    }
  }
}

import 'package:equatable/equatable.dart';

import '../models/event.dart';

abstract class CategoriesUsageState extends Equatable {}

class InitialState extends CategoriesUsageState {
  @override
  List<Object> get props => <Object>[];
}

class FetchedUsageState extends CategoriesUsageState {
  FetchedUsageState(this.usage);

  final UsedQuestions usage;

  /// If two timeouts in row - we have to have timestamp to know it is a different one
  final DateTime _timestamp = DateTime.now();

  @override
  List<Object> get props => <Object>[usage, _timestamp];
}

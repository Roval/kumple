import 'package:collection/collection.dart';
import 'package:equatable/equatable.dart';

import '../models/event.dart';
import '../widget/new_game/change_language_setting.dart';
import '../widget/new_game/change_questions_setting.dart';

abstract class GameState extends Equatable {}

class InitialState extends GameState {
  @override
  List<Object> get props => <Object>[];
}

class DisconnectState extends GameState {
  @override
  List<Object> get props => <Object>[];
}

class ErrorState extends GameState {
  ErrorState(this.error);

  final String error;

  @override
  List<Object> get props => <Object>[error];
}

class KickedState extends GameState {
  KickedState(this.roomKey);

  final String roomKey;

  @override
  List<Object> get props => <Object>[roomKey];
}

class PlayersState extends GameState {
  PlayersState(this.players, this.rounds);

  final Players players;
  final Rounds rounds;

  @override
  List<Object> get props => <Object>[players, rounds];
}

class RejoinState extends PlayersState {
  RejoinState(final Players players, final Rounds rounds, this.roomKey)
      : super(players, rounds);

  final String roomKey;

  @override
  List<Object> get props => super.props + <Object>[roomKey];
}

class JoinState extends PlayersState {
  JoinState(
    final Players players,
    final Rounds rounds,
    this.roomKey,
    this.readyPlayers,
    this.owner,
    this.categories,
    final String languageRaw,
    final List<String> excludedQuestionsStr,
  )   : language = Language.valueForStr(languageRaw),
        excludedQuestions = excludedQuestionsStr
            .map((final String e) => QuestionSetting.valueForStr(e))
            .whereNotNull()
            .toSet(),
        super(players, rounds);

  final String roomKey;
  final String owner;
  final List<String> categories;
  final Set<QuestionSetting> excludedQuestions;
  final Language language;
  final List<String> readyPlayers;

  @override
  List<Object> get props =>
      super.props +
      <Object>[
        roomKey,
        readyPlayers,
        owner,
        categories,
        excludedQuestions,
        language,
      ];
}

class JoinTimeout extends GameState {
  JoinTimeout(this.isCreatingNewGame);

  final bool isCreatingNewGame;

  /// If two timeouts in row - we have to have timestamp to know it is a different one
  final DateTime _timestamp = DateTime.now();

  @override
  List<Object> get props => <Object>[isCreatingNewGame, _timestamp];
}

class QuestionState extends PlayersState {
  QuestionState(
      final Players players, final Rounds rounds, this.data, this.host)
      : super(players, rounds);

  final QuestionData data;
  final String host;

  String get category => data.id.split(':')[1];

  @override
  List<Object> get props => super.props + <Object>[data, host];
}

class QuestionResultState extends PlayersState {
  QuestionResultState(
      final Players players, final Rounds rounds, this.data, this.readyPlayers)
      : super(players, rounds);

  final QuestionResultData data;
  final List<String> readyPlayers;

  @override
  List<Object> get props => super.props + <Object>[data, readyPlayers];
}

class FinishState extends PlayersState {
  FinishState(
      final Players players, final Rounds rounds, this.host, this.nextRoomKey)
      : super(players, rounds);

  final String nextRoomKey;
  final String host;

  @override
  List<Object> get props => super.props + <Object>[nextRoomKey, host];
}

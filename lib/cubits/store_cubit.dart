import 'dart:async';

import 'package:collection/collection.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:injectable/injectable.dart';
import 'package:intl/intl.dart';

import '../environment_config.dart';
import '../models/store.dart';
import '../repositories/local_storage.dart';
import '../repositories/remote_config.dart';
import '../utils/logger.dart';
import 'store_state.dart';

@singleton
class StoreCubit extends Cubit<StoreState> {
  StoreCubit() : super(InitialState());

  /// Contains last fetched store details
  FetchStoreState? lastFetchedStoreResponse;

  Future<void> fetchStore() async {
    emit(
        InitialState()); // FetchedState would not trigger any change because of Equatable protocol

    final StoreDefinition response = await RemoteConfigHelper.storeContent();
    // if (EnvironmentConfig.isTestPackage) {
    //   response.store.add(StoreItem.fromJson(<String, String>{
    //     'iap_ios': 'com.kumple.iap.dev',
    //     'iap_android': 'com.kumple.iap.dev',
    //     'color': '#cc2200',
    //     'logo_url':
    //         'https://firebasestorage.googleapis.com/v0/b/kumple-e666c.appspot.com/o/assets%2F0012.png?alt=media',
    //     'title_pl': 'DEV',
    //     'title_en': 'DEV',
    //     'bundle': 'DEV',
    //   }));
    // }
    // Check if unlock all should be displayed
    bool shouldDisplayUnlockAll = RemoteConfigHelper.isFullUnlockVisible;

    // Bought by IAP
    List<String> boughtInStoreIaps = await LocalStorage.getBoughtProducts();
    // NOT bought products
    final List<String> notBoughtInStoreIaps = response.store
        .where((final StoreItem item) => !boughtInStoreIaps.contains(item.iap))
        .map((final StoreItem item) => item.iap)
        .toList();

    if (notBoughtInStoreIaps.isEmpty) {
      shouldDisplayUnlockAll = false;
    }

    if (boughtInStoreIaps.contains(response.unlockAll.iap)) {
      for (final String iap in notBoughtInStoreIaps) {
        await LocalStorage.saveBoughtProduct(id: iap);
      }
      boughtInStoreIaps = await LocalStorage.getBoughtProducts();
      notBoughtInStoreIaps.clear();
      shouldDisplayUnlockAll = false;
    } else {
      // "Unlock all" item, add to not bought to fetch it's price
      final IapItem unlockAll = response.unlockAll;
      notBoughtInStoreIaps.add(unlockAll.iap);
    }

    // Available for free
    final List<String> temporaryFreeInStoreIaps = response.store
        .where((final StoreItem item) =>
            item.daysRemainingForPromo > 0 &&
            item.daysToPromo <= 0 &&
            notBoughtInStoreIaps.contains(item.iap))
        .map((final StoreItem item) => item.iap)
        .toList();

    // Seasonal in store
    final List<String> seasonalInStore = response.seasonal
        .where((final StoreItem item) =>
            item.daysToPromo <= 0 && item.daysRemainingForPromo > 0)
        .map((final StoreItem item) => item.iap)
        .toList();

    // All unlocked products
    final List<String> unlockedProductsIaps = boughtInStoreIaps +
        // Add free
        <String>[response.free.iap] +
        // Add seasonal
        seasonalInStore +
        // Add temporary free
        temporaryFreeInStoreIaps;

    // Full (all bundles) calculation
    String priceForAllString = '';

    // Get product prices
    final List<ProductDetails> products;
    if (!EnvironmentConfig.isTestPackage) {
      products = (await InAppPurchase.instance
              .queryProductDetails(notBoughtInStoreIaps.toSet()))
          .productDetails;
    } else {
      products = <ProductDetails>[];
    }
    final Map<String, String> prices = <String, String>{};
    for (final String toBuy in notBoughtInStoreIaps) {
      final ProductDetails? product = products.firstWhereOrNull(
          (final ProductDetails product) => product.id == toBuy);
      if (product == null) {
        if (!EnvironmentConfig.isTestPackage) {
          logger.e('unable to find product $toBuy in store, no price');
        }
        prices[toBuy] = '...';
      } else {
        final String currencySymbol = product.price
            .replaceAll(RegExp(r'\d'), '')
            .replaceAll('.', '')
            .replaceAll(',', '')
            .trim();

        final bool shouldCalculateFinalPrice =
            toBuy != response.unlockAll.iap && priceForAllString.isEmpty;
        final double finalRawPrice =
            response.store.length.toDouble() * product.rawPrice;

        final NumberFormat formatter = NumberFormat.currency(
            symbol: currencySymbol.isNotEmpty
                ? currencySymbol
                : product.currencyCode);
        prices[toBuy] = formatter.format(product.rawPrice);
        // Calculate final price, this is fired only once
        if (shouldCalculateFinalPrice) {
          priceForAllString = formatter.format(finalRawPrice);
        }
      }
    }

    // Remove "Unlock all" from not bought (it was added to fetch price)
    notBoughtInStoreIaps.removeWhere(
        (final String element) => element == response.unlockAll.iap);

    lastFetchedStoreResponse = FetchStoreState(
      response,
      prices,
      unlockedProductsIaps,
      temporaryFreeInStoreIaps,
      shouldDisplayUnlockAll,
      priceForAllString,
      boughtInStoreIaps,
      notBoughtInStoreIaps,
    );
    emit(lastFetchedStoreResponse!);
  }

  Future<void> restorePurchases() async {
    logger.i('restore purchases');
    await InAppPurchase.instance.restorePurchases();
  }
}

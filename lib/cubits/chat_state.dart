import 'package:equatable/equatable.dart';

abstract class ChatState extends Equatable {}

class InitialState extends ChatState {
  @override
  List<Object> get props => <Object>[];
}

class ChannelState extends ChatState {
  ChannelState(
    this.channel,
    this.messagesCount,
  );

  final String channel;
  final int messagesCount;

  /// Sometimes channel and count doesn't change (when status of message changes)
  /// So we use timestamp to always say status is the newest here
  final DateTime _timestamp = DateTime.now();

  @override
  List<Object> get props => <Object>[channel, messagesCount, _timestamp];
}

import 'package:equatable/equatable.dart';

abstract class LoadingState extends Equatable {}

class LoadingHidden extends LoadingState {
  @override
  List<Object> get props => <Object>[];
}

class LoadingVisible extends LoadingState {
  @override
  List<Object> get props => <Object>[];
}

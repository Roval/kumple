import 'dart:async';
import 'dart:io';

import 'package:cloud_functions/cloud_functions.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:injectable/injectable.dart';

import '../environment_config.dart';
import '../models/store.dart';
import '../plugins/facebook_plugin.dart';
import '../repositories/local_storage.dart';
import '../repositories/remote_config.dart';
import '../utils/appsflyer_helper.dart';
import '../utils/logger.dart';
import 'store_buy_state.dart';
import 'store_cubit.dart';

enum BuySource {
  newGame,
  store,
  usageOffer,
  offer;

  String get value {
    switch (this) {
      case BuySource.newGame:
        return 'newGame';
      case BuySource.store:
        return 'store';
      case BuySource.offer:
        return 'offer';
      case BuySource.usageOffer:
        return 'usageOffer';
    }
  }
}

@singleton
class StoreBuyCubit extends Cubit<StoreBuyState> {
  StoreBuyCubit(this._storeCubit) : super(InitialBuyState()) {
    _startListeningForUpdated();
  }

  late StreamSubscription<List<PurchaseDetails>> _subscription;
  final Map<String, ProductDetails> _productsCache = <String, ProductDetails>{};

  final StoreCubit _storeCubit;

  @override
  Future<void> close() {
    _subscription.cancel();
    return super.close();
  }

  void _startListeningForUpdated() {
    final Stream<List<PurchaseDetails>> purchaseUpdated =
        InAppPurchase.instance.purchaseStream;
    _subscription = purchaseUpdated.listen(
        (final List<PurchaseDetails> purchaseDetailsList) {
      _listenToPurchaseUpdated(purchaseDetailsList);
    }, onDone: () {
      _subscription.cancel();
    });
  }

  Future<void> _listenToPurchaseUpdated(
      final List<PurchaseDetails> purchaseDetailsList) async {
    for (final PurchaseDetails purchaseDetails in purchaseDetailsList) {
      logger.i(
          '[store] transaction in progress: ${purchaseDetails.productID}, status: ${purchaseDetails.status}');
      if (purchaseDetails.status == PurchaseStatus.pending) {
        // DO NOTHING
      } else if (purchaseDetails.status == PurchaseStatus.canceled) {
        logger.i('[store] transaction cancelled');
        if (Platform.isIOS) {
          // Cancelled transaction also has to be marked as completed otherwise no other purchase will be performed
          await InAppPurchase.instance.completePurchase(purchaseDetails);
        }
        emit(PurchaseCancaledState());
        return;
      } else if (purchaseDetails.status == PurchaseStatus.error &&
          !_isRestoredOnAndroid(purchaseDetails)) {
        logger.e('[store] transaction error: ${purchaseDetails.error!}');
        // hide with delay as there is a little big of the lag
        // before store UI is shown
        Future<void>.delayed(const Duration(seconds: 1), () {
          emit(PurchaseErrorState(PurchaseError.storeError));
        });
        return;
      } else if (_isRestoredOnAndroid(purchaseDetails)) {
        _storeCubit.restorePurchases();
        // To hide loading screen
        emit(PurchaseCancaledState());
        return;
      } else if (purchaseDetails.status == PurchaseStatus.purchased ||
          purchaseDetails.status == PurchaseStatus.restored) {
        final bool valid = await _verifyPurchase(purchaseDetails);
        if (valid) {
          await _deliverProduct(purchaseDetails);
        } else {
          await _handleInvalidPurchase(purchaseDetails);
        }
      }
      if (purchaseDetails.pendingCompletePurchase) {
        await InAppPurchase.instance.completePurchase(purchaseDetails);
      }
    }

    // Refetch store and bought ids
    _storeCubit.fetchStore();
  }

  bool _isRestoredOnAndroid(final PurchaseDetails purchaseDetails) {
    return Platform.isAndroid &&
        (purchaseDetails.error?.message.contains('itemAlreadyOwned') ?? false);
  }

  Future<bool> _verifyPurchase(final PurchaseDetails purchaseDetails) async {
    logger.i('[store] start validating purchase ${purchaseDetails.productID}');
    final String platform = Platform.isIOS ? 'ios' : 'android';

    final FirebaseFunctions functions = FirebaseFunctions.instance;
    final HttpsCallable callable = functions.httpsCallable('validateReceipt');
    try {
      final Map<String, String> params = <String, String>{
        'receipt': purchaseDetails.verificationData.serverVerificationData,
        'platform': platform,
        'product': purchaseDetails.productID,
      };
      if (kDebugMode || EnvironmentConfig.isTestPackage) {
        params['env'] = 'sandbox';
      }

      final HttpsCallableResult<Map<dynamic, dynamic>> response =
          await callable.call(params);

      logger.i('[store] response from validate receipt: ${response.data}');
      if (response.data['status'] == 'accepted') {
        logger.i(
            '[store] validation success, add product: ${purchaseDetails.productID}!');
        return true;
      } else {
        logger.w(
            '[store] validation failed for product ${purchaseDetails.productID}');
        return false;
      }
    } on FirebaseFunctionsException catch (error) {
      logger.e('[store] unable to validate purchase ${error.message}');
      return false;
    }
  }

  Future<void> _handleInvalidPurchase(
      final PurchaseDetails purchaseDetails) async {
    emit(PurchaseErrorState(PurchaseError.notVerified));
  }

  Future<void> _deliverProduct(final PurchaseDetails purchaseDetails) async {
    await InAppPurchase.instance.completePurchase(purchaseDetails);

    final String productId = purchaseDetails.productID;
    if (await _applyUnlockAllIfNeeded(productId)) {
      logger.i('[store] unlocked all categories!');
    }
    await LocalStorage.saveBoughtProduct(id: productId);

    if (purchaseDetails.status == PurchaseStatus.purchased) {
      final ProductDetails? product = _productsCache[productId];
      if (product != null) {
        if (Platform.isAndroid) {
          // Purchase event is not working automatically on Android (for some reason)
          // So we have to send it manually
          FacebookPlugin.logPurchase(
              amount: product.rawPrice,
              currency: product.currencyCode,
              parameters: <String, dynamic>{
                'fb_iap_product_type': 'inapp',
                'fb_content_id': product.id,
                'fb_content_title': product.title,
                'fb_num_items': 1,
              });
        }

        logAppsFlyerEvent('af_purchase', params: <String, dynamic>{
          'af_price': product.rawPrice,
          'af_revenue': product.rawPrice,
          'af_currency': product.currencyCode,
          'af_content_id': product.id,
          'af_quantity': 1,
        });
      }

      emit(PurchaseSuccessState(purchaseDetails.productID));
    }
  }

  Future<void> buyLocally({required final String productId}) async {
    await LocalStorage.saveBoughtProduct(id: productId);
    emit(PurchaseSuccessState(productId));
  }

  Future<void> buyProduct({
    required final String productId,
    required final BuySource source,
  }) async {
    // For test package
    if (EnvironmentConfig.isTestPackage) {
      _buyOnTest(productId);
      return;
    }

    emit(PurchaseStartedState());
    FirebaseAnalytics.instance.logSelectItem(
      itemListName: source.value,
      items: <AnalyticsEventItem>[
        AnalyticsEventItem(
          quantity: 1,
          itemId: productId,
        ),
      ],
    );

    // Dialog for loading will not blink thans to this
    await Future<void>.delayed(const Duration(milliseconds: 500));

    final bool available = await InAppPurchase.instance.isAvailable();
    if (!available) {
      logger.e('[store] store is not available');
      emit(PurchaseErrorState(PurchaseError.storeUnavailable));
      return;
    }

    final Set<String> ids = <String>{productId};
    final ProductDetailsResponse response =
        await InAppPurchase.instance.queryProductDetails(ids);

    if (response.notFoundIDs.isNotEmpty) {
      logger.e('[store] product $productId was not found on store');
      emit(PurchaseErrorState(PurchaseError.productNotFound));
      return;
    }

    final ProductDetails product = response.productDetails.first;
    _productsCache[productId] = product;

    FirebaseAnalytics.instance.logBeginCheckout(
      value: product.rawPrice,
      currency: product.currencyCode,
      items: <AnalyticsEventItem>[
        AnalyticsEventItem(
          price: product.rawPrice,
          currency: product.currencyCode,
          quantity: 1,
          itemId: productId,
        ),
      ],
    );

    logAppsFlyerEvent('af_initiated_checkout', params: <String, dynamic>{
      'af_price': product.rawPrice,
      'af_currency': product.currencyCode,
      'af_content_id': product.id,
      'af_quantity': 1,
    });

    final PurchaseParam purchaseParam = PurchaseParam(productDetails: product);
    InAppPurchase.instance.buyNonConsumable(purchaseParam: purchaseParam);
  }

  Future<void> _buyOnTest(final String productId) async {
    emit(PurchaseStartedState());
    if (await _applyUnlockAllIfNeeded(productId)) {
      logger.i('[store] unlocked all categories!');
    }
    await LocalStorage.saveBoughtProduct(id: productId);

    // Because it is fast on test device sometimes alert "stucks" because it doesn't have time to show and dismiss is called immidiatly
    await Future<void>.delayed(const Duration(seconds: 2));
    emit(PurchaseSuccessState(productId));
    // emit(PurchaseErrorState(PurchaseError.notVerified));
    return;
  }

  Future<bool> _applyUnlockAllIfNeeded(final String productId) async {
    // Iterate over all products and add them as purchased
    final StoreDefinition response = await RemoteConfigHelper.storeContent();
    if (productId == response.unlockAll.iap) {
      for (final StoreItem item in response.store) {
        await LocalStorage.saveBoughtProduct(id: item.iap);
      }
      return true;
    }
    return false;
  }
}

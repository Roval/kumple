import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

import '../cubits/game_cubit.dart';
import '../di/dependencies.dart';
import '../gen/translations.gen.dart';
import '../routing/router.gr.dart';
import '../scenes/alert_screen.dart';
import '../utils/logger.dart';
import '../widget/game_button.dart';

abstract class CloseDialog {
  void close({final String? roomKey}) {
    logger.d('close game');
    _naviagateBack(roomKey: roomKey);
  }

  void showCloseDialog({final String? roomKey}) {
    logger.d('show close dialog');
    final AppRouter appRouter = getIt<AppRouter>();
    appRouter.push(
      InfoAlert(
        message: LocaleKeys.alerts_leave_hostTitle.tr(),
        actions: <InfoAlertAction>[
          InfoAlertAction(
            onTap: (final _) {
              FirebaseAnalytics.instance.logEvent(name: 'close_game');
              _naviagateBack(roomKey: roomKey);
            },
            title: LocaleKeys.alerts_yes.tr(),
          ),
          InfoAlertAction(
            onTap: (final _) {
              getIt<AppRouter>().pop();
            },
            title: LocaleKeys.alerts_no.tr(),
            smaller: true,
            style: const GameButtonStyle.none(),
          ),
        ],
      ),
    );
  }

  void showCloseOrLeaveDialog({final String? roomKey}) {
    logger.d('show close or leave dialog');

    final AppRouter appRouter = getIt<AppRouter>();
    appRouter.push(
      InfoAlert(
        message: LocaleKeys.alerts_leave_participantTitle.tr(),
        subtitle: LocaleKeys.alerts_leave_participantSubtitle.tr(),
        actions: <InfoAlertAction>[
          InfoAlertAction(
            onTap: (final _) {
              FirebaseAnalytics.instance.logEvent(name: 'close_game');
              _naviagateBack(roomKey: roomKey);
            },
            title: LocaleKeys.alerts_leave_goBack.tr(),
          ),
          InfoAlertAction(
            onTap: (final _) {
              FirebaseAnalytics.instance.logEvent(name: 'exit_game');
              _naviagateBack(roomKey: roomKey, withExit: true);
            },
            title: LocaleKeys.alerts_leave_confirm.tr(),
            smaller: true,
            style: const GameButtonStyle.red(),
          ),
        ],
      ),
    );
  }

  void _goBackToWelcomeScreen() {
    final AppRouter appRouter = getIt<AppRouter>();
    appRouter.replaceAll(<PageRouteInfo>[const Welcome()]);
  }

  void _naviagateBack({final String? roomKey, final bool withExit = false}) {
    // Disconnect and return to welcome screen
    if (roomKey != null) {
      if (withExit) {
        // Will remove user from lobby
        getIt<GameCubit>().exitGameRoom(room: roomKey);
      } else {
        // Preserves user in lobby
        getIt<GameCubit>().leaveGameRoom(room: roomKey);
      }
    }
    _goBackToWelcomeScreen();
  }
}

import 'package:app_settings/app_settings.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

import '../di/dependencies.dart';
import '../gen/translations.gen.dart';
import '../routing/router.gr.dart';
import '../scenes/alert_screen.dart';
import '../utils/logger.dart';

mixin CameraPermissionDialog<T extends StatefulWidget> on State<T> {
  Future<bool> checkPermissions() async {
    if (await Permission.camera.request().isGranted) {
      return true;
    } else {
      logger.d('show permission required for camera dialog');
      getIt<AppRouter>().push(
        InfoAlert(
          message: LocaleKeys.alerts_camera_requiredTitle.tr(),
          subtitle: LocaleKeys.alerts_camera_requiredSubtitle.tr(),
          actions: <InfoAlertAction>[
            InfoAlertAction(
              title: LocaleKeys.alerts_camera_settingsOpen.tr(),
              onTap: (final _) async {
                FirebaseAnalytics.instance.logEvent(
                  name: 'open_settings',
                );
                getIt<AppRouter>().pop(); // Hide this alert
                await AppSettings.openAppSettings();
              },
            ),
          ],
        ),
      );
      return false;
    }
  }
}

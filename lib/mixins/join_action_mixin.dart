import 'package:auto_route/auto_route.dart';
import 'package:easy_localization/easy_localization.dart';

import '../cubits/game_cubit.dart';
import '../cubits/loading_cubit.dart';
import '../di/dependencies.dart';
import '../gen/translations.gen.dart';
import '../repositories/local_storage.dart';
import '../routing/router.gr.dart';
import '../utils/camera_util.dart';
import '../utils/logger.dart';
import '../utils/throttler.dart';

// Sometimes multiple join commends can be send at once, throttle them
Throttler _joinThrottler = Throttler();

abstract class JoinAction {
  /// Joins the room.
  /// It firsly checks if user has avatar and name - if not it shows edit profile.
  ///
  /// `changeRoomIfNeeded` is used when user clicks "Play again" so
  /// this will not fail when user switches between channels
  /// (otherwise it will say that user is already in game screen)
  ///
  /// `isJoiningForTheFirstTime` should be sent with appropriate value
  /// so this methods now how to setup analytics correctly
  /// (only first time joiners will receive some events)
  Future<void> joinRoom(
    final String room, {
    final bool changeRoomIfNeeded = false,
    required final bool isJoiningForTheFirstTime,
  }) async {
    final AppRouter router = getIt<AppRouter>();
    final RouteData current = router.stackData.first;
    if (!changeRoomIfNeeded &&
        (current.name == WaitingRoom.name || current.name == GameRoom.name)) {
      // TODO: If I click on the same room (via deeplink) will it say I cannot change it? Where is it detected it is not the same?
      logger.i('user already in another room, unable to join');
      _showAlreadyInGameDialog(args: current.args, newRoomKey: room);
      return;
    }

    logger
        .i('user entered room to join: $room, current screen: ${current.name}');

    await verifyIfNameAndPhotoExistsThenProceed(
      success: (final bool isNewUser, final String name) {
        _joinThrottler.run(() {
          _showLoadingDialog();
          getIt<GameCubit>().joinGameRoom(
            name: name,
            room: room,
            isJoiningForTheFirstTime: isJoiningForTheFirstTime,
          );
        });
      },
    );
  }

  Future<void> verifyIfNameAndPhotoExistsThenProceed({
    required final Function(bool, String) success,
  }) async {
    _showLoadingDialog();

    // ignore: always_specify_types
    final List<Object?> response;
    try {
      response = await Future.wait(<Future<Object?>>[
        LocalStorage.getUsername(),
        myAvatarMetadata(),
      ]);
    } on AvatarException catch (e) {
      logger.i('unable to find avatar on the internent: ${e.cause}');
      getIt<LoadingCubit>().hideLoading();
      getIt<AppRouter>().push(InfoAlert(
        subtitle: e.cause == AvatarException.noInternetCause
            ? LocaleKeys.alerts_error_noInternet.tr()
            : LocaleKeys.alerts_error_unknown.tr(),
        message: LocaleKeys.alerts_error_ups.tr(),
      ));
      return;
    }
    _hideLoadingDialog();

    if (response[0] == null ||
        (response[0]! as String).isEmpty ||
        response[1] == null) {
      logger.i('name/avatar not specified, open edit profile screen');
      _showEditProfile(callback: (final String newName) {
        success(true, newName);
      });
    } else {
      success(false, response[0]! as String);
    }
  }

  void _showEditProfile({
    required final Function(String) callback,
  }) {
    getIt<AppRouter>().push(
      EditProfile(onSave: callback),
    );
  }

  void _hideLoadingDialog() {
    getIt<LoadingCubit>().hideLoading();
  }

  void _showLoadingDialog() {
    getIt<LoadingCubit>().showLoading();
  }

  void _showAlreadyInGameDialog(
      {required final Object? args, required final String newRoomKey}) {
    String? currentRoomKey;
    if (args is WaitingRoomArgs) {
      currentRoomKey = args.session.roomKey;
    } else if (args is GameRoomArgs) {
      currentRoomKey = args.session.roomKey;
    }

    if (currentRoomKey == newRoomKey) {
      logger.i('entered the same room with deeplink, skip');
      return;
    }

    getIt<AppRouter>().push(InfoAlert(
      message: LocaleKeys.welcome_error_inOtherRoom.tr(),
    ));
  }
}

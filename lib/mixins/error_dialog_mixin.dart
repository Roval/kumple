import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';

import '../cubits/loading_cubit.dart';
import '../di/dependencies.dart';
import '../gen/translations.gen.dart';
import '../routing/router.gr.dart';
import '../utils/logger.dart';

mixin ErrorDialog on Widget {
  void showErrorDialog({required final String msg}) {
    logger.d('show error dialog');
    getIt<LoadingCubit>().hideLoading();
    getIt<AppRouter>().push(
        InfoAlert(message: LocaleKeys.alerts_error_ups.tr(), subtitle: msg));
  }
}

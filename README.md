# kumple

## Generate translations files
flutter pub run easy_localization:generate --source-dir ./assets/translations --output-dir ./lib/gen -o translations.gen.dart -f keys

# Generate injectable & routes
flutter packages pub run build_runner build  

# Optimeze SVGs
dart run vector_graphics_compiler --input-dir ~/Downloads/svgs
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:integration_test/integration_test.dart';

import 'package:kumple/main.dart' as app;

void main() {
  IntegrationTestWidgetsFlutterBinding.ensureInitialized();

  testWidgets('healthcheck test', (final WidgetTester tester) async {
    app.main();
    // wait for splash to finish
    await tester.pumpAndSettle();
    await waitFor(tester, find.byKey(const ValueKey<String>('createGame')));

    // Tap new game
    await tester.tap(find.byKey(const ValueKey<String>('createGame')));

    // Wait for profile edit
    await waitFor(tester, find.byKey(const ValueKey<String>('nameInput')));

    // Insert predefined user name
    await tester.enterText(find.byKey(const ValueKey<String>('nameInput')),
        '0E8XMGPXHOXM3OOIQ1C1');
    await waitFor(tester, find.byKey(const ValueKey<String>('saveButton')));
    await tester.tap(find.byKey(const ValueKey<String>('saveButton')));

    // Start game
    await waitFor(tester, find.byKey(const ValueKey<String>('createButton')));
    await waitFor(tester, find.text('2/17'));
    await tester.tap(find.byKey(const ValueKey<String>('createButton')));

    // Wait for game start
    await waitFor(
        tester, find.byKey(const ValueKey<String>('inviteFriendsButton')));
  });
}

Future<void> waitFor(
  final WidgetTester tester,
  final Finder finder, {
  final Duration timeout = const Duration(seconds: 20),
}) async {
  final DateTime end = tester.binding.clock.now().add(timeout);

  do {
    if (tester.binding.clock.now().isAfter(end)) {
      throw Exception('Timed out waiting for $finder');
    }

    await tester.pump(const Duration(milliseconds: 100));
    await Future<dynamic>.delayed(const Duration(milliseconds: 100));
  } while (finder.evaluate().isEmpty);
}
